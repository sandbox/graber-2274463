<?php

/**
 * @file
 * Definition of views_handler_field_cart_form.
 */

/**
 * Field handler to present aa add to cart form.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_cart_form extends views_handler_field_entity {

  function option_definition() {
    $options=parent::option_definition();
    $options['quantity']=array('default' => 1, 'translatable' => FALSE);
    $options['view_mode']=array('default' => 'form', 'translatable' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['view_mode']=array(
      '#type' => 'radios',
      '#options' => array(
        'link' => t('Link'),
        'form' => t('Form')
      ),
      '#default_value' => $this->options['view_mode'],
    );
    $form['quantity']=array(
      '#type' => 'checkbox',
      '#title' => t('Quantity option'),
      '#default_value' => $this->options['quantity'],
    );
    parent::options_form($form, $form_state);

    // The path is set by render_link function so don't allow to set it.
    $form['alter']['path'] = array('#access' => FALSE);
    $form['alter']['external'] = array('#access' => FALSE);
  }

  function render($values) {
    if ($entity = $this->get_value($values)) {
      return $this->render_form($entity, $values);
    }
  }

  function render_form($node, $values) {
    if (user_access('make shopping')) {
      $settings=gp_get_settings('graber_cart');
      if (in_array($node->type, $settings['buyable_types'], TRUE)) {
        if ($this->options['view_mode'] == 'form') {
          if ($this->options['quantity'] == 1) $view_mode='include_quantity';
          else $view_mode='no_quantity';
          $inner_id=_graber_cart_get_add_id();
          $form=drupal_get_form('graber_cart_add_form_'.$inner_id, $node->nid, $view_mode);
          return render($form);
        }
        elseif ($this->options['view_mode'] == 'link') {
          return '<div class="gc-add-link-wrapper">'.l(
            t('Add to cart'),
            'cart/add/nojs/'.$node->nid,
            array('attributes' => array(
              'class' => array(
                'ctools-use-modal',
                'ctools-modal-gc-add-modal',
                'cart-add-'.$node->nid.'-btn'
              )
            ))
          ).'</div>';
        }
      }
    }
  }
}
