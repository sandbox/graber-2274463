<?php
function graber_cart_views_data () {
  $data['node']['add_to_cart']['moved to'] = array('views_entity_node', 'add_to_cart');
  $data['views_entity_node']['add_to_cart'] = array(
    'field' => array(
      'title' => t('Add to cart form'),
      'help' => t('Add to cart option.'),
      'handler' => 'views_handler_field_cart_form',
    ),
  );
  return $data;
}
