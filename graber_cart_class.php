<?php
class graber_cart {
  var $cid;
  var $uid;
  var $data;
  var $status=1;
  var $contents=array();
  var $currency;
  var $fully_loaded;

  function __construct($cid=false) {
    if ($cid !== FALSE) {
      $cart_obj=db_select('graber_carts', 'c')->fields('c')->condition('cid', $cid)->execute()->fetchObject();
      if (is_object($cart_obj)) {
        if (!empty($cart_obj->data)) $this->data=unserialize($cart_obj->data);
        foreach ($cart_obj as $key => $value) {
          if ($key == 'data') continue;
          $this->{$key}=$value;
        }
      }
      $this->data['uid']=$this->uid;
    }
    elseif (user_access('make shopping')) {
      global $user;
      $uid=$user->uid;
      if ($uid==0) {
        $sid=graber_cart_session();
      }
      else $sid='0';
      
      $this->cid=db_select('graber_carts', 'c')->fields('c', array('cid'))->condition('uid', $uid)->condition('sid', $sid)->condition('status', 1)->execute()->fetchField();
      if (!$this->cid) {
        $this->cid=0;
        $this->uid=$uid;
        $this->sid=$sid;
        $this->currency=(isset($user->data['currency'])) ? $user->data['currency'] : variable_get('graber_cart_currency', 'PLN');
      }
      else {
        $cart_obj=db_select('graber_carts', 'c')->fields('c')->condition('cid', $this->cid)->execute()->fetchObject();
        if (is_object($cart_obj)) {
          if (!empty($cart_obj->data)) $this->data=unserialize($cart_obj->data);
          foreach ($cart_obj as $key => $value) {
            if ($key == 'data') continue;
            $this->{$key}=$value;
          }
          $this->status=(int) $this->status;
        }
      }
      $this->data['uid']=$this->uid;
    }
    else $this->currency=variable_get('graber_cart_currency', 'PLN');
  }
  
  function initialize() {
    $fields=array(
      'uid' => $this->uid,
      'sid' => $this->sid,
      'modified' => REQUEST_TIME
    );
    if (isset($this->status)) $fields['status']=$this->status;
    if (isset($this->name)) $fields['name']=$this->name;
    if (isset($this->currency)) $fields['currency']=$this->currency;

    if (empty($this->cid)) {
      $cid=db_insert('graber_carts')->fields($fields)->execute();
      $this->cid=$cid;
    }
    else db_update('graber_carts')->fields($fields)->condition('cid', $this->cid)->execute();
  }
  
  function update_status($new_status) {
    $new_status=(int) $new_status;
    if ($new_status != $this->status && $new_status < 16) {
      $this->status=(int) $new_status;
      if ($this->cid) {
        db_update('graber_carts')->fields(array('status' => $new_status))->condition('cid', $this->cid)->execute();
      }
    }
  }
  
  function save_data() {
    if (empty($this->cid)) $this->initialize();
    db_update('graber_carts')->fields(array('data' => serialize($this->data)))->condition('cid', $this->cid)->execute();
  }
  
  function load_item(&$item, $iid, $force_reload=FALSE) {
    if ($iid && ($force_reload || (!isset($item['title']) || !isset($item['node']) || !isset($item['price']) || !isset($item['product_id'])))) {
      if (!isset($item['nid'])) $item['nid']=db_select('graber_cartdata', 'c')->fields('c', array('nid'))->condition('iid', $iid)->execute()->fetchColumn();
      $node=node_load($item['nid']);
      if (!$node) {
        $this->remove($iid);
        return;
      }
      $item['title']=$node->title;
      $item['node']=$node;
      
      //Load properties for active carts. Otherwise props are in db.
      if ($this->status != 0 || $force_reload) {
        $price=$this->get_price($item);
        if (isset($price['new']) && $price['new'] != $price['base']) {
          $item['price']=$price['new'];
        }
        else $item['price']=$price['base'];
        $item['base_price']=$price['base'];
        
        //Product ID
        if (!empty($item['node']->field_sku[LANGUAGE_NONE][0]['value'])) $item['product_id']=$item['node']->field_sku[LANGUAGE_NONE][0]['value'];
        else $item['product_id']=$item['node']->nid;
        
        //Allow modules to add/change item properties
        foreach (module_implements('graber_cart_item_load') as $module) {
          call_user_func_array($module.'_graber_cart_item_load', array(&$item));
        }
        
        $this->contents[$iid]=$item;
      }
    }
  }
  
  function load() {
    if (empty($this->cid)) return FALSE;
    $items=db_select('graber_cartdata', 'c')->fields('c')->condition('cid', $this->cid)->execute()->fetchAllAssoc('iid', PDO::FETCH_ASSOC);
    foreach ($items as $iid => $item) {
      $item['unique']=array('nid' => $item['nid']);
      
      if (!empty($item['data'])) {
        $extra=unserialize($item['data']);
        if (is_array($extra)) {
          if (isset($extra['unique']) && is_array($extra['unique'])) {
            $item['unique']+=$extra['unique'];
          }
          $item+=$extra;
        }
        unset($item['data']);
      }
      //Load node and get price and product ID

      $this->load_item($item, $iid);
      $this->contents[$iid]=$item;
    }
    $this->fully_loaded=TRUE;
    
    return $this;
  }
  
  function find($unique_data) {
    foreach ($this->contents as $iid => $item) {
      $found=TRUE;
      foreach ($unique_data as $param => $value) {
        if (isset($item['unique'][$param])) {
          if ($item['unique'][$param] !== $value) {
            $found=FALSE;
            continue;
          }
        }
        else {
          $found=FALSE;
          continue;
        }
      }
      if ($found) break;
      $found=FALSE;
    }
    if (isset($found) && $found) return $iid;
    else return 0;
  }
  
  function add($node, $data=array()) {
    $this->initialize();
    $data['unique']['nid']=$node->nid;
    $data['title']=$node->title;
    //Prevent wiseguys from beeing wise
    $this->validate_input($data);
    $data['original']=$data;
        
    drupal_alter('graber_cart_add', $node, $data, $this);

    $iid=$this->find($data['unique']);
    
    //Prevent adding to cart in some cases
    $result=module_invoke_all('graber_cart_add', $node, $data, $this, $iid);
    if (is_array($result) && in_array(FALSE, $result)) {
      return FALSE;
    }
    
    if ($iid == 0) {
      $record=$data;
      if (empty($record['quantity'])) $record['quantity']=1;
    }
    else {
      if (empty($data['quantity'])) $this->contents[$iid]['quantity']++;
      else $this->contents[$iid]['quantity']+=$data['quantity'];
      $record=$this->contents[$iid];
    }

    $iid=$this->save_record($iid, $record);
    $this->load_item($this->contents[$iid], $iid, TRUE);
    $this->calculate();
    return $this->contents[$iid];
  }
  
  function remove($iid) {
    if (isset($this->contents[$iid])) {
      $node=node_load($this->contents[$iid]['unique']['nid']);
      if ($node) module_invoke_all('graber_cart_remove', $this->contents[$iid], $node);
      unset($this->contents[$iid]);
    }
    return $this->save_record($iid, 'remove');
  }
  
  function update($iid, $data) {
    $node=node_load($this->contents[$iid]['unique']['nid']);
    $result=module_invoke_all('graber_cart_update', $this->contents[$iid], $data, $node, $this);
    if (!is_array($result) || !in_array(FALSE, $result)) {
      $this->contents[$iid]=self::array_join($this->contents[$iid], $data);
    }
    return $this;
  }
  
  function save_record($iid, $record=FALSE) {
    if ($record == 'remove') {
      if ($iid) {
        db_delete('graber_cartdata')->condition('iid', $iid)->execute();
      }
    }
    else {
      if ($record === FALSE && $iid) {
        foreach ($this->contents[$iid] as $prop => $value) {
          if (!in_array($prop, array('node', 'quantity'))) {
            $record[$prop]=$value;
          }
        }
      }
      
      $nid=isset($record['unique']['nid']) ? $record['unique']['nid'] : $this->contents[$iid]['unique']['nid'];
      $quantity=isset($record['quantity']) ? $record['quantity'] : $this->contents[$iid]['quantity'];
      
      if (isset($record['node'])) unset($record['node']);
      $fields=array(
        'nid' => $nid,
        'cid' => $this->cid,
        'data' => (empty($record)) ? '' : serialize($record),
        'quantity' => $quantity
      );

      if ($iid) db_update('graber_cartdata')->fields($fields)->condition('iid', $iid)->execute();
      else $iid=db_insert('graber_cartdata')->fields($fields)->execute();
      
      //Update cart object
      if ($record) {
        foreach ($record as $prop => $value) {
          $this->contents[$iid][$prop]=$value;
        }
      }
    }
    return $iid;
  }
  
  function archivize() {
    $this->initialize();
    foreach($this->contents as $iid => $item) {
      $this->save_record($iid);
    }
    db_update('graber_carts')->fields(array('status' => 0))->condition('cid', $this->cid)->execute();
  }
  
  function delete() {
    module_invoke_all('graber_cart_delete', $this->contents, $this->uid);
    foreach ($this->contents as $iid => $data) $this->remove($iid);
    db_delete('graber_carts')->condition('cid', $this->cid)->execute();
  }
  
  function calculate() {
    $amount=0;
    foreach ($this->contents as $iid => $item) {
      if ($this->status == 1) {
        if (!isset($item['node'])) $item['node']=node_load($item['unique']['nid']);
        $price=$this->get_price($item);
        if (isset($price['new']) && $price['new'] != $price['base']) {
          $item['price']=$price['new'];
          $item['discount']=($price['base']-$price['new'])/$price['base'];
        }
        else $item['price']=$price['base'];
        $this->contents[$iid]['price']=$item['price'];
      }
      if (is_numeric($this->contents[$iid]['quantity']) && is_numeric($this->contents[$iid]['price'])) $amount+=$this->contents[$iid]['quantity']*$this->contents[$iid]['price'];
    }
    $new_data['articles_price']=round($amount, 2);
    if (isset($this->data['shipping_method'])) {
      $shipping_methods=module_invoke_all('graber_cart_shipping_info', $this);
      if (isset($shipping_methods[$this->data['shipping_method']]['cost'])) {
        $new_data['shipping_price']=$shipping_methods[$this->data['shipping_method']]['cost'];
      }
    }
    
    $new_data['total_price']=$new_data['articles_price'];
    if (isset($new_data['shipping_price'])) $new_data['total_price']+=$new_data['shipping_price'];
        
    drupal_alter('cart_calculate', $new_data, $this);
    
    foreach ($new_data as $key => $value) $this->data[$key]=$value;
    $this->save_data();
    return $this->data['articles_price'];
  }
  
  function render($display='form', $params=array('checkout' => TRUE), $order=FALSE) {
    if (empty($this->fully_loaded)) $this->load();
    if ($display=='form') {
      $cart_form=drupal_get_form('graber_cart_form', $this, $params, $order);
      return render($cart_form);
    }
    
    elseif ($display == 'summary' || $display == 'min_summary') {
      if (isset($params['type'])) $type=$params['type'];
      else {
        if ($display == 'summary') $type='normal';
        elseif ($display == 'min_summary') $type='min';
      }
      return theme('cart_summary', array('cart' => $this, 'type' => $type, 'params' => $params));
    }
  }
  
  //Auxilliary functions
  public function get_price($item) {
    $price=array();
    
    if (!empty($item['node']->field_product_price)) $price['base']=$item['node']->field_product_price[LANGUAGE_NONE][0]['value'];
    else $price['base']=0;

    //Alter prices
    $alter_modules=variable_get('price_alter_modules', array('offer_params', 'vat_verification', 'promotions', 'currency'));
    foreach ($alter_modules as $module) {
      $function=$module.'_cart_price_alter';
      if (function_exists($function)) $function($price, $item, $this);
    }
    return $price;
  }
  
  function validate_input(&$data) {
    drupal_alter('cart_validate_add_input', $data, $this);
    if (isset($data['base_price']) || isset($data['price']) || isset($data['vat'])) {
      unset($data['base_price']);
      unset($data['price']);
      unset($data['vat']);
      watchdog('graber_cart', 'Price hack attempt from: IP !ip, uid = !uid.', array('!ip' => $_SERVER['REMOTE_ADDR'], '!uid' => $GLOBALS['user']->uid), WATCHDOG_WARNING);
    }
  }
  
  private function array_join() {
    $arrays = func_get_args();
    $original = array_shift($arrays);
    foreach ($arrays as $array) {
      foreach ($array as $key => $value) {
        if (is_array($value)) $original[$key] = graber_cart::array_join($original[$key], $array[$key]);
        else $original[$key] = $value;
      }
    }
    return $original;
  }
}

class graber_order {
  
  function __construct($data) {
    if (is_numeric($data)) $this->load($data);
    elseif (is_object($data) || is_array($data)) {
      foreach ($data as $key => $value) {
        $this->{$key}=$value;
      }
    }
  }
  
  function save($new_fields=array()) {
    _gc_apply_defaults($new_fields, (array) $this, TRUE);
    if (!empty($this->oid)) {
      $result=module_invoke_all('graber_order_update', $this, $new_fields);
      if ($this->order_data['cid']) {
        if ($this->status <= 1 && $new_fields['status'] > 1) {
          $cart=new graber_cart($this->order_data['cid']);
          $cart->data['sent']=TRUE;
          $cart->save_data();
        }
        elseif ($this->status > 1 && $new_fields['status'] <= 1) {
          $cart=new graber_cart($this->order_data['cid']);
          $cart->data['sent']=FALSE;
          $cart->save_data();
        }
      }
    }
    else $result=module_invoke_all('graber_order_create', $this, $new_fields);
    if (is_array($result)) {
      if (in_array(FALSE, $result)) return FALSE;
      if (!empty($result)) $new_fields['order_data']=array_replace_recursive($new_fields['order_data'], $result);
    }
    
    if (isset($new_fields['description']) && (strlen($new_fields['description']) > 240)) $new_fields['description']=substr($new_fields['description'], 0, 238).'..';
    
    $new_fields['modified']=REQUEST_TIME;
    $new_fields['orderer']=_graber_cart_generate_orderer($new_fields['order_data']['billing_addr']);

    if (empty($new_fields['oid'])) {
      if (!isset($new_fields['created'])) $new_fields['created']=REQUEST_TIME;
      
      if (!isset($new_fields['deadline'])) {
        $deadline_period=gp_get_settings('graber_cart', 'default_deadline');
        if (!empty($deadline_period)) $new_fields['deadline']=REQUEST_TIME+24*60*60*$deadline_period;
      }
      
      do {
        $code=_gp_generate_random_code(7, TRUE);
      } while (db_query("SELECT COUNT(code) FROM {graber_orders} WHERE code = '$code'")->fetchField());
      $new_fields['code']=$code;
    }

    //Update order object
    foreach ($new_fields as $field => $value) $this->{$field}=$value;
    
    if (isset($new_fields['order_data'])) $new_fields['order_data']=serialize($new_fields['order_data']);
    
    //Save order in database
    if (!empty($new_fields['oid'])) {
      db_update('graber_orders')->fields($new_fields)->condition('oid', $new_fields['oid'])->execute();
    }
    else {
      $this->oid=db_insert('graber_orders')->fields($new_fields)->execute();
    }
    return TRUE;
  }
  
  function load($oid) {
    $oid=(int) $oid;
    $data=db_query("SELECT * FROM {graber_orders} WHERE oid = :oid", array(':oid' => $oid))->fetchObject();
    if ($data) {
      foreach ($data as $key => $value) {
        if ($key=='order_data') $this->{$key}=unserialize($value);
        else $this->{$key}=$value;
      }
      $this->oid=$oid;
      return TRUE;
    }
    else return FALSE;
  }
  
  function delete() {
    if (isset($this->oid)) {
      module_invoke_all('graber_order_delete', $this);
      if (!empty($this->order_data['cid'])) {
        $cart=new graber_cart($this->order_data['cid']);
        $cart->delete();
      }
      db_delete('graber_orders')->condition('oid', $this->oid)->execute();
      unset($this);
    }
  }
  
  function review($settings=array()) {
    $settings+=array(
      'billing' => TRUE,
      'shipping' => TRUE,
      'status' => FALSE
    );
    $cart=new graber_cart($this->order_data['cid']);
    $output='';
    
    if ($settings['status']) $output.=theme('graber_order_status', array('status' => $this->status, 'statuses' => graber_order_statuses()));

    $output.=$cart->render('summary', array('absolute' => TRUE));

    if ($settings['shipping']) $output.=theme('graber_address_data', array('data' => $this->order_data['shipping_addr'], 'type' => 'shipping', 'header' => t('Shipping address')));

    if ($settings['billing']) $output.=theme('graber_address_data', array('data' => $this->order_data['billing_addr'], 'type' => 'billing', 'header' => t('Billing address')));
    
    $extra_arr=array();
    $unalterable=array(
      'order' => clone($this),
      'cart' => clone($cart)
    );
    $alterable=array(
      'extra' => &$extra_arr
    );
    
    drupal_alter('graber_order_review', $alterable, $unalterable);
    
    if (!empty($extra_arr)) $extra=implode(PHP_EOL, $extra_arr);
    else $extra='';

    return '<h3>'.t('Order summary:').'</h3>'.$output.$extra;
  }
}