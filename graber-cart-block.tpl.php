<a class="icon" href="<?php print $link; ?>">
</a>
<div class="msg-wrapper">
	<a class="details" href="<?php print $link; ?>">
		<p id="cart-msg">
			<?php print($message); ?>
		</p>
	</a>
</div>

<?php if (!empty($summary)) : ?>
	<div id="cart-block-summary">
		<?php print $summary; ?>
		<a class="checkout" href="<?php print $link; ?>"><?php print t('Go to checkout'); ?></a>
	</div>
<?php endif; ?>
