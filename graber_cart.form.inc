<?php

//Add to cart form

function graber_cart_forms($form_id, $args) {
  
  if (substr($form_id, 0, 20) == 'graber_cart_add_form') {
    $forms[$form_id]=array(
      'callback' => 'graber_cart_add_form',
      'callback arguments' => $args
    );
    return $forms;
  }
}

function graber_cart_add_form($form, &$form_state, $nid=0, $view_mode='full') {
  $settings=gp_get_settings('graber_cart');
  if ($nid) $form_state['nid']=$nid;
  elseif (isset($form_state['nid'])) $nid=$form_state['nid'];
  else return array();
  $form['#destination']='cart';
    
  if (!isset($form_state['view_mode'])) $form_state['view_mode']=$view_mode;
  
  $form['options']=array('#tree' => TRUE);
  if ($settings['quantity_selector'] || $view_mode == 'include_quantity') {
    $form['options']['quantity']=array(
      '#title' => t('quantity'),
      '#type' => 'textfield',
      '#default_value' => 1,
      '#size' => 2,
    );
  }

  $form['actions']=array('#type' => 'actions');
  
  $form['actions']['add']=array(
    '#type' => 'submit',
    '#value' => t('Add to cart'),
    '#validate' => array('graber_cart_add_form_validate'),
  );
  
  $form['#attributes']['class'][]='graber-cart-add-form';
  $form['#attributes']['class'][]='add-to-cart-'.$nid.'-'.$form_state['view_mode'];
  
  $form['#attached']['js']=array(
    drupal_get_path('module', 'graber_cart').'/graber_cart.js'
  );
  $form['#skip_fields']=array('add', 'form_build_id', 'form_token', 'form_id', 'op');
  $form['#submit']=array('graber_cart_add_process');

  return $form;
}

function graber_cart_add_form_validate($form, &$form_state) {
  if (isset($form_state['values']['quantity'])) {
    if (!preg_match('#^[0-9]+$#', $form_state['values']['quantity'])) form_set_error('quantity', t('Please enter an integer value.'));
  }
}

function graber_cart_add_process($form, &$form_state) {
  if ($form_state['values']['op'] == t('Add to cart')) {
    global $cart;
    if (!empty($cart)) {
      $node=node_load($form_state['nid']);
      $data=array();
      if (!empty($form_state['values']['options'])) {
        foreach ($form_state['values']['options'] as $name => $value) {
          $data[$name]=$value;
        }
      }
      $data['title']=$node->title;
      
      if ($cart->add($node, $data)) {
        $form_state['#add_success']=TRUE;
        if (isset($form_state['ajax']) && $form_state['ajax']) {
          _graber_cart_modal_initialize();
          $form_state['ajax_commands'][]=ctools_modal_command_dismiss();
        }
        if (isset($form['#destination'])) $form_state['redirect']=$form['#destination'];
        return TRUE;
      }
      else {
        $form_state['#add_success']=FALSE;
        return FALSE;
      }
    }
  }
}

//Price summary renderer
function _graber_cart_render_price_summary($cart) {
  $summary_data['prices']=array(
    'total_price' => isset($cart->data['total_price']) ? $cart->data['total_price'] : NULL,
  );
  
  if (!isset($cart->data['articles_price'])) $cart->calculate();
  
  $summary_data['prices']['articles_price']=array(
    'data' => t('Price of selected articles:').' '.number_format($cart->data['articles_price'], 2, ',', ' ').' '.$cart->currency,
    '#raw' => $cart->data['articles_price'],
    '#weight' => 0
  );
  
  if (isset($cart->data['shipping_price'])) {
    $summary_data['prices']['shipping_price']=array(
      'data' => t('Shipping price:').' '.number_format($cart->data['shipping_price'], 2, ',', ' ').' '.$cart->currency,
      '#raw' => $cart->data['shipping_price'],
      '#weight' => 1
    );
  }
  
  if (!empty($cart->data['total_price'])) $summary_data['prices']['total_price']=array(
    'data' => t('Total price:').' '.number_format($cart->data['total_price'], 2, ',', ' ').' '.$cart->currency,
    '#raw' => $cart->data['total_price'],
    '#weight' => 2
  );
  else $summary_data['info']=array(
    'data' => t('Select shipping method to see total price.'),
    '#weight' => 2
  );
  
  drupal_alter('cart_price_summary', $summary_data, $cart);
    
  $output=array(
    '#markup' => '<div id="checkout-summary-wrapper">'.theme('price_summary', array('summary_data' => $summary_data)).'</div>',
  );
  return $output;
}

//The cart
function graber_cart_form($form, &$form_state, $cart, $params=array('checkout' => TRUE)) {
  if (!isset($form_state['#cart'])) $form_state['#cart']=&$cart;
  else $cart=&$form_state['#cart'];
  $form['#cart_params']=$params;
  if (!empty($params['admin'])) {
    $form['#is_admin']=TRUE;
    $form['add_widget']=array(
      '#type' => 'fieldset',
      '#title' => t('Add an article'),
      '#tree' => TRUE,
      '#weight' => 2
    );
    $form['add_widget']['article']=array(
      '#type' => 'textfield',
      '#autocomplete_path' => 'admin/store/ajax/offer-autocomplete',
      '#title' => t('Article')
    );
    $form['add_widget']['add_admin']=array(
      '#type' => 'submit',
      '#submit' => array('graber_cart_form_admin_add'),
      '#ajax' => array(
        'callback' => 'graber_cart_form_admin_add_ajax',
        'wrapper' => 'graber-cart-wrapper'
      ),
      '#value' => t('Add article')
    );    
  }
  
  //Cart table
  $form['cart_table']=array(
    '#tree' => TRUE,
    '#prefix' => '<div id="graber-cart-wrapper">',
    '#suffix' => '</div>',
    '#cart' => $cart,
    '#weight' => 1
  );
  
  $cart_version=gp_get_settings('graber_cart', 'cart_version');
  if ($cart_version == 'responsive') $form['cart_table']['#theme']='graber_cart_responsive';
  else $form['cart_table']['#theme']='graber_cart';
  
  foreach ($cart->contents as $iid => $item) {
    $form['cart_table'][$iid]['remove_item']=array(
      '#type' => 'submit',
      '#name' => 'remove_'.$iid,
      '#value' => t('remove'),
      '#submit' => array('graber_cart_remove_item'),
      '#validate' => array(),
      '#attributes' => array('class' => array('remove-item')),
      '#limit_validation_errors' => array()
    );
    $form['cart_table'][$iid]['quantity']=array(
      '#type' => 'textfield',
      '#title' => t('quantity'),
      '#title_display' => 'invisible',
      '#size' => 5,
      '#default_value' => $item['quantity'],
      '#attributes' => array('class' => array('item-quantity'))
    );
  }
      
  if (!empty($cart->contents)) {
    
    //Shipping
    $form['cart_options']=array(
      '#type' => 'fieldset',
      '#title' => t('Shipping & payment'),
      '#prefix' => '<div id="cart-options">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
      '#weight' => 3
    );
    
    $shipping_methods=module_invoke_all('graber_cart_shipping_info', $cart);
    drupal_alter('graber_shipping_methods', $shipping_methods, $cart);

    if (!empty($shipping_methods)) {
      $form['cart_options']['shipping_method']=array(
        '#type' => 'radios',
        '#default_value' => isset($cart->data['shipping_method']) ? $cart->data['shipping_method'] : NULL,
        '#title' => t('Shipping method'),
        '#required' => TRUE
      );
      if (empty($params['admin'])) $form['cart_options']['shipping_method']['#ajax']=array(
        'callback' => 'graber_cart_options_change_callback',
        'effect' => 'fade'      
      );
      
      if (!empty($cart->data['shipping_method'])) $shipping_method=$shipping_methods[$cart->data['shipping_method']];
      else $shipping_method=FALSE;
      
      foreach ($shipping_methods as $option => $data) {
        $form['cart_options']['shipping_method']['#options'][$option]='<span class="title">'.$data['title'].'</span> <span class="cost">('.number_format($data['cost'], 2, ',', ' ').' '.$cart->currency.')</span>';
      }
      
      if ($shipping_method !== FALSE && isset($shipping_method['payment']) && $shipping_method['payment']) $payment=TRUE;
      else $payment=FALSE;
    }
    else $payment=TRUE;
    
    //Billing method
    if ($payment) {
      if (!isset($cart->data['shipping_method'])) $shipping_method_str=FALSE;
      else $shipping_method_str=$cart->data['shipping_method'];
      $payment_methods=module_invoke_all('graber_cart_payment_info', $shipping_method_str, $cart);
      
      $temp=array_keys($payment_methods);
      if (!empty($payment_methods)) {
        if (isset($cart->data['payment_method']) && in_array($cart->data['payment_method'], $temp)) $default_pm=$cart->data['payment_method'];
        else $default_pm=$temp[0];
        $payment_options=array();
        
        foreach ($payment_methods as $key => $method) {
          if (!is_array($method)) {
            $method=$payment_methods[$key]=array('title' => $method);
          }
          if (isset($payment_methods[$key]['title'])) {
            $description=isset($method['description']) ? '<span class="description">'.$method['description'].'</span>' : '';
            $payment_options[$key]='<span class="title">'.$payment_methods[$key]['title'].'</span>'.$description;
          }
        }
        $form['cart_options']['payment_method']=array(
          '#type' => 'radios',
          '#options' => $payment_options,
          '#title' => t('Payment method'),
          '#default_value' => $default_pm,
          '#required' => TRUE
        );
        if (empty($params['admin'])) {
          $form['cart_options']['payment_method']['#ajax']=array(
            'callback' => 'graber_cart_options_change_callback',
            'progress' => array('type' => 'none')
          );
        }
      }
    }

    //Price summary    
    $form['price_summary']=_graber_cart_render_price_summary($cart);
    $form['price_summary']['#weight']=4;

    $form['submit-wrapper']=array(
      '#prefix' => '<div id="cart-submit-buttons">',
      '#suffix' => '</div>',
      '#type' => 'actions',
      '#weight' => 52
    );

    if ($form['#cart_params']['checkout']) {
      $form['submit-wrapper']['checkout']=array(
        '#type' => 'submit',
        '#op' => 'checkout',
        '#name' => 'checkout',
        '#value' => t('Go to checkout'),
        '#weight' => 51
      );
    }
    
    $form['submit-wrapper']['update']=array(
      '#type' => 'submit',
      '#op' => 'update',
      '#value' => t('Update'),
      '#weight' => 50
    );
  }
    
  $form['#attached']=array(
    'css' => array(drupal_get_path('module', 'graber_cart').'/css/graber_cart.css'),
    'js' => array(drupal_get_path('module', 'graber_cart').'/graber_cart.js'),
  );
  $form['#submit']=array('graber_cart_update_cart');
  return $form;
}

function graber_cart_options_change_callback($form, $form_state) {
  $commands=array();
  
  foreach ($form_state['values']['cart_options'] as $option => $value) {
    $form_state['#cart']->data[$option]=$value;
  }
  $form_state['#cart']->calculate();
  $form=drupal_rebuild_form($form['#form_id'], $form_state, $form);
  $main_output='<div id="cart-options">'.drupal_render($form['cart_options']);
  $messages=theme('status_messages');
  if (!empty($messages)) $main_output.=$messages;
  $main_output.='</div>';
  
  $commands[]=ajax_command_replace('#cart-options', $main_output);
  $commands[]=ajax_command_replace("#checkout-summary-wrapper", drupal_render($form['price_summary']));
  
  return array('#type' => 'ajax', '#commands' => $commands);
}

function graber_cart_form_validate($form, &$form_state) {
  foreach ($form['cart_table'] as $iid => $fields) {
    if (substr($iid, 0, 1) == '#') continue;
    my_api_validate_integer($fields['quantity'], $form_state, array(0, '>='));
  }
  if ($form_state['triggering_element']['#parents'][0] == 'add_widget') {
    preg_match('#\[nid=([0-9]+)\]#', $form_state['values']['add_widget']['article'], $matches);
    if (isset($matches[1])) $nid=db_select('node', 'n')->fields('n', array('nid'))->condition('nid', (int) $matches[1])->execute()->fetchColumn();
    if (!$nid) form_set_error('add_widget][article', t('Incorrect node. Use autocomplete list.'));
    else $form_state['values']['add_widget']['nid']=$nid;
  }
  
  if (isset($form_state['values']['op']) && $form_state['values']['op'] == 'checkout') {
    if ($form_state['values']['cart_options']['payment_method'] != $form_state['#cart']->data['payment_method']) form_set_error('cart_options][payment_method', t('Please select payment method.'));
  }
}

function graber_cart_remove_item($form, &$form_state) {
  $form_state['rebuild']=TRUE;
  $iid=intval($form_state['clicked_button']['#parents'][1]);
  $form_state['#cart']->remove($iid);
  $form_state['#cart']->calculate();
  if ($GLOBALS['cart']->cid == $form_state['#cart']->cid) $GLOBALS['cart']=$form_state['#cart'];
  if (isset($form['#cart_params']['oid'])) graber_update_order_from_cart($form['#cart_params']['oid'], $form_state['#cart']);
}

function graber_cart_update_cart($form, &$form_state, $redirect=TRUE) {
  if (!empty($form_state['values']['cart_options'])) {
    foreach ($form_state['values']['cart_options'] as $option => $value) {
      $form_state['#cart']->data[$option]=$value;
    }
    $form_state['#cart']->save_data();
  }
  foreach ($form_state['#cart']->contents as $iid => $item) {
    if ($form_state['values']['cart_table'][$iid]['quantity'] != $item['quantity']) {
      if ($form_state['values']['cart_table'][$iid]['quantity'] == 0) $form_state['#cart']->remove($iid);
      else $form_state['#cart']->update($iid, array('quantity' => $form_state['values']['cart_table'][$iid]['quantity']))->save_record($iid);
    }
  }
  
  if ($GLOBALS['cart']->cid == $form_state['#cart']->cid) $GLOBALS['cart']=&$form_state['#cart'];
  $form_state['#cart']->calculate();
  
  if (isset($form['#cart_params']['oid'])) graber_update_order_from_cart($form['#cart_params']['oid'], $form_state['#cart']);
  if (!isset($form['#redirect_to_checkout']) || $form['#redirect_to_checkout'] == TRUE) {
    if (isset($form_state['clicked_button'])) {
      if ($redirect && $form_state['clicked_button']['#name']=='checkout') drupal_goto('checkout');
    }
  }
  if (isset($form['#is_admin']) && $form['#is_admin']) drupal_set_message(t('Cart updated.'));
}

//Load offers for cart admin autocomplete
function graber_cart_fetch_offers($string, $js=TRUE) {
  $node_types=gp_get_settings('graber_cart', 'buyable_types');
  $matches=array();
  $query=db_select('node', 'n');
  $return=$query
    ->fields('n', array('nid', 'title'))
    ->condition('n.type', $node_types, 'IN')
    ->condition(db_or()
      ->condition('n.title', '%'.db_like($string).'%', 'LIKE')
      ->condition('n.nid', (int) $string)
    )
    ->range(0, 10)
    ->execute();
 
  foreach ($return as $row) {
    $value=$row->title.' [nid='.$row->nid.']';
    $matches[$value]=$value;
  }
 
  // return for JS
  if ($js) drupal_json_output($matches);
  else {
    if (empty($matches)) return FALSE;
    else return $matches;
  }
}

function graber_cart_form_admin_add_ajax($form, $form_state) {
  return $form['cart_table'];
}

function graber_cart_form_admin_add($form, &$form_state) {
  $node=node_load($form_state['values']['add_widget']['nid']);
  if ($node) {
    $data=array();
    $data['title']=$node->title;
    foreach (module_implements('graber_cart_alter') as $module) {
      $function = $module . '_graber_cart_alter';
      $function('add', $node, $data);
    }
      
    if ($form_state['#cart']->add($node, $data)) {
      $form_state['#cart']->archivize();
      drupal_set_message(t('Article "!title" has been added to the cart.', array('!title' => $node->title)));
      $form_state['rebuild']=TRUE;
    }
  }
}
