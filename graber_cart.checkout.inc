<?php
function graber_cart_checkout_page() {
  $checkout_form=drupal_get_form('graber_cart_checkout_form');
  return $checkout_form;
}

function graber_cart_checkout_form($form, &$form_state) {
  if (empty($form_state['step'])) $form_state['step']=1;
  $step=&$form_state['step'];
  
  global $cart;
  if (empty($cart->contents)) {
    return array('info' => array(
      '#markup' => t('Your cart is empty.')
    ));
  }
  $form['#cart']=&$cart;

  $checkout_panes=array();
  foreach (module_implements('graber_checkout_panes') as $module) {
    $panes=call_user_func_array($module.'_graber_checkout_panes', array($cart, $form_state));
    if (is_array($panes)) $checkout_panes+=$panes;
  }

  //Apply weights, execute custom handlers and sort panes
  $weights=variable_get('graber_checkout_weights', array());
  $weight=0;
  foreach ($checkout_panes as $pane_key => $data) {
    if (substr($pane_key, 0, 1) != '#') {
      drupal_alter('graber_pane_'.$pane_key, $checkout_panes[$pane_key], $form_state);
      if (isset($weights[$pane_key])) $checkout_panes[$pane_key]['weight']=$weights[$pane_key];
      elseif (!isset($data['weight'])) $checkout_panes[$pane_key]['weight']=$weight;
      $weight++;
    }
  }
  uasort($checkout_panes, 'drupal_sort_weight');
  
  $full_form=array('#submit' => array(), '#validate' => array());
  foreach ($checkout_panes as $pane_key => $data) {
    unset($data['weight']);
    if (isset($data['#submit'])) {
      foreach ($data['#submit'] as $callback) array_push($full_form['#submit'], $callback);
    }
    if (isset($data['#validate'])) {
      foreach ($data['#validate'] as $callback) {
        array_push($full_form['#validate'], $callback);
      }
      unset ($data['#validate']);
    }
    if (isset($data['#data'])) {
      if (!isset($full_form['#data']) || !is_array($full_form['#data'])) $full_form['#data']=array();
      $full_form['#data']=array_merge($full_form['#data'], $data['#data']);
      unset ($data['#data']);
    }
    if (empty($data)) continue;
    if (!isset($data['#attributes']['class'])) $data['#attributes']['class']=array($pane_key);
    $full_form[$pane_key]=$data;
  }

  $form['submit-wrapper']=array(
    '#prefix' => '<div id="checkout-submit-buttons">',
    '#suffix' => '</div>',
    '#weight' => 110
  );

  if ($step == 1) {
    $form+=$full_form;
    
    //All other submit handlers must be omitted in step 1
    $form['#submit']=array('graber_checkout_first_step_submit');

    $statute_addr=gp_get_settings('graber_cart', 'statute_addr');
    if ($statute_addr) $link=l(t('Terms and Conditions'), $statute_addr, array('attributes' => array('target'=>'_blank')));
    else $link=t('Terms and Conditions');
    $form['statute']=array(
      '#type' => 'checkbox',
      '#title' => '<span class="statute">'.t('I declare that I am familiar with and I accept !statute of !site_name.', array('!statute' => $link, '!site_name' => variable_get('site_name', 'this store'))).'</span><span class="form-required" title="This field is required.">*</span>',
      '#default_value' => isset($form_state['values']['statute']) ? $form_state['values']['statute'] : 0
    );
    
    $form['submit-wrapper']['continue']=array(
      '#type' => 'submit',
      '#value' => t('Proceed'),
      //Prevent double-click action from frontend
      '#attributes' => array('onclick' => 'this.disabled=true; this.form.submit();'),
      '#weight' => 100
    );
  }
  elseif ($step == 2) {
    $form['#submit']=$full_form['#submit'];
    
    $form['cart_summary']['#markup']=$cart->render('summary');
    $form['order_summary']['#markup']=theme('graber_checkout_summary', array('cart' => $cart, 'form' => $full_form, 'values' => $form_state['values']));
    $form['submit-wrapper']['back']=array(
      '#type' => 'submit',
      '#value' => t('Back'),
      '#op' => 'back',
      '#submit' => array('graber_checkout_back'),
      '#weight' => 100
    );
    $form['submit-wrapper']['buy']=array(
      '#type' => 'submit',
      '#value' => t('Buy'),
      '#op' => 'buy',
      '#weight' => 101
    );
  }
  $form['#attributes']['class']=array('graber-checkout-form');
  $form['#action']=url('checkout');
  $form['#attached']=array(
    'css' => array(drupal_get_path('module', 'graber_cart').'/css/graber_cart.css'),
    'js' => array(drupal_get_path('module', 'graber_cart').'/graber_cart.js')
  );
  return $form;
}

function graber_cart_order_data_form_fields($defaults=array()) {

  //Primary data pane
  $primary=array(
    '#type' => 'fieldset',
    '#title' => t('Primary data')
  );
  $primary['mail']=array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#required' => TRUE,
    '#default_value' => isset($defaults['mail']) ? $defaults['mail'] : '',
    '#attributes' => array('class' => array('required')),
  );
  
  //Billing pane
  $billing=array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Orderer data')
  );
  $billing['legal_entity']=array(
    '#type' => 'radios',
    '#title' => t('Legal status'),
    '#options' => array(
      'person' => t('Person'),
      'company' => t('Company')
    ),
    '#ajax' => array(
      'callback' => 'graber_checkout_toggle_entity',
      'wrapper' => 'data-toggable-wrapper'
    ),
    '#default_value' => isset($defaults['billing']['legal_entity']) ? $defaults['billing']['legal_entity'] : '',
  );
  
  $billing['name']=array(
    '#prefix' => '<div class="data-toggable-wrapper">',
    '#type' => 'textfield',
    '#title' => t('Name', array(), array('context' => 'person')),
    '#default_value' => isset($defaults['billing']['name']) ? $defaults['billing']['name'] : '',
    '#required' => TRUE
  );
  $billing['family']=array(
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Family'),
    '#default_value' => isset($defaults['billing']['family']) ? $defaults['billing']['family'] : '',
    '#required' => TRUE
  );
  $billing['company']=array(
    '#prefix' => '<div class="data-toggable-wrapper">',
    '#type' => 'textfield',
    '#title' => t('Company'),
    '#default_value' => isset($defaults['billing']['company']) ? $defaults['billing']['company'] : '',
    '#required' => TRUE
  );
  $billing['tin']=array(
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('TIN'),
    '#default_value' => isset($defaults['billing']['tin']) ? $defaults['billing']['tin'] : '',
    '#required' => TRUE
  );
  $billing['phone']=array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#attributes' => array('class' => array('phone')),
    '#default_value' => isset($defaults['billing']['phone']) ? $defaults['billing']['phone'] : ''
  );
  $billing['address']=array(
    '#type' => 'textarea',
    '#rows' => 2,
    '#resizable' => FALSE,
    '#title' => t('Street address'),
    '#attributes' => array('class' => array('required')),
    '#default_value' => isset($defaults['billing']['address']) ? $defaults['billing']['address'] : '',
    '#required' => TRUE
  );
  $billing['city']=array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#attributes' => array('class' => array('required')),
    '#default_value' => isset($defaults['billing']['city']) ? $defaults['billing']['city'] : '',
    '#required' => TRUE
  );
  $billing['postal_code']=array(
    '#type' => 'textfield',
    '#title' => t('Postal code'),
    '#attributes' => array('class' => array('required')),
    '#default_value' => isset($defaults['billing']['postal_code']) ? $defaults['billing']['postal_code'] : '',
    '#required' => TRUE
  );
  
  //Shipping pane
  $shipping=array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Shipping data'),
    '#collapsible' => !empty($defaults['checkout']) ? FALSE : TRUE,
    '#collapsed' => !empty($defaults['checkout']) ? FALSE : FALSE
  );

  $shipping['different_shipping']=array(
    '#type' => 'checkbox',
    '#title' => t('Different shipping data'),
    '#description' => t('If not checked, orderer data will be used.'),
    '#default_value' => isset($defaults['shipping']['different_shipping']) ? $defaults['shipping']['different_shipping'] : 0,
    '#ajax' => array(
      'callback' => 'graber_checkout_toggle_shipping',
      'wrapper' => 'checkout-billing-wrapper'
    ),
    '#weight' => -10
  );
  if (isset($defaults['shipping']['different_shipping'])) {
    if ($defaults['shipping']['different_shipping']) $shipping['#gc_displayed']=TRUE;
    else $shipping['#gc_displayed']=FALSE;
  }
  else $shipping['#gc_displayed']=FALSE;
  
  if ($shipping['#gc_displayed']) {
    
    $shipping['name']=array(
      '#type' => 'textfield',
      '#title' => t('Name', array(), array('context' => 'person')),
      '#default_value' => isset($defaults['shipping']['name']) ? $defaults['shipping']['name'] : '',
      '#attributes' => array('class' => array('required')),
    );
    $shipping['family']=array(
      '#type' => 'textfield',
      '#title' => t('Family name'),
      '#default_value' => isset($defaults['shipping']['family']) ? $defaults['shipping']['family'] : '',
      '#attributes' => array('class' => array('required')),
    );
    $shipping['company']=array(
      '#type' => 'textfield',
      '#title' => t('Company'),
      '#default_value' => isset($defaults['shipping']['company']) ? $defaults['shipping']['company'] : '',
      '#attributes' => array('class' => array('required'))
    );
    
    $shipping['phone']=array(
      '#type' => 'textfield',
      '#title' => t('Phone'),
      '#attributes' => array('class' => array('phone')),
      '#default_value' => isset($defaults['shipping']['phone']) ? $defaults['shipping']['phone'] : ''
    );
    $shipping['address']=array(
      '#type' => 'textarea',
      '#rows' => 2,
      '#resizable' => FALSE,
      '#title' => t('Street address'),
      '#attributes' => array('class' => array('required')),
      '#default_value' => isset($defaults['shipping']['address']) ? $defaults['shipping']['address'] : '',
      '#required' => TRUE
    );
    $shipping['city']=array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#attributes' => array('class' => array('required')),
      '#default_value' => isset($defaults['shipping']['city']) ? $defaults['shipping']['city'] : '',
      '#required' => TRUE
    );
    $shipping['postal_code']=array(
      '#type' => 'textfield',
      '#title' => t('Postal code'),
      '#attributes' => array('class' => array('required')),
      '#default_value' => isset($defaults['shipping']['postal_code']) ? $defaults['shipping']['postal_code'] : '',
      '#required' => TRUE
    );
    
  }
    
  $shipping['#prefix']='<div id="checkout-billing-wrapper">';
  $shipping['#suffix']='</div>';
  
  $invoice['#gc_displayed']=FALSE;
  $invoice_setting=gp_get_settings('graber_cart', 'invoice_option');
  if ($invoice_setting) {
    $invoice=array(
      '#type' => 'fieldset',
      '#title' => t('Invoice'),
      '#tree' => TRUE
    );
    $invoice['invoice_selector']=array(
      '#type' => 'checkbox',
      '#title' => t('I would like to receive an invoice'),
      '#default_value' => isset($defaults['invoice_selector']) ? $defaults['invoice_selector'] : 0,
      '#ajax' => array(
        'callback' => 'graber_checkout_toggle_invoice',
        'wrapper' => 'checkout-invoice-wrapper'
      ),
      '#default_value' => isset($defaults['invoice']['invoice_selector']) ? $defaults['invoice']['invoice_selector'] : NULL,
      '#weight' => -10
    );
    
    if (isset($defaults['invoice']['invoice_selector']) && $defaults['invoice']['invoice_selector']) {
      $invoice['#gc_displayed']=TRUE;
      $invoice['copy_data']['#markup']='<a href="javascript:void(0)" id="gc-invoice-copy-data" onclick="graber_chcekout_copy_data();">'.t('Copy values from orderer data >>').'</a>';
      foreach ($billing as $key => $field) {
        if (substr($key, 0, 1) == '#' || $key == 'phone') continue;
        $invoice[$key]=$field;
        $invoice[$key]['#default_value']=isset($defaults['invoice'][$key]) ? $defaults['invoice'][$key] : '';
      }
      //Hide obsolete fields for invoice
      if (!isset($defaults['invoice']['legal_entity']) || $defaults['invoice']['legal_entity'] == 'company') {
        unset($invoice['name']);
        unset($invoice['family']);
      }
      else {
        unset($invoice['company']);
        unset($invoice['tin']);
      }
    }  
    $invoice['#prefix']='<div id="checkout-invoice-wrapper">';
    $invoice['#suffix']='</div>';
  }
  else $invoice=NULL;
  
  //Hide obsolete fields for billing
  if (!isset($defaults['billing']['legal_entity']) || $defaults['billing']['legal_entity'] == 'person') {
    unset($billing['company']);
    unset($billing['tin']);
  }
  else {
    unset($billing['name']);
    unset($billing['family']);
  }
  
  //Remarks pane
  $remarks=array(
    '#type' => 'fieldset',
    '#title' => t('Remarks'),
  );
  $remarks['remarks']=array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#title' => '&nbsp;',
    '#resizable' => FALSE,
    '#default_value' => isset($defaults['remarks']) ? $defaults['remarks'] : ''
  );
  
  if (empty($defaults['checkout'])) {
    $shipping_method=array(
      '#type' => 'fieldset',
      '#title' => t('Shipping selection'),
    );
    $shipping_method['#collapsible']=TRUE;
    $shipping_method['#collapsed']=TRUE;
  }
  return array(
    'primary' => $primary,
    'billing' => $billing,
    'shipping' => $shipping,
    'invoice' => $invoice,
    'remarks' => $remarks,
  );
}

function graber_checkout_toggle_entity($form, $form_state) {
  $pane=$form_state['triggering_element']['#parents'][0];
  $value=$form_state['triggering_element']['#value'];
  
  //Load billing field values from user profile, if they exist
  if ($pane === 'billing') {
    $map=array('company' => array('company', 'tin'), 'person' => array('name', 'family'));
    if (!isset($form['#order'])) {
      $user=user_load($GLOBALS['user']->uid);
      $fields=field_info_instances('user', 'user');
      if (!empty($map[$value])) {
        foreach ($map[$value] as $field_name) {
          if (!empty($fields['field_'.$field_name])) {
            $field_data=field_get_items('user', $user, 'field_'.$field_name);
            if (!empty($field_data[0]['value'])) $form[$pane][$field_name]['#value']=$field_data[0]['value'];
          }
        }
      }
    }
    else {
      foreach ($map[$value] as $field_name) {
        if (!empty($form['#order']->order_data['billing_addr'][$field_name])) {
          $form[$pane][$field_name]['#value']=$form['#order']->order_data['billing_addr'][$field_name];
        }
      }
    }
  }
  
  if ($value == 'company') $output=drupal_render($form[$pane]['company']).drupal_render($form[$pane]['tin']);
  elseif ($value == 'person') $output=drupal_render($form[$pane]['name']).drupal_render($form[$pane]['family']);
  
  $commands=array();
  $selector='form .'.$pane.' .data-toggable-wrapper';
  $commands[]=ajax_command_replace($selector, $output);
  return array('#type' => 'ajax', '#commands' => $commands);
}

function graber_checkout_toggle_shipping($form, $form_state) {
  return $form['shipping'];
}

function graber_checkout_toggle_invoice($form, $form_state) {
  return $form['invoice'];
}

function graber_cart_graber_checkout_panes($cart, &$form_state) {
  if ($cart->uid == $GLOBALS['user']->uid) global $user;
  else $user=user_load($cart->uid);
  
  //Populate default values
  if (isset($form_state['values'])) {
    $defaults=$form_state['values'];
  }
  else {
    $form_state['values']=array();
    if ($user->uid > 0)  {
      $user=user_load($user->uid);
      if (!isset($defaults['mail'])) $defaults['mail']=$user->mail;
      $fields=field_info_instances('user', 'user');
      foreach ($fields as $name => $info) {
        $field_data=field_get_items('user', $user, $name);
        if (!empty($field_data[0]['value'])) {
          $elem_name=substr($name, 6);
          if (!isset($defaults['billing'][$elem_name])) $defaults['billing'][$elem_name]=$field_data[0]['value'];
        }
      }
    }
    $form_state['gc_defaults']=$defaults;
  }
  if (!isset($defaults['billing']['legal_entity'])) $defaults['billing']['legal_entity']='person';
  if (!isset($defaults['invoice']['legal_entity'])) $defaults['invoice']['legal_entity']='company';
    
  $defaults['checkout']=TRUE;
  
  if (empty($form_state['values'])) drupal_alter('graber_checkout_panes_defaults', $defaults, $cart);
  
  $form=graber_cart_order_data_form_fields($defaults);
      
  $form['handlers']['#submit']=array('graber_cart_checkout_submit');
  $form['handlers']['#validate']=array('graber_cart_checkout_validate');
  $form['handlers']['weight']=999;
  return $form;
}

function graber_cart_checkout_validate($form, &$form_state) {
  
  $form_state['values']['mail']=strtolower($form_state['values']['mail']);
  if (!valid_email_address($form_state['values']['mail'])) form_set_error('mail', t('Please provide a valid e-mail address.'));
  
  //Shipping
  if (!empty($form_state['values']['shipping']['different_shipping'])) {
    $shipping=$form_state['values']['shipping'];
    if (
      empty($form_state['values']['shipping']['company']) && (
        empty($form_state['values']['shipping']['name']) || 
        empty($form_state['values']['shipping']['family'])
      )
    ) {
      form_set_error('shipping][name', t('Please provide name and family or company name in your shipping address.'));
      form_set_error('shipping][family');
      form_set_error('shipping][company');
    }
  }
  
  //Statute
  if (isset($form_state['values']['statute']) && empty($form_state['values']['statute'])) form_set_error('statute', t('You must accept our terms and conditions to make a purchase.'));
  
  //Prevent double-click action from backend
  if ($form['#cart']->status == 0) {
    drupal_set_message(t('Thank you, your order will be processed as soon as possible.'));
    drupal_goto('');
  }
}

function graber_checkout_first_step_submit($form, &$form_state) {
  $form_state['step_data'][$form_state['step']]['values']=$form_state['values'];
  if (!empty($form['#data'])) $form_state['step_data'][$form_state['step']]['data']=$form['#data'];
  $form_state['step']=2;
  $form_state['rebuild']=TRUE;
}

function graber_checkout_back($form, &$form_state) {
  $form_state['step']=1;
  $form_state['values']=$form_state['step_data'][$form_state['step']]['values'];
  $form_state['rebuild']=TRUE;
}

function graber_cart_checkout_submit($form, &$form_state) {
  if ($form_state['step'] == 2) {
    if ($form_state['clicked_button']['#op'] == 'buy') {
      $values=$form_state['step_data'][1]['values'];
      if (!empty($form_state['step_data'][1]['data'])) $data=$form_state['step_data'][1]['data'];
      else $data=array();
    }
  }
  
  if (isset($values)) {

    //Create a new order
    if (isset($values['invoice'])) {
      $invoice=array();
      foreach ($values['invoice'] as $key => $value) {
        if (is_array($value)) {
          foreach ($value as $key2 => $value2) {
            if (!empty($value2)) $invoice[$key2]=$value2;
          }
        }
        elseif (!empty($value)) $invoice[$key]=$value;
      }
      unset($values['invoice']);
    }
    
    $billing=array();
    foreach ($values['billing'] as $key => $value) {
      if (is_array($value)) {
        foreach ($value as $key2 => $value2) {
          if (!empty($value2)) $billing[$key2]=$value2;
        }
      }
      elseif (!empty($value)) $billing[$key]=$value;
    }
    unset($values['billing']);

    if ($values['shipping']['different_shipping'] == 0) {
      $shipping=$billing;
      if (!empty($invoice)) $shipping['different_shipping']=1;
      else $shipping['different_shipping']=0;
      unset($values['shipping']);
    }
    else {
      $shipping=array();
      foreach ($values['shipping'] as $key => $value) {
        if (is_array($value)) {
          foreach ($value as $key2 => $value2) {
            if (!empty($value2)) $shipping[$key2]=$value2;
          }
        }
        elseif (!empty($value)) $shipping[$key]=$value;
      }
    }
            
    $unset=array('form_build_id', 'form_token', 'form_id', 'op', 'buy', 'continue');
    foreach ($unset as $index) unset($values[$index]);

    //Unset cart payment method setting if shipping changed after checkout deployment
    $shipping_methods=module_invoke_all('graber_cart_shipping_info', $form['#cart']);
    if (!empty($shipping_methods)) {
      $shipping_method=$shipping_methods[$form['#cart']->data['shipping_method']];
      if (isset($shipping_method['payment']) && $shipping_method['payment'] == FALSE) {
        unset($form['#cart']->data['payment_method']);
        $form['#cart']->save_data();
      }
      $data['shipping_method']=$form['#cart']->data['shipping_method'];
      $data['shipping_price']=$form['#cart']->data['shipping_price'];
    }

    //Pass cart data to order    
    $data['payment_method']=empty($form['#cart']->data['payment_method']) ? 'upon' : $form['#cart']->data['payment_method'];
    $data['articles_price']=$form['#cart']->data['articles_price'];
    $data['total_price']=$form['#cart']->data['total_price'];
    
    
    if (!empty($values)) $data+=$values;
    
    $desc='';
    if (!empty($form['#cart']->contents)) {
      foreach ($form['#cart']->contents as $item) {
        if (!empty($desc)) {
          $desc.=' / ';
        }
        $desc.=$item['quantity'].'x '.$item['title'];
        if (strlen($desc > 255)) {
          $desc=substr($desc, 0, 253).'..';
          break;
        }
      }
    }

    global $language;
    
    $order_data=array(
      'uid' => $form['#cart']->uid,
      'mail' => $data['mail'],
      'amount' => $data['total_price'],
      'currency' => $form['#cart']->currency,
      'description' => $desc,
      'order_data' => array(
        'shipping_addr' => $shipping,
        'billing_addr' => empty($invoice) ? $billing : $invoice,
        'language' => array(
          'prefix' => $language->prefix,
          'langcode' => $language->language
        ),
        'cid' => $form['#cart']->cid,
        'currency' => $form['#cart']->currency,
        'data' => $data
      ),
      'status' => 1
    );
    $order=new graber_order($order_data);
    
    $order->order_data['update_profile']=TRUE;
    
    $result=module_invoke_all('graber_checkout_before_save', $form['#cart'], $order, $values);

    //Update user profile data
    $account=user_load($form['#cart']->uid);
    if ($account->uid && $order->order_data['update_profile']) {
      $update=FALSE;
      foreach ($billing as $field_name => $value) {
        if ($field_name == 'mail') continue; //Legacy
        elseif (empty($account->{'field_'.$field_name}[LANGUAGE_NONE][0]['value']) || $account->{'field_'.$field_name}[LANGUAGE_NONE][0]['value'] != $value) {
          $account->{'field_'.$field_name}[LANGUAGE_NONE][0]['value']=$value;
          $update=TRUE;
        }
      }
      if ($update) {
        try {
          user_save($account);
        }
        catch (Exception $e) {
          watchdog('graber_cart', 'Possible user fields inconsistency: !message', array('!message' => $e->getMessage()), WATCHDOG_ERROR);
        }
      }
    }
    
    unset($order->order_data['update_profile']);
    
    //Archivize cart
    $form['#cart']->archivize();

    if (!in_array(FALSE, $result)) {
      $order->save();
      //Create a new payment
      $payment=new_payment($account, $order, array($data));
      if (isset($data['payment_method'])) {
        $payment_addr=get_payment_links($payment, $data['payment_method']);
        if (!empty($payment_addr)) $payment['link']=array_pop($payment_addr);
      }

      //Invoke module hooks
      module_invoke_all('graber_checkout_complete', $form['#cart'], $order);

      //Redirect user
      if (!empty($form['#redirect'])) drupal_goto($form['#redirect']);
      else {
        if (!empty($payment['link'])) drupal_goto($payment['link']);
        else {
          if ($ty_page=variable_get('payment_upon_receiving_landing', FALSE)) {
            drupal_goto($ty_page);
          }
          else {
            drupal_set_message(t('Thank you, your order will be processed as soon as possible.'));
            drupal_goto('');
          }
        }
      }
    }
    else drupal_goto('');
  }
}