<?php
function graber_cart_entity_info() {
  return array(
    'order' => array(
      'label' => t('Order'),
	  'module' => 'graber_cart',
      'plural label' => t('Orders'),
      'entity class' => 'Entity',
      'controller class' => 'GCOrderController',
      'base table' => 'graber_orders',
      'entity keys' => array(
        'oid' => 'oid',
        'label' => t('Order'),
      ),
      'view modes' => array(
        'view' => array(
          'label' => t('Admin view'),
        ),
        'customer' => array(
          'label' => t('Customer view'),
        ),
      ),
    ),
  );
}

function graber_cart_entity_property_info() {
  $info = array();
  $properties = &$info['order']['properties'];
  $properties = array(
    'oid' => array(
      'type'  => 'integer',
      'label' => t('Order id'),
      'schema field' => 'oid',
    ),
    // Other fields...
  );
  return $info;
}

class GCOrderController extends DrupalDefaultEntityController {
}