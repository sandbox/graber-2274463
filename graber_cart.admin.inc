<?php
/* --------- Store global settings ---------- */
function graber_cart_settings_form($form, &$form_state) {
  $settings=gp_get_settings('graber_cart');
  
  $form['default_deadline']=array(
    '#type' => 'textfield',
    '#title' => t('Default deadline period'),
    '#default_value' => isset($settings['default_deadline']) ? $settings['default_deadline'] : '',
    '#description' => t('Value in days. leave empty for no deadline.')
  );
    
  if (user_access('graber cart superadmin')) {
    $type_options=array();
    $node_types=node_type_get_types();
    foreach ($node_types as $type => $data) $type_options[$type]=$data->name;
    
    $form['buyable_types']=array(
      '#type' => 'checkboxes',
      '#title' => t('Buyable node types'),
      '#options' => $type_options,
      '#default_value' => isset($settings['buyable_types']) ? $settings['buyable_types'] : array(),
      '#description' => t('Check at least one node type to enable your store.')
    );
    
    $entity_info=entity_get_info('node');
    $vm_options=array();
    foreach($entity_info['view modes'] as $mode => $data) $vm_options[$mode]=$data['label'];
    $form['view_modes']=array(
      '#type' => 'checkboxes',
      '#title' => t('View modes with add to cart option'),
      '#options' => $vm_options,
      '#default_value' => isset($settings['view_modes']) ? $settings['view_modes'] : array('teaser', 'full'),
      '#description' => t('Check at least one view mode to enable your store.')
    );
    
    $form['full_view_modes']=array(
      '#type' => 'checkboxes',
      '#title' => t('View modes with full form'),
      '#options' => $vm_options,
      '#default_value' => isset($settings['full_view_modes']) ? $settings['full_view_modes'] : array('full'),
      '#description' => t('Remaining view modes will have options form displayed in a modal popup.')
    );
    
    $form['quantity_selector']=array(
      '#type' => 'checkbox',
      '#title' => t('Quantity selector'),
      '#default_value' => isset($settings['quantity_selector']) ? $settings['quantity_selector'] : 0,
      '#description' => t('Display quantity selector?')
    );
    
    $form['anonymous_duration']=array(
      '#type' => 'textfield',
      '#size' => 3,
      '#title' => t('Anonymous cart duration'),
      '#field_suffix' => t('hours'),
      '#description' => t('How long is cart data kept in the database (and products stay reserved in carts) for anonymous customers since last cart update (hours). Value from 1 to 240.'),
      '#default_value' => isset($settings['anonymous_duration']) ? $settings['anonymous_duration'] : '',
      '#required' => TRUE
    );
    $form['user_duration']=array(
      '#type' => 'textfield',
      '#size' => 3,
      '#title' => t('User cart duration'),
      '#field_suffix' => t('hours'),
      '#description' => t('How long is cart data kept in the database (and products stay reserved in carts) for logged-in customers since last cart update (hours). Value from 1 to 240.'),
      '#default_value' => isset($settings['user_duration']) ? $settings['user_duration'] : '',
      '#required' => TRUE
    );
    
    $form['statute_addr']=array(
      '#type' => 'textfield',
      '#title' => t('Terms and conditions page'),
      '#description' => t('Enter page internal address (eg. node/1)'),
      '#default_value' => isset($settings['statute_addr']) ? $settings['statute_addr'] : ''
    );
    
    $form['invoice_option']=array(
      '#type' => 'checkbox',
      '#title' => t('Include invoice pane on checkout'),
      '#default_value' => isset($settings['invoice_option']) ? $settings['invoice_option'] : 0
    );
    
    $form['cart_version']=array(
      '#type' => 'select',
      '#title' => t('Cart version'),
      '#options' => array(
        'classic' => t('classic'),
        'responsive' => t('responsive')
      ),
      '#default_value' => isset($settings['cart_version']) ? $settings['cart_version'] : 'classic'
    );
    
    $form['quantity_calaculation']=array(
      '#type' => 'radios',
      '#title' => t('Cart block quantity calculation method'),
      '#options' => array(
        'total' => t('Total cart articles'),
        'offers' => t('Number of offers (rows)')
      ),
      '#default_value' => isset($settings['quantity_calaculation']) ? $settings['quantity_calaculation'] : 'total'
    );

    $form['random_order_ids'] = array(
      '#type' => 'checkbox',
      '#title' => t('Random order IDs'),
      '#default_value' => isset($settings['random_order_ids']) ? $settings['random_order_ids'] : 1,
    );
        
    $checkout_panes=module_invoke_all('checkout_panes_info');
    foreach ($checkout_panes as $pane_key => $data) if (!is_array($data)) $checkout_panes[$pane_key]=array('title' => $data);
    $checkout_weights=variable_get('graber_checkout_weights', array());
    foreach ($checkout_weights as $pane_key => $weight) {
      if (isset($checkout_panes[$pane_key])) $checkout_panes[$pane_key]['weight']=$weight;
    }
    uasort($checkout_panes, 'drupal_sort_weight');
    $form['checkout_order']=array(
      '#tree' => TRUE,
      '#theme' => 'graber_tabledrag',
      '#title' => t('Checkout panes order')
    );
    
    foreach ($checkout_panes as $pane_key => $data) {
      $form['checkout_order'][$pane_key]=array(
        'title' => array(
          '#markup' => isset($data['title']) ? $data['title'] : $pane_key,
          '#title' => t('Pane')
        ),
        'weight' => array(
          '#type' => 'weight',
          '#title' => t('Weight'),
          '#default_value' => isset($checkout_weights[$pane_key]) ? $checkout_weights[$pane_key] : 0,
          '#delta' => 10,
          '#title-display' => 'invisible',
        )
      );
    }
  }

  $form['save']=array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#weight' => 100
  );
  return $form;
}

function graber_cart_settings_form_validate($form, &$form_state) {
  if ($form_state['values']['anonymous_duration'] < 1 || $form_state['values']['anonymous_duration'] > 240) form_set_error('anonymous_duration', t('Anonymous cart duration must range from 1 to 240 hours.'));
  if ($form_state['values']['user_duration'] < 1 || $form_state['values']['user_duration'] > 240) form_set_error('user_duration', t('User cart duration must range from 1 to 240 hours.'));
  if (!empty($form_state['values']['statute_addr'])) {
    if (!menu_get_item($form_state['values']['statute_addr'])) form_set_error('statute_addr', t('Specified address does not exist.'));
  }
}

function graber_cart_settings_form_submit($form, &$form_state) {
  $settings=array(
    'anonymous_duration' =>  $form_state['values']['anonymous_duration'],
    'user_duration' => $form_state['values']['user_duration'],
    'buyable_types' => $form_state['values']['buyable_types'],
    'view_modes' => $form_state['values']['view_modes'],
    'full_view_modes' => $form_state['values']['full_view_modes'],
    'quantity_selector' => $form_state['values']['quantity_selector'],
    'statute_addr' => $form_state['values']['statute_addr'] ? $form_state['values']['statute_addr'] : FALSE,
    'invoice_option' => $form_state['values']['invoice_option'] ? $form_state['values']['invoice_option'] : FALSE,
    'cart_version' => $form_state['values']['cart_version'] ? $form_state['values']['cart_version'] : 'classic',
    'quantity_calaculation' => $form_state['values']['quantity_calaculation'],
    'default_deadline' => $form_state['values']['default_deadline'],
    'random_order_ids' => $form_state['values']['random_order_ids'],
  );
  
  gp_save_settings('graber_cart', $settings);
  $pane_order=array();
  foreach ($form_state['values']['checkout_order'] as $pane_key => $data) {
    $pane_order[$pane_key]=$data['weight'];
  }
  variable_set('graber_checkout_weights', $pane_order);
  drupal_set_message(t('Store settings saved.'));
}

/* --------- Orders management ---------- */
function graber_cart_orders_page() {
  return drupal_get_form('graber_cart_orders_form');
}

function graber_cart_orders_form($form, &$form_state) {
  if (!empty($form_state['values']['action'])) {
    $form=graber_orders_actions_confirm_form($form, $form_state);
    $form['#submit']=array('graber_orders_actions_confirm_form_submit');
  }
  else {
    $form['filter']=graber_orders_filter_form();
    $form['filter']['#title']=t('Filters');
    $form['filter']['#type']='fieldset';
    $form['filter']['#attributes']['class']=array('graber-filter-wrapper');
    $form['#submit'][]='graber_orders_filter_form_submit';
    $form['actions']=graber_orders_actions_form(array(), $form_state);
    $form['actions']['#title']=t('Actions');
    $form['actions']['#type']='fieldset';
    $form['orders']=graber_cart_orders_table();
  }

  return $form;
}

function graber_orders_actions_form($form, &$form_state) {
  $actions=array(0 => t('Choose an action'));
  $statuses=graber_order_statuses();
  foreach ($statuses as $status_id => $title) $actions['S_'.$status_id]=t('Update status: !status', array('!status' => $title));
  if (module_exists('payment')) {
    $actions['P_1']=t('Update payment: received');
    $actions['P_0']=t('Update payment: not received');
  }
  $actions['D_1']=t('Delete orders');
  $form['action']=array(
    '#type' => 'select',
    '#title' => t('Action'),
    '#options' => $actions
  );
  $form['apply_action']=array(
    '#type' => 'submit',
    '#value' => t('Apply action'),
    '#submit' => array('graber_orders_save_action_values')
  );
  return $form;
}

function graber_orders_save_action_values($form, &$form_state) {
  $form_state['rebuild']=TRUE;
  $form_state['action_data']['action']=substr($form_state['values']['action'], 0, 1);
  $form_state['action_data']['value']=substr($form_state['values']['action'], 2);
  $oids=array();
  foreach ($form_state['values']['orders'] as $oid) {
    if ($oid) $oids[]=$oid;
  }
  if (empty($oids)) form_set_error('orders', t('Please select at least one order.'));
  else $form_state['action_data']['oids']=$oids;
}

function graber_orders_actions_confirm_form($form, &$form_state) {
  $order_data=db_select('graber_orders', 'o')->fields('o', array('oid', 'mail', 'description'))->condition('oid', $form_state['action_data']['oids'], 'IN')->execute()->fetchAll();
  $confirm_list=array();
  foreach ($order_data as $result) {
    if (strlen($result->description) > 100) $result->description=substr($result->description, 0, 98).'..';
    $confirm_list[]=$result->description.' ('.$result->mail.')';
  }
  switch ($form_state['action_data']['action']) {
    case 'S':
      $message=t('Are you sure you want to update status of the following orders to !status?', array('!status' => graber_order_statuses($form_state['action_data']['value'])));
      break;
    case 'P':
      $message=t('Are you sure you want to update payment status of the following orders to !status?', array('!status' => ($form_state['action_data']['value']) ? t('paid') : t('not paid')));
      break;
    case 'D':
      $message=t('Are you sure you want to delete the following orders?');
      break;
  }
  $form['confirm']=array(
    '#type' => 'submit',
    '#value' => t('Confirm'),
    '#prefix' => '<p class="message">'.$message.'</p>'.theme('item_list', array('items' => $confirm_list))
  );
  return $form;
}

function graber_orders_actions_confirm_form_submit($form, &$form_state) {
  $count=0;
  switch ($form_state['action_data']['action']) {
    case 'S':
      foreach ($form_state['action_data']['oids'] as $oid) {
        if ($oid) {
          $order=new graber_order($oid);
          if (!empty($order->oid) && $order->status != $form_state['action_data']['value']) {
            if ($order->save(array('status' => $form_state['action_data']['value']))) {
              $count++;
            }
          }
        }
      }
      drupal_set_message(t('!count orders updated.', array('!count' => $count)));
      break;
    case 'P':
      foreach ($form_state['action_data']['oids'] as $oid) {
        if ($oid) {
          $order=new graber_order($oid);
          if (!empty($order->oid) && $order->paid != $form_state['action_data']['value']) {
            if ($pid=db_query("SELECT pid FROM {payment_data} WHERE oid = :oid", array(':oid' => $order->oid))->fetchColumn()) {
              if ($form_state['action_data']['value']) payment_received($pid);
              else $order->save(array('paid' => 0));
              $count++;
            }
          }
        }
      }
      drupal_set_message(t('!count orders updated.', array('!count' => $count)));
      break;
    case 'D':
      if (!empty($form_state['action_data']['value'])) {
        foreach ($form_state['action_data']['oids'] as $oid) {
          if ($oid) {
            $order=new graber_order($oid);
            if (!empty($order->oid)) {
              $order->delete();
              $count++;
            }
          }
        }
      }
      drupal_set_message(t('!count orders deleted.', array('!count' => $count)));
      break;
  }
}

function graber_orders_filter_form() {
  $status_options=array('no_filter' => t('all'));
  $status_options+=graber_order_statuses();
  $form['filters']=array('#tree' => TRUE);
  $form['filters']['status']=array(
    '#type' => 'select',
    '#title' => t('Order status'),
    '#options' => $status_options,
    '#default_value' => isset($_SESSION['graber_orders_filter']['status']) ? $_SESSION['graber_orders_filter']['status'] : 'no_filter'
  );
  $form['filters']['paid']=array(
    '#type' => 'select',
    '#title' => t('Payment status'),
    '#options' => array('no_filter' => t('all'), 0 => t('unpaid'), 1 => t('paid')),
    '#default_value' => isset($_SESSION['graber_orders_filter']['paid']) ? $_SESSION['graber_orders_filter']['paid'] : 'no_filter'
  );
  $form['filters']['code']=array(
    '#type' => 'textfield',
    '#title' => t('Order code'),
    '#size' => 20,
    '#autocomplete_path' => 'admin/store/ajax/order-autocomplete/code',
    '#default_value' => isset($_SESSION['graber_orders_filter']['code']) ? $_SESSION['graber_orders_filter']['code'] : ''
  );
  $form['filters']['mail']=array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#size' => 20,
    '#autocomplete_path' => 'admin/store/ajax/order-autocomplete/mail',
    '#default_value' => isset($_SESSION['graber_orders_filter']['mail']) ? $_SESSION['graber_orders_filter']['mail'] : ''
  );
  $form['filters']['created_from']=array(
    '#prefix' => '<div class="created-filter">',
    '#type' => 'textfield',
    '#title' => t('Created from'),
    '#default_value' => isset($_SESSION['graber_orders_filter']['created_from']) ? $_SESSION['graber_orders_filter']['created_from'] : ''
  );
  $form['filters']['created_to']=array(
    '#type' => 'textfield',
    '#title' => t('Created to'),
    '#default_value' => isset($_SESSION['graber_orders_filter']['created_to']) ? $_SESSION['graber_orders_filter']['created_to'] : '',
    '#suffix' => '</div>'
  );
  gp_element_add_date_popup($form['filters']['created_from']);
  gp_element_add_date_popup($form['filters']['created_to']);
  
  $form['npage']=array(
    '#type' => 'select',
    '#title' => t('Orders per page'),
    '#options' => array(
      'all' => t('All'),
      20 => '20',
      50 => '50',
      100 => '100'
    ),
    '#default_value' => isset($_SESSION['graber_orders_npage']) ? $_SESSION['graber_orders_npage'] : 20,
  );
  
  if(isset($_SESSION['graber_orders_filter'])) $form['undo']=array(
    '#type' => 'submit',
    '#value' => t('Undo')
  );
  $form['filter']=array(
    '#type' => 'submit',
    '#value' => t('Filter')
  );
  
  return $form;

}

function graber_orders_filter_form_submit($form, &$form_state) {
  switch ($form_state['values']['op']) {
    case t('Filter'):
      foreach(array('created_from', 'created_to') as $field) {
        if (!isset($form_state['values']['filters'][$field])) $form_state['values']['filters'][$field]='';
      }
      foreach ($form_state['values']['filters'] as $filter => $value) {
        if ($value !== '' && $value !== 'no_filter') {
          $_SESSION['graber_orders_filter'][$filter]=$value;
        }
        else unset($_SESSION['graber_orders_filter'][$filter]);
      }
      //Npage
      if (!empty($form_state['values']['npage'])) {
        $_SESSION['graber_orders_npage']=$form_state['values']['npage'];
      }
      break;
    case t('Undo'):
      unset($_SESSION['graber_orders_filter']);
      break;
  }
}

function graber_cart_build_filter_query(SelectQueryInterface $query) {
  //Rule out archive orders if status filter is not set
  if (!isset($_SESSION['graber_orders_filter']['status'])) {
    $query->condition('status', 4, '<>');
    $query->condition('status', 5, '<>');
  }

  //Apply filters
  if (isset($_SESSION['graber_orders_filter'])) {
    foreach ($_SESSION['graber_orders_filter'] as $type => $value) {
      $data=explode('_', $type);
      if (sizeof($data) > 1) {
        $column=$data[0];
        switch ($data[1]) {
          case 'from':
            $operator='>';
            break;
          case 'to':
            $operator='<';
            break;
        }
      }
      else {
        $column=$type;
        $operator='=';
      }
      $query->condition($column, $value, $operator);
    }
  }
  
  //Apply per page setting
  if (isset($_SESSION['graber_orders_npage'])) $limit=$_SESSION['graber_orders_npage'];
  else $limit=20;
  if ($limit != 'all') {
    $query=$query->extend('PagerDefault')->limit($limit);
  }
  
  return $query;
}

function graber_cart_orders_table() {

  if (gp_get_settings('graber_cart', 'random_order_ids')) {
    $code_column = 'code';
  }
  else {
    $code_column = 'oid';
  }
  $header=array(
    'code' => array('data' => t('Order code'), 'field' => 'o.' . $code_column),
    'created' => array('data' => t('Created'), 'field' => 'o.created', 'sort' => 'desc'),
    'orderer' => array('data' => t('Orderer'), 'field' => 'o.orderer'),
    'amount' => array('data' => t('Amount'), 'field' => 'o.amount'),
    'status' => array('data' => t('Status'), 'field' => 'o.status'),
    'paid' => array('data' => t('Payment'), 'field' => 'o.paid'),
    'shipping_method' => array('data' => t('Shipping method')),
    'payment_method' => array('data' => t('Payment method')),
    'operations' => array('data' => t('Operations'))
  );
  $query=db_select('graber_orders', 'o')->fields('o');
  $query=$query->extend('TableSort');
  $query=graber_cart_build_filter_query($query);
  $query->orderByHeader($header);
  $order_data=$query->execute()->fetchAllAssoc('oid');
  
  $all_statuses=graber_order_statuses();
  $rows=array();
  
  $shipping_methods=module_invoke_all('graber_cart_shipping_info');
  $payment_methods=module_invoke_all('graber_cart_payment_info');
  if (!isset($payment_methods['upon'])) $payment_methods['upon']['title']=t('Upon receiving');
  
  foreach ($order_data as $oid => $result) {
    $order_data=unserialize($result->order_data);
    $operations=array(
      my_api_l(t('View'), 'admin/store/orders/'.$result->oid.'/edit/summary', array('dest' => trim(request_uri(), '/'))),
      l(t('Edit'), 'admin/store/orders/'.$result->oid.'/edit')
    );
    if (!isset($order_data['data']['payment_method'])) $order_data['data']['payment_method']='upon';
    
    if (isset($order_data['data']['shipping_method'])) {
      $shipping_method=isset($shipping_methods[$order_data['data']['shipping_method']]) ? $shipping_methods[$order_data['data']['shipping_method']]['title'] : $order_data['data']['shipping_method'];
    }
    else $shipping_method=t('N/A');
    
    $rows[$oid]=array(
      'code' => $result->$code_column,
      'created' => format_date($result->created, 'short'),
      'orderer' => $result->uid ? l($result->orderer, 'user/'.$result->uid, array('attributes' => array('target' => '_blank'))) : $result->orderer,
      'amount' => $result->amount.' '.$result->currency,
      'status' => isset($all_statuses[$result->status]) ? $all_statuses[$result->status] : $result->status,
      'paid' => $result->paid ? t('yes') : t('no'),
      'shipping_method' => $shipping_method,
      'payment_method' => isset($payment_methods[$order_data['data']['payment_method']]) ? $payment_methods[$order_data['data']['payment_method']]['title'] : $order_data['data']['payment_method'],
      'operations' => theme('item_list', array('items' => $operations)),
    );
  }
  $pager=theme('pager');
  
  $form['orders'] = array(
    '#prefix' => $pager,
    '#suffix' => $pager,
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $rows,
    '#empty' => t('No orders available.'),
    );

  return $form;
}

/* --------- Single order edition ---------- */
function graber_cart_edit_order_page($oid) {
  if ($oid) {
    $order=new graber_order($oid);
    return drupal_get_form('graber_edit_order_form', $order);
  }
  else return t('Argument missing.');
}

function graber_cart_edit_cart_page($oid) {
  if ($oid) {
    $order=new graber_order($oid);
    $cart=new graber_cart($order->order_data['cid']);
    if ($order->status > 1) $rendered_cart=$cart->render('form', array('checkout' => FALSE, 'oid' => $oid, 'admin' => FALSE));
    else $rendered_cart=$cart->render('form', array('checkout' => FALSE, 'oid' => $oid, 'admin' => TRUE));
    return $rendered_cart;
  }
}

function graber_edit_order_form($form, &$form_state, $order) {
  module_load_include('inc', 'graber_cart', 'graber_cart.checkout');
  $defaults=array();
  if (isset($form_state['values'])) {
    $defaults=$form_state['values'];
    //Damn street and city fieldsets
    foreach (array('billing', 'shipping') as $pane) {
      foreach (array('street', 'city') as $fieldset) {
        if (isset($form_state['values'][$pane][$fieldset]) && is_array($form_state['values'][$pane][$fieldset])) {
          foreach ($form_state['values'][$pane][$fieldset] as $name => $value) {
            $defaults[$pane][$name]=$value;
          }
        }
      }
    }
  }
  else {
    foreach ($order->order_data['billing_addr'] as $key => $value) $defaults['billing'][$key]=$value;
    foreach ($order->order_data['shipping_addr'] as $key => $value) $defaults['shipping'][$key]=$value;
    foreach ($order->order_data['data'] as $key => $value) {
      if (!isset($defaults[$key])) $defaults[$key]=$value;
    }
    $defaults['mail']=$order->mail;
  }
  $defaults['checkout']=FALSE;
  //Dane zamówienia
  $form['order_data']=array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Order data'),
    '#weight' => -1
  );
  $form['order_data']['created']=array(
    '#markup' => '<div class="created">'.t('Order creation date:').' '.format_date($order->created, 'medium').'</div>'
  );
  $form['order_data']['order_status']=array(
    '#type' => 'select',
    '#options' => graber_order_statuses(),
    '#default_value' => $order->status,
    '#title' => t('Status')
  );
  $form['order_data']['order_paid']=array(
    '#type' => 'checkbox',
    '#default_value' => $order->paid,
    '#title' => t('Paid')
  );
  $form['order_data']['deadline']=array(
    '#type' => 'textfield',
    '#title' => t('Payment deadline'),
    '#default_value' => empty($order->deadline) ? '' : $order->deadline
  );
  gp_element_add_date_popup($form['order_data']['deadline']);
  
  $form['order_data']['amount']=array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => $order->amount,
    '#field_suffix' => $order->order_data['currency'],
    '#description' => t('You can change the exact amount your customer should pay.')
  );
  $form['order_data']['package_info']=array(
    '#type' => 'textarea',
    '#title' => t('Package info'),
    '#description' => t('Shipping code if available and other optional information'),
    '#default_value' => (isset($defaults['package_info'])) ? $defaults['package_info'] : ''
  );

  //Standard form fields
  $form+=graber_cart_order_data_form_fields($defaults);
  
  $invoice_setting=gp_get_settings('graber_cart', 'invoice_option');
  if ($invoice_setting) {
    unset($form['invoice']);
    $form['billing']['invoice_selector']=array(
      '#type' => 'checkbox',
      '#title' => t('Customer requires an invoice'),
      '#default_value' => isset($defaults['invoice_selector']) ? $defaults['invoice_selector'] : 0,
      '#default_value' => isset($defaults['billing']['invoice_selector']) ? $defaults['billing']['invoice_selector'] : NULL,
      '#weight' => -1
    );
  }
    
  $form['save']=array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  $form['delete']=array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#attributes' => array('onclick' => 'return confirm(\''.t('Are you sure?').'\');')
  );
  $form['archivise']=array(
    '#type' => 'submit',
    '#value' => t('Archivise'),
    '#attributes' => array('onclick' => 'return confirm(\''.t('Are you sure?').'\');')
  );

  $form['#order']=$order;

  foreach ($form as $key => $data) {
    if (is_array($data) && isset($data['#type']) && ($data['#type'] == 'fieldset')) {
      $form[$key]['#attributes']['class']=array($key);
    }
  }

  return $form;
}

function graber_edit_order_form_validate($form, &$form_state) {
}

function graber_edit_order_form_submit($form, &$form_state) {
  $order=$form['#order'];
  if ($form_state['values']['op'] == t('Save')) {
    $values=$form_state['values'];
        
    $billing=array();
    if (!empty($values['billing'])) {
      foreach ($values['billing'] as $key => $value) {
        if (is_array($value)) foreach ($value as $key2 => $value2) $billing[$key2]=$value2;
        else $billing[$key]=$value;
      }
    }
    
    if ($values['shipping']['different_shipping'] == 0) {
      $shipping=$billing;
      unset($shipping['invoice_selector']);
    }
    else {
      $shipping=array();
      foreach ($values['shipping'] as $key => $value) {
        if (is_array($value)) {
          foreach ($value as $key2 => $value2) {
            if (!empty($value2)) $shipping[$key2]=$value2;
          }
        }
        elseif (!empty($value)) $shipping[$key]=$value;
      }
    }
    
    $new_fields=array(
      'status' => $values['order_data']['order_status'],
      'paid' => $values['order_data']['order_paid'],
      'mail' => $values['mail'],
      'amount' => $values['order_data']['amount'],
      'deadline' => empty($values['order_data']['deadline']) ? NULL : $values['order_data']['deadline'],
      'order_data' => array(
        'billing_addr' => $billing,
        'shipping_addr' => $shipping,
        'data' => array(
          'remarks' => $values['remarks'],
          'package_info' => $values['order_data']['package_info'],
          'invoice_selector' => isset($values['invoice_selector']) ? $values['invoice_selector'] : FALSE,
          'mail' => $values['mail']
        )
      )
    );
    
    $order->save($new_fields);
    drupal_set_message(t('Order details updated.'));
  }
  elseif ($form_state['values']['op']==t('Delete')) {
    $order->delete();
    drupal_set_message(t('Order deleted.'));
    drupal_goto('admin/store/orders');
  }
  elseif ($form_state['values']['op']==t('Archivise')) {
    $order->save(array('status' => 7));
    drupal_set_message(t('Order Archivised.'));
  }
}