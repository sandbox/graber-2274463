(function ($) {
	Drupal.behaviors.graber_cart = {
		attach: function (context, settings) {
			$("#graber-cart-form input").keypress(function (e) {
				if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
					$('#edit-update').click();
					return false;
				} else {
					return true;
				}
			});
		}
	}	
})(jQuery);

function graber_chcekout_copy_data() {
  jQuery.each(['input', 'textarea'], function(index, value) {
    var values=Array();
    jQuery('#checkout-form-wrapper fieldset.billing '+value).each(function(index) {
      var id=jQuery(this).attr('id');
      id=id.replace("edit-billing", "edit-invoice");
      values[id]=jQuery(this).val();
    });
    
    for (var key in values) {
      if (values.hasOwnProperty(key)) {
        if (jQuery('#'+key).length) {
          jQuery('#'+key).val(values[key]);
        }
      }
    }
  });
}
