<?php

function graber_orders_list_page($uid=FALSE) {
  if (!$account) {
    $uid=$GLOBALS['user']->uid;
  }
  else {
    $uid = $account->uid;
  }
	$table_vars['header']=array(
		array('data' => t('Code')),
		array('data' => t('Created'), 'field' => 'created', 'sort' => 'desc'),
		array('data' => t('Amount'), 'field' => 'amount'),
		array('data' => t('Status'), 'field' => 'status'),
		array('data' => t('Shipping method'), 'class' => array('shipping')),
		array('data' => t('Payment method')),
		array('data' => t('Summary'), 'field' => 'description'),
		array('data' => t('Actions')),
	);
	$query=db_select('graber_orders', 'o');
	$query->fields('o');
	$query->condition('uid', $uid);
	$query=$query->extend('TableSort')->extend('PagerDefault')->limit(20);
	$query->orderByHeader($table_vars['header']);
	$results=$query->execute()->fetchAll();
	$statuses=graber_order_statuses();
	
	$table_vars['rows']=array();
	$destination=trim(request_uri(), '/');
	
	$shipping_methods=module_invoke_all('graber_cart_shipping_info');
	$payment_methods=module_invoke_all('graber_cart_payment_info');
  if (!isset($payment_methods['upon'])) $payment_methods['upon']['title']=t('Upon receiving');
	
	foreach ($results as $result) {
		$actions=array(
			l(t('View'), 'order-summary/'.$result->code, array('query' => array('destination' => $destination)))
		);
		$order_data=&$result->order_data;
    $result->order_data=unserialize($result->order_data);
    
		if (!$result->paid) {
			if (isset($order_data['data']['payment_method'])) {
				$payment_links=get_payment_links($result->oid, $order_data['data']['payment_method']);
				if (!empty($payment_links)) {
					$actions[]=l(t('Pay'), array_shift($payment_links));
				}
			}
		}
		
		if (!empty($order_data['billing_addr'])) {
			$file_prefix='FV';
			$dir='invoices';
		}
		else {
			$file_prefix='R';
			$dir='bills';
		}
    
		if (module_exists('accounting_documents')) {
			$documents=db_select('accounting_sd_data', 'asd')->fields('asd', array('iid', 'path', 'document_type', 'document_no'))->condition('oid', $result->oid)->orderBy('modified', 'ASC')->execute()->fetchAllAssoc('iid', PDO::FETCH_ASSOC);
      if (!empty($documents)) {
        $Ncor=0;
        foreach ($documents as $document) {
          $actions[]=l(
            t('!document !doc_no',
              array(
                '!document' => _accounting_documents_get_doctype($document['document_type']),
                '!doc_no' => $document['document_no']
              )
            ),
            'admin/store/accounting/get-document',
            array(
              'query' => array('path' => $document['path']),
              'attributes' => array('class' => array('file-download pdf'))
            )
          );
        }
      }
		}
    
    if (!isset($order_data['data']['shipping_method'])) $order_data['data']['shipping_method']=FALSE;
    
    if (gp_get_settings('graber_cart', 'random_order_ids')) {
      $code = $result->code;
    }
    else {
      $code = $result->oid;
    }
		$table_vars['rows'][]=array(
			array('data' => $code, 'class' => array('code')),
			array('data' => format_date($result->created, 'medium'), 'class' => array('created')),
			array('data' => $result->amount.' '.$result->currency, 'class' => array('amount')),
			array('data' => $statuses[$result->status], 'class' => array('status')),
			array('data' => isset($shipping_methods[$order_data['data']['shipping_method']]) ? $shipping_methods[$order_data['data']['shipping_method']]['title'] : $order_data['data']['shipping_method'], 'class' => array('shipping')),
			array('data' => isset($payment_methods[$order_data['data']['payment_method']]['title']) ? $payment_methods[$order_data['data']['payment_method']]['title'] : $order_data['data']['payment_method'], 'class' => array('payment')),
			array('data' => $result->description, 'class' => array('description')),
			array('data' => theme('item_list', array('items' => $actions)), 'class' => array('actions'))
		);
	}
  
  $table_vars['sticky']=FALSE;
  $table_vars['empty']=t('You have no orders yet.');
  $info_data=array(
    'uid' => $uid,
    'order_data' => $results
  );
  drupal_alter('graber_user_order_list', $table_vars, $info_data);
	
	return theme('table', $table_vars).theme('pager');
}

function graber_order_summary($type, $data) {
  if ($type !== 'admin') $oid=db_query("SELECT oid FROM {graber_orders} WHERE code = :code", array(':code' => $data))->fetchField();
  else $oid=$data;
  if (!$oid) return drupal_not_found();
  
	$order=new graber_order($oid);
	if ($order->oid) {
		$output='';
		drupal_set_title($order->description);
    if (isset($_GET['destination'])) {
      $output.=l(t('Go back'), $_GET['destination'], array('attributes' => array('class' => array('back-to-orders'))));
    }
		$output.=$order->review(array('status' => TRUE));
		return $output;
	}
	else return drupal_not_found();
}