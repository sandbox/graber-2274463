<?php
function offer_params_field_info() {
	return array(
		'offer_parameters' => array(
			'label' => t('Offer parameters'),
			'description' => t('Allows to sell different product types in one offer'),
			'default_widget' => 'offer_parameters_field_input',
			'default_formatter' => 'offer_parameters_field_view',
		)
	);
}

function offer_params_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
	foreach ($items as $delta => $item) {
		if (!empty($item['pid'])) {
			if (!is_numeric($item['pid'])) {
				$errors[$field['field_name']][$langcode][$delta][] = array(
					'error' => 'field_offer_parameters_invalid',
					'message' => t('Error in offer parameters: product does not exist.'),
				);
			}
		}
	}
}

function offer_params_field_is_empty($item, $field) {
  if ($field['type'] == 'offer_parameters') {
    $defined_params=variable_get('gc_offer_params', array());
    foreach ($defined_params as $param => $data) {
      if (empty($item['param_'.$param])) return TRUE;
    }
  }
}

function offer_params_field_formatter_info() {
	return array(
		'offer_parameters_field_view' => array(
			'label' => t('Table'),
			'field types' => array('offer_parameters'),
		),
    'offer_parameters_list_view' => array(
			'label' => t('List'),
			'field types' => array('offer_parameters'),
		),
    'offer_parameters_views_list_view' => array(
			'label' => t('Views compatibile'),
			'field types' => array('offer_parameters'),
    )
	);
}

function offer_params_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  if ($field['type'] == 'offer_parameters') {
    $param_display=variable_get('offer_params_display_params', array());
    if (empty($param_display)) return array();
    $defined_params=get_offer_params();
    if (empty($defined_params)) return array();
    $types=array(
      'unit_price' => t('Unit price'),
      'quantity' => t('Offer quantity'),
      'price' => t('Price'),
    );

    foreach ($items as $delta => $item) {
      if (empty($item['price'])) {
        $price_field=field_get_items($entity_type, $entity, 'field_product_price');
        $item['price']=$items[$delta]['price']=$price_field[0]['value'];
      }
      
      if (module_exists('promotions')) {
        $new_price=promotions_get_promotion('offer', array('node' => $entity), $item['price']);
        if ($new_price !== FALSE) {
          $items[$delta]['price']=$item['price']=$new_price;
        }
      }
      
      if (module_exists('currency')) {
        currency_update_price($item['price']);
        $items[$delta]['price']=$item['price'];
      }
    }

   
    if ($display['type'] == 'offer_parameters_views_list_view') {
      $element=array();
      $Nitems=0;
      foreach ($items as $delta => $item) {
        $identifier='offer-param-';
        $markup='<label>';
        foreach ($defined_params as $param) {
          $markup.=$param['label'].': '.$item['param_'.$param['machine']];
          $identifier.=$item['param_'.$param['machine']];
        }
        $markup.='</label>';
        foreach ($param_display as $type => $value) {
          if ($value) {
            if ($type === 'quantity') $output=(float) $item[$type];
            elseif ($type === 'price') {
              $output=number_format($item[$type], 2, ',', ' ').' '.$GLOBALS['cart']->currency;
            }
            elseif ($type === 'unit_price') {
              $output=number_format($item['price']/$item['quantity'], 2, ',', ' ').' '.$GLOBALS['cart']->currency;
            }
            else $output=$item[$type];
            $markup.='<div class="property property-'.drupal_html_class($type).'"><span class="label">'.$types[$type].':</span> <span class="value">'.$output.'</span></div>';
          }
        }
        $markup='<div id="'.$identifier.'">'.$markup.'</div>';
        $element[$delta]=array(
          '#markup' => $markup
        );
      }
      return $element;
    }
    if ($display['type'] == 'offer_parameters_list_view') {
      $ids=entity_extract_ids($entity_type, $entity);
      $renderable=array(
        '#items' => array(),
        '#theme' => 'item_list',
        '#attributes' => array('class' => array('offer-parameters-'.$ids[0])),
        '#prefix' => '<div class="offer-parameters field">',
        '#suffix' => '</div>'
      );
      $Nitems=0;
      foreach ($items as $item) {
        $identifier='offer-param-';
        $renderable['#items'][++$Nitems]['data']='<label>';
        foreach ($defined_params as $param) {
          $renderable['#items'][$Nitems]['data'].=$param['label'].': '.$item['param_'.$param['machine']];
          $identifier.=$item['param_'.$param['machine']];
        }
        $renderable['#items'][$Nitems]['id']=$identifier;
        $renderable['#items'][$Nitems]['data'].='</label>';
        foreach ($param_display as $type => $value) {
          if ($value) {
            if ($type === 'quantity') $output=(float) $item[$type];
            elseif ($type === 'unit_price') {
              $output=number_format($item['price']/$item['quantity'], 2, ',', ' ').' '.$GLOBALS['cart']->currency;
            }
            elseif ($type === 'price') {
              $output=number_format($item[$type], 2, ',', ' ').' '.$GLOBALS['cart']->currency;
            }
            else $output=$item[$type];
            $renderable['#items'][$Nitems]['data'].='<div class="property property-'.$type.'"><span class="label">'.$types[$type].':</span><span class="value">'.$output.'</span></div>';
          }
        }
      }
      if (!empty($renderable['#items'])) {
        return $renderable;
      }
    }
    
    if ($display['type'] == 'offer_parameters_field_view') {
      $vars['header']=array();
      $vars['rows']=array();
      $fields=array();
      foreach ($defined_params as $param) {
        $vars['header'][]=$param['label'];
        $fields[]='param_'.$param['machine'];
      }
          
      foreach ($param_display as $type => $value) {
        if ($value) {
          if ($type === 'price') $vars['header'][]=$types[$type].' ('.$GLOBALS['cart']->currency.')';
          else $vars['header'][]=$types[$type];
          
          $fields[]=$type;
        }
      }
      $Nrows=0;
      foreach ($items as $item) {
        $Nrows++;
        $vars['rows'][$Nrows]=array();
        foreach ($fields as $table_field) {
          if ($table_field == 'quantity') {
            if (!empty($item[$table_field])) $value=(float) $item[$table_field];
            else $value=1;
            if (module_exists('warehouse')) {
              $product=product_load($item['pid']);
              if ($product->pid) $value.=' '.$product->unit;
            }
            $vars['rows'][$Nrows][]=$value;
          }
          elseif ($table_field == 'price') {
            $vars['rows'][$Nrows][]=number_format($item[$table_field], 2, ',', ' ');
          }
          elseif ($table_field === 'unit_price') {
            $vars['rows'][$Nrows][]=number_format($item['price']/$item['quantity'], 2, ',', ' ');
          }
          else $vars['rows'][$Nrows][]=$item[$table_field];
        }
      }
      if (!empty($vars['rows'])) {
        return array(
          '#theme' => 'table',
          '#header' => $vars['header'],
          '#rows' => $vars['rows'],
          '#sticky' => FALSE
        );
      }
    }
  
  }
}

function offer_params_field_widget_info() {
	return array(
		'offer_parameters_field_input' => array(
			'label' => t('Default'),
			'field types' => array('offer_parameters'),
		),
	);
}

function offer_params_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

	if ($instance['widget']['type'] == 'offer_parameters_field_input') {
    $params=variable_get('gc_offer_params', array());
    if (!empty($params)) {
    
      $form_state['offer_params_field']['#field_name']=$field['field_name'];

      if (!empty($form_state['values'][$field['field_name']][$langcode][$delta])) {
        $values=$form_state['values'][$field['field_name']][$langcode][$delta];
      }
      elseif (!empty($items[$delta])) $values=$items[$delta];

      if (!empty($values['pid'])) $product=product_load($values['pid']);
      else if (!empty($form_state['warehouse_product'])) $product=$form_state['warehouse_product'];
      
      $element+=array(
        '#type' => 'fieldset',
        '#title' => t('Offer parameters')
      );
      foreach ($params as $param) {
        $fields['param_'.$param['machine']]=array(
          '#type' => 'textfield',
          '#title' => t($param['label']),
          '#autocomplete_path' => 'offer-params/autocomplete/'.$field['field_name'].'/'.$param['machine'],
          '#default_value' => isset($values['param_'.$param['machine']]) ? $values['param_'.$param['machine']] : ''
        );
      }
      
      if (module_exists('warehouse')) {
        $Nwarehouses=db_query("SELECT COUNT(wid) FROM {warehouses}")->fetchColumn();
        if ($Nwarehouses > 1) {
          $options=db_select('warehouses', 'w')->fields('w', array('wid', 'title'))->execute()->fetchAllKeyed(0,1);
          $fields['wid']=array(
            '#type' => 'select',
            '#title' => t('Warehouse'),
            '#options' => $options,
            '#default_value' => isset($values['wid']) ? $values['wid'] : 0
          );
        }
        else {
          $wid=db_query("SELECT wid FROM {warehouses} LIMIT 1")->fetchColumn();
          $fields['wid']=array('#type' => 'value', '#value' => $wid);
        }
        $fields['pid']=array(
          '#type' => 'textfield',
          '#autocomplete_path' => 'admin/store/warehouse/product-autocomplete/name',
          '#title' => t('Product'),
          '#ajax' => array(
            'wrapper' => 'field-param-quantity-wrapper',
            'event' => 'blur',
            'callback' => 'offer_params_widget_form_ajax',
            'method' => 'replace',
            'effect' => 'fade',
          ),
          '#element_validate' => array('warehouse_validate_product_reference', 'offer_params_validate_product_reference'),
          '#default_value' => empty($product) ? '' : $product->identifier_data['string']
        );
      }
      
      if (isset($product)) $unit=$product->unit;
      else $unit='';

      if (module_exists('warehouse') || module_exists('product_stock')) {
        $fields['quantity']=array(
          '#type' => 'textfield',
          '#title' => t('Offer quantity'),
          '#size' => 15,
          '#field_suffix' => ' '.$unit,
          '#default_value' => empty($values['quantity']) ? '' : strtr($values['quantity'], array('.' => ',')),
          '#element_validate' => array('offer_params_validate_decimal_input'),
        );
        if (module_exists('warehouse')) {
          $fields['quantity']['#suffix']='</div>';
          $fields['quantity']['#prefix']='<div id="field-param-quantity-wrapper">';
        }
      }
      
      
      $fields['price']=array(
        '#type' => 'textfield',
        '#title' => t('Price'),
        '#default_value' => empty($values['price']) ? '' : strtr($values['price'], array('.' => ',')),
        '#element_validate' => array('offer_params_validate_decimal_input'),
      );
      
      $element+=$fields;
    }
	}
  
	return $element;
}

function offer_params_widget_form_ajax($form, $form_state) {
	$lang=$form[$form_state['offer_params_field']['#field_name']]['#language'];
	$field=$form[$form_state['offer_params_field']['#field_name']][$lang][0]['quantity'];
	return $field;
}

function offer_params_validate_product_reference($element, &$form_state, $form) {
  if (empty($element['#value'])) {
    form_set_value($element, 0, $form_state);
  }
}

function offer_params_validate_decimal_input($element, &$form_state, $form) {
  if ($element['#value'] === '') {
    $element['#value']=NULL;
    form_set_value($element, NULL, $form_state);
  }
  else {
    $element['#value']=strtr($element['#value'], array(',' => '.'));
    if (!is_numeric($element['#value'])) {
      form_error($element, t('Value must be numeric.'));
    }
    elseif ($element['#value'] < 0) {
      form_error($element, t('Value must be positive.'));
    }
    else form_set_value($element, $element['#value'], $form_state);
  }
}
