<?php
function offer_params_settings_form($form, &$form_state) {
  if (!isset($form_state['params'])) $form_state['params']=variable_get('gc_offer_params', array());
  $form['params']=array(
    '#title' => t('Defined parameters'),
    '#prefix' => '<div id="offer-params-wrapper">',
    '#suffix' => '</div>',
    '#tree' => TRUE
  );
  
  if (!empty($form_state['params'])) {
    $form['params']['#theme']='graber_tabledrag';
    uasort($form_state['params'], 'drupal_sort_weight');
    foreach ($form_state['params'] as $pid => $data) {
      $form['params'][$pid]['label']=array(
        '#title' => t('Label'),
        '#markup' => $data['label'],
      );
      $form['params'][$pid]['machine']=array(
        '#title' => t('Machine name'),
        '#markup' => $data['machine'],
      );
        
      $form['params'][$pid]['delete']=array(
        '#type' => 'submit',
        '#value' => t('remove'),
        '#op' => 'delete_item',
        '#name' => 'delete_'.$pid,
        '#ajax' => array(
          'callback' => 'offer_params_settings_items_ajax',
          'wrapper' => 'offer-params-wrapper'
        ),
        '#title-display' => 'invisible',
      );
      
      $form['params'][$pid]['weight']=array(
        '#type' => 'weight',
        '#delta' => 10,
        '#title-display' => 'invisible',
        '#default_value' => $data['weight']
      );
    }
  }
  else {
    $form['params']['#type']='fieldset';
    $form['params']['info']=array('#markup' => t('No content here.'));
  }
  
  $form['add_param']=array(
    '#type' => 'fieldset',
    '#title' => t('Add parameter')
  );
  $form['add_param']['label']=array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('Please input labels in English and translate them afterwards in translation interface.')
  );
  $form['add_param']['machine']=array(
    '#type' => 'textfield',
    '#title' => t('Machine name'),
    '#description' => t('Please use only lowercase letters, numbers and underscores.')
  );
  $form['add_content']['add']=array(
		'#type' => 'submit',
		'#value' => t('Add'),
		'#op' => 'add_new',
		'#ajax' => array(
			'callback' => 'offer_params_settings_items_ajax',
			'wrapper' => 'offer-params-wrapper'
		),
  );
  
  $form['display_parameters']=array(
    '#type' => 'checkboxes',
    '#title' => t('Display options'),
    '#options' => array(
      'price' => t('Price'),
      'quantity' => t('Quantity')
    ),
    '#default_value' => variable_get('offer_params_display_params', array())
  );
  
  $form['save_data']=array(
		'#type' => 'submit',
		'#value' => t('Save data'),
		'#op' => 'save_data'
  );
  return $form;
}

function offer_params_settings_items_ajax($form, $form_state) {
  return $form['params'];
}

function offer_params_settings_form_validate($form, &$form_state) {
  $op=$form_state['triggering_element']['#op'];
  if ($op == 'add_new') {
    if (empty($form_state['values']['label'])) form_set_error('label', t('Label must not be empty.'));
    if (!preg_match('#[0-9a-z_]+#', $form_state['values']['machine'])) form_set_error('machine', t('Machine name incorrect.'));
  }
}

function offer_params_settings_form_submit($form, &$form_state) {
  $op=$form_state['triggering_element']['#op'];
  if ($op == 'save_data') {
    if (empty($form_state['params'])) variable_del('gc_offer_params');
    else {
      //Fix 0 weights
      $last_weight=-11;
      foreach ($form_state['params'] as $pid => $item) {
        if (empty($item['weight'])) {
          $item['weight']=$last_weight+1;
          $form_state['params'][$pid]['weight']=$item['weight'];
        }
        $last_weight=$item['weight'];
      }
      
      uasort($form_state['params'], 'drupal_sort_weight');
      variable_set('gc_offer_params', $form_state['params']);
    }
    variable_set('offer_params_display_params', $form_state['values']['display_parameters']);
    drupal_set_message(t('Data saved.'));
  }
  elseif ($op == 'add_new') {
    $form_state['rebuild']=TRUE;
    $item=array(
      'label' => $form_state['values']['label'],
      'machine' => $form_state['values']['machine'],
      'weight' => 10
    );
    $form_state['params'][$form_state['values']['machine']]=$item;
  }
  elseif ($op == 'delete_item') {
    $form_state['rebuild']=TRUE;
    $pid=$form_state['triggering_element']['#parents'][1];
    unset($form_state['params'][$pid]);
  }
}
