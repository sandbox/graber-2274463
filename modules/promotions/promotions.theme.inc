<?php

function theme_promotion_info($vars) {
	$items=array();
	foreach ($vars['promotions'] as $type => $promotions) {
		foreach ($promotions as $promotion) {
			if (!empty($promotion->message)) $items[]=$promotion->message;
		}
	}
	if (!empty($items)) {
		return '<h4>'.t('Promotions').':</h4>'.theme('item_list', array('items' => $items));
	}
	return '';
}