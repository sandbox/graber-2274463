<?php
function _promotions_action_type_options($type, $filter='select') {
  if ($filter === 'select') $output=array(0 => t('Select one..'));
  elseif ($filter === 'all') $output=array(0 => t('-All-'));
  else $output=array();

  $data['actions']=array(
    1 => t('reduce articles price by given % value'),
    2 => t('reduce articles price by given currency value'),
    3 => t('overwrite articles price with given value'),
    4 => t('reduce shipping cost by given % value'),
    5 => t('reduce total cost by given currency value (voucher)')
  );
  $data['conditions']=array(
    'code' => t('Promotion code'),
    'uid' => t('Specific user'),
    'rid' => t('User roles'),
    'tid' => t('Taxonomy term'),
    'articles_price' => t('Articles price')
  );
  $output+=$data[$type];
  return $output;
}

function promotions_promotion_form($form, &$form_state, $pid=0) {
  if (!isset($form['#pid'])) $form['#pid']=$pid;
  $form['#tree']=TRUE;
  if (!isset($form_state['promotion']) && $pid) {
    $form_state['promotion']=promotion_load($pid);
  }
  if (!isset($form_state['promotion']) || !is_object($form_state['promotion'])) $form_state['promotion']=new promotion();
  if (empty($form_state['promotion']->conditions)) $form_state['promotion']->conditions=array(array());
  
  $form['primary_data']=array(
    '#type' => 'fieldset',
    '#title' => t('Primary data')
  );
  $form['primary_data']['starts']=array(
    '#type' => 'textfield',
    '#title' => t('Beginning date'),
    '#description' => t('Promotion start date. Leave empty to start now.'),
    '#default_value' => !empty($form_state['promotion']->starts) ? $form_state['promotion']->starts : ''
  );
    
  $form['primary_data']['expires']=array(
    '#type' => 'textfield',
    '#title' => t('Expiry date'),
    '#description' => t('Promotion expiry date. Leave empty for endless promotion.'),
    '#default_value' => !empty($form_state['promotion']->expires) ? $form_state['promotion']->expires : ''
  );
  gp_element_add_date_popup($form['primary_data']['starts']);
  gp_element_add_date_popup($form['primary_data']['expires']);
  
  $form['primary_data']['limit_count']=array(
    '#type' => 'textfield',
    '#title' => t('Usage limit'),
    '#size' => 5,
    '#description' => t('How many times this promotion can be used. Leave empty for unlimited usage.'),
    '#default_value' => isset($form_state['promotion']->limit_count) ? $form_state['promotion']->limit_count : ''
  );
  
  $form['primary_data']['message']=array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#required' => TRUE,
    '#default_value' => isset($form_state['promotion']->message) ? $form_state['promotion']->message : ''
  );
  
  $form['conditions']=array(
    '#type' => 'fieldset',
    '#title' => t('Promotion conditions'),
    '#attributes' => array('id' => 'promo-conditions-wrapper'),
  );
  
  $number=0;
  foreach ($form_state['promotion']->conditions as $index => $data) {
    $number++;
    $form['conditions'][$index]=array(
      '#type' => 'fieldset',
      '#title' => t('Condition').' '.$number,
      '#attributes' => array('id' => 'promo-condition-'.$index.'-wrapper')
    );
    $form['conditions'][$index]['type']=array(
      '#type' => 'select',
      '#title' => t('Condition type'),
      '#required' => TRUE,
      '#op' => 'condition',
      '#ajax' => array(
        'callback' => 'promotions_update_condition_ajax',
        'wrapper' => 'promo-condition-'.$index.'-wrapper'
      ),
      '#options' => _promotions_action_type_options('conditions'),
      '#default_value' => isset($data['type']) ? $data['type'] : ''
    );
    if (isset($data['type'])) {
      switch ($data['type']) {
        //Promotion code
        case 'code':
          $form['conditions'][$index]['code']=array(
            '#required' => TRUE,
            '#type' => 'textfield',
            '#title' => t('Promotion code'),
            '#default_value' => isset($data['code']) ? $data['code'] : ''
          );
          break;
        //Specific user
        case 'uid':
          $form['conditions'][$index]['uid']=array(
            '#required' => TRUE,
            '#type' => 'textfield',
            '#title' => t('User'),
            '#autocomplete_path' => 'gp-autocomplete/user',
            '#default_value' => isset($data['uid']) ? gp_user_autocomplete_reverse($data['uid']) : ''
          );
          break;
        //User roles
        case 'rid':
          $form['conditions'][$index]['rid']=array(
            '#required' => TRUE,
            '#type' => 'select',
            '#multiple' => TRUE,
            '#options' => user_roles(),
            '#title' => t('Roles'),
            '#default_value' => isset($data['rid']) ? $data['rid'] : 0
          );
          break;
        //Taxonomy
        case 'tid':
          if (!isset($vocabularies)) {
            $vocabularies=array(0 => t('select one..'));
            $results=db_query("SELECT vid, name FROM {taxonomy_vocabulary}")->fetchAllKeyed(0, 1);
            if ($results) $vocabularies+=$results;
          }
          $form['conditions'][$index]['vid']=array(
            '#required' => TRUE,
            '#type' => 'select',
            '#options' => $vocabularies,
            '#title' => t('Vocabulary'),
            '#ajax' => array(
              'callback' => 'promotions_update_condition_ajax',
              'wrapper' => 'promo-condition-'.$index.'-wrapper'
            ),
            '#default_value' => isset($data['vid']) ? $data['vid'] : 0
          );
          if (isset($data['vid'])) {
            $form['conditions'][$index]['tid']=array(
              '#required' => TRUE,
              '#type' => 'textfield',
              '#title' => t('Term'),
              '#autocomplete_path' => 'admin/store/promotions/get-data/term-'.$data['vid']
            );
            if (isset($data['tid'])) {
              $term_name=db_query("SELECT name FROM {taxonomy_term_data} WHERE tid = :tid", array(':tid' => $data['tid']))->fetchColumn();
              if ($term_name) $form['conditions'][$index]['tid']['#default_value']=$term_name.' [tid='.$data['tid'].']';
            }
          }
          break;
        //Articles price
        case 'articles_price':
          $form['conditions'][$index]['articles_price']=array(
            '#required' => TRUE,
            '#type' => 'textfield',
            '#title' => t('Price'),
            '#description' => t('Condition is met when total articles price in customer cart is higher than this value.'),
            '#default_value' => isset($data['articles_price']) ? $data['articles_price'] : ''
          );
          break;
      }
    }
    if (sizeof($form_state['promotion']->conditions) > 1) {
      $form['conditions'][$index]['remove']=array(
        '#type' => 'submit',
        '#value' => t('remove condition'),
        '#name' => 'remove_condition_'.$index,
        '#op' => 'remove_condition',
        '#ajax' => array(
          'callback' => 'promotions_update_conditions_ajax',
          'wrapper' => 'promo-conditions-wrapper'
        ),
        '#submit' => array('promotions_promotion_form_submit'),
        '#limit_validation_errors' => array()
      );
    }
  }
  $form['conditions']['extra']['add']=array(
    '#type' => 'submit',
    '#value' => t('add condition'),
    '#name' => 'add_condition',
    '#op' => 'add_condition',
    '#ajax' => array(
      'callback' => 'promotions_update_conditions_ajax',
      'wrapper' => 'promo-conditions-wrapper'
    ),
    '#submit' => array('promotions_promotion_form_submit'),
    '#limit_validation_errors' => array()
  );
    
  $form['action']=array(
    '#type' => 'fieldset',
    '#title' => t('Action'),
  );
    
  $form['action']['action_type']=array(
    '#required' => TRUE,
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => _promotions_action_type_options('actions'),
    '#default_value' => isset($form_state['promotion']->action_type) ? $form_state['promotion']->action_type : NULL
  );
  $form['action']['action_value']=array(
    '#required' => TRUE,
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#default_value' => isset($form_state['promotion']->action_value) ? $form_state['promotion']->action_value : ''
  );
    
  $form['save_data'] = array(
    '#type' => 'submit',
    '#op' => 'save_data',
    '#value' => t('Save data'),
  );
  
  return $form;
}

//Ajax support
function promotions_update_conditions_ajax($form, $form_state) {
  return $form['conditions'];
}

function promotions_update_condition_ajax($form, $form_state) {
  $property=$form_state['triggering_element']['#parents'][2];
  $index=$form_state['triggering_element']['#parents'][1];
  $form_state['promotion']->conditions[$index][$property]=$form_state['triggering_element']['#value'];
  $form=drupal_rebuild_form($form['#form_id'], $form_state, $form);
  return $form['conditions'][$index];
}

//Validation handler
function promotions_promotion_form_validate($form, &$form_state) {
  if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][0] == 'save_data') {
    my_api_validate_integer($form['primary_data']['limit_count'], $form_state, 0);
    foreach ($form_state['values']['conditions'] as $index => $data) {
      if (!is_numeric($index)) continue;
      switch ($data['type']) {
        case 'code':
          if (isset($code_set)) form_set_error(implode('][', $form['conditions'][$index]['type']['#parents']), t('Promotion can have only one code condition.'));
          $code_set=TRUE;
          if (isset($form['conditions'][$index]['code'])) my_api_validate_short_text($form['conditions'][$index]['code'], $form_state);
          break;
        case 'uid':
          my_api_validate_autocomplete_id($form['conditions'][$index]['uid'], $form_state, 'uid', 'users');
          break;
        case 'tid':
          if ($data['vid'] == 0) form_set_error(implode('][', $form['conditions'][$index]['vid']['#parents']), t('Please select a vocabulary.'));
          else my_api_validate_taxonomy_autocomplete($form['conditions'][$index]['tid'], $form_state, $data['vid']);
          break;
        case 'articles_price':
          if (!in_array($form_state['values']['action']['action_type'], array(4, 5))) form_set_error(implode('][', $form['conditions'][$index]['type']['#parents']), t('This condition can only be used to actions performed on summary prices (shipping price reduction, vouchers). Otherwise it may cause an infinite loop.'));
          if (isset($articles_price_set)) form_set_error(implode('][', $form['conditions'][$index]['type']['#parents']), t('Promotion can have only one articles price condition.'));
          $articles_price_set=TRUE;
          my_api_validate_number($form['conditions'][$index]['articles_price'], $form_state, 0);
          break;
      }
    }
  }
}

//Submit handler
function promotions_promotion_form_submit($form, &$form_state) {
  if (isset($form_state['triggering_element'])) {
    //Ajax handling
    if ($form_state['triggering_element']['#op'] == 'add_condition') {
      $form_state['promotion']->conditions[]=array();
      $form_state['rebuild']=TRUE;
    }
    elseif ($form_state['triggering_element']['#op'] == 'remove_condition') {
      $index=$form_state['triggering_element']['#parents'][1];
      unset($form_state['promotion']->conditions[$index]);
      $form_state['rebuild']=TRUE;
    }
    
    //Save promotion
    elseif ($form_state['triggering_element']['#parents'][0] == 'save_data') {
      $promotion=$form_state['promotion'];
      
      foreach ($form_state['values']['primary_data'] as $field => $value) {
        if (!empty($value)) $promotion->{$field}=$value;
        else $promotion->{$field}=NULL;
      }
      if (empty($form_state['values']['primary_data']['limit_count'])) $promotion->has_limit=0;
      else $promotion->has_limit=1;
      
      $promotion->conditions=array();
      unset($form_state['values']['conditions']['extra']);
      
      $conditions=array();
      foreach ($form_state['values']['conditions'] as $condition) {
        unset($condition['remove']);
        $promotion->conditions[]=$condition;
      }
      
      foreach ($form_state['values']['action'] as $field => $value) {
        if (!empty($value)) $promotion->{$field}=$value;
      }
      
      $form_state['redirect']='admin/store/promotions';
      
      if ($form['#pid']) $promotion->pid=$form['#pid'];
      $promotion->save();      
    }
  }
}

//Promotions list
function promotions_list_page() {
  $output='<div>'.l(t('Add a new promotion'), 'admin/store/promotions/add').'</div>';
  
  $filter_form=drupal_get_form('promotion_list_filter_form');
  $output.=drupal_render($filter_form);
  
  $header=array(
    array('data' => t('Promotion'), 'field' => 'p.action_type'),
    array('data' => t('Created'), 'field' => 'p.created', 'sort' => 'desc'),
    array('data' => t('Starts'), 'field' => 'p.starts'),
    array('data' => t('Expires'), 'field' => 'p.expires'),
    array('data' => t('Limit'), 'field' => 'p.limit'),
    array('data' => t('Conditions')),
    array('data' => t('Message')),
    array('data' => t('Actions'))
  );
  $query=db_select('promotions', 'p')->fields('p');
  
  //Apply filters
  _propmotions_list_filters($query);
  
  //Remove system promotions from list
  $query->condition('p.creator', 0, '<>');
  
  //Finish and execute query
  $Npage=20;
  $query=$query->extend('TableSort')->extend('PagerDefault')->limit($Npage);
  $query->orderByHeader($header);
  $promotions=$query->execute()->fetchAllAssoc('pid', PDO::FETCH_ASSOC);

  $rows=array();
  if (is_array($promotions) && !empty($promotions)) {
    $types=promotion_get_promotion_types();
    foreach ($promotions as $pid => $data) {
      $rows[$pid]['type']=strtr($types[$data['action_type']], array('%value' => $data['action_value']));
      $rows[$pid]['created']=format_date($data['created'], 'short');
      $rows[$pid]['starts']=empty($data['starts']) ? t('N/A') : format_date($data['starts'], 'short');
      $rows[$pid]['expires']=empty($data['expires']) ? t('N/A') : format_date($data['expires'], 'short');
      $rows[$pid]['limit']=($data['has_limit']) ? $data['limit_count'] : t('N/A');
      
      //Conditions info
      $conditions=unserialize($data['conditions']);
      $cond_items=array();
      foreach ($conditions as $condition) {
        switch ($condition['type']) {
          case 'code':
            $cond_items[]=t('promotion code: !code', array('!code' => $condition['code']));
            break;
          case 'uid':
            $cond_items[]=t('user: !user', array('!user' => l(gp_user_autocomplete_reverse($condition['uid']), 'user/'.$condition['uid'])));
            break;
          case 'rid':
            $roles=db_select('role', 'r')->fields('r', array('name'))->condition('rid', $condition['rid'], 'IN')->execute()->fetchAllKeyed(0, 0);
            $cond_items[]=t('user roles: !roles', array('!roles' => implode(', ', $roles)));
            break;
          case 'tid':
            $cond_items[]=t('taxonomy term: !term', array('!term' => db_query("SELECT name FROM {taxonomy_term_data} WHERE tid = :tid", array(':tid' => $condition['tid']))->fetchColumn()));
            break;
          case 'articles_price':
            $cond_items[]=t('min articles price: !price', array('!price' => $condition['articles_price']));
            break;
        }
      }
      $rows[$pid]['conditions']=theme('item_list', array('items' => $cond_items));

      $rows[$pid]['message']=$data['message'];
      
      $actions=array(
        l(t('Edit'), 'admin/store/promotions/'.$pid.'/edit'),
        l(t('Delete'), 'admin/store/promotions/'.$pid.'/delete')
      );
      $rows[$pid]['actions']=theme('item_list', array('items' => $actions));
    }
  }
  $output.=theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('You have no promotions at the moment.')
  ));
  $output.=theme('pager');
  return $output;
}

function promotion_list_filter_form($form, &$form_state) {
  $form['filters']=array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Filters'),
    '#attributes' => array('class' => array('graber-filter-wrapper'))
  );
  
  $form['filters']['action']=array(
    '#type' => 'select',
    '#title' => t('Action type'),
    '#options' => _promotions_action_type_options('actions', 'all')
  );
  
  $form['filters']['conditions']=array(
    '#type' => 'checkboxes',
    '#title' => t('Condition types'),
    '#options' => _promotions_action_type_options('conditions', 'none'),
  );
  
  foreach ($form['filters'] as $filter => $element) {
    if (substr($filter, 0, 1) === '#') continue;
    if (isset($_SESSION['promotion_admin_filters'][$filter])) {
      $form['filters'][$filter]['#default_value']=$_SESSION['promotion_admin_filters'][$filter];
    }
  }
  
  $form['filters']['actions']=array('#type' => 'actions');
  if (!empty($_SESSION['promotion_admin_filters'])) {
    $form['filters']['actions']['undo']=array(
      '#type' => 'submit',
      '#op' => 'clear',
      '#value' => t('Clear')
    );
  }

  $form['filters']['actions']['filter']=array(
    '#type' => 'submit',
    '#op' => 'filter',
    '#value' => t('Filter')
  );
  
  return $form;
}
function promotion_list_filter_form_submit($form, &$form_state) {
  $op=$form_state['triggering_element']['#op'];
  $filters=$form_state['values']['filters'];
  unset($filters['actions']);
  if ($op === 'filter') {
    //Action filter
    if (!empty($filters['action'])) $_SESSION['promotion_admin_filters']['action']=$filters['action'];
    else unset($_SESSION['promotion_admin_filters']['action']);
    
    //Conditions filters
    $selected=FALSE;
    foreach ($filters['conditions'] as $selection) {
      if (!empty($selection)) $selected=TRUE;
    }
    if ($selected) $_SESSION['promotion_admin_filters']['conditions']=$filters['conditions'];
    else unset($_SESSION['promotion_admin_filters']['conditions']);
  }
  elseif ($op === 'clear') unset($_SESSION['promotion_admin_filters']);
}
function _propmotions_list_filters(SelectQueryInterface $query) {
  if (!empty($_SESSION['promotion_admin_filters'])) {
    if (!empty($_SESSION['promotion_admin_filters']['action'])) $query->condition('p.action_type', $_SESSION['promotion_admin_filters']['action']);
    if (!empty($_SESSION['promotion_admin_filters']['conditions'])) {
      foreach ($_SESSION['promotion_admin_filters']['conditions'] as $cond_type) {
        if ($cond_type) {
          $query->join('promotion_condition_'.$cond_type, 'c'.$cond_type, 'c'.$cond_type.'.pid = p.pid');
          $query->isNotNull('c'.$cond_type.'.'.$cond_type);
        }
      }
    }
  }
}


function promotions_delete_form($form, &$form_state, $pid=0) {
  if ($pid) {
    $promotion=db_query("SELECT message FROM {promotions} WHERE pid = :pid", array(':pid' => $pid))->fetchColumn();
    if ($promotion) {
      $form['delete']=array(
        '#type' => 'submit',
        '#value' => t('Delete promotion'),
        '#prefix' => '<div>'.t('Are you sure you want to delete "!promotion"?', array('!promotion' => $promotion)).'</div>'
      );
      $form['#pid']=$pid;
    }
  }
  
  return $form;
}

function promotions_delete_form_submit($form, &$form_state) {
  $form_state['redirect']='admin/store/promotions';
  promotion_delete($form['#pid']);
  drupal_set_message(t('Promotion deleted.'));
}

//Finally some settings
function promotions_settings_form($form, &$form_state) {
  $settings=promotion_get_settings();
  $form['settings']['#tree']=TRUE;
  $form['settings']['show_cart']=array(
    '#type' => 'checkbox',
    '#title' => t('Show promotion code widget under cart form'),
    '#default_value' => $settings['show_cart']
  );
  $form['settings']['code_expanded']=array(
    '#type' => 'checkbox',
    '#title' => t('Always expand cart form promotion code widget'),
    '#default_value' => $settings['code_expanded']
  );
  $form['settings']['code_info_page']=array(
    '#type' => 'textfield',
    '#title' => t('Code info page address'),
    '#default_value' => $settings['code_info_page']
  );
  $form['save_data']=array(
    '#type' => 'submit',
    '#value' => t('Save data')
  );
  return $form;
}

function promotions_settings_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['settings']['code_info_page'])) {
    if (!menu_get_item($form_state['values']['settings']['code_info_page'])) form_set_error('settings][code_info_page', t('Specified address does not exist.'));
  }
}

function promotions_settings_form_submit($form, &$form_state) {
  $settings=array();
  foreach ($form_state['values']['settings'] as $name => $setting) {
    if (empty($setting)) $settings[$name]=FALSE;
    else $settings[$name]=$setting;
  }
  variable_set('promotion_settings', $settings);
}