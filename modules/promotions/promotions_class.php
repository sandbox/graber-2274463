<?php

class promotion {
	function save() {
		module_invoke_all('promotion_save', $this);
    
    //Save promotion data
    if (empty($this->limit_count) && !isset($this->has_limit)) $this->has_limit=0;
		$fields=(array) $this;
		if (!empty($fields['conditions'])) $fields['conditions']=serialize($fields['conditions']);
		if (!empty($this->pid)) {
			db_update('promotions')->fields($fields)->condition('pid', $this->pid)->execute();
			$op='update';
		}
		else {
      $fields['created']=REQUEST_TIME;
      if (!isset($fields['creator'])) $fields['creator']=$GLOBALS['user']->uid;
			unset($fields['pid']); //Just in case..
			$this->pid=db_insert('promotions')->fields($fields)->execute();
			$op='insert';
		}
    
    //Input conditions to their tables
    if (!empty($this->conditions)) {
      $conditions=array();
      foreach ($this->conditions as $condition) $conditions[$condition['type']][]=$condition;

      $condition_info=promotion_get_conditions();
      foreach ($condition_info as $name => $data) {
        db_delete('promotion_condition_'.$name)->condition('pid', $this->pid)->execute();
        if (isset($conditions[$name])) {
          $values=array();
          foreach ($conditions[$name] as $condition) {
            if (is_array($condition[$name])) $values+=$condition[$name];
            else $values[]=$condition[$name];
          }
          if ($name == 'tid') _promotion_include_term_children($values);
          foreach ($values as $value) db_insert('promotion_condition_'.$name)->fields(array(
            'pid' => $this->pid,
            $name => $value
          ))->execute();
        }
        else db_insert('promotion_condition_'.$name)->fields(array(
          'pid' => $this->pid
        ))->execute();
      }
    }
	}
	
	function load($data) {
		if (is_numeric($data)) $promotion=db_query("SELECT * FROM {promotions} WHERE pid = :pid", array(':pid' => $data))->fetchObject();
		else $promotion=db_query("SELECT * FROM {promotions} WHERE code = :code", array(':code' => $data))->fetchObject();
		if (!isset($promotion) || !$promotion) return FALSE;
		if (!empty($promotion->conditions)) {
			$promotion->conditions=unserialize($promotion->conditions);
		}
		foreach ($promotion as $prop => $data) $this->{$prop}=$data;
		return $this;
	}
  
  function delete($pid) {
    $condition_info=promotion_get_conditions();
    foreach ($condition_info as $name => $data) {
      db_delete('promotion_condition_'.$name)->condition('pid', $pid)->execute();
    }
    db_delete('promotions')->condition('pid', $pid)->execute();
  }
}

class promotions {
  var $promotions=array();
  
  function get_current() {
    if (!empty($this->promotions)) {
      $output=array();
      foreach ($this->promotions as $p_type => $type_data) {
        foreach ($type_data as $promotion) {
          if (!$promotion) continue;
          if (isset($output[$p_type][$promotion->pid])) continue;
          else {
            unset($promotion->new_price);
            $output[$p_type][$promotion->pid]=$promotion;
          }
        }
      }
      return $output;
    }
    else return FALSE;
  }
  
  function get($type='offer', $data=array(), $reload=FALSE) {
    if (isset($data['node'])) $identifier=$data['node']->nid;
    else {
      $data['node']=(object) array('nid' => 0);
      if (isset($data['shipping_method'])) $identifier=$data['shipping_method']['method'];
      else $identifier=0;
    }
    if (!isset($data['user'])) $data['user']=$GLOBALS['user'];
    if (!isset($data['cart'])) $data['cart']=$GLOBALS['cart'];
    
    //Try to get promotion data from cache
    //TODO: implement
    if (empty($this->promotions[$type][$identifier]) || $reload) {
      $this->promotions[$type][$identifier]=FALSE;
      
      //Prepare data for query
      $articles_price=isset($data['cart']->data['articles_price']) ? $data['cart']->data['articles_price'] : NULL;
      if (module_exists('currency')) currency_update_price($articles_price, $data['cart']->currency, TRUE);
      
      $query_data=array(
        'time' => (isset($data['time'])) ? $data['time'] : NULL,
        'rid' => array_keys($data['user']->roles),
        'uid' => $data['user']->uid,
        'articles_price' => $articles_price,
        'code' => isset($data['cart']->data['promotion_code']) ? $data['cart']->data['promotion_code'] : NULL,
      );
      
      //Type-specific conditions
      if ($type == 'offer') {
        $query_data['tid']=db_query('SELECT tid FROM {taxonomy_index} WHERE nid = :nid', array(':nid' => $data['node']->nid))->fetchAllKeyed(0, 0);
      }
      elseif ($type == 'shipping') {
        $query_data['action_type']=4;
      }
      elseif ($type == 'total') {
        $query_data['action_type']=5;
      }
    
      $results=self::get_query_results($query_data);
      $data['type']=$type;
      drupal_alter('promotion_get', $results, $data);
      if (!empty($results)) {
        $check_price=$base_price=1;
        
        //Select best option for the customer
        foreach ($results as $promotion) {
          $new_price=$this->apply($base_price, $promotion, FALSE);
          if ($new_price !== FALSE && ($new_price < $check_price)) {
            $this->promotions[$type][$identifier]=$promotion;
            $check_price=$new_price;
          }
        }
      }
    }
    $promotion=$this->promotions[$type][$identifier];
    if (empty($promotion)) return FALSE;
    else return $promotion;
  }
  
  function apply($base_price, $promotion, $round=TRUE) {
    $promotion_price=FALSE;
    
    switch ($promotion->action_type) {
      case 1:
        $promotion_price=$base_price*(1-$promotion->action_value/100);
        break;
      case 2:
        $action_value=$promotion->action_value;
        $promotion_price=$base_price-$action_value;
        break;
      case 3:
        $action_value=$promotion->action_value;
        $promotion_price=$action_value;
        break;
      case 4:
        $promotion_price=$base_price*(1-$promotion->action_value/100);
        break;
      case 5:
        $action_value=$promotion->action_value;
        if (module_exists('currency')) currency_update_price($action_value);
        $promotion_price=$base_price-$action_value;
        break;
    }
    if ($promotion_price !== FALSE) {
      if ($promotion_price < $base_price) {
        if ($round) {
          $new_price=round($promotion_price, 2);
          if ($new_price < 0) $new_price=0;
        }
        else $new_price=$promotion_price;
      }
    }
    if (isset($new_price)) return $new_price;
    return FALSE;
  }
  
  function get_new_price($type='offer', $data=array(), $base_price=1, $reload=FALSE) {
    $promotion=$this->get($type, $data, $reload);
    if ($promotion) return $this->apply($base_price, $promotion);
    else return FALSE;
  }
  
  private function get_query_results($data) {
    $query=db_select('promotions', 'p');
    //Primary conditions
    if (empty($data['time'])) $data['time']=REQUEST_TIME;
    $query->condition(db_or()->condition('p.starts', $data['time'], '<')->isNull('p.starts'));
    $query->condition(db_or()->condition('p.expires', $data['time'], '>=')->isNull('p.expires'));
    $query->condition(db_or()->condition('p.has_limit', 0)->condition('p.limit_count', 0, '>'));
    
    //Promotion-specific conditions
    $conditions=promotion_get_conditions();
    foreach ($conditions as $name => $c_data) $query->join('promotion_condition_'.$name, 'c'.$name, 'c'.$name.'.pid = p.pid');
    
    foreach ($conditions as $name => $c_data) {
      if (!empty($data[$name])) {
        
        $or=db_or()->isNull('c'.$name.'.'.$name);
        if (is_array($data[$name])) $or->condition('c'.$name.'.'.$name, $data[$name], 'IN');
        else {
          if (isset($c_data['operator'])) $operator=$c_data['operator'];
          else $operator='=';
          $or->condition('c'.$name.'.'.$name, $data[$name], $operator);
        }
        $query->condition($or);
        $query->addField('c'.$name, $name);
      }
      else $query->isNull('c'.$name.'.'.$name);
    }
    
    //Action type condition
    if (isset($data['action_type'])) $query->condition('p.action_type', $data['action_type']);
    else {
      $query->condition('p.action_type', 4, '<>');
      $query->condition('p.action_type', 5, '<>');
    }
    
    $query->fields('p', array('pid', 'action_type', 'action_value', 'message', 'has_limit', 'limit_count'));
    
    $results=$query->execute()->fetchAll();
    return $results;
  }  
}
