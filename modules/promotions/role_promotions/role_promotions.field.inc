<?php

function role_promotions_field_info() {
	return array(
		'field_role_promotion' => array(
			'label' => t('Per-role promotion'),
			'description' => t('Allows to define per-role discounts.'),
			'default_widget' => 'role_promotion_input',
			'default_formatter' => 'role_promotion_view',
		),
	);
}

function role_promotions_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
	$rids=array();
	foreach ($items as $delta => $item) {
		if (!empty($item['discount'])) {
			if ($item['rid'] == 0) {
				$errors[$field['field_name']][$langcode][$delta][] = array(
					'error' => 'field_role_promotion_invalid',
					'message' => t('Error in role promotion: select role.'),
				);
			}
			if (in_array($item['rid'], $rids)) {
				$errors[$field['field_name']][$langcode][$delta][] = array(
					'error' => 'field_role_promotion_invalid',
					'message' => t('Error in role promotion: Multiple promotions for the same user role.'),
				);
			}
			$rids[]=$item['rid'];
			
			$item['discount']=strtr($item['discount'], array(',' => '.'));
			if (!is_numeric($item['discount'])) {
				$errors[$field['field_name']][$langcode][$delta][] = array(
					'error' => 'field_role_promotion_invalid',
					'message' => t('Error in role promotion: discount must be numeric.'),
				);
			}
			if ($item['unit'] == '%' && ($item['discount'] < 0.01 || $item['discount'] > 99.99)) {
				$errors[$field['field_name']][$langcode][$delta][] = array(
					'error' => 'field_role_promotion_invalid',
					'message' => t('Error in role promotion: discount must be between 0.01 and 99.9.'),
				);
			}
			if ($item['unit'] == 'unit' &&($item['discount'] < 0)) {
				$errors[$field['field_name']][$langcode][$delta][] = array(
					'error' => 'field_role_promotion_invalid',
					'message' => t('Error in role promotion: discount must be positive.'),
				);
			}
		}
	}
}

function role_promotions_field_is_empty($item, $field) {
	if (empty($item['rid']) || empty($item['discount'])) return true;
	else return false;
}

function role_promotions_field_formatter_info() {
	return array(
		'role_promotion_view' => array(
			'label' => t('Default'),
			'field types' => array('field_role_promotion'),
		)
	);
}

function role_promotions_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
	$element = array();

	switch ($display['type']) {
		case 'role_promotion_view':
			global $user;
			foreach ($items as $delta => $item) {
				foreach ($user->roles as $rid => $name) {
					if ($rid == $item['rid']) $discount=$item['discount'];
				}
			}
			if (!empty($discount)) $element[0]['#markup']=t('Your discount for this product: !discount%', array('!discount' => $discount));
			break;
	}
	return $element;
}

function role_promotions_field_widget_info() {
	return array(
		'promotions_roles_widget' => array(
			'label' => t('Default'),
			'field types' => array('field_role_promotion'),
		)
	);
}

function role_promotions_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

	if ($instance['widget']['type'] == 'promotions_roles_widget') {
		$promotion_roles=variable_get('promotion_roles', array());
		if (!empty($promotion_roles)) {
			$roles=user_roles();
			$role_options=array();
			foreach ($promotion_roles as $rid => $discount) $role_options[$rid]=$roles[$rid];
			
			$fields['rid']=array(
				'#type' => 'select',
				'#title' => t('User role'),
				'#options' => $role_options,
				'#default_value' => empty($items[$delta]['rid']) ? 0 : $items[$delta]['rid']
			);
			$fields['discount']=array(
				'#type' => 'textfield',
				'#title' => t('Discount'),
				'#size' => 15,
				'#default_value' => empty($items[$delta]['discount']) ? '' : $items[$delta]['discount']
			);
			$fields['unit']=array(
				'#type' => 'select',
				'#title' => t('Unit'),
				'#options' => array(
					'%' => '%',
					'unit' => variable_get('graber_cart_currency', 'PLN')
				),
				'#default_value' => empty($items[$delta]['unit']) ? '' : $items[$delta]['unit'],
			);
			$element+=$fields;
		}
	}

	return $element;
}