<?php
function theme_rp_table($vars) {
	$element=$vars['rp_table'];
	$header=array(t('No', array(), array('context' => 'number')));
	$rows=array();
	$Nrows=0;
	foreach ($element as $rid => $data) {
		if (!is_numeric($rid)) continue;
		$Nrows++;
		$rows[$Nrows][]=$Nrows;
		foreach ($data as $prop => $cell) {
			if (substr($prop, 0, 1) != '#') {
				if (isset($cell['#title'])) {
					if ($Nrows == 1 && !in_array($cell['#title'], $header)) $header[]=$cell['#title'];
					unset($cell['#title']);
				}
				$rows[$Nrows][]=render($cell);
			}
		}
	}
	return theme('table', array('header' => $header, 'rows' => $rows, 'sticky' => FALSE)).render($element['save_settings']);
}

function theme_promo_info_table($vars) {
	$header=array(t('Role'), t('Discount'));
	if (!empty($vars['node']->field_product_price[LANGUAGE_NONE])) {
		$base_price=$vars['node']->field_product_price[LANGUAGE_NONE][0]['value'];
		$header[]=t('Price');
		$currency=variable_get('graber_cart_currency', 'PLN');
		$rows=array(array(t('base price'), '-', $base_price.' '.$currency));
	}
	else $rows=array();
	$Nrows=0;
	foreach ($vars['promotion_roles'] as $rid => $discount) {
		$Nrows++;
		$rows[$Nrows]=array(
			$vars['roles'][$rid],
			$discount.'%'
		);
		if (isset($base_price)) $rows[$Nrows][]=number_format($base_price*(1-$discount/100), 2, '.', ' ').' '.$currency;
	}
	return '<h4>'.t('Default role discounts:').'</h4>'.theme('table', array('header' => $header, 'rows' => $rows, 'sticky' => FALSE));
}