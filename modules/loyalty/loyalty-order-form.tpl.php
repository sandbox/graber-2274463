<?php 
  $teaser=render($form['prize_teaser']);
?>
<?php if ($form['#prize_type'] === 'item'): ?>
  <div class="row">
    <div class="col-md-8">
      <?php print drupal_render_children($form); ?>
    </div>
    <div class="col-md-4">
      <?php print $teaser; ?>
    </div>
  </div>
<?php else: ?>
  <?php print $teaser; ?>
  <?php print drupal_render_children($form); ?>
<?php endif; ?>