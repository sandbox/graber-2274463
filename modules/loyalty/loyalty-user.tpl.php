<div class="user-points">
  <p><?php print $points_info; ?></p>
</div>
<div class="filters row">
  <?php foreach ($filters as $filter): ?>
    <div class="col-sm-3">
      <?php print render($filter); ?>
    </div>
  <?php endforeach; ?>
</div>
<?php print $prizes; ?>