<div class="row">
  <?php foreach ($prizes as $prize): ?>
    <div class="col-md-4">
      <?php print render($prize->node); ?>
    </div>
  <?php endforeach; ?>
</div>
<?php print $pager; ?>