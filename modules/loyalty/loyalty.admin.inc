<?php
function loyalty_settings_form($form, &$form_state) {
  $settings=gp_get_settings('loyalty');
  $form['settings']['#tree']=TRUE;
  $form['settings']['point_cost']=array(
    '#type' => 'textfield',
    '#title' => t('Point cost'),
    '#description' => t('User has to spend this much to get 1 loyalty point.'),
    '#default_value' => isset($settings['point_cost']) ? $settings['point_cost'] : ''
  );
  $form['settings']['point_expiry']=array(
    '#type' => 'radios',
    '#title' => t('Points expiry'),
    '#options' => array(
      'never' => t('Never'),
      'month' => t('after 30 days'),
      'year' => t('after 365 days')
    ),
    '#description' => t('Do points expire after a given time period?'),
    '#default_value' => isset($settings['point_expiry']) ? $settings['point_expiry'] : 'never'
  );
  
  $node_types=node_type_get_types();
  $type_options[0]=t('Any');
  foreach ($node_types as $type => $data) $type_options[$type]=$data->name;
  $form['settings']['prize_node_type']=array(
    '#type' => 'select',
    '#options' => $type_options,
    '#title' => t('Prize node type'),
    '#description' => t('Node type used for prize presentation purposes.'),
    '#default_value' => isset($settings['prize_node_type']) ? $settings['prize_node_type'] : 0
  );
  
  $form['save_data']=array(
    '#type' => 'submit',
    '#value' => t('Save data')
  );
  
  return $form;
}

function loyalty_settings_form_submit($form, &$form_state) {
  gp_save_settings('loyalty', $form_state['values']['settings']);
  drupal_set_message(t('Settings saved.'));
}