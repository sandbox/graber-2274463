<div class="row">
  <div class="col-md-4">
    <?php print render($teaser); ?>
  </div>
  <div class="col-md-8">
    <h3><?php print $details_title; ?></h3>
    <?php print $details; ?>
  </div>
</div>