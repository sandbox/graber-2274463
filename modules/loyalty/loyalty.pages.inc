<?php
function _loyalty_prize_types($empty_option=TRUE) {
  if ($empty_option) $types=array(0 => t('Select one..'));
  else $types=array();
  $types+=array(
    'promotion' => t('Create a user-bound promotion'),
    'item' => t('Material prize')
  );
  return $types;
}
function loyalty_admin_selected_prizes_page() {
  $vars=array(
    'rows' => array(),
    'sticky' => TRUE,
    'empty' => t('Nobody selected any prizes yet.')
  );
  
  $vars['header']=array(
    array('data' => t('User'), 'field' => 'u.name'),
    array('data' => t('Prize type'), 'field' => 'p.type'),
    array('data' => t('Prize'), 'field' => 'p.title'),
    array('data' => t('Selected on'), 'field' => 's.selected', 'sort' => 'desc'),
    array('data' => t('Actions'))
  );

  $query=db_select('loyalty_selected', 's');
  $query->join('loyalty_prizes', 'p', 'p.pid = s.pid');
  $query->join('users', 'u', 'u.uid = s.uid');
  $query
    ->fields('s')
    ->fields('p', array('nid', 'type', 'title'))
    ->fields('u', array('name'));

  $Npage=20;
  $query=$query->extend('TableSort')->extend('PagerDefault')->limit($Npage);
  $query->orderByHeader($vars['header']);
  $results=$query->execute()->fetchAll();
  
  $types=_loyalty_prize_types(FALSE);

  foreach ($results as $result) {
    $actions=array();
    $actions[]=l(t('details'), 'user/'.$result->uid.'/loyalty/selected/'.$result->sid, array('attributes' => array('target' => '_blank')));
    //$actions[]=l(t('delete'), 'admin/store/loyalty/selected/'.$result->sid.'/delete', array('attributes' => array('query' => drupal_get_destination())));
        
    $vars['rows'][]=array(
      l($result->name, 'user/'.$result->uid, array('attributes' => array('target' => '_blank'))),
      $types[$result->type],
      empty($result->nid) ? $result->title : l($result->title, 'node/'.$result->nid, array('attributes' => array('target' => '_blank'))),
      format_date($result->selected, 'short'),
      theme('item_list', array('items' => $actions))
    );
  }
  $prizes_table=theme('table', $vars);
  $pager=theme('pager');
  return $prizes_table.$pager;
}

//Prizes table
function loyalty_admin_prizes_page() {
  $vars=array(
    'rows' => array(),
    'sticky' => TRUE,
    'empty' => t('You don\'t have any prizes defined yet. Add a prize: !add_link', array('!add_link' => l(t('Add'), 'admin/store/loyalty/prize/add')))
  );
  
  $vars['header']=array(
    array('data' => t('Prize'), 'field' => 'p.title'),
    array('data' => t('Type'), 'field' => 'p.type'),
    array('data' => t('Created'), 'field' => 'p.created', 'sort' => 'desc'),
    array('data' => t('Point cost'), 'field' => 'p.point_cost'),
    array('data' => t('Limit'), 'field' => 'p.p_limit'),
    array('data' => t('Actions'))
  );
  
  $Npage=20;
  $query=db_select('loyalty_prizes', 'p');
  $query->fields('p');
  $query=$query->extend('TableSort')->extend('PagerDefault')->limit($Npage);
  $query->orderByHeader($vars['header']);
  $results=$query->execute()->fetchAll();
  
  $types=_loyalty_prize_types(FALSE);
  
  foreach ($results as $result) {
    $actions=array();
    if (!empty($result->nid)) $actions[]=l(t('view'), 'node/'.$result->nid, array('attributes' => array('target' => '_blank')));
    $actions[]=l(t('edit'), 'admin/store/loyalty/prize/'.$result->pid.'/edit', array('query' => drupal_get_destination()));
    $actions[]=l(t('delete'), 'admin/store/loyalty/prize/'.$result->pid.'/delete', array('query' => drupal_get_destination()));
    
    $vars['rows'][]=array(
      empty($result->nid) ? $result->title : l($result->title, 'node/'.$result->nid, array('attributes' => array('target' => '_blank'))),
      $types[$result->type],
      format_date($result->created, 'short'),
      $result->point_cost,
      $result->p_limit,
      theme('item_list', array('items' => $actions))
    );
  }
  $prizes_table=theme('table', $vars);
  $pager=theme('pager');
  return $prizes_table.$pager;
}

//User points table
function loyalty_admin_user_points_page() {
  $vars=array(
    'rows' => array(),
    'sticky' => TRUE,
    'empty' => t('Users don\'t have any points yet.')
  );
  
  $vars['header']=array(
    array('data' => t('User'), 'field' => 'u.name'),
    array('data' => t('E-mail'), 'field' => 'u.mail'),
    array('data' => t('Points'), 'field' => 'lp.points', 'sort' => 'desc'),
    array('data' => t('Actions'))
  );
  
  $Npage=20;
  $query=db_select('users', 'u');
  $query->join('loyalty_points', 'lp', 'lp.uid = u.uid');
  $query->fields('lp', array('points'));
  $query->fields('u', array('uid', 'name', 'mail'));
  $query=$query->extend('TableSort')->extend('PagerDefault')->limit($Npage);
  $query->orderByHeader($vars['header']);
  $results=$query->execute()->fetchAll();
  
  if (!empty($results)) {
    $dest=drupal_get_destination();
    foreach ($results as $result) {
      $vars['rows'][]=array(
        l($result->name, 'user/'.$result->uid, array('attributes' => array('target' => '_blank'))),
        $result->mail,
        empty($result->points) ? 0 : $result->points,
        l(t('Edit'), 'admin/store/loyalty/user/'.$result->uid.'/edit', array('query' => $dest))
      );
    }
  }
  
  $user_table=theme('table', $vars);
  $pager=theme('pager');
  return $user_table.$pager;
}

function loyalty_edit_user_points($form, &$form_state, $uid=0) {
  if (!$uid) {
    $form['user']=array(
      '#type' => 'textfield',
      '#autocomplete_path' => 'gp-autocomplete/user',
      '#title' => t('User')
    );
  }
  else {
    $form_state['uid']=$uid;
    $current_points=db_select('loyalty_points', 'lp')->fields('lp', array('points'))->condition('uid', $uid)->execute()->fetchColumn();
  }
  $form['points']=array(
    '#type' => 'textfield',
    '#title' => t('User points'),
    '#required' => TRUE,
    '#default_value' => empty($current_points) ? 0 : $current_points
  );
  $form['save_data']=array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  
  return $form;
}

function loyalty_edit_user_points_validate($form, &$form_state) {
  if (!empty($form_state['values']['user'])) {
    my_api_validate_autocomplete_id($form['user'], $form_state, 'uid', 'users');
  }
  my_api_validate_integer($form['points'], $form_state, 0);
}

function loyalty_edit_user_points_submit($form, &$form_state) {
  db_merge('loyalty_points')->key(array('uid' => $form_state['uid']))->fields(array(
    'uid' => $form_state['uid'],
    'points' => $form_state['values']['points']
  ))->execute();
  drupal_set_message(t('User loyalty points updated.'));
}

//Prize edition
function loyalty_edit_prize_form($form, &$form_state, $pid=0) {
  if (isset($form_state['values']['prize'])) {
    $prize=$form_state['values']['prize'];
  }
  elseif ($pid) {
    $prize=db_select('loyalty_prizes', 'p')->fields('p')->condition('pid', $pid)->execute()->fetchAssoc();
    $prize['data']=unserialize($prize['data']);
  }
  if ($pid) $form_state['pid']=$pid;
    
  $form['prize']=array('#tree' => TRUE);
  if ($type=gp_get_settings('loyalty', 'prize_node_type')) $autocomplete_path='gp-autocomplete/node/'.$type;
  else $autocomplete_path='gp-autocomplete/node';
  $form['prize']['nid']=array(
    '#title' => t('Prize node'),
    '#type' => 'textfield',
    '#autocomplete_path' => $autocomplete_path,
    '#description' => t('Node used for prize presentation purposes.'),
    '#default_value' => empty($prize['nid']) ? '' : gp_node_autocomplete_reverse($prize['nid'])
  );
  $form['prize']['title']=array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Leave empty to use selected node\'s title.'),
    '#default_value' => isset($prize['title']) ? $prize['title'] : ''
  );
  $form['prize']['point_cost']=array(
    '#type' => 'textfield',
    '#title' => t('Point cost'),
    '#default_value' => isset($prize['point_cost']) ? $prize['point_cost'] : ''
  );
  $form['prize']['p_limit']=array(
    '#type' => 'textfield',
    '#title' => t('Limit'),
    '#description' => t('This many prizes will be available.'),
    '#default_value' => isset($prize['p_limit']) ? $prize['p_limit'] : ''
  );
  $form['prize']['type']=array(
    '#title' => t('Prize type'),
    '#type' => 'select',
    '#op' => 'change_type',
    '#options' => _loyalty_prize_types(),
    '#ajax' => array(
      'callback' => 'loyalty_prize_form_ajax',
      'wrapper' => 'loyalty-prize-data-wrapper'
    ),
    '#default_value' => isset($prize['type']) ? $prize['type'] : 0
  );
  
  $form['prize']['data']=array(
    '#prefix' => '<div id="loyalty-prize-data-wrapper">',
    '#suffix' => '</div>'
  );
    
  if (isset($prize['type'])) {
    $form['prize']['data']['#type']='fieldset';
    $form['prize']['data']['#title']=t('Prize data');
    
    switch ($prize['type']) {
      case 'promotion':
        module_load_include('inc', 'promotions', 'promotions.admin');
  
        $form['prize']['data']['validity_period']=array(
          '#type' => 'textfield',
          '#title' => t('Validity period'),
          '#description' => t('Enter value in days. Promotion will be valid only for this period. Leave empty for endless promotion.'),
          '#default_value' => isset($prize['data']['validity_period']) ? $prize['data']['validity_period'] : ''
        );
        $form['prize']['data']['limit']=array(
          '#type' => 'textfield',
          '#title' => t('Usage limit'),
          '#description' => t('Enter an integer value. Promotion will be usable exactly this many times - each completed checkout reduces this number. Leave empty for unlimited promotion.'),
          '#default_value' => isset($prize['data']['limit']) ? $prize['data']['limit'] : ''
        );
        
        $form['prize']['data']['conditions']=array(
          '#type' => 'fieldset',
          '#title' => t('Promotion conditions'),
          '#attributes' => array('id' => 'promo-conditions-wrapper'),
          '#tree' => TRUE
        );
        
        $number=0;
        if (empty($prize['data']['conditions'])) $prize['data']['conditions']=array(array());
        else unset($prize['data']['conditions']['add']);
        foreach ($prize['data']['conditions'] as $index => $data) {
          if (!is_numeric($index)) continue;
          $number++;
          $form['prize']['data']['conditions'][$index]=array(
            '#type' => 'fieldset',
            '#title' => t('Condition').' '.$number,
            '#attributes' => array('id' => 'promo-condition-'.$index.'-wrapper')
          );
          $types=_promotions_action_type_options('conditions');
          unset($types['uid']);
          unset($types['rid']);
          $form['prize']['data']['conditions'][$index]['type']=array(
            '#type' => 'select',
            '#title' => t('Condition type'),
            '#op' => 'condition_type',
            '#ajax' => array(
              'callback' => 'loyalty_prize_form_ajax',
              'wrapper' => 'promo-condition-'.$index.'-wrapper'
            ),
            '#options' => $types,
            '#default_value' => isset($data['type']) ? $data['type'] : ''
          );
          if (isset($data['type'])) {
            switch ($data['type']) {
              //Promotion code
              case 'code':
                $form['prize']['data']['conditions'][$index]['code']=array(
                  '#type' => 'value',
                  '#value' => 1,
                );
                $form['prize']['data']['conditions'][$index]['code_info']=array(
                  '#markup' => '<p>'.t('Promotion code will be generated for every user.').'</p>'
                );
                break;
              //Taxonomy
              case 'tid':
                if (!isset($vocabularies)) {
                  $vocabularies=array(0 => t('select one..'));
                  $results=db_query("SELECT vid, name FROM {taxonomy_vocabulary}")->fetchAllKeyed(0, 1);
                  if ($results) $vocabularies+=$results;
                }
                $form['prize']['data']['conditions'][$index]['vid']=array(
                  '#type' => 'select',
                  '#op' => 'change_vid',
                  '#options' => $vocabularies,
                  '#title' => t('Vocabulary'),
                  '#ajax' => array(
                    'callback' => 'loyalty_prize_form_ajax',
                    'wrapper' => 'promo-condition-'.$index.'-wrapper'
                  ),
                  '#default_value' => isset($data['vid']) ? $data['vid'] : 0
                );
                if (isset($data['vid'])) {
                  $form['prize']['data']['conditions'][$index]['tid']=array(
                    '#type' => 'textfield',
                    '#title' => t('Term'),
                    '#autocomplete_path' => 'admin/store/promotions/get-data/term-'.$data['vid'],
                    '#default_value' => isset($data['tid']) ? gp_term_autocomplete_reverse($data['tid']) : ''
                  );
                }
                break;
              //Articles price
              case 'articles_price':
                $form['prize']['data']['conditions'][$index]['articles_price']=array(
                  '#type' => 'textfield',
                  '#title' => t('Price'),
                  '#description' => t('Condition is met when total articles price in customer cart is higher than this value.'),
                  '#default_value' => isset($data['articles_price']) ? $data['articles_price'] : ''
                );
                break;
            }
          }
          if (sizeof($prize['data']['conditions']) > 1) {
            $form['prize']['data']['conditions'][$index]['remove']=array(
              '#type' => 'submit',
              '#value' => t('remove condition'),
              '#name' => 'remove_condition_'.$index,
              '#op' => 'remove_condition',
              '#ajax' => array(
                'callback' => 'loyalty_prize_form_ajax',
                'wrapper' => 'promo-conditions-wrapper'
              )
            );
          }
        }
        $form['prize']['data']['conditions']['add']=array(
          '#type' => 'submit',
          '#value' => t('add condition'),
          '#op' => 'add_condition',
          '#name' => 'add_condition',
          '#ajax' => array(
            'callback' => 'loyalty_prize_form_ajax',
            'wrapper' => 'promo-conditions-wrapper'
          ),
        );
        $form['prize']['data']['action']=array(
          '#type' => 'fieldset',
          '#title' => t('Action'),
        );
          
        $form['prize']['data']['action']['action_type']=array(
          '#type' => 'select',
          '#title' => t('Type'),
          '#options' => _promotions_action_type_options('actions'),
          '#default_value' => isset($prize['data']['action']['action_type']) ? $prize['data']['action']['action_type'] : NULL
        );
        $form['prize']['data']['action']['action_value']=array(
          '#type' => 'textfield',
          '#title' => t('Value'),
          '#default_value' => isset($prize['data']['action']['action_value']) ? $prize['data']['action']['action_value'] : ''
        );
        break;
      case 'item':
        $form['prize']['data']['description']=array(
          '#markup' => t('Node selected for promotion presentation will be used.')
        );
        break;
    }
  }
  $form['save_data']=array(
    '#type' => 'submit',
    '#op' => 'save',
    '#name' => 'save_data',
    '#value' => t('Save data')
  );

  return $form;
}

function loyalty_edit_prize_form_validate($form, &$form_state) {
  if ($form_state['triggering_element']['#op'] === 'save') {
    gp_validate_not_empty(array(
      $form['prize']['nid'],
      $form['prize']['point_cost'],
      $form['prize']['p_limit'],
      $form['prize']['type'],
    ), $form_state);
    my_api_validate_autocomplete_id($form['prize']['nid'], $form_state, 'nid', 'node');
    my_api_validate_short_text($form['prize']['title'], $form_state, 128);
    my_api_validate_number($form['prize']['point_cost'], $form_state, 0);
    my_api_validate_integer($form['prize']['p_limit'], $form_state, array(0, '>='));
    
    switch ($form_state['values']['prize']['type']) {
      case 'promotion':
        my_api_validate_integer($form['prize']['data']['validity_period'], $form_state, array(0, '>='));
        my_api_validate_integer($form['prize']['data']['limit'], $form_state, array(0, '>='));
        
        unset($form_state['values']['prize']['data']['conditions']['add']);
        
        foreach ($form['prize']['data']['conditions'] as $index => $elements) {
          if (!is_numeric($index)) continue;
          $condition_type=drupal_array_get_nested_value($form_state['values'], $elements['type']['#parents']);
          switch ($condition_type) {
            case 'tid':
              gp_validate_not_empty(array($elements['vid'], $elements['tid']), $form_state);
              my_api_validate_autocomplete_id($elements['tid'], $form_state, 'tid', 'taxonomy_term_data');
              break;
            case 'articles_price':
              my_api_validate_number($elements['articles_price'], $form_state, 0);
              if (!in_array($form_state['values']['prize']['data']['action']['action_type'], array(4, 5))) form_error($elements['type'], t('This condition can only be used to actions performed on summary prices (shipping price reduction, vouchers). Otherwise it may cause an infinite loop.'));
              break;
          }
          
          unset($form_state['values']['prize']['data']['conditions'][$index]['remove']);
          
        }
        
        gp_validate_not_empty(array(
          $form['prize']['data']['action']['action_type'],
          $form['prize']['data']['action']['action_value']
        ), $form_state);
        
        break;
    }
  }
}

function loyalty_edit_prize_form_submit($form, &$form_state) {
  $op=$form_state['triggering_element']['#op'];
  $prize=&$form_state['values']['prize'];
  if ($op === 'save') {
    if (!isset($prize['data'])) $prize['data']=array();
    
    if ($prize['type'] === 'promotion') {
      foreach ($prize['data']['conditions'] as $index => $data) {
        if (!$data['type']) unset($prize['data']['conditions'][$index]);
      }
    }
    
    $prize['data']=serialize($prize['data']);
    if (empty($prize['title'])) $prize['title']=db_query("SELECT title FROM {node} WHERE nid = :nid", array(':nid' => $prize['nid']))->fetchField();
        
    if (isset($form_state['pid'])) {
      db_update('loyalty_prizes')->fields($prize)->condition('pid', $form_state['pid'])->execute();
      drupal_set_message(t('Prize updated.'));
    }
    else {
      $form_state['values']['prize']['created']=REQUEST_TIME;
      db_insert('loyalty_prizes')->fields($prize)->execute();
      drupal_set_message(t('Prize created.'));
    }
  }
  elseif ($op === 'add_condition') {
    $prize['data']['conditions'][]=array();
    $form_state['rebuild']=TRUE;
  }
  elseif ($op === 'remove_condition') {
    $parents=$form_state['triggering_element']['#parents'];
    $index=$parents[(sizeof($parents)-2)];
    unset($prize['data']['conditions'][$index]);
    $form_state['rebuild']=TRUE;
  }
}

function loyalty_prize_form_ajax($form, $form_state) {
  $op=$form_state['triggering_element']['#op'];
  if ($op === 'change_type') {
    $form_state['values']['prize']['type']=$form_state['triggering_element']['#value'];
    $form=drupal_rebuild_form($form['#form_id'], $form_state, $form);
    return $form['prize']['data'];
  }
  elseif ($op === 'condition_type' || $op === 'change_vid') {
    $parents=$form_state['triggering_element']['#parents'];
    $index=$parents[(sizeof($parents)-2)];
    return $form['prize']['data']['conditions'][$index];
  }
  elseif ($op === 'add_condition' || $op === 'remove_condition') {
    return $form['prize']['data']['conditions'];
  }
}

function loyalty_delete_prize_form($form, &$form_state, $pid) {
  if ($prize=db_select('loyalty_prizes', 'p')->fields('p', array('title', 'nid'))->condition('pid', $pid)->execute()->fetchAssoc()) {
    $form_state['pid']=$pid;
    $form['info']['#markup']='<p>'.t('Are you sure you wish to delete the prize !prize?', array('!prize' => l($prize['title'], 'node/'.$prize['nid'], array('attributes' => array('target' => '_blank'))))).'</p>';
    $form['delete']=array(
      '#type' => 'submit',
      '#value' => t('Delete')
    );
  }
  else {
    $form['info']['#markup']='<p>'.t('Prize not found.').'</p>';
    $form['back']=array(
      '#type' => 'submit',
      '#value' => t('Back')
    );
  }
  
  return $form;
}

function loyalty_delete_prize_form_submit($form, &$form_state) {
  if (isset($form_state['pid'])) {
    db_delete('loyalty_prizes')->condition('pid', $form_state['pid'])->execute();
    drupal_set_message('Prize deleted.');
  }
}
