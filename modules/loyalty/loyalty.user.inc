<?php
function loyalty_user_get_prizes($uid) {
  $Npage=gp_get_settings('loyalty', 'user_prize_npage');
  $query=db_select('loyalty_prizes', 'p');
    
  _loyalty_prize_list_filters($query, $uid);
  
  $query->fields('p');
  $query=$query->extend('PagerDefault')->limit($Npage);
  $results=$query->execute()->fetchAll();
  
  if (!empty($results)) {
    $nids=array();
    foreach ($results as $result) {
      if (!in_array($result->nid, $nids)) $nids[]=$result->nid;
    }
    $nodes=node_load_multiple($nids);
    foreach ($results as $index => $result) {
      $renderable=node_view($nodes[$result->nid], 'prize_teaser');
      $renderable['#theme']='prize';
      _loyalty_node_renderable_alter($renderable, $result->pid);
      $results[$index]->node=$renderable;
    }
  }
  return $results;
}

function loyalty_get_user_selected($uid) {
  $query=db_select('loyalty_selected', 's');
  $query->join('loyalty_prizes', 'p', 'p.pid = s.pid');
  $query
    ->fields('s')
    ->fields('p', array('nid', 'type', 'title'));

  _loyalty_prize_list_filters($query, $uid);
  
  $Npage=gp_get_settings('loyalty', 'user_prize_npage');
  $query=$query->extend('PagerDefault')->limit($Npage);
  $results=$query->execute()->fetchAll();
  
  if (!empty($results)) {
    foreach ($results as $result) $nids[]=$result->nid;
    $nodes=node_load_multiple($nids);
    foreach ($results as $index => $result) {
      $renderable=node_view($nodes[$result->nid], 'prize_teaser');
      $renderable['#theme']='prize';
      $renderable['#prize']=$result;
      $renderable['prize_details']=array(
        '#prefix' => '<div class="'.drupal_html_class('prize_details').'">',
        '#markup' => l(t('details'), 'user/'.$uid.'/loyalty/selected/'.$result->sid),
        '#suffix' => '</div>',
        '#weight' => 61
      );
      $results[$index]->node=$renderable;
    }
  }
  return $results;
}

//Filters and sorting
function _loyalty_prize_list_filters($query, $uid) {
  if (!empty($_SESSION['user_prize_filters']['affordable'])) {
    $user_points=db_query("SELECT points FROM {loyalty_points} WHERE uid = :uid", array(':uid' => $uid))->fetchField();
    if (!$user_points) $user_points=0;
    $query->condition('p.point_cost', $user_points, '<=');
  }
  if (!empty($_SESSION['user_prize_filters']['type'])) {
    $query->condition('p.type', $_SESSION['user_prize_filters']['type']);
  }  
}


function loyalty_user_page($user=NULL) {
  if (!isset($user)) global $user;
  
  $vars=array();
  
  $vars['points']=db_query("SELECT points FROM {loyalty_points} WHERE uid = :uid", array('uid' => $user->uid))->fetchField();
  if (!$vars['points']) $vars['points']=0;
  $vars['points_info']=t('Your points in loyalty program: !points.', array('!points' => $vars['points']));
  
  drupal_add_js('misc/ajax.js');

  $filters=array(
    'all' => array(
      l(t('All'), 'loyalty/nojs/'.$user->uid.'/prizes/all', array('attributes' => array('class' => array('use-ajax'))))
    ),
    'affordability' => array(
      l(t('Affordable'), 'loyalty/nojs/'.$user->uid.'/prizes/affordable', array('attributes' => array('class' => array('use-ajax'))))
    ),
    'type' => array(
      l(t('Material'), 'loyalty/nojs/'.$user->uid.'/prizes/type_item', array('attributes' => array('class' => array('use-ajax')))),
      l(t('Promotion codes'), 'loyalty/nojs/'.$user->uid.'/prizes/type_promo', array('attributes' => array('class' => array('use-ajax'))))
    ),
    'category' => array(
      l(t('Selectable'), 'loyalty/nojs/'.$user->uid.'/prizes/selectable', array('attributes' => array('class' => array('use-ajax')))),
      l(t('Selected'), 'loyalty/nojs/'.$user->uid.'/prizes/selected', array('attributes' => array('class' => array('use-ajax'))))
    ),
  );
  $vars['filters']=array();
  foreach ($filters as $list) $vars['filters'][]=array(
    '#theme' => 'item_list',
    '#items' => $list
  );
  
  if (!isset($_SESSION['user_prize_filters']['category']) || $_SESSION['user_prize_filters']['category'] === 'selectable') {
    $prize_data=loyalty_user_get_prizes($user->uid);
  }
  else {
    $prize_data=loyalty_get_user_selected($user->uid);
  }
  
  if (!empty($prize_data)) $vars['prizes']='<div id="loyalty-user-prizes">'.theme('loyalty_prizes', array('prizes' => $prize_data, 'pager' => theme('pager'))).'</div>';
  else $vars['prizes']='<div id="loyalty-user-prizes">'.t('No prizes.').'</div>';
  return theme('loyalty_user', $vars);
}

function loyalty_user_prizes_ajax($js, $user, $filter) {
  switch ($filter) {
    case 'affordable':
      $_SESSION['user_prize_filters']['affordable']=TRUE;
      break;
    case 'all':
      unset($_SESSION['user_prize_filters']['affordable']);
      unset($_SESSION['user_prize_filters']['type']);
      break;
    case 'type_item':
      $_SESSION['user_prize_filters']['type']='item';
      break;
    case 'type_promo':
      $_SESSION['user_prize_filters']['type']=array('voucher', 'discount');
      break;
    case 'selectable':
      $_SESSION['user_prize_filters']['category']='selectable';
      break;
    case 'selected':
      $_SESSION['user_prize_filters']['category']='selected';
      break;
  }
  
  if ($js === 'nojs') drupal_goto('user/'.$user->uid.'/loyalty');
  else {
    if (!isset($_SESSION['user_prize_filters']['category']) || $_SESSION['user_prize_filters']['category'] === 'selectable') {
      $prize_data=loyalty_user_get_prizes($user->uid);
    }
    else $prize_data=loyalty_get_user_selected($user->uid);
    
    if (empty($prize_data)) $output='<div id="loyalty-user-prizes">'.t('No prizes.').'</div>';
    else $output='<div id="loyalty-user-prizes">'.theme('loyalty_prizes', array('prizes' => $prize_data, 'pager' => theme('pager'))).'</div>';
    
    $commands=array();
    $commands[]=ajax_command_replace('#loyalty-user-prizes', $output);
    ajax_deliver(array('#type' => 'ajax', '#commands' => $commands));
  }
}

//Prize ordering form
function loyalty_order_prize_form($form, &$form_state, $user, $pid) {
  $prize=db_select('loyalty_prizes', 'p')->fields('p')->condition('pid', $pid)->execute()->fetchObject();
  if (!$prize) return drupal_not_found();
  
  $form['#theme']='loyalty_order_form';
  
  $prize_node=node_load($prize->nid);
  $prize_node->title=$prize->title;
  $renderable=node_view($prize_node, 'prize_teaser');
  _loyalty_node_renderable_alter($renderable, $prize->pid);
  unset($renderable['order_link']);
  $form['prize_teaser']=array('#markup' => drupal_render($renderable));
  
  $user_points=db_query("SELECT points FROM {loyalty_points} WHERE uid = :uid", array(':uid' => $user->uid))->fetchField();
  if ($user_points < $prize->point_cost) {
    $form['info']['#markup']=t('You don\'t have enough points to order this prize.');
  }
  else {
    $form_state['user']=$user;
    $form_state['prize']=&$prize;
    $prize->data=unserialize($prize->data);
    $form['#prize_type']=$prize->type;
    
    switch ($prize->type) {
      case 'item':
        module_load_include('inc', 'graber_cart', 'graber_cart.checkout');
        
        $defaults=array();
        $defaults['shipping']['different_shipping']=1;
        $fields=field_info_instances('user', 'user');
        foreach ($fields as $name => $info) {
          $field_data=field_get_items('user', $user, $name);
          if (!empty($field_data[0]['value'])) {
            $elem_name=substr($name, 6);
            if (!isset($defaults['shipping'][$elem_name])) $defaults['shipping'][$elem_name]=$field_data[0]['value'];
          }
        }
        $checkout_fields=graber_cart_order_data_form_fields($defaults);
        unset($checkout_fields['shipping']['different_shipping']);
        $form['shipping']=$checkout_fields['shipping'];
        break;
      case 'promotion':
        break;
    }
    
    $form['order']=array(
      '#type' => 'submit',
      '#value' => t('Order now')
    );
  }
  
  return $form;
}

function loyalty_order_prize_form_validate($form, &$form_state) {
  if ($form_state['prize']->type === 'item') {
    if (
      (empty($form_state['values']['shipping']['name']) || empty($form_state['values']['shipping']['family'])) &&
      empty($form_state['values']['shipping']['company'])
    ) {
      form_set_error('shipping][name', '');
      form_set_error('shipping][family', '');
      form_set_error('shipping][company', '');
      form_set_error('', t('Please fill out your name and family or company name'));
    }
  }
}

function loyalty_order_prize_form_submit($form, &$form_state) {
  $user_points=db_query("SELECT points FROM {loyalty_points} WHERE uid = :uid", array(':uid' => $form_state['user']->uid))->fetchField();
  $prize=&$form_state['prize'];
  if ($user_points < $prize->point_cost) {
    drupal_set_message(t('You don\'t have enough points.'));
    drupal_goto('');
  }

  $fields=array(
    'uid' => $form_state['user']->uid,
    'pid' => $prize->pid,
    'selected' => REQUEST_TIME
  );
  
  switch ($prize->type) {
    //Material prize
    case 'item':
      $fields['data']=serialize($form_state['values']['shipping']);
      
      $language=user_preferred_language($form_state['user']);
      $prize_node=node_load($prize->nid);
      $renderable=node_view($prize_node, 'prize_teaser');
      unset($renderable['order_link']);

      $params=array(
        'username' => $form_state['user']->name,
        'prize' => drupal_render($renderable),
        'shipping' => theme('graber_address_data', array('data' => $form_state['values']['shipping'])),
        'site_name' => variable_get('site_name', 'our site')
      );
      
      $message=drupal_mail(
        'graber_notifications',
        'loyalty_confirm',
        $form_state['user']->mail,
        $language->language,
        $params
      );
      $message=drupal_mail(
        'graber_notifications',
        'loyalty_admin_notification',
        $form_state['user']->mail,
        $language->language,
        $params
      );
      drupal_set_message(t('Prize selected. You will receive a confirmation message shortly.'));
      break;
    
    //Promotion
    case 'promotion':
      $promotion=new promotion();
      if (!empty($prize->data['validity_period'])) {
        $promotion->expires=$prize->data['expires']=REQUEST_TIME+$prize->data['validity_period']*24*60*60;
      } 
      if (!empty($prize->data['limit'])) {
        $promotion->limit_count=$prize->data['limit'];
        $promotion->has_limit=1;
      }
      else $promotion->has_limit=0;
      $promotion->creator=0; # System promotion
      $promotion->action_type=$prize->data['action']['action_type'];
      $promotion->action_value=$prize->data['action']['action_value'];
      $promotion->message=t('Loyalty program prize active.');
            
      //Conditions
      $promotion->conditions=array(
        array(
          'type' => 'uid',
          'uid' => $form_state['user']->uid
        )
      );
      if (!empty($prize->data['conditions'])) {
        foreach ($prize->data['conditions'] as $index => $condition) {
          if ($condition['type'] === 'code') {
            $promotion_code=$condition['code']=_gp_generate_random_code(8, TRUE);
            $prize->data['conditions'][$index]=$condition;
            $promotion->conditions[]=array(
              'type' => 'code',
              'code' => $condition['code']
            );
            
            //This type of promotion must not be unlimited
            if (!$promotion->has_limit) {
              $promotion->limit_count=1;
              $promotion->has_limit=1;
            }
          }
          else $promotion->conditions[]=$condition;
        }
      }
      $fields['data']=serialize($prize->data);
      $promotion->save();
      if (isset($promotion_code)) drupal_set_message(t('Prize selected. Your promotion code: !code. You can view the details of your selected prizes in your loyalty program tab by applying "selected" filter.', array('!code' => $promotion_code)));
      else drupal_set_message(t('Prize selected. You can view the details of your selected prizes in your loyalty program tab by applying "selected" filter.'));
  }
  db_insert('loyalty_selected')->fields($fields)->execute();
  
  //Subtract from user's points
  db_update('loyalty_points')->fields(array('points' => ($user_points-$form_state['prize']->point_cost)))->condition('uid', $form_state['user']->uid)->execute();
  drupal_set_message(t('!points points subtracted from your loyalty program account.', array('!points' => $prize->point_cost)));
  
  drupal_goto('');
}

//Selected prize view
function loyalty_user_selected_prize($user, $sid) {
  $query=db_select('loyalty_selected', 's');
  $query->join('loyalty_prizes', 'p', 'p.pid = s.pid');
  $query->fields('s');
  $query->fields('p', array('nid', 'type', 'title'));
  $query->condition('s.sid', $sid)->condition('s.uid', $user->uid);
  $prize=$query->execute()->fetchObject();
  
  if (!$prize) return drupal_not_found();
  
  $prize->data=unserialize($prize->data);
  $vars=array();
  $vars['type']=$prize->type;
  $prize_node=node_load($prize->nid);

  $vars['teaser']=node_view($prize_node, 'prize_teaser');
  $vars['teaser']['#theme']='prize';
  $vars['teaser']['#prize']=$prize;
  unset($vars['teaser']['order_link']);
  
  switch ($prize->type) {
    case 'item':
      $vars['details_title']=t('Shipping address').':';
      $vars['details']=theme('graber_address_data', array('data' => $prize->data));
      break;
    case 'promotion':
      $vars['details_title']=t('Promotion information').':';
      $output=_loyalty_promotion_info($prize->data);
      $vars['details']='';
      foreach ($output as $item => $html) {
        $vars['details'].='<div class="'.drupal_html_class($item).'">'.$html.'</div>';
      }
      break;
  }
    
  return theme('loyalty_selected_prize', $vars);
}

