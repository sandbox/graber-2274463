<?php
function price_groups_settings_form($form, &$form_state) {
	$form['#pg_settings']=db_select('price_groups', 'p')->fields('p')->execute()->fetchAllAssoc('gid', PDO::FETCH_ASSOC);
	if (!empty($form['#pg_settings'])) {
		$form['pg_table']=array(
			'#type' => 'fieldset',
			'#theme' => 'price_groups_table',
			'#tree' => TRUE
		);
		foreach ($form['#pg_settings'] as $index => $data) {
			$form['pg_table'][$index]['gid']=array(
				'#title' => t('Group ID'),
				'#markup' => $data['gid']
			);
			$form['pg_table'][$index]['name']=array(
				'#type' => 'textfield',
				'#title' => t('Group name'),
				'#default_value' => $data['name']
			);
			$form['pg_table'][$index]['price']=array(
				'#type' => 'textfield',
				'#title' => t('Price'),
				'#size' => 10,
				'#default_value' => $data['price']
			);
			$form['pg_table'][$index]['delete']=array(
				'#type' => 'submit',
				'#name' => 'delete_'.$index,
				'#op' => 'delete',
				'#value' => t('delete'),
				'#ajax' => array(
					'callback' => 'price_groups_ajax_callback',
					'wrapper' => 'pg-table-wrapper'
				)
			);
		}
		$form['pg_table']['save_settings']=array(
			'#type' => 'submit',
			'#value' => t('Save settings'),
			'#ajax' => array(
				'callback' => 'price_groups_ajax_callback',
				'wrapper' => 'pg-table-wrapper'
			)
		);
	}
	$form['pg_table']['#prefix']='<div id="pg-table-wrapper">';
	$form['pg_table']['#suffix']='</div>';
	
	$form['add_pg']=array(
		'#type' => 'fieldset',
		'#title' => t('Add price group'),
		'#tree' => TRUE
	);
	$form['add_pg']['name']=array(
		'#type' => 'textfield',
		'#title' => t('Group name'),
	);
	$form['add_pg']['price']=array(
		'#type' => 'textfield',
		'#title' => t('Price'),
		'#size' => 10,
		'#field_suffix' => ' '.variable_get('graber_cart_currency', 'PLN')
	);
	$form['add_pg']['add']=array(
		'#type' => 'submit',
		'#value' => t('Add'),
		'#ajax' => array(
			'callback' => 'price_groups_ajax_callback',
			'wrapper' => 'pg-table-wrapper'
		)
	);
	return $form;
}

function theme_price_groups_table($vars) {
	$element=$vars['pg_table'];
	$header=array(t('No', array(), array('context' => 'number')));
	$rows=array();
	$Nrows=0;
	foreach ($element as $rid => $data) {
		if (!is_numeric($rid)) continue;
		$Nrows++;
		$rows[$Nrows][]=$Nrows;
		foreach ($data as $prop => $cell) {
			if (substr($prop, 0, 1) != '#') {
				if (isset($cell['#title']) || isset($cell['#value'])) {
					$title=empty($cell['#title']) ? $cell['#value'] : $cell['#title'];
					if ($Nrows == 1 && !in_array($title, $header)) $header[]=$title;
					unset($cell['#title']);
				}
				$rows[$Nrows][]=render($cell);
			}
		}
	}
	return theme('table', array('header' => $header, 'rows' => $rows, 'sticky' => FALSE)).render($element['save_settings']);
}

function price_groups_ajax_callback($form, $form_state) {
	return $form['pg_table'];
}

function price_groups_settings_form_validate($form, &$form_state) {
	if (isset($form_state['values']['op'])) {
		if ($form_state['values']['op'] == t('Add')) {
			if (empty($form_state['values']['add_pg']['name'])) form_set_error('add_pg][name', t('Group name is required.'));
			else {
				foreach ($form['#pg_settings'] as $index => $data) {
					if ($form_state['values']['add_pg']['name'] == $data['name']) form_set_error('add_pg][name', t('Price group already exists.'));
				}
			}
			
			if (empty($form_state['values']['add_pg']['price'])) form_set_error('add_pg][price', t('Price is required.'));
			else {
				$form_state['values']['add_pg']['price']=strtr($form_state['values']['add_pg']['price'], array(',' => '.'));
				if (!is_numeric($form_state['values']['add_pg']['price']) || $form_state['values']['add_pg']['price'] <= 0) form_set_error('add_pg][price', t('Price is invalid.'));
			}
		}
		elseif ($form_state['values']['op'] == t('Save settings')) {
			foreach ($form_state['values']['pg_table'] as $index => $data) {
				if (!is_numeric($index)) continue;
				if (empty($data['name'])) form_set_error('pg_table]['.$index.'][name', t('Group name is required.'));
				else {
					foreach ($form['#pg_settings'] as $inner_index => $inner_data) {
						if ($index == $inner_index) continue;
						if ($data['name'] == $inner_data['name']) form_set_error('pg_table]['.$index.'][name', t('Price group already exists.'));
					}
				}
				
				if (empty($data['price'])) form_set_error('add_pg][price', t('Price is required.'));
				else {
					$data['price']=strtr($data['price'], array(',' => '.'));
					if (!is_numeric($data['price']) || $data['price'] <= 0) form_set_error('add_pg][price', t('Price is invalid.'));
					$form_state['values']['pg_table'][$index]['price']=$data['price'];
				}
			}			
		}
	}
}

function price_groups_settings_form_submit($form, &$form_state) {
	if (isset($form_state['values']['op'])) {
		if ($form_state['values']['op'] == t('Add')) {
			$form_state['rebuild']=TRUE;
			$fields=array(
				'name' => $form_state['values']['add_pg']['name'],
				'price' => $form_state['values']['add_pg']['price']
			);
			$form['#pg_settings'][]=$fields;
			db_insert('price_groups')->fields($fields)->execute();
		}
		elseif ($form_state['values']['op'] == t('Save settings')) {
			$form['#pg_settings']=array();
			foreach ($form_state['values']['pg_table'] as $gid => $data) {
				if (!is_numeric($gid)) continue;
				$fields=array(
					'name' => $data['name'],
					'price' => $data['price']
				);
				$form['#pg_settings'][$gid]=$fields;
				db_update('price_groups')->fields($fields)->condition('gid', $gid)->execute();
			}
		}
	}
	elseif ($form_state['clicked_button']['#op'] == 'delete') {
		$form_state['rebuild']=TRUE;
		$gid=$form_state['clicked_button']['#parents'][1];
		db_delete('price_groups')->condition('gid', $gid)->execute();
		unset($form['#pg_settings'][$gid]);
	}
}