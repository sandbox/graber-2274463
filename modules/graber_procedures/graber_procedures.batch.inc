<?php
//Common node operation with custom invoke
function gp_node_export_operation($conditions, &$context) {
  if (!isset($context['sandbox']['total'])) {
    $query=db_select('node', 'n')->fields('n', array('nid'));
    if (!empty($conditions)) {
      foreach ($conditions as $condition) {
        if (empty($condition[2])) $query->condition($condition[0], $condition[1]);
        else $query->condition($condition[0], $condition[1], $condition[2]);
      }
    }
    $query->addTag('gp_node_batch_query');
    $context['sandbox']['total']=$query->countQuery()->execute()->fetchField();
    $context['sandbox']['processed']=$context['sandbox']['nid']=0;
  }
  
  $query=db_select('node', 'n')->fields('n', array('nid'));
  $query->condition('n.nid', $context['sandbox']['nid'], '>');
  if (!empty($conditions)) {
    foreach ($conditions as $condition) {
      if (empty($condition[2])) $query->condition($condition[0], $condition[1]);
      else $query->condition($condition[0], $condition[1], $condition[2]);
    }
  }
  $query->orderBy('n.nid', 'ASC');
  $query->addTag('gp_node_batch_query');
  $nid=$query->execute()->fetchField();
  
  if ($nid) {
    $node=node_load($nid);
    $operations=module_invoke_all('gp_node_batch', $node);
    if (!empty($operations)) {
      if (is_array($operations)) {
        foreach ($operations as $operation) $context['results'][]=$operation;
        else $context['results'][]=$operations;
      }
    }
    else $context['results'][]=t('undefined');
    
    $context['sandbox']['nid']=$nid;
    $context['sandbox']['processed']++;
    $context['finished']=$context['sandbox']['processed']/$context['sandbox']['total'];
    $context['message']=t('Processing item !current of !total', array('!current' => $context['sandbox']['processed'], '!total' => $context['sandbox']['total']));
  }
}

//Batch finished callback
function gp_batch_finished($success, $results, $operations) {
  if ($success) {
    $Nres=count($results);
    $operations=array_count_values($results);
    $details=array();
    foreach ($operations as $op => $count) $details[]=t($op).': '.$count;
    $message = t('!nres operations performed: !operations.', array('!nres' => $Nres, '!operations' => implode(', ', $details)));
    drupal_set_message($message);
  }
  else {
    $error_operation=reset($operations);
    $message=t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}



//Node operation that exports nodes to csv - may come in handy one day..
function gp_node_export_operation($node) {
  if (!empty($node->field_kategoria[LANGUAGE_NONE][0]['tid'])) {
    $tid=$node->field_kategoria[LANGUAGE_NONE][0]['tid'];
    $terms=array();
    while ($tid) {
      $term_name=db_query("SELECT name FROM {taxonomy_term_data} WHERE tid = :tid", array('tid' => $tid))->fetchField();
      if ($term_name) $terms[]=$term_name;
      $tid=db_query("SELECT parent FROM {taxonomy_term_hierarchy} WHERE tid = :tid", array('tid' => $tid))->fetchField();
    }
    $terms=array_reverse($terms);
    $cat_path=implode(' / ', $terms);
  }
  else $cat_path='';
  $fields=array(
    $node->title,
    $node->body[LANGUAGE_NONE][0]['value'],
    $cat_path,
    $node->created
  );
  $fh=fopen(drupal_get_path('module', 'console').'/export.csv', 'a');
  fputcsv($fh, $fields);
  fclose($fh);
}