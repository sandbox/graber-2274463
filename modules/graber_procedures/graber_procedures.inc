<?php
function my_api_version() {
  return '1.2';
}

//Form validation functions
function my_api_validate_autocomplete_id($element, &$form_state, $key, $table=FALSE) {
  if (!is_array($element['#parents'])) return;
  $value=drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  if ($value == '') return TRUE;
  $path=implode('][', $element['#parents']);
  preg_match('#\['.$key.'=([0-9]+)\]#', $value, $matches);

  if (isset($matches[1])) {
    if ($table) {
      $id=db_select($table, 't')->fields('t', array($key))->condition($key, $matches[1])->execute()->fetchColumn();
      if (!$id) form_set_error($path, t('!name is invalid. Record cannot be found in !table table.', array('!name' => $element['#title'], '!table' => $table)));
    }
    else $id=$matches[1];
    
    if ($id) {
      form_set_value($element, $id, $form_state);
      return TRUE;
    }
  }
  else form_set_error($path, t('!name must be a value from autocomplete list.', array('!name' => $element['#title'])));
  return FALSE;
}

function my_api_validate_number($element, &$form_state, $min=FALSE, $max=FALSE) {
  if (!is_array($element['#parents'])) return;
  $value=drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  if ($value == '') return;
  $path=implode('][', $element['#parents']);
  
  if (!is_numeric($value)) {
    $value=strtr($value, array(',' => '.'));
    $update_val=TRUE;
  }
  if (!is_numeric($value)) form_set_error($path, t('!name must be a number.', array('!name' => $element['#title'])));
  else {
    $pass=TRUE;
    if ($min !== FALSE && !($value >= $min)) {
      form_set_error($path, t('!name must be equal to or greater than !value', array('!name' => $element['#title'], '!value' => $min)));
      $pass=FALSE;
    }
    elseif ($max !== FALSE && !($value <= $max)) {
      form_set_error($path, t('!name must be equal to or less than !value', array('!name' => $element['#title'], '!value' => $max)));
      $pass=FALSE;
    }
    if ($pass && isset($update_val)) form_set_value($element, $value, $form_state);
  }
}

function my_api_validate_integer($element, &$form_state, $min=FALSE, $max=FALSE) {
  $validated=TRUE;
  if (!is_array($element['#parents'])) return;
  $value=drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  if ($value === '') return $validated;
  $path=implode('][', $element['#parents']);
  if (!preg_match('/^\-?[0-9]+$/', $value)) {
    form_set_error($path, t('!name must be an integer.', array('!name' => $element['#title'])));
    $validated=FALSE;
  }
  else {
    if ($min !== FALSE) {
      if (is_array($min)) {
        if (($min[1] == '>=') && !($value >= $min[0])) {
          form_set_error($path, t('!name must be greater or equal to !value', array('!name' => $element['#title'], '!value' => $min[0])));
          $validated=FALSE;
        }
        elseif (($min[1] == '>') && !($value > $min[0])) {
          form_set_error($path, t('!name must be greater than !value', array('!name' => $element['#title'], '!value' => $min[0])));
          $validated=FALSE;
        }
      }
      elseif (!($value > $min)) {
        form_set_error($path, t('!name must be greater than !value', array('!name' => $element['#title'], '!value' => $min)));
        $validated=FALSE;
      }
    }
    elseif ($max !== FALSE && !($value < $max)) {
      form_set_error($path, t('!name must be lower than !value', array('!name' => $element['#title'], '!value' => $max)));
      $validated=FALSE;
    }
  }
  return $validated;
}


function my_api_validate_short_text($element, &$form_state, $max_length=255) {
  if (!is_array($element['#parents'])) return;
  $value=drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  if ($value != '') {
    if (strlen($value) > $max_length) {
      $path=implode('][', $element['#parents']);
      form_set_error($path, t('!name is too long, max length is !length characters.', array('!name' => $element['#title'], '!length' => $max_length)));
    }
  }
}

function my_api_validate_taxonomy_autocomplete($element, &$form_state, $vid=FALSE) {
  if (!is_array($element['#parents'])) return;
  $value=drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  preg_match('#\[tid=([0-9]+)\]#', $value, $matches);
  if (isset($matches[1])) {
    $query=db_select('taxonomy_term_data', 't')->fields('t', array('tid'))->condition('tid', $matches[1]);
    if ($vid !== FALSE) $query->condition('vid', $vid);
    $tid=$query->execute()->fetchColumn();
  }

  $path=implode('][', $element['#parents']);
  if (empty($tid)) form_set_error($path, t('Taxonomy term does not exist. Please use autocomplete dropdown.'));
  else form_set_value($element, $tid, $form_state);
}

function my_api_taxonomy_autocomplete_validate($element, &$form_state) {
  $value=array();
  if ($tags=$element['#value']) {
    $field=field_info_field($element['#field_name'], $form_state);
    $vocabularies=array();
    foreach ($field['settings']['allowed_values'] as $tree) {
      if ($vocabulary=taxonomy_vocabulary_machine_name_load($tree['vocabulary'])) {
        $vocabularies[$vocabulary->vid]=$vocabulary;
      }
    }

    $typed_terms = drupal_explode_tags($tags);
    foreach ($typed_terms as $typed_term) {
      if ($possibilities=taxonomy_term_load_multiple(array(), array('name' => trim($typed_term), 'vid' => array_keys($vocabularies)))) {
        $term=array_pop($possibilities);
        $value[] = (array) $term;
      }
    }
    
    if (empty($value)) form_error($element, t('Incorrect term value. Please use autocomplete, new terms are not allowed.'));
    else form_set_value($element, $value, $form_state);
  }
}

//Validators implementations for '#element_validate'
function my_api_validate_element_number($element, &$form_state, $form) {
  my_api_validate_number($element, $form_state);
}

function my_api_validate_element_mail($element, &$form_state, $form) {
  $value=drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  if (!valid_email_address($value)) form_error($element, t('E-mail address is incorrect.'));
}



function my_api_get_last_correction($dir, $filename) {
  if (!file_exists($dir.'/'.$filename)) return FALSE;
  $base=substr($filename, 0, strrpos($filename, '.'));
  $ext=substr($filename, strrpos($filename, '.')+1);
  $No=1;
  $loops=0;
  while (file_exists($dir.'/'.$base.'('.$No.').'.$ext)) {
    $No++;
    $loops++;
  }
  if ($loops) return $base.'('.($No-1).').'.$ext;
  else return $base.'.'.$ext;
}

function my_api_prepare_fitem($data, $params=array()) {
  if (!empty($params['type'])) {
    $item=array(
      '#type' => $params['type'],
      '#title' => $data['name'],
      '#options' => $params['options']
    );
  }
  switch ($data['type']) {
    case 'int':
    case 'numeric':
    case 'text':
      $item=array(
        '#type' => 'textfield',
        '#title' => $data['name'],
      );
      break;
    default:
      $item=array(
        '#type' => $data['type'],
        '#title' => $data['name'],
      );
  }
  foreach ($data as $key => $value) {
    if (substr($key, 0, 1) == '#') $item[$key]=$value;
  }
  if (isset($item)) return $item;  
}

function my_api_l($text, $url, $data=array()) {
  $options=array();
  if (!isset($data['title'])) $data['title']=$text;
  $options['attributes']['title']=$data['title'];
  if (!isset($data['class'])) $data['class']=array(drupal_html_class($data['title']));
  $options['attributes']['class']=$data['class'];
  if (isset($data['dest'])) $options['query']['destination']=$data['dest'];
  return l($text, $url, $options);
}


if (!function_exists('array_replace_recursive'))
{
  function array_replace_recursive($array, $array1)
  {
    function recurse($array, $array1)
    {
      foreach ($array1 as $key => $value)
      {
        // create new key in $array, if it is empty or not an array
        if (!isset($array[$key]) || (isset($array[$key]) && !is_array($array[$key])))
        {
          $array[$key] = array();
        }
  
        // overwrite the value in the base array
        if (is_array($value))
        {
          $value = recurse($array[$key], $value);
        }
        $array[$key] = $value;
      }
      return $array;
    }
  
    // handle the arguments, merge one by one
    $args = func_get_args();
    $array = $args[0];
    if (!is_array($array))
    {
      return $array;
    }
    for ($i = 1; $i < count($args); $i++)
    {
      if (is_array($args[$i]))
      {
        $array = recurse($array, $args[$i]);
      }
    }
    return $array;
  }
}

//Format plural that supports polish language
function my_api_format_plural($count, $singular, $plural, $args = array(), $options = array()) {
  if (isset($options['langcode'])) $langcode=$options['langcode'];
  else $langcode = $GLOBALS['language']->language;
  if ($langcode == 'pl') {
    if ($count == 1) $version=1;
    else {
      $last_digit = $count % 10;
      if ($last_digit == 0 || $last_digit == 1) $version=3;
      elseif ($last_digit > 1 && $last_digit < 5) $version=2;
      else $version=3;
    }
    switch ($version) {
      case 1:
        $translated_string=strtr($singular, array('@count' => '@plcount1'));
        return t($translated_string, array('@plcount1' => $count)+$args);
      case 2:
        $translated_string=strtr($plural, array('@count' => '@plcount2-4'));
        return t($translated_string, array('@plcount2-4' => $count)+$args);
      case 3:
        $translated_string=strtr($plural, array('@count' => '@plcount5+'));
        return t($translated_string, array('@plcount5+' => $count)+$args);
    }
  }
  else {
    return format_plural($count, $singular, $plural, $args, $options);
  }
}


/* My node class for creating node objects */
class my_node {
	
	function prepare($input_fields, $title=FALSE, $node_type, $nid=NULL, $lang=NULL, $uid=1) {
		
		if (isset($nid) && $nid) $node=node_load($nid);
		else $node=new stdClass();
		
		$node->type=$node_type;
		if ($title) $node->title=$title;
		elseif (empty($node->title)) $node->title='Untitled';
		$node->uid=$uid;
		
		if (isset($lang)) $node->language=$lang;
		else {
			$language=language_default();
			$node->language=$language->language;
		}
		
		foreach ($input_fields as $field_name => $field_data) {
			$this->node_field_value($node, $field_name, $field_data);
		}
		
		return $node;
	}

	private function node_field_value(&$node, $field_name, $field_data) {
		
		if (!$field_data) return;
		if ($field_info=field_info_field($field_name)) {
			$instance_info = field_info_instance('node', $field_name, $node->type);
			if (!$instance_info) return;
			
			if (isset($node->nid)) unset($node->{$field_name});
			if ($field_info['translatable']) $lang=$node->language;
			else $lang=LANGUAGE_NONE;
			
			switch($field_info['type']) {
				case 'taxonomy_term_reference':
					if (!is_array($field_data)) $field_data=array($field_data);
					foreach ($field_data as $tid) {
						if ($tid) $node->{$field_name}[$lang][] = array('tid' => (int) $tid);
					}
					break;
				case 'image':
				case 'file':

					$path = $field_info['settings']['uri_scheme'] . '://';
             
					if (isset($instance_info['settings']['file_directory'])) {
						// Set up and create path if it is not already set up
						$path.=$instance_info['settings']['file_directory'] . '/';
						if (!is_dir($path)) {
							drupal_mkdir($path, NULL, TRUE);
						}
					}
					
					if (isset($node->nid)) {
						$items=field_get_items('node', $node, $field_name);
            if (is_array($items)) {
              foreach ($items as $item) file_delete((object) $item);
            }
					}
						
					foreach ($field_data as $data) {

						if (isset($data['filename'])) {
							$base=substr($data['filename'], 0, strrpos($data['filename'], '.'));
							$ext=substr($data['filename'], strrpos($data['filename'], '.')+1);
							if (empty($ext)) $ext='';
							$destination=$path.$this->code_url_str($base, '_').'.'.$ext;
						}
						else $destination = $path . uniqid('my_node');
						
						if ($uri=file_unmanaged_save_data($data['content'], $destination, FILE_EXISTS_RENAME)) {
							if ($fid=db_query("SELECT fid FROM {file_managed} WHERE uri = :uri", array(':uri' => $uri))->fetchColumn()) {
								$file=file_load($fid);
							}
							else {
								global $user;
								$file=new stdClass();
								$file->fid=NULL;
								$file->uri=$uri;
								$file->filename=drupal_basename($uri);
								$file->filemime=file_get_mimetype($file->uri);
								$file->uid=$user->uid;
								$file->status=FILE_STATUS_PERMANENT;
								$file=file_save($file);
							}
						}
						
						// add file
						if ($file) {
							$file->display = 1;
							$node->{$field_name}[$lang][] = (array)$file;
						}
					}
					break;

				case 'text':
        case 'text_long':
				case 'text_with_summary':
          $default_format=filter_default_format();
          if (!$default_format) $default_format='full_html';
          
					if (!is_array($field_data)) $field_data=array($field_data);
					foreach ($field_data as $data) {
            if (is_array($data)) {
              if (!isset($data['value'])) break;
              if (!isset($data['summary'])) $data['summary']='';
              $node->{$field_name}[$lang][]=array(
                'value' => $data['value'],
                'summary' => $data['summary'],
                'format' => $default_format
              );
            }
            else {
              $node->{$field_name}[$lang][]=array(
                'value' => $data,
                'format' => $default_format
              );
            }
					}
					break;
					

				case 'date':
				case 'datestamp':
				case 'datetime':
				case 'number':
				case 'number_decimal':
				case 'number_float':
				case 'number_integer':
				case 'list_text':
				case 'list_integer':

					if (!is_array($field_data)) $field_data=array($field_data);
					foreach ($field_data as $data) {
            if (is_array($data)) $node->{$field_name}[$lang][]=$data;
            else {
  						$node->{$field_name}[$lang][]=array(
  							'value' => $data
  						);
            }
					}
					break;
					
				case 'options_onoff':
				case 'options_buttons':
					$node->{$field_name}=array();
					foreach ($field_data as $data) {
						if ($data && $data != 'false') $node->{$field_name}[$lang][]=array('value' => 1);
						else $node->{$field_name}[$lang][]=array('value' => 0);
					}
					break;
          
        case 'link_field':
					$node->{$field_name}=array();
					foreach ($field_data as $data) {
            if (!empty($data['url'])) {
              $node->{$field_name}[$lang][]=$data;
            }
          }
          break;
          
          
        /* Graber cart fields */
				case 'field_product_stock':
					if (!is_array($field_data)) $field_data=array($field_data);
					$field_data=array_shift($field_data);
					$node->{$field_name} = array();
					$node->{$field_name}[$lang][0]['product_stock']=$field_data;
					break;
					
				case 'offer_parameters':
					if (!is_array($field_data)) throw new Exception(t('Invalid data for offer_parameters field, array expected.'));
					$node->{$field_name}=array();
					$node->{$field_name}[$lang]=$field_data;
					break;
          
				case 'price_group':
					if (!is_array($field_data)) $field_data=array($field_data);
					$field_data=array_shift($field_data);
					$node->{$field_name} = array();
					$gid=db_query("SELECT gid FROM {price_groups} WHERE name = :name", array(':name' => $field_data))->fetchColumn();
					if ($gid) {
						$node->{$field_name}[$lang][0]['value']=$gid;
						if (!empty($node->field_product_price[LANGUAGE_NONE][0]['value'])) $node->{$field_name}[$lang][0]['usage']=0;
						else $node->{$field_name}[$lang][0]['usage']=1;
					}
					break;
          
        case 'product_reference':
					if (!is_array($field_data)) $field_data=array(
            'pid' => $field_data,
            'wid' => 1,
            'quantity' => 1
          );
          if (!isset($field_data['pid'])) throw new Exception(t('Invalid data for product_reference field.'));
					if (!isset($field_data['wid'])) $field_data['wid']=1;
					if (!isset($field_data['quantity'])) $field_data['quantity']=1;
          $node->{$field_name}[$lang][0]=$field_data;
          break;
					
				default:
					throw new Exception(t('Unsupported content field type [@field_type] for field [@field_name]. Please review your configuration.', array(
						'@field_name' => $field_name,
						'@field_type' => $field_info['type']    
					)));
					break;
			}
		}
		else {
			$node->$field_name = $field_data;
		}
	}
	
	private function convert_name(&$name) {
		if (substr($name, -15)==='(Kopia robocza)') {
			return FALSE;
		}
		else {
			$chars = array(
				'Ą'=>'A', 'Ć'=>'C', 'Ę'=>'E', 'Ł'=>'L', 'Ń'=>'N', 'Ó'=>'O', 'Ś'=>'S', 'Ż'=>'Z', 'Ź'=>'Z', 'ą'=>'a',	'ć'=>'c', 'ę'=>'e', 'ł'=>'l', 'ń'=>'n', 'ó'=>'o', 'ś'=>'s', 'ż'=>'z', 'ź'=>'z', ' '=>'_'
			);
			$name=preg_replace('/[^a-zA-Z0-9_]/s', '',strtr($name, $chars));
			$name=preg_replace('/[^(\x20-\x7F)]*/','', $name);
			return TRUE;
		}
	}
	
	private function code_url_str($string, $connector='-') {
		$chars=array(
			'Ą'=>'A', 'Ć'=>'C', 'Ę'=>'E', 'Ł'=>'L', 'Ń'=>'N', 'Ó'=>'O', 'Ś'=>'S', 'Ż'=>'Z', 'Ź'=>'Z', 'ą'=>'a',	'ć'=>'c', 'ę'=>'e', 'ł'=>'l', 'ń'=>'n', 'ó'=>'o', 'ś'=>'s', 'ż'=>'z', 'ź'=>'z', ' '=>'_'
		);
		$string=trim($string);
		$permalink = strtolower(strtr($string, $chars));
		return trim(str_replace(" ", $connector, preg_replace("/[^a-zA-Z0-9 ]/", $connector, $permalink) ), $connector);
	}	
}

function gp_get_field_data($fields) {
  $field_data=array();
  if (!is_array($fields)) $fields=array($fields);
  foreach ($fields as $field) {
    $field_info_instance=field_info_instance('product', $field, 'product');
    if ($field_info_instance) {
      $field_info=field_info_field($field);
      $field_data[$field]['label']=$field_info_instance['label'];
      $storage_info=$field_info['storage']['details']['sql']['FIELD_LOAD_CURRENT'];
      foreach ($storage_info as $table => $column_data) {
        foreach ($column_data as $name => $db_column) {
          $field_data[$field]['columns'][$name]=array(
            'table' => $table,
            'column' => $db_column
          );
        }
      }
    }
  }
  return $field_data;
}

//Module settings API
function _gc_apply_defaults(&$settings, $defaults, $deep=FALSE) {
  if (!is_array($settings)) $settings=array();
  foreach ($defaults as $key => $value) {
    if (!isset($settings[$key])) $settings[$key]=$value;
    elseif ($deep) {
      if (is_array($value)) _gc_apply_defaults($settings[$key], $value, TRUE);
    }
  }
}

function gp_get_settings($module, $setting=FALSE) {
  static $settings;
  if (!isset($settings[$module])) {
    $settings[$module]=variable_get($module.'_gp_settings', array());
    $defaults=module_invoke($module, 'gp_settings');
    _gc_apply_defaults($settings[$module], $defaults);
  }
  if ($setting) {
    if (isset($settings[$module][$setting])) return $settings[$module][$setting];
    else return FALSE;
  }
  else return $settings[$module];
}

function gp_save_settings($module, $value, $setting=FALSE, $flush=FALSE) {
  if (!$setting) {
    if (!$flush) {
      $settings=variable_get($module.'_gp_settings', array());
      _gc_apply_defaults($value, $settings);
    }
    variable_set($module.'_gp_settings', $value);
  }
  else {
    $settings=variable_get($module.'_gp_settings', array());
    $settings[$setting]=$value;
    variable_set($module.'_gp_settings', $settings);
  }
}

function gp_del_settings($module, $setting=FALSE) {
  if (!$setting) variable_del($module.'_gp_settings');
  else $settings=variable_get($module.'_gp_settings', array());
  if (isset($settings[$setting])) {
    unset($settings[$setting]);
    variable_set($module.'_gp_settings', $setting);
  }
}

//Machine name generation
function gp_machine_name($string) {
	static $replace;
	if (empty($replace)) {
		$replace=array('Ę' => 'e', 'Ó' => 'o', 'Ą' => 'a', 'Ś' => 's', 'Ł' => 'l', 'Ż' => 'z', 'Ź' => 'z', 'Ć' => 'c', 'Ń' => 'n', 'ę' => 'e', 'ó' => 'o', 'ą' => 'a', 'ś' => 's', 'ł' => 'l', 'ż' => 'z', 'ź' => 'z', 'ć' => 'c', 'ń' => 'n');
	}
  if (strlen($string) > 64) $string=substr($string, 0, 64);
  $output=preg_replace('/[^a-z0-9\-]/', '_', strtolower(strtr(trim($string), $replace)));
	return $output;
}

//Standard batch finished function
function gp_batch_finished($success, $results, $operations) {
  if ($success) {
    $Nres=count($results);
    $operations=array_count_values($results);
    $details=array();
    foreach ($operations as $op => $count) $details[]=t($op).': '.$count;
    $message = t('!nres operations performed: !operations.', array('!nres' => $Nres, '!operations' => implode(', ', $details)));
    drupal_set_message($message);
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}

//Add date popup widget functions
function gp_element_add_date_popup(&$element, $format='d.m.Y') {
  if (module_exists('date_popup')) {
    $element['#type']='date_popup';
    $element['#date_format']=$format;
    $element['#date_type']='U';
    if (!empty($element['#default_value'])) $element['#default_value']=date('Y-m-d', $element['#default_value']);
  }
}

function _gp_ts_from_date_format($date, $format) {
  $date=DateTime::createFromFormat($format, $date);
  if ($date) return $date->format("U");
  else return FALSE;
}

//Error reporting
function gp_watchdog($module, $message, $severity='warning') {
  $severities=array(
    'notice' => WATCHDOG_NOTICE,
    'warning' => WATCHDOG_WARNING,
    'error' => WATCHDOG_ERROR
  );
  $w_severity=(isset($severities[$severity])) ? $severities[$severity] : WATCHDOG_NOTICE;
  watchdog($module, $message, array(), $w_severity);
  if ($GLOBALS['user']->uid === 1 || in_array('Administrator', $GLOBALS['user']->roles)) drupal_set_message(t('Message from !module module: ', array('!module' => $module)).$message, $severity);
}

//Function to force loading template files from default theme directory even if admin theme is active. Usefull for sending e-mails from admin theme.
function gp_default_theme_templates(&$items, $module=FALSE) {
  $paths=array();
  
  $theme_name=variable_get('theme_default', FALSE);
  if ($theme_name) $theme_path=drupal_get_path('theme', $theme_name);
  if (isset($theme_path)) {
    $paths[]=$theme_path.'/templates';
    $paths[]=$theme_path;
  }
  
  if ($module) {
    $module_path=drupal_get_path('module', $module);
    if ($module_path) {
      $paths[]=$module_path.'/templates';
      $paths[]=$module_path;
    }
  }
  
  if (!empty($paths)) {
    foreach ($items as $key => $item) {
      if (!isset($item['template'])) continue;
      foreach ($paths as $path) {
        if (file_exists($path.'/'.$item['template'].'.tpl.php')) {
          $items[$key]['path']=$path;
          break;
        }
      }
    }
  }
}

function gp_confirm_form_elements($data, &$form) {
  if (isset($data['message'])) $form['message']=array('#markup' => '<div class="confirm-form-msg">'.$data['message'].'</div>');
  $form['back']=array(
    '#type' => 'submit',
    '#op' => 'confirm_back',
    '#value' => t('Back'),
    '#submit' => array('gp_confirm_form_elements_back')
  );
  $form[$data['op']]=array(
    '#type' => 'submit',
    '#op' => $data['op'],
    '#value' => $data['op_label']
  );
  $form['confirmed']=array('#type' => 'value', '#value' => TRUE);
  if (isset($data['data'])) $form['data']=array('#type' => 'value', '#value' => $data['data']);
  return $form;
}

function gp_confirm_form_elements_back($form, &$form_state) {
  $form_state['#rebuild']=TRUE;
}

function _gp_generate_random_code($length, $an=FALSE) {
  $characters='0123456789';
  if ($an) $characters.='ABCDEFGHIJKLMNOPRSTUWXYZ';
  $string='';
  for ($i=0; $i < $length; $i++) {
    $string.=$characters[rand(0, strlen($characters) - 1)];
  }
  return $string;
}

function gp_move_implementations(&$implementations, $hook, $module, $top=array(), $bottom=array()) {
  if (!empty($top)) {
    if (in_array($hook, $top)) {
      $group=$implementations[$module];
      unset($implementations[$module]);
      $new=array($module => FALSE);
      foreach ($implementations as $key => $value) $new[$key]=$value;
      $implementations=$new;
    }
  }
  if (!empty($bottom)) {
    if (in_array($hook, $bottom)) {
      $group=$implementations[$module];
      unset($implementations[$module]);
      $implementations[$module]=FALSE;
    }
  }
}

//Process key-value row format
function gp_process_form_kv_pairs(&$variable) {
  if (is_array($variable)) {
    $temp=array();
    foreach ($variable as $row) {
      foreach ($row as $key => $value) $temp[]=$key.'|'.$value;
    }
    $variable=implode(PHP_EOL, $temp);
  }
  elseif (!empty($variable)) {
    $rows=explode(PHP_EOL, $variable);
    $temp=array();
    foreach ($rows as $row) {
      $kv=explode('|', $row);
      if (isset($kv[1])) $temp[]=array($kv[0] => $kv[1]);
      else $temp[]=array($kv[0] => $kv[0]);
    }
    $variable=$temp;
  }
  else $variable='';
}

function gp_user_autocomplete_reverse($uid) {
  $name=db_query("SELECT name FROM {users} WHERE uid = :uid", array(':uid' => $uid))->fetchField();
  if ($name) return $name.' [uid='.$uid.']';
  else return '';
}

function gp_node_autocomplete_reverse($nid) {
  $name=db_query("SELECT title FROM {node} WHERE nid = :nid", array(':nid' => $nid))->fetchField();
  if ($name) return $name.' [nid='.$nid.']';
  else return '';
}

function gp_term_autocomplete_reverse($tid) {
  $name=db_query("SELECT title FROM {taxonomy_term_data} WHERE tid = :tid", array('tid' => $tid))->fetchField();
  if ($name) return $name.' [tid='.$tid.']';
  else return '';
}

function gp_validate_not_empty($elements, $form_state) {
  foreach ($elements as $element) {
    $value=drupal_array_get_nested_value($form_state['values'], $element['#parents']);
    if (empty($value)) {
      if (isset($element['#title'])) form_error($element, t('!name must not be empty.', array('!name' => $element['#title'])));
      else form_error($element, t('Field must not be empty.'));
    }
  }
}
