<?php
function theme_graber_tabledrag($vars) {
  $element=$vars['element'];
  //For multiple tabledrags on one page
  static $unique;
  if (!isset($unique)) $unique=0;
  else $unique++;
  
  $Nrows=0;
  $rows=array();
  $after_elements='';
  foreach (element_children($element) as $id) {
    if (isset($element[$id]['#type'])) {
      $after_elements.=drupal_render($element[$id]);
      continue;
    }
    if (!isset($first_id)) $first_id=$id;
    $element[$id]['weight']['#attributes']['class'] = array('tabledrag-item-weight');
    foreach ($element[$id] as $key => $cell_data) {
      if (substr($key, 0, 1) == '#') continue;
      if (isset($cell_data['#type']) && $cell_data['#type'] === 'value') continue;
      unset($cell_data['#title']);
      $rows[$Nrows]['data'][]=array('data' => drupal_render($cell_data));
    }
    $rows[$Nrows]['class']=array('draggable');
    $Nrows++;
  }
  
  $output='';
  if (isset($element['#prefix'])) $output=$element['#prefix'];
  if (isset($element['#title'])) $output.='<label>'.$element['#title'].'</label>';
  
  //Get header from the first row
  $header=array();
  if ($Nrows > 0) {
    foreach ($element[$first_id] as $key => $cell_data) {
      if (substr($key, 0, 1) == '#') continue;
      if (isset($cell_data['#type']) && $cell_data['#type'] === 'value') continue;
      if (isset($cell_data['#title'])) $header[]=$cell_data['#title'];
      elseif (isset($cell_data['#value'])) $header[]=$cell_data['#value'];
      else $header[]=$key;
    }
    $table_id=($unique) ? 'graber-tabledrag-'.$unique : 'graber-tabledrag';
    
    $output.=theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array('id' => $table_id),
      'sticky' => FALSE
    ));
    drupal_add_tabledrag($table_id, 'order', 'sibling', 'tabledrag-item-weight');
  }
  
  $output.=$after_elements;
  if (isset($element['#suffix'])) $output.=$element['#suffix'];
  return $output;
}

function theme_graber_form_table($vars) {
  $element=$vars['element'];
  $settings=empty($element['#settings']) ? array() : $element['#settings'];
  
  $settings+=array('row_numbers' => TRUE);
  
  if ($settings['row_numbers']) $header=array(t('No', array(), array('context' => 'number')));
  else $header=array();
  $rows=array();
  $Nrows=0;
  foreach ($element as $index => $data) {
    if (substr($index, 0, 1) === '#') continue;
    if (!empty($data['#outside'])) continue;
    $Nrows++;
    if ($settings['row_numbers']) {
      $rows[$Nrows]['data'][]=$Nrows;
      $data['#class']=array('row-no');
    }
    if (isset($data['#class'])) $rows[$Nrows]['class']=$data['#class'];
    foreach ($data as $prop => $cell) {
      if (substr($prop, 0, 1) != '#') {
        if ($Nrows == 1) {
          if (isset($cell['#title'])) $title=$cell['#title'];
          elseif (isset($cell['#value'])) $title=$cell['#value'];
          else $title='';
          $header[]=$title;
        }
        unset($cell['#title']);
        $rows[$Nrows]['data'][]=render($cell);
      }
    }
    unset($element[$index]);
  }
  
  if (empty($rows)) {
    if (!empty($vars['empty'])) return '<div class="gp-empty-table">'.$vars['empty'].'</div>';
    else return '';
  }
  //Render table
  $table=theme('table', array(
    'header' => $header, 
    'rows' => $rows, 
    'sticky' => FALSE, 
    'attributes' => array('class' => array('gp-form-table'))
  ));
  
  //Render extra elements under the table
  $extra=drupal_render_children($element);
  
  //Apply prefix and suffix if set
  $output='';
  if (isset($element['#prefix'])) $output.=$element['#prefix'];
  $output.=$table.$extra;
  if (isset($element['#suffix'])) $output.=$element['#suffix'];
  
  return $output;
}

function theme_image_upload_preview($variables) {
  $element=$variables['element'];
  $output='';
  if (isset($element['#default_value']) && $element['#default_value'] != 0) {
    // if image is uploaded show its thumbnail to the output HTML
    $output.='<div class="thumbnail">';
    $output.=theme('image_style', array('style_name' => 'thumbnail', 'path' => file_load($element['#default_value'])->uri));
    $output.='</div>';
  }
  unset($element['#title']);
  $element['#theme']='file';
  $output.=drupal_render($element); // renders rest of the element as usual
  return $output;
}
