<?php
function subiekt_integration_router($operation) {
  //var_monit($operation, 'AFa');
  $operation=explode(',', substr($operation, 0, -4));
  $sub=new subiekt_integration();
  $sub->login();
  
  if ($sub->authorized) {
    switch ($operation[0]) {
      case 'zaloguj':
        $sub->login_output();
        break;
      case 'plik':
        if (isset($operation[1])) {
          if ($operation[1] == 11) $sub->import_offer();
          elseif ($operation[1] == 235) $sub->export_statuses();
          elseif ($operation[1] == 4) $sub->export_orders_router();
        }
        break;
    }
  }
  else $sub->login_output();
  exit;
}


class subiekt_integration {
  var $authorized=FALSE;
  var $login_data=array();
  var $output='';
  
  function __construct() {
  }
  
  function login() {
    $login_data=variable_get('subiekt_login_data', array(
      'user' => 'test',
      'pass' => 'test'
    ));
    $this->authorized=FALSE;
    if (isset($_POST['auth_user']) && isset($_POST['auth_pwd'])) {
      if ($_POST['auth_user'] === $login_data['user'] && $_POST['auth_pwd'] === $login_data['pass']) $this->authorized=TRUE;
    }
  }
  
  function login_output() {
    if ($this->authorized) {
      $this->output.='<error>0</error>';
      $this->output.='<msg>Zalogowany: '.$_POST['auth_user'].'</msg>';
    }
    else {
      $this->output.='<error>1</error>';
      $this->output.='<msg>Incorrect username or password.</msg>';
    }
    
    $this->xml();
  }
  
  function import_offer() {
    if (empty($_POST['p_PN']) || empty($_POST['p_Nazwa'])) {
      $this->output.='<error>1</error>';
      $this->output.='<msg>Empty product identifier or title.</msg>';
      $this->xml();
      return;
    }
    
    $iid=$_POST['p_PN'];
    $nid=db_query("SELECT nid FROM {offer_importer_data} WHERE iid = :iid", array(':iid' => $iid))->fetchColumn();
    if ($nid) $op='update';
    else $op='create';
    
    if (function_exists('offer_importer_subiekt_field_map')) $data=offer_importer_subiekt_field_map();
    else {
      $data['title']=$_POST['p_Nazwa'];
      $data['node_type']='offer';
    
      $data['fields']=array(
        'body' => $_POST['p_Opis'],
        'field_product_price' => strtr($_POST['Brutto7'], array(',' => '.')),
        'field_symbol' => $iid,
        'field_product_stock' => intval(strtr($_POST['p_Ilosc'], array(',' => '.')))
      );
    }
    
    $my_node=new my_node();
    $node=$my_node->prepare($data['fields'], $data['title'], $data['node_type'], $nid);
    try {
      node_save($node);
    }
    catch (Exception $e) {
      $this->output.='<error>1</error>';
      $this->output.='<msg>Error in offer '.$iid.': '.$e->getMessage().'</msg>';
      $this->xml();
      return;
    }
          
    db_merge('offer_importer_data')->key(array('iid' => $iid))->fields(array(
      'iid' => $iid,
      'nid' => $node->nid,
      'last_sync' => REQUEST_TIME
    ))->execute();
    
    $this->output.='<error>0</error>';
    if ($op == 'update') $this->output.='<action>zmien</action>';
    if ($op == 'create') $this->output.='<action>dodaj</action>';
    $this->output.='<produkt>'.$node->nid.'</produkt>';
    $this->xml();
  }
  
  function export_statuses() {
    $statuses=graber_order_statuses();
    $this->output['statusy']['status']=array();
    $this->output.='<statusy>';
    foreach ($statuses as $id => $name) {
      $this->output.='<status id="'.$id.'" nazwa="'.$name.'"/>';
    }
    $this->output.='</statusy>';
    $this->xml();
  }
  
  function export_orders_router() {
    if ( isset($_POST['action']) && $_POST['action'] == 'status') $this->update_status();
    elseif ( isset($_POST['action']) && $_POST['action'] == 'status2') $this->blank();
    elseif ( isset($_POST['action']) && $_POST['action'] == 'flagi') $this->blank();
    else $this->export_orders();
  }
  
  function blank() {
    $this->output.="<error>0</error>";
    $this->output.="<msg>OK</msg>";
    $this->xml();
  }
  
  function update_status() {
    $success=FALSE;
    if (isset($_POST['dok_Id'])) {
      $order=new graber_order($_POST['dok_Id']);
      if (isset($order->oid)) {
        //$order->save(array('status' => 2));
        $this->output.="<error>0</error>";
        $this->output.="<msg>OK</msg>";
      }
      else {
        $this->output.="<error>1</error>";
        $this->output.="<msg>Order not found.</msg>";
      }
    }
    else {
      $this->output.="<error>1</error>";
      $this->output.="<msg>Order id missing.</msg>";
    }
    $this->xml();
  }
  
  function export_orders() {
    $last_export=0;
    $query=db_select('graber_orders', 'o')->fields('o');
    if ($last_export) $query->condition('modified', $last_export, '>');
    $query->condition('status', 1);
    $orders=$query->execute()->FetchAll();
    $this->output.="<webserwis>e1 Sklep</webserwis>";
    $this->output.="<dokumenty>";
    if (!empty($orders)) {
      foreach ($orders as $order) {
        
        $order->order_data=unserialize($order->order_data);
        $cart=new graber_cart($order->order_data['cid']);
        if (empty($cart->contents)) continue;
        
        $this->output.="<dokument>";
        
        //Dane zamówienia
        $this->output.='<dok_Id>'.$order->oid.'</dok_Id>';
        $this->output.='<dok_Licznik>'.$order->oid.'</dok_Licznik>';
        $this->output.='<dok_Numer>'.$order->oid.'</dok_Numer>';
        $this->output.='<dok_Referencja>0</dok_Referencja>';
        $this->output.='<dok_Typ>1</dok_Typ>';
        
        $order_date=date('m/d/Y', $order->created); //Format daty do sprawdzenia
        $this->output.='<dok_Data>'.$order_date.'</dok_Data>';
        $this->output.='<dok_DataRz>'.$order_date.'</dok_DataRz>';
        $this->output.='<dok_Klient>'.$order->uid.'</dok_Klient>';
        $this->output.='<dok_User>'.$order->uid.'</dok_User>';
        $this->output.='<dok_Platnosc>0</dok_Platnosc>';
        $this->output.='<dok_PlatnoscTekst></dok_PlatnoscTekst>';
        $this->output.='<dok_PlatnoscDni>0</dok_PlatnoscDni>';
        $this->output.='<dok_Bank>-</dok_Bank>';
        $this->output.='<dok_Konto>-</dok_Konto>';
        $this->output.='<dok_Host>127.0.0.1</dok_Host>';
        $this->output.='<dok_Suma>'.$order->amount.'</dok_Suma>';
        $this->output.='<dok_Status>'.$order->status.'</dok_Status>';
        $this->output.='<dok_Widok>1</dok_Widok>';
        
        //Tutaj pewnie trzeba będzie uwzględnić
        $this->output.='<dok_Waluta>PLN</dok_Waluta>'; 
        $this->output.='<dok_Kurs>1</dok_Kurs>';
        
        $this->output.='<dok_Prefix>/e1</dok_Prefix>';
        
        $name='';
        foreach (array('name', 'family', 'company') as $name_field) {
          if (isset($order->order_data['billing_addr'][$name_field])) {
            $name.=$order->order_data['billing_addr'][$name_field].' ';
          }
        }
        $name=substr($name, 0, -1);
        $this->output.='<f_Nazwa>'.$name.'</f_Nazwa>';
        
        $this->output.='<f_NIP>'.(empty($order->order_data['billing_addr']['tin']) ? '' : $order->order_data['billing_addr']['tin']).'</f_NIP>';
        $this->output.='<f_Mail>'.$order->mail.'</f_Mail>';
        $this->output.='<f_Id>'.$order->uid.'</f_Id>';
        $this->output.='<f_REGON></f_REGON>';
        $this->output.='<f_Telefon>'.(empty($order->order_data['billing_addr']['phone']) ? '' : $order->order_data['billing_addr']['phone']).'</f_Telefon>';
        $this->output.='<f_Faks></f_Faks>';
        $this->output.='<f_Zrodlo>0</f_Zrodlo>';
        $this->output.='<f_KluczObcy>0</f_KluczObcy>';
        $this->output.='<f_Modyfikacja></f_Modyfikacja>';
        $this->output.='<f_Dodanie></f_Dodanie>';
        $this->output.='<dok_Uwaga>'.$order->order_data['data']['remarks'].'</dok_Uwaga>';
        $this->output.='<dok_KGO></dok_KGO>';
        
        //Dane kupującego
        $this->output.='<user>';
        
        $this->output.='<u_Id>'.$order->uid.'</u_Id>';
        $this->output.='<u_User>'.$order->mail.'</u_User>';
        $this->output.='<u_Imie>'.(empty($order->order_data['billing_addr']['name']) ? '' : $order->order_data['billing_addr']['name']).'</u_Imie>';
        $this->output.='<u_Nazwisko>'.(empty($order->order_data['billing_addr']['family']) ? '' : $order->order_data['billing_addr']['family']).'</u_Nazwisko>';
        
        $this->output.='</user>';
        
        //Adresy
        $this->output.='<adresy>';
        
        //Kupujący
        $this->output.='<adres typ="1">';
        $this->output.='<dka_Id>1'.$order->oid.'</dka_Id>';
        $this->output.='<dka_Dokument>'.$order->oid.'</dka_Dokument>';
        $this->output.='<dka_Ulica>'.$order->order_data['billing_addr']['address'].'</dka_Ulica>';
        $this->output.='<dka_Kod>'.$order->order_data['billing_addr']['postal_code'].'</dka_Kod>';
        $this->output.='<dka_Miasto>'.$order->order_data['billing_addr']['city'].'</dka_Miasto>';
        $this->output.='<dka_Nazwa>'.$name.'</dka_Nazwa>';
        $this->output.='<dka_Typ>1</dka_Typ>';
        $this->output.='<dka_NIP>'.(empty($order->order_data['billing_addr']['tin']) ? '' : $order->order_data['billing_addr']['tin']).'</dka_NIP>';
        $this->output.='<dka_Kraj>'.(empty($order->order_data['billing_addr']['country']) ? '' : $order->order_data['billing_addr']['country']).'</dka_Kraj>';
          
        $this->output.='</adres>';
        
        //Dostawa
        $this->output.='<adres typ="2">';
        
        $this->output.='<dka_Id>2'.$order->oid.'</dka_Id>';
        $this->output.='<dka_Dokument>'.$order->oid.'</dka_Dokument>';
        $this->output.='<dka_Ulica>'.$order->order_data['shipping_addr']['address'].'</dka_Ulica>';
        $this->output.='<dka_Kod>'.$order->order_data['shipping_addr']['postal_code'].'</dka_Kod>';
        $this->output.='<dka_Miasto>'.$order->order_data['shipping_addr']['city'].'</dka_Miasto>';
        
        $shipping_name='';
        foreach (array('name', 'family', 'company') as $name_field) {
          if (isset($order->order_data['shipping_addr'][$name_field])) {
            $shipping_name.=$order->order_data['shipping_addr'][$name_field].' ';
          }
        }
        $this->output.='<dka_Nazwa>'.$shipping_name.'</dka_Nazwa>';
        $this->output.='<dka_Typ>2</dka_Typ>';
        $this->output.='<dka_NIP>'.(empty($order->order_data['shipping_addr']['tin']) ? '' : $order->order_data['shipping_addr']['tin']).'</dka_NIP>';
        $this->output.='<dka_Kraj>'.(empty($order->order_data['shipping_addr']['country']) ? '' : $order->order_data['shipping_addr']['country']).'</dka_Kraj>';
        
        $this->output.='</adres>';
        
        
        $this->output.='</adresy>';
        
        //Produkty
        $this->output.='<linie>';
                
        foreach ($cart->contents as $iid => $product) {
          $this->output.='<linia>';
          
          $this->output.='<dl_Id>'.$product['nid'].'</dl_Id>';
          $this->output.='<dl_Dokument>'.$order->oid.'</dl_Dokument>';
          $this->output.='<dl_Produkt>'.$product['nid'].'</dl_Produkt>';
          $this->output.='<dl_Nazwa>'.$product['title'].'</dl_Nazwa>';
          $this->output.='<dl_Symbol>'.(isset($product['node']->field_symbol[LANGUAGE_NONE][0]['value']) ? $product['node']->field_symbol[LANGUAGE_NONE][0]['value'] : $product['nid']).'</dl_Symbol>';
          
          //Zakładamy że w sklepie wyświetlają się ceny brutto ***
          if (!isset($product['vat'])) $product['vat']=23;
          $vat=round($product['price']*(1-1/(1+$product['vat']/100)), 2);
          $cenaNetto=$product['price']-$vat;
          
          $this->output.='<dl_CenaNetto>'.$cenaNetto.'</dl_CenaNetto>';
          $this->output.='<dl_CenaBrutto>'.$product['price'].'</dl_CenaBrutto>';
          $this->output.='<dl_CenaZakupu>'.$product['price'].'</dl_CenaZakupu>';
          $this->output.='<dl_Ilosc>'.$product['quantity'].'</dl_Ilosc>';
          $this->output.='<dl_Wartosc>'.($product['quantity']*$product['price']).'</dl_Wartosc>';
          $this->output.='<dl_Vat>0</dl_Vat>';
          $this->output.='<dl_VatS>'.$product['vat'].'</dl_VatS>';
          $this->output.='<dl_VatW>'.($product['vat']/100).'</dl_VatW>';
          $this->output.='<dl_JM>0</dl_JM>';
          $this->output.='<dl_JMs>szt</dl_JMs>';
          
          $this->output.='</linia>';
        }
        
        //Wysyłka
        if (isset($order->order_data['data']['shipping_price']) && ($order->order_data['data']['shipping_price'] != 0)) {
          $this->output.='<linia>';
          
          $this->output.='<dl_Id>999999</dl_Id>';
          $this->output.='<dl_Dokument>'.$order->oid.'</dl_Dokument>';
          $this->output.='<dl_Produkt>-1</dl_Produkt>';
          $this->output.='<dl_Nazwa>Koszty Dostawy</dl_Nazwa>';
          $this->output.='<dl_Symbol>Koszty Dostawy</dl_Symbol>';
          
          $vat=round($order->order_data['data']['shipping_price']*(1-1/(1+23/100)), 2);
          $cenaNetto=$order->order_data['data']['shipping_price']-$vat;
          $vat=round(($order->order_data['data']['shipping_price']*22/100), 2);

          $this->output.='<dl_CenaNetto>'.$cenaNetto.'</dl_CenaNetto>';
          $this->output.='<dl_CenaBrutto>'.$order->order_data['data']['shipping_price'].'</dl_CenaBrutto>';
          $this->output.='<dl_CenaZakupu>'.$order->order_data['data']['shipping_price'].'</dl_CenaZakupu>';
          $this->output.='<dl_Ilosc>1</dl_Ilosc>';
          $this->output.='<dl_Wartosc>'.$order->order_data['data']['shipping_price'].'</dl_Wartosc>';
          $this->output.='<dl_Vat>0</dl_Vat>';
          $this->output.='<dl_VatS>23</dl_VatS>';
          $this->output.='<dl_VatW>0.23</dl_VatW>';
          $this->output.='<dl_JM>0</dl_JM>';
          $this->output.='<dl_JMs>szt</dl_JMs>';
          $this->output.='</linia>';
        }
        $this->output.='</linie>';
        
        
        $this->output.="</dokument>";
      }
    }
    $this->output.="</dokumenty>";
    $this->xml();
  }
      
  private function xml() {
    $output='<?xml version="1.0" encoding="UTF-8" ?>'.PHP_EOL.'<koperta>';
    $output.=$this->output;
    $output.='</koperta>';
    //var_monit($output, 'AFa');
    header('Content-Type: text/xml');
    print $output;
    $this->output='';
  }
}
