<?php
function importer_offer_form_elements(&$form, &$form_state) {
  $form['offer_import']=array(
    '#type' => 'fieldset',
    '#title' => t('Offer synchronization from CSV'),
    '#tree' => TRUE
  );
      
  if (file_exists('private://import.csv')) {
    $form['#last_import']=filemtime('private://import.csv');
    $info=t('Import file from !date is available.', array('!date' => format_date($form['#last_import'], 'medium')));
  }
  else {
    $info=t('No import file available.');
    $form['#last_import']=0;
  }
  $form['offer_import']['info']['#markup']=$info;

  $form['offer_import']['import_file']=array(
    '#type' => 'file',
    '#title' => t('Import file'),
  );
    
  $form['offer_import']['upload']=array(
    '#type' => 'submit',
    '#value' => t('Upload')
  );
    
  if ($form['#last_import']) {
    $form['offer_import']['delete']=array(
      '#type' => 'checkbox',
      '#title' => t('Delete offers that are not in the csv.')
    );
    if (user_access('graber cart superadmin')) {
      $form['offer_import']['delete_all']=array(
        '#type' => 'checkbox',
        '#title' => t('Delete all earlier synchronised offers before import.')
      );
    }
    $form['offer_import']['synchronize']=array(
      '#type' => 'submit',
      '#value' => t('Synchronize offers')
    );
    
    $form['#validate'][]='offer_import_main_form_validate';
  }
}

function offer_import_main_form_validate($form, &$form_state) {
  if (!function_exists('offer_importer_offer_field_map')) form_set_error('', t('Field map definition does not exist.'));
}

function offer_importer_node_delete($node) {
  db_delete('offer_importer_data')->condition('nid', $node->nid)->execute();
}

function offer_importer_taxonomy_term_delete($term) {
  db_delete('offer_importer_taxonomy')->condition('tid', $term->tid)->execute();
}

function delete_imported_offers(&$context) {
  if (!isset($context['sandbox']['last'])) {
    $context['sandbox']['last']=0;
    $context['sandbox']['progress']=0;
    $context['sandbox']['total']=db_query("SELECT COUNT(nid) FROM {offer_importer_data}")->fetchColumn();
  }
  $nid=db_query("SELECT nid FROM {offer_importer_data} WHERE nid > :last ORDER BY nid ASC LIMIT 1", array(':last' => $context['sandbox']['last']))->fetchColumn();
  if ($nid) {
    node_delete($nid);
    $context['sandbox']['last']=$nid;
    $context['results'][$nid]='delete';
    $context['sandbox']['progress']++;
    $context['finished']=$context['sandbox']['progress']/$context['sandbox']['total'];
    $context['message']=t('Deleting offer !current of !total', array('!current' => $context['sandbox']['progress'], '!total' => $context['sandbox']['total']));
  }
}

function delete_imported_terms(&$context) {
  if (!isset($context['sandbox']['last'])) {
    $context['sandbox']['last']=0;
    $context['sandbox']['progress']=0;
    $context['sandbox']['total']=db_query("SELECT COUNT(tid) FROM {offer_importer_taxonomy}")->fetchColumn();
  }
  $tid=db_query("SELECT tid FROM {offer_importer_taxonomy} WHERE tid > :last ORDER BY tid ASC LIMIT 1", array(':last' => $context['sandbox']['last']))->fetchColumn();
  if ($tid) {
    taxonomy_term_delete($tid);
    $context['sandbox']['last']=$tid;
    $context['results'][$tid]='delete';
    $context['sandbox']['progress']++;
    $context['finished']=$context['sandbox']['progress']/$context['sandbox']['total'];
    $context['message']=t('Deleting term !current of !total', array('!current' => $context['sandbox']['progress'], '!total' => $context['sandbox']['total']));
  }
}

function import_single_offer($filepath, $encoding, &$context) {
  if (!isset($context['sandbox']['position'])) {
    $context['sandbox']['length']=filesize($filepath);
    $context['sandbox']['position']=0;
    $context['sandbox']['current']=1;
    $fh=fopen($filepath, 'r');
    $context['sandbox']['header']=_offer_importer_get_row($fh, $encoding);
    $context['sandbox']['types']=_offer_importer_get_row($fh, $encoding);
  }
  if (!isset($fh)) {
    $fh=fopen($filepath, 'r');
    fseek($fh, $context['sandbox']['position']);
  }
  
  if ($row=_offer_importer_get_row($fh, $encoding)) {
    $op='';
    
    $data=offer_importer_offer_field_map($row);
    if (empty($data)) $op='error';
    else {
      if (empty($data['iid']) || empty($data['node_type']) || empty($data['title'])) {
        if (empty($data['iid'])) drupal_set_message(t('Offer no !no has no identifier specified.', array('!no' => $context['sandbox']['current'])), 'error');
        if (empty($data['node_type'])) drupal_set_message(t('Offer no !no has no node type specified.', array('!no' => $context['sandbox']['current'])), 'error');
        if (empty($data['title'])) drupal_set_message(t('Offer no !no has no title.', array('!no' => $context['sandbox']['current'])), 'error');
        return;
      }
      
      $iid=$data['iid'];
    }
    
    if ($op !== 'error') {
      $my_node=new my_node();
      $nid=db_query("SELECT nid FROM {offer_importer_data} WHERE iid = :iid", array(':iid' => $iid))->fetchColumn();
      if ($nid) $op='update';
      else $op='create';
      $node=$my_node->prepare($data['fields'], $data['title'], $data['node_type'], $nid);
      
      try {
        node_save($node);
      }
      catch (Exception $e) {
        drupal_set_message(t('Error in offer !index.<br>', array('!index' => $iid)).$e->getMessage(), 'error');
        return;
      }
      
      db_merge('offer_importer_data')->key(array('iid' => $iid))->fields(array(
        'iid' => $iid,
        'nid' => $node->nid,
        'last_sync' => REQUEST_TIME
      ))->execute();
      
    }
    $context['results'][]=$op;
    $context['sandbox']['position']=ftell($fh);
    $context['sandbox']['current']++;
    $context['finished']=$context['sandbox']['position']/$context['sandbox']['length'];
    $context['message']=t('Now processing offer "!title" (offset !current of !total)', array('!title' => $node->title, '!current' => $context['sandbox']['position'], '!total' => $context['sandbox']['length']));
  }
}

function _oi_stock_sync($filepath, $encoding, &$context) {
  if (!isset($context['sandbox']['position'])) {
    $context['sandbox']['length']=filesize($filepath);
    $context['sandbox']['position']=0;
  }
  $fh=fopen($filepath, 'r');
  fseek($fh, $context['sandbox']['position']);
  
  if ($row=_offer_importer_get_row($fh, $encoding)) {
    $nid=db_query("SELECT nid FROM {offer_importer_data} WHERE iid = :code", array(':code' => $row[0]))->fetchColumn();
    if ($nid) {
      $node=node_load($nid);
      if ($node) {
        $node->field_product_stock[LANGUAGE_NONE][0]['product_stock']=$row[1];
        field_attach_update('node', $node);
        $op='update';
      }
      else $op='node missing';
    }
    else $op='missing';
        
    $context['results'][]=$op;
    $context['sandbox']['position']=ftell($fh);
    $context['finished']=$context['sandbox']['position']/$context['sandbox']['length'];
    $context['message']=t('Now processing offset !current of !total', array('!current' => $context['sandbox']['position'], '!total' => $context['sandbox']['length']));
  }
}

function check_offer_deletions($filepath, &$context) {
  if (!isset($context['sandbox']['available'])) {
    $context['sandbox']['available']=array();
    $context['sandbox']['current']=0;
    $context['sandbox']['progress']=0;
    $context['sandbox']['total']=db_query("SELECT COUNT(iid) FROM {offer_importer_data}")->fetchColumn();
    $fh=fopen($filepath, 'r');
    static $Ncells;
    for ($i=0; $i < 2; $i++) $row=fgetcsv($fh, 0, ';');
    while ($row=fgetcsv($fh, 0, ';')) {
      if (!isset($Ncells)) $Ncells=sizeof($row);
      $context['sandbox']['available'][]=$row[18];
    }
  }
  
  $data=db_query("SELECT iid, nid FROM {offer_importer_data} WHERE nid > :nid ORDER BY nid ASC LIMIT 1", array(':nid' => $context['sandbox']['current']))->fetchObject();
  if ($data) {
    if (!in_array($data->iid, $context['sandbox']['available'])) {
      node_delete($data->nid);
      $context['results'][]='delete';
    }
    
    $context['sandbox']['current']=$data->nid;
    $context['sandbox']['progress']++;
    $context['finished']=$context['sandbox']['progress']/$context['sandbox']['total'];
    $context['message']=t('Now checking offer !current of !total', array('!current' => $context['sandbox']['progress'], '!total' => $context['sandbox']['total']));
  }
}

function _oi_stock_sync_checkall($filepath, &$context) {
  if (!isset($context['sandbox']['available'])) {
    $context['sandbox']['available']=array();
    $context['sandbox']['current']=0;
    $context['sandbox']['progress']=0;
    $context['sandbox']['total']=db_query("SELECT COUNT(nid) FROM {offer_importer_data}")->fetchColumn();
    $fh=fopen($filepath, 'r');
    static $Ncells;
    while ($row=fgetcsv($fh, 0, ';')) {
      if (!isset($Ncells)) $Ncells=sizeof($row);
      if (!empty($row[0])) $context['sandbox']['available'][]=$row[0];
    }
  }
  $data=db_query("SELECT nid, iid FROM {offer_importer_data} WHERE nid > :nid ORDER BY nid ASC LIMIT 1", array(':nid' => $context['sandbox']['current']))->fetchObject();
  if ($data) {
    if (empty($data->iid)) $op='no iid';
    elseif (!in_array($data->iid, $context['sandbox']['available'])) {
      $node->field_product_stock[LANGUAGE_NONE][0]['product_stock']=0;
      field_attach_update('node', $node);
      $op='out of stock';
    }
    $context['results'][]=$op;
    $context['sandbox']['current']=$data->nid;
    $context['sandbox']['progress']++;
    $context['finished']=$context['sandbox']['progress']/$context['sandbox']['total'];
    $context['message']=t('Now checking offer !current of !total', array('!current' => $context['sandbox']['progress'], '!total' => $context['sandbox']['total']));
  }
}
