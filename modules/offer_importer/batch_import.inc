<?php
function _oi_batch_import($filepath, $encoding, $data) {
  $warehouse_version=(int) db_query("SELECT schema_version FROM {system} WHERE (type = 'module' AND name = 'warehouse')")->fetchColumn();
  $fh=fopen($filepath, 'r');
  $header=_offer_importer_get_row($fh, $encoding);
  $types=_offer_importer_get_row($fh, $encoding);
  
  $batches=array();
  $Nbatches=0;
  $error=FALSE;
  while ($row=_offer_importer_get_row($fh, $encoding)) {
    $pid=FALSE;
    if (sizeof($row > 2)) {
      $batch_data=array(
        'barcode' => $row[0],
        'quantity' => trim(strtr($row[1], array(',' => '.'))),
        'cost' => trim(strtr($row[2], array(',' => '.')))
      );
      
      if ($warehouse_version < 7140) {
        $pid=db_query("SELECT pid FROM {warehouse_products} WHERE barcode = :barcode", array(':barcode' => $batch_data['barcode']))->fetchColumn();
      }
      else {
        $query=new entityFieldQuery();
        $query->entityCondition('entity_type', 'product');
        $query->fieldCondition('field_product_barcode', 'value', $batch_data['barcode']);
        $results=$query->execute();
        if (!empty($results['product'])) {
          $product=reset($results['product']);
          $pid=$product->pid;
        }
      }
    }
    
    if ($pid) {
      $batches[$Nbatches]=array(
        'product' => $pid,
        'quantity' => $batch_data['quantity'],
        'cost' => $batch_data['cost'],
        'remarks' => isset($data['remarks']) ? $data['remarks'] : t('imported')
      );
      $Nbatches++;
    }
    else {
      if (isset($batch_data['barcode'])) {
        drupal_set_message(t('There is no product with barcode: !barcode in database. Import cancelled.', array('!barcode' => $batch_data['barcode'])));
      }
      else drupal_set_message(t('Error in import file.'));
      $error=TRUE;
    }
  }
  if ($Nbatches && !$error) {
    $form_state['values']['batches']=$batches;
    $form_state['values']['op']=t('Save data');
    $form_state['values']['code']=$data['invoice_no'];
    $form_state['values']['currency']='PLN';
    $form_state['values']['received']=REQUEST_TIME;
    $form['#wid']=$data['wid'];
    module_load_include('inc', 'warehouse', 'warehouse.pages');
    warehouse_batch_bulk_form_submit($form, $form_state);
  }
}