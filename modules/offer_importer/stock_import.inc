<?php
function importer_stock_form_elements(&$form, &$form_state) {
  if (module_exists('product_stock')) {
    $form['stock_import']=array(
      '#type' => 'fieldset',
      '#title' => t('Stock synchronization'),
      '#tree' => TRUE
    );
      
    if (file_exists('private://stock.txt')) {
      $form['#last_stock_import']=filemtime('private://stock.txt');
      $info=t('Stock file from !date is available.', array('!date' => format_date($form['#last_stock_import'], 'medium')));
    }
    else {
      $info=t('No stock file available.');
      $form['#last_stock_import']=0;
    }
    $form['stock_import']['info']['#markup']=$info;
    
    $form['stock_import']['stock_file']=array(
      '#type' => 'file',
      '#title' => t('Stock file'),
    );
      
    $form['stock_import']['upload']=array(
      '#type' => 'submit',
      '#value' => t('Upload')
    );
    
    if ($form['#last_stock_import']) {
      $form['stock_import']['include_all']=array(
        '#type' => 'checkbox',
        '#title' => t('Set stock to 0 for offers that are not included in the csv.')
      );
      $form['stock_import']['sync_stock']=array(
        '#type' => 'submit',
        '#value' => t('Update stock')
      );
    }
  }
}

function importer_batch_form_elements(&$form, &$form_state) {
  if (module_exists('warehouse')) {
    $form['batch_import']=array(
      '#type' => 'fieldset',
      '#title' => t('Product batch import'),
      '#tree' => TRUE
    );
      
    $form['batch_import']['batch_file']=array(
      '#type' => 'file',
      '#title' => t('Import file'),
    );
    
    $warehouses=_warehouse_get_warehouses();
    $form['batch_import']['warehouse']=array(
      '#type' => 'select',
      '#title' => t('Warehouse'),
      '#options' => array(0 => t('Select one'))+$warehouses
    );
    
    $form['batch_import']['invoice_no']=array(
      '#type' => 'textfield',
      '#title' => t('Invoice no.')
    );
    
    $form['batch_import']['remarks']=array(
      '#type' => 'textfield',
      '#title' => t('Remarks'),
      '#default_value' => t('imported')
    );
    
    $form['batch_import']['sync_batches']=array(
      '#type' => 'submit',
      '#value' => t('Import batches')
    );
  }
}

function _oi_batch_import($filepath, $encoding, $data) {
  $warehouse_version=(int) db_query("SELECT schema_version FROM {system} WHERE (type = 'module' AND name = 'warehouse')")->fetchColumn();
  $fh=fopen($filepath, 'r');
  $header=_offer_importer_get_row($fh, $encoding);
  $types=_offer_importer_get_row($fh, $encoding);
  
  $batches=array();
  $Nbatches=0;
  $error=FALSE;
  while ($row=_offer_importer_get_row($fh, $encoding)) {
    $pid=FALSE;
    if (sizeof($row > 2)) {
      $batch_data=array(
        'barcode' => $row[0],
        'quantity' => trim(strtr($row[1], array(',' => '.'))),
        'cost' => trim(strtr($row[2], array(',' => '.')))
      );
      
      $query=new entityFieldQuery();
      $query->entityCondition('entity_type', 'product');
      $query->fieldCondition('field_product_barcode', 'value', $batch_data['barcode']);
      $results=$query->execute();
      if (!empty($results['product'])) {
        $product=reset($results['product']);
        $pid=$product->pid;
      }
    }
    
    if ($pid) {
      $batches[$Nbatches]=array(
        'product' => $pid,
        'quantity' => $batch_data['quantity'],
        'cost' => $batch_data['cost'],
        'remarks' => isset($data['remarks']) ? $data['remarks'] : t('imported')
      );
      $Nbatches++;
    }
    else {
      if (isset($batch_data['barcode'])) {
        drupal_set_message(t('There is no product with barcode: !barcode in database. Import cancelled.', array('!barcode' => $batch_data['barcode'])));
      }
      else drupal_set_message(t('Error in import file.'));
      $error=TRUE;
    }
  }
  if ($Nbatches && !$error) {
    $form_state['values']['batches']=$batches;
    $form_state['values']['op']=t('Save data');
    $form_state['values']['code']=$data['invoice_no'];
    $form_state['values']['currency']='PLN';
    $form_state['values']['received']=REQUEST_TIME;
    $form['#wid']=$data['wid'];
    module_load_include('inc', 'warehouse', 'warehouse.pages');
    warehouse_batch_bulk_form_submit($form, $form_state);
  }
}