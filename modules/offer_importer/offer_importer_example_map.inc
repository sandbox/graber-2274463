<?php function offer_importer_offer_field_map ($row) {
  $data=array();
  $data['iid']=$row[1];
  $data['title']=$row[2];
  $data['node_type']='offer';
  
  //Multivalue fields
  $exploadable=array(5, 6, 7);
  foreach ($exploadable as $index) {
    $array=explode(',', $row[$index]);
    foreach ($array as $key => $value) $array[$key]=(int) trim($value);
    $row[$index]=$array;
  }
    
  $data['fields']=array(
    'body' => $row[3],
    'field_indeks_produktu' => $row[4],
    'field_kategoria' => $row[5],
    'field_kategoria_dodatkowa' => $row[6],
    'field_seria_produktu' => $row[7],
  );
  
  //Images
  $images=explode(',', $row[8]);
  foreach ($images as $address) {
    $address=trim($address);
    if (!empty($address)) {
      
      //Image within internal file system
      if (substr($address, 0, 4) != 'http') {
        $filepath='public://'.trim($address);
        if (file_exists($filepath) && is_file($filepath)) {
          $fields['field_product_image'][]=array(
            'filename' => basename($filepath),
            'content' => file_get_contents($filepath)
          );
        }
      }
      
      //External image
      else {
        if ($contents=file_get_contents($address)) {
          $fields['field_product_image'][]=array(
            'filename' => basename($address),
            'content' => $contents
          );
        }
      }
    }
  }
  
  //Warehouse product reference field
  if (module_exists('warehouse')) {
    if ($pid=db_select('offer_importer_product_data', 'pd')->fields('pd', array('pid'))->condition('iid', $data['iid'])->execute()->fetchColumn()) {
      $fields['field_product_reference']=array(
        'pid' => $pid,
        'wid' => empty($row[9]) ? 1 : $row[9],
        'quantity' => 1
      );
    }      
  }
  
  return $data;
}
