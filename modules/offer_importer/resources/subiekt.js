(function ($) {
  $(document).ready(function() {
    $('#oi-subiekt-import').click(function(event) {
      event.preventDefault();

      Shell = new ActiveXObject("WScript.Shell");
      oGT = new ActiveXObject("InsERT.GT");
        
      UruchomSubiekta();

      function UruchomSubiekta() {
        try {
          oGT = new ActiveXObject("InsERT.GT");

          // Podanie parametrów połączenia
          oGT.Produkt = 1;
          oGT.Wczytaj("C:\\ProgramData\\InsERT\\InsERT GT\\Subiekt.xml");
          oSubiekt = oGT.Uruchom(0,0);
          if (oSubiekt != null) {
            console.log("Nazwa okna podłączonego Subiekta: " + oSubiekt.Okno.Tytul);
            oSubiekt.Okno.Widoczne = true;
          }
        }
        catch(e) {
          console.log("Nie udało się połączyć z Subiektem GT. " + e.Description);
        }
      }
    });
  });
})(jQuery);
