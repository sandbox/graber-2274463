<?php
function offer_importer_settings_form($form, &$form_state) {
  $settings=gp_get_settings('offer_importer');
  $options=array(
    'subiekt_gt' => t('Integration with Subiekt GT Sfera'),
    'offer_csv' => t('Offer import from csv'),
  );
  if (module_exists('aqua_import')) $options['aqua_import']=t('Import from Aquavip platform');
  if (module_exists('product_stock')) $options['stock_import']=t('Import product stock to offer field (no warehouse).');
  if (module_exists('warehouse')) {
    $options['product_import']=t('Import products from csv.');  
    $options['batch_import']=t('Import product batches from csv.');  
  }
  $form['methods']=array(
    '#type' => 'checkboxes',
    '#title' => t('Available methods'),
    '#options' => $options,
    '#default_value' => empty($settings['methods']) ? array() : $settings['methods']
  );
    
  $form['save_data']=array(
    '#type' => 'submit',
    '#value' => t('Save settings')
  );
  return $form;
}

function offer_importer_settings_form_submit($form, &$form_state) {
  $settings=array(
    'methods' => $form_state['values']['methods']
  );
  gp_save_settings('offer_importer', $settings);
}