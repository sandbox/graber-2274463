<?php
function importer_product_form_elements(&$form, &$form_state) {
  if (module_exists('warehouse')) {
    $form['product_import']=array(
      '#type' => 'fieldset',
      '#title' => t('Product synchronization'),
      '#tree' => TRUE
    );
      
    if (file_exists('private://product_import.csv')) {
      $form['#last_product_import']=filemtime('private://product_import.csv');
      $info=t('Import file from !date is available.', array('!date' => format_date($form['#last_product_import'], 'medium')));
    }
    else {
      $info=t('No import file available.');
      $form['#last_product_import']=0;
    }
    $form['product_import']['info']['#markup']=$info;
    
    $form['product_import']['product_file']=array(
      '#type' => 'file',
      '#title' => t('Import file'),
    );
      
    $form['product_import']['upload']=array(
      '#type' => 'submit',
      '#value' => t('Upload')
    );
    
    if ($form['#last_product_import']) {
      $form['product_import']['delete']=array(
        '#type' => 'checkbox',
        '#title' => t('Delete products that are not in the csv.')
      );
      if (user_access('graber cart superadmin')) {
        $form['product_import']['delete_all']=array(
          '#type' => 'checkbox',
          '#title' => t('Delete all earlier synchronised products before import.')
        );
      }
      $form['product_import']['sync_products']=array(
        '#type' => 'submit',
        '#value' => t('Synchronize products')
      );
    }
  }
}

function _oi_delete_product($pid) {
  db_delete('warehouse_quantities')->condition('pid', $pid)->execute();
  db_delete('warehouse_batches')->condition('pid', $pid)->execute();
  db_delete('warehouse_products')->condition('pid', $pid)->execute();
  db_delete('offer_importer_product_data')->condition('pid', $pid)->execute();
}

//Delete products
function delete_imported_products_old(&$context) {
	if (!isset($context['sandbox']['last'])) {
		$context['sandbox']['last']=0;
		$context['sandbox']['progress']=0;
		$context['sandbox']['total']=db_query("SELECT COUNT(pid) FROM {offer_importer_product_data}")->fetchColumn();
	}
	$pid=db_query("SELECT pid FROM {offer_importer_product_data} WHERE pid > :last ORDER BY pid ASC LIMIT 1", array(':last' => $context['sandbox']['last']))->fetchColumn();
	if ($pid) {
    _oi_delete_product($pid);
		$context['sandbox']['last']=$pid;
		$context['results'][$pid]='delete';
		$context['sandbox']['progress']++;
		$context['finished']=$context['sandbox']['progress']/$context['sandbox']['total'];
		$context['message']=t('Deleting product !current of !total', array('!current' => $context['sandbox']['progress'], '!total' => $context['sandbox']['total']));
	}
}

function delete_imported_products(&$context) {
	if (!isset($context['sandbox']['last'])) {
		$context['sandbox']['last']=0;
		$context['sandbox']['progress']=0;
		$context['sandbox']['total']=db_query("SELECT COUNT(pid) FROM {offer_importer_product_data}")->fetchColumn();
	}
	$pid=db_query("SELECT pid FROM {offer_importer_product_data} WHERE pid > :last ORDER BY pid ASC LIMIT 1", array(':last' => $context['sandbox']['last']))->fetchColumn();
	if ($pid) {
    $product=product_load($pid);
    product_delete($product);
		$context['sandbox']['last']=$pid;
		$context['results'][$pid]='delete';
		$context['sandbox']['progress']++;
		$context['finished']=$context['sandbox']['progress']/$context['sandbox']['total'];
		$context['message']=t('Deleting product !current of !total', array('!current' => $context['sandbox']['progress'], '!total' => $context['sandbox']['total']));
	}
}

//Import product
function import_single_product_old($filepath, $encoding, &$context) {
	if (!isset($context['sandbox']['position'])) {
		$context['sandbox']['length']=filesize($filepath);
		$context['sandbox']['position']=0;
		$fh=fopen($filepath, 'r');
		$context['sandbox']['header']=_offer_importer_get_row($fh, $encoding);
		$context['sandbox']['types']=_offer_importer_get_row($fh, $encoding);
		$context['sandbox']['position']=ftell($fh);
		$context['sandbox']['row']=0;
	}
	if (!isset($fh)) {
		$fh=fopen($filepath, 'r');
		fseek($fh, $context['sandbox']['position']);
	}
	
	if ($row=_offer_importer_get_row($fh, $encoding)) {
    $context['sandbox']['row']++;
		$op='';
		$iid=$row[3];
    if (!$iid) {
      $op='error';
      drupal_set_message(t('Product index (Excel column D) invalid for row !number', array('!number' => $context['sandbox']['row'])), 'error');
    }
    
		$definitions=array(
      0 => array(
        'field' => 'name',
        'type' => 'string'
      ),
      1 => array(
        'field' => 'unit',
        'type' => 'string'
      ),
      2 => array(
        'field' => 'vat',
        'type' => 'number'
      ),
      3 => array(
        'field' => 'product_index',
        'type' => 'string'
      ),
      4 => array(
        'field' => 'barcode',
        'type' => 'string'
      ),
      5 => array(
        'field' => 'min_stock',
        'type' => 'int'
      ),
      6 => array(
        'field' => 'min_order',
        'type' => 'int'
      ),
      7 => array(
        'field' => 'category',
        'type' => 'string'
      ),
      8 => array(
        'field' => 'bestseller',
        'type' => 'bool'
      ),
      9 => array(
        'field' => 'old_stock',
        'type' => 'bool'
      ),
      10 => array(
        'field' => 'reccommended_price',
        'type' => 'number'
      ),
      11 => array(
        'field' => 'supplier',
        'type' => 'string'
      )
    );

    $fields=array();
    foreach ($row as $index => $value) {
      if (isset($definitions[$index])) {
        if (db_field_exists('warehouse_products', $definitions[$index]['field'])) {
          switch ($definitions[$index]['type']) {
            case 'string':
              $fields[$definitions[$index]['field']]=$value;
              break;
            case 'number':
              $fields[$definitions[$index]['field']]=strtr($value, array(',' => '.'));
              if (!is_numeric($fields[$definitions[$index]['field']])) {
                $op='error';
                drupal_set_message(t('Number format error in field !field of product !index.', array('!field' => $context['sandbox']['header'][$index], '!index' => $iid)));
              }
              break;
            case 'int':
              $fields[$definitions[$index]['field']]=intval($value);
              break;
            case 'bool':
              if (empty($value) || $value == '0') $fields[$definitions[$index]['field']]=0;
              else $fields[$definitions[$index]['field']]=1;
              break;
          }
        }
      }
    }

		if ($op !== 'error') {
      //add product index to product name
      //$fields['name']=$fields['name'].' ('.$fields['product_index'].')';
			if ($pid=db_select('offer_importer_product_data', 'pd')->fields('pd', array('pid'))->condition('iid', $iid)->execute()->FetchColumn()) {
        $op='update';
        $fields['modified']=REQUEST_TIME;
        db_update('warehouse_products')->fields($fields)->condition('pid', $pid)->execute();
      }
      else {
        $op='create';
        $fields['modified']=$fields['created']=REQUEST_TIME;
        $pid=db_insert('warehouse_products')->fields($fields)->execute();
        
        //set quantities to 0
        $warehouses=db_query("SELECT wid FROM {warehouses}")->fetchAllKeyed(0, 0);
        foreach ($warehouses as $wid) {
          $quantity=0;        
          db_merge('warehouse_quantities')->key(array('wid' => $wid, 'pid' => $pid))->fields(array(
            'wid' => $wid,
            'pid' => $pid,
            'stock' => $quantity,
            'available' => $quantity
          ))->execute();
        }
      }
      
			db_merge('offer_importer_product_data')->key(array('iid' => $iid))->fields(array(
				'iid' => $iid,
				'barcode' => $row[4],
				'pid' => $pid,
				'last_sync' => REQUEST_TIME
			))->execute();
			
			$context['results'][$iid]=$op;
			$context['sandbox']['position']=ftell($fh);
			$context['finished']=$context['sandbox']['position']/$context['sandbox']['length'];
			$context['message']=t('Now processing product "!title" (offset !current of !total)', array('!title' => $row[0], '!current' => $context['sandbox']['position'], '!total' => $context['sandbox']['length']));
		}
	}
}

function import_single_product($filepath, $encoding, &$context) {
	if (!isset($context['sandbox']['position'])) {
		$context['sandbox']['length']=filesize($filepath);
		$context['sandbox']['position']=0;
		$fh=fopen($filepath, 'r');
		$context['sandbox']['header']=_offer_importer_get_row($fh, $encoding);
		$context['sandbox']['types']=_offer_importer_get_row($fh, $encoding);
		$context['sandbox']['position']=ftell($fh);
		$context['sandbox']['row']=0;
	}
	if (!isset($fh)) {
		$fh=fopen($filepath, 'r');
		fseek($fh, $context['sandbox']['position']);
	}
	
	if ($row=_offer_importer_get_row($fh, $encoding)) {
    $context['sandbox']['row']++;
		$op='';
		$iid=$row[3];
    if (!$iid) {
      $op='error';
      drupal_set_message(t('Product index (Excel column D) invalid for row !number', array('!number' => $context['sandbox']['row'])), 'error');
    }
    else {
			if ($pid=db_select('offer_importer_product_data', 'pd')->fields('pd', array('pid'))->condition('iid', $iid)->execute()->FetchColumn()) {
        $op='update';
        $product=product_load($pid);
      }
      else {
        $op='create';
        $product=product_create();
      }
    }
    
    if ($op !== 'error') {
    
      $definitions=array(
        0 => array(
          'name' => 'name',
          'type' => 'property',
          'data_type' => 'string'
        ),
        1 => array(
          'name' => 'unit',
          'type' => 'property',
          'data_type' => 'string'
        ),
        2 => array(
          'name' => 'vat',
          'type' => 'property',
          'data_type' => 'number'
        ),
        3 => array(
          'name' => 'field_product_index',
          'type' => 'field',
          'data_type' => 'string'
        ),
        4 => array(
          'name' => 'field_product_barcode',
          'type' => 'field',
          'data_type' => 'string'
        ),
        5 => array(
          'name' => 'field_min_stock',
          'type' => 'field',
          'data_type' => 'int'
        ),
        6 => array(
          'name' => 'field_min_order',
          'type' => 'field',
          'data_type' => 'int'
        ),
        7 => array(
          'name' => 'field_category',
          'type' => 'field',
          'data_type' => 'string'
        ),
        8 => array(
          'name' => 'field_bestseller',
          'type' => 'field',
          'data_type' => 'bool'
        ),
        9 => array(
          'name' => 'field_old_stock',
          'type' => 'field',
          'data_type' => 'bool'
        ),
        10 => array(
          'name' => 'field_reccommended_price',
          'type' => 'field',
          'data_type' => 'number'
        ),
        11 => array(
          'name' => 'field_dostawca',
          'type' => 'field',
          'data_type' => 'string'
        )
      );

      $instance_fields=field_info_instances('product', 'product');
      
      foreach ($row as $index => $value) {
        if (isset($definitions[$index])) {
        
          switch ($definitions[$index]['data_type']) {
            case 'string':
              $field_value=$value;
              break;
            case 'number':
              $field_value=strtr($value, array(',' => '.'));
              if (!is_numeric($field_value)) {
                $op='error';
                drupal_set_message(t('Number format error in field !field of product !index.', array('!field' => $context['sandbox']['header'][$index], '!index' => $iid)));
              }
              break;
            case 'int':
              $field_value=intval($value);
              break;
            case 'bool':
              if (empty($value) || $value == '0') $field_value=0;
              else $field_value=1;
              break;
            case 'array':
              $field_value=explode(',', $value);
              foreach ($field_value as $index => $single_element) {
                $field_value[$index]=trim($single_element);
              }
              break;
          }
          if ($definitions[$index]['type'] == 'property') $product->{$definitions[$index]['name']}=$field_value;
          elseif ($definitions[$index]['type'] == 'field') {
            if (isset($instance_fields[$definitions[$index]['name']])) {
            
              if (isset($definitions[$index]['column'])) $column=$definitions[$index]['column'];
              else $column='value';
              if (!is_array($field_value)) $field_value=array($field_value);
              
              if (isset($product->{$definitions[$index]['name']})) $product->{$definitions[$index]['name']}=array();
              foreach ($field_value as $delta => $value) {
                $product->{$definitions[$index]['name']}[LANGUAGE_NONE][$delta][$column]=$value;
              }
            }
          }
        }
      }
    }
    
		if ($op !== 'error') {
      if ($op == 'create') {
        $warehouses=db_query("SELECT wid FROM {warehouses}")->fetchAllKeyed(0, 0);
        foreach ($warehouses as $wid) {
          $quantity=0;
          $product->field_warehouse_quantities[LANGUAGE_NONE][]=array(
            'wid' => $wid,
            'stock' => $quantity,
            'available' => $quantity
          );
        }
      }
      
      product_save($product);
      
			db_merge('offer_importer_product_data')->key(array('iid' => $iid))->fields(array(
				'iid' => $iid,
				'barcode' => $row[4],
				'pid' => $product->pid,
				'last_sync' => REQUEST_TIME
			))->execute();
			
			$context['results'][$iid]=$op;
			$context['sandbox']['position']=ftell($fh);
			$context['finished']=$context['sandbox']['position']/$context['sandbox']['length'];
			$context['message']=t('Now processing product "!title" (offset !current of !total)', array('!title' => $row[0], '!current' => $context['sandbox']['position'], '!total' => $context['sandbox']['length']));
		}
	}
}

//Check deletions
function check_product_deletions_old($filepath, &$context) {
	if (!isset($context['sandbox']['available'])) {
		$context['sandbox']['available']=array();
		$context['sandbox']['current']=0;
		$context['sandbox']['progress']=0;
		$context['sandbox']['total']=db_query("SELECT COUNT(iid) FROM {offer_importer_data}")->fetchColumn();
		$fh=fopen($filepath, 'r');
		for ($i=0; $i < 2; $i++) $row=fgetcsv($fh, 0, ';');
		while ($row=fgetcsv($fh, 0, ';')) {
			$context['sandbox']['available'][]=$row[2];
		}
	}
	
	$data=db_query("SELECT iid, pid FROM {offer_importer_product_data} WHERE pid > :pid ORDER BY pid ASC LIMIT 1", array(':pid' => $context['sandbox']['current']))->fetchObject();
	if ($data) {
		if (!in_array($data->iid, $context['sandbox']['available'])) {
			_oi_delete_product($data->pid);
			$context['results'][$data->pid]='delete';
		}
		
		$context['sandbox']['current']=$data->pid;
		$context['sandbox']['progress']++;
		$context['finished']=$context['sandbox']['progress']/$context['sandbox']['total'];
		$context['message']=t('Now checking offer !current of !total', array('!current' => $context['sandbox']['progress'], '!total' => $context['sandbox']['total']));
	}
}

function check_product_deletions($filepath, &$context) {
	if (!isset($context['sandbox']['available'])) {
		$context['sandbox']['available']=array();
		$context['sandbox']['current']=0;
		$context['sandbox']['progress']=0;
		$context['sandbox']['total']=db_query("SELECT COUNT(iid) FROM {offer_importer_data}")->fetchColumn();
		$fh=fopen($filepath, 'r');
		for ($i=0; $i < 2; $i++) $row=fgetcsv($fh, 0, ';');
		while ($row=fgetcsv($fh, 0, ';')) {
			$context['sandbox']['available'][]=$row[2];
		}
	}
	
	$data=db_query("SELECT iid, pid FROM {offer_importer_product_data} WHERE pid > :pid ORDER BY pid ASC LIMIT 1", array(':pid' => $context['sandbox']['current']))->fetchObject();
	if ($data) {
		if (!in_array($data->iid, $context['sandbox']['available'])) {
      $product=product_load($data->pid);
			product_delete($product);
			$context['results'][$data->pid]='delete';
		}
		
		$context['sandbox']['current']=$data->pid;
		$context['sandbox']['progress']++;
		$context['finished']=$context['sandbox']['progress']/$context['sandbox']['total'];
		$context['message']=t('Now checking offer !current of !total', array('!current' => $context['sandbox']['progress'], '!total' => $context['sandbox']['total']));
	}
}
