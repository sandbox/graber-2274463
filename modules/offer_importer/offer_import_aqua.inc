<?php
function importer_aqua_form_elements(&$form, &$form_state) {
  if (module_exists('aqua_import')) {
    $form['offer_import_aqua']=array(
      '#type' => 'fieldset',
      '#title' => t('Offer import from Aquavip'),
      '#tree' => TRUE
    );
    $form['offer_import_aqua']['category']=array(
      '#type' => 'textfield',
      '#title' => t('Aquavip category ID'),
    );
    $form['offer_import_aqua']['site_category']=array(
      '#type' => 'textfield',
      '#autocomplete_path' => 'taxonomy/autocomplete/field_category',
      '#title' => t('Site category'),
      '#field_name' => 'field_category',
      '#element_validate' => array('my_api_taxonomy_autocomplete_validate'),
      '#description' => t('Leave empty to import category structure from Aquavip.')
    );
    $form['offer_import_aqua']['delete_all']=array(
      '#type' => 'checkbox',
      '#title' => t('Delete all earlier synchronised offers and categories before import.')
    );
    $form['offer_import_aqua']['aqua_import']=array(
      '#type' => 'submit',
      '#value' => t('Import offers')
    );
  }
}

function import_single_offer_aqua($category, $site_terms, $Nimport, &$context) {
  
  if (!isset($context['sandbox']['aqua_items'])) {
    $aquavip=new aqua_import();
    $data=$aquavip->getData($category);
    if ($data) {
      $context['sandbox']['aqua_items']=$data['products'];
      $context['sandbox']['aqua_features']=$data['features'];
      $context['sandbox']['categories']=array();
    }
    else return;
  }
  $items=&$context['sandbox']['aqua_items'];
  $features=&$context['sandbox']['aqua_features'];
  
  if (!isset($context['sandbox']['position'])) {
    $context['sandbox']['position']=0;
    $context['sandbox']['max']=sizeof($items);
  }
  
  if ($Nimport) $rows=array_slice($items, $context['sandbox']['position'], $Nimport);
  while ($row=array_shift($rows)) {
    $row=(array) $row;
    $op='';
    $iid=$row['productId'];
    $nid=db_query("SELECT nid FROM {offer_importer_data} WHERE iid = :iid", array(':iid' => $iid))->fetchColumn();
    $node_type='offer';
    $title=$row['name'];
    if (empty($title)) {
      drupal_set_message(t('Offer !index has no title', array('!index' => $iid)), 'error');
      return;
    }
    
    $fields=array(
      'body' => array(
        0 => array(
          'value' => $row['long'],
          'summary' => $row['short']
        )
      ),
      'field_summary' => $row['short'],
      'field_product_price' => strtr($row['price'], array(',' => '.')),
    );
    
    if (!empty($site_terms)) {
      foreach ($site_terms as $term) {
        $fields['field_category'][]=$term['tid'];
      }
    }
    else {
      $row['categoryPath']=array_reverse((array) $row['categoryPath'], TRUE);
      
      $parent=0;
      foreach ($row['categoryPath'] as $ciid => $categoryName) {
        if ($ciid == 0) {
          var_monit($row);
          return;
        }
        $term_data=db_select('offer_importer_taxonomy', 'it')->fields('it', array('tid', 'last_sync'))->condition('iid', $ciid)->execute()->fetchAssoc();
        if ($term_data && !empty($term_data['tid'])) $term=taxonomy_term_load($term_data['tid']);
        else $term=new stdClass();
        
        if ($term_data && ($term_data['last_sync'] < (REQUEST_TIME-72000))) {
          $parent=$term->tid;
          continue;
        }
        
        $term->vid=variable_get('aqua_import_vocabulary_id', 2);
        $term->name=$categoryName;
        $term->parent=$parent;
        taxonomy_term_save($term);
        db_merge('offer_importer_taxonomy')->key(array('iid' => $ciid))->fields(array(
          'iid' => $ciid,
          'tid' => $term->tid,
          'last_sync' => REQUEST_TIME
        ))->execute();
        $parent=$term->tid;
                
        drupal_get_messages();
      }
      
      $fields['field_category']=array();
      $existing_tids=db_query("SELECT tid FROM {taxonomy_index} WHERE nid = :nid", array(':nid' => $nid))->fetchAllKeyed(0, 0);
      if (!empty($existing_tids)) {
        foreach ($existing_tids as $tid) $fields['field_category'][]=$tid;
      }
      if (!(in_array($term->tid, $fields['field_category']))) $fields['field_category'][]=$term->tid;
    }
    
    $image_path=trim($row['image']);
    if (!empty($image_path)) {
      if ($content=file_get_contents($image_path)) {
        $fields['field_product_image'][]=array(
          'filename' => basename($image_path),
          'content' => $content
        );
      }
    }
    
    $my_node=new my_node();
    if ($nid) $op='update';
    else $op='create';
    $node=$my_node->prepare($fields, $title, $node_type, $nid);
      
    try {
      node_save($node);
    }
    catch (Exception $e) {
      drupal_set_message(t('Error in offer !index.<br>', array('!index' => $iid)).$e->getMessage(), 'error');
      return;
    }
    
    module_invoke('aqua_filters', 'aqua_import_init', $term->tid, $ciid, $features[$iid]);
    if (!empty($features[$iid])) module_invoke('aqua_filters', 'aqua_import', $category, $node, $features[$iid]);
      
    db_merge('offer_importer_data')->key(array('iid' => $iid))->fields(array(
      'iid' => $iid,
      'nid' => $node->nid,
      'last_sync' => REQUEST_TIME
    ))->execute();
      
    $context['results'][]=$op;
    $context['sandbox']['position']++;
    $context['finished']=$context['sandbox']['position']/$context['sandbox']['max'];
    $context['message']=t('Now processing offer "!title" (!current of !total)', array('!title' => $node->title, '!current' => $context['sandbox']['position'], '!total' => $context['sandbox']['max']));
  }
}

function aqua_update_cron_job() {
  $import_cats=variable_get('aqua_update_remaining_cats', array());
  if (empty($import_cats)) $import_cats=variable_get('aqua_update_selected_cats', array());
  
  if (!empty($import_cats)) {
    $start_time=time();
    $limit=150;
    $results=array();
    foreach ($import_cats as $cat_id) {
      $aquavip=new aqua_import();
      $items=$aquavip->getProducts($cat_id);
      if (!empty($items)) {
        $context=array(
          'aqua_items' => $items
        );
        import_single_offer_aqua(NULL, array(), FALSE, $context);
        foreach ($context['results'] as $op) $results[]=$op;
      }
      unset($import_cats[$cat_id]);
      if (($start_time-time()) > $limit) {
        break;
      }
    }
    
    drupal_get_messages();
    
    //Process results
    if (!empty($results)) {
      $Nres=count($results);
      $operations=array_count_values($results);
      $details=array();
      foreach ($operations as $op => $count) $details[]=t($op).': '.$count;
      $message=t('Aqua sync preformed, !nres operations: !operations.', array('!nres' => $Nres, '!operations' => implode(', ', $details)));
      watchdog('offer_importer', $message);
    }

    
    if (!empty($import_cats)) {
      variable_set('aqua_update_remaining_cats', $import_cats);
    }
    else {
      variable_del('aqua_update_remaining_cats');
    }
  }
}
