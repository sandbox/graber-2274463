<!DOCTYPE  html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title><?php print $invoice_no; ?></title>
	<style type="text/css"> 
		body{color: black; font-family:Arial, sans-serif; font-size:12px;}
		.all_page{margin:0 auto 25px; width:830px; padding:20px 40px;}
		.header {width: 100%; margin-bottom: 10px;}
		.header .title h1 {text-align: center;}
		h1{text-align:center;font-size:15px; margin:15px 0 0 0; padding:0;}
		h2{font-size:16px; font-weight:normal; margin:30px 0 5px 0; padding:0;}
		h3{font-size:15px; font-weight:normal; margin:0 0 5px 0; padding:0;}
		h4{text-align:center;font-size:15px; font-weight:normal; margin:20px 0; padding:0;}
		p{padding:0; margin:0 0 5px 0;}
		hr{clear:both;text-align:left;margin:20px 0;color:#000;border:0;background-color:#000;height:1px;}
		.footer hr {margin: 5px 0;}
		hr.hr_half{width:420px;}
		.data {text-align: left; width: 100%;}
		.invoice_tb{margin-top:20px;border-collapse:collapse;width:100%;}
		.invoice_tb td,.invoice_tb th{border:1px solid #000; padding:5px; vertical-align:top; width:33%;}
		.invoice_tb th{font-weight:normal; text-align:left; background:#EFEFEF;}
		.products_tb{margin-top:20px; margin-bottom: 15px; border-collapse:collapse; width:100%; font-size:10px;}
		.products_tb td{border-bottom:1px solid #000; padding:3px 5px; vertical-align:top;}
		.products_tb th{border-top:1px solid #000;padding:3px 4px;text-align:left; background:#EFEFEF;}
		.products_tb td.c_1,.products_tb th.c_1{text-align:right;}
    .products_tb .items-total td {font-weight: bold; border-bottom: 2px solid #000;}
		.total {margin:15px 5px 0 0;font-size:10px;}
		.total .total_h {text-align:right;font-weight:bold;}
		.total .total_d{text-align:left;}
		.summary {margin: 0 0 15px auto}
		.footer div{width:80%;margin:0 auto;text-align:center; font-size:10px;}
	</style>
</head>
<?php if (isset($order->currency)) $currency=$order->currency; else $currency='PLN'; ?>
<body>
<div class="all_page">
	
	<table class="header"><tr>
		<td class="logo" style="width: 250px; vertical-align: top;" ><img src="<?php print $logo_url; ?>" alt="." style="width: 150px;"/></td>
		<td class="title">
      <h1>
        <?php 
          if ($type == 'bill') print 'RACHUNEK';
          elseif ($type == 'invoice_pro') print 'FAKTURA PRO FORMA';
          else print "FAKTURA VAT";
        ?> nr: <?php print $invoice_no; ?>
      </h1>
			<div class="b_s2">
        <p>&nbsp;</p>
				<p>Data wystawienia: <?php print date('d.m.Y', $created); ?> r.</p>
				<?php if (isset($order->created)): ?>
          <p>Data zamówienia: <?php print date('d.m.Y', $order->created); ?> r.</p>
        <?php endif; ?>
				<p>Termin płatności: <?php print date('d.m.Y', $due_date); ?> r.</p>
        <?php if (!$order): ?>
          <p>Forma płatności: <?php print $payment_method; ?></p>
          <p>Nr konta: <?php print $store_data['bank']; ?></p>
          <p>Do zapłaty: <?php print number_format($totals['gross'], 2, ',', ' ').' '.$currency; ?></p>
        <?php endif; ?>
			</div>
    </td>
	</tr></table>
	
	<hr />

	<table class="data"><tr>
    <td class="left" style="width: 250px;">
      <h2>Nabywca: </h2>
      <?php if ($billing['legal_entity'] == 'company'): ?>
        <p><?php print $billing['company']; ?></p>
        <p>NIP: <?php print $billing['tin']; ?></p>
      <?php elseif ($billing['legal_entity'] == 'person'): ?>
        <p><?php print $billing['name']; ?> <?php print $billing['family']; ?></p>
      <?php endif; ?>
      <p><?php print $billing['address']; ?></p>
      <p><?php print $billing['postal_code']; ?> <?php print $billing['city']; ?></p>
    </td>
		<td class="right">
			<h2>Sprzedawca:</h2>
			<div class="b_s1">
				<p><?php print $store_data['company']; ?></p>
				<p>NIP: <?php print $store_data['nip']; ?></p>
				<p><?php print $store_data['street']; ?></p>
				<p><?php print $store_data['postal']; ?> <?php print $store_data['city']; ?></p>
			</div>
		</td>
	</tr></table>
				
  <?php if ($order): ?>
    <table class="invoice_tb">
      <tr>
        <th colspan="<?php if (!empty($payment_method)) print '3'; else print '2'; ?>">
        <?php 
          if ($type == 'bill') print 'RACHUNEK';
          else print "FAKTURA VAT";
        ?> nr: <?php print $invoice_no; ?> z dnia <?php print date('d.m.Y', REQUEST_TIME); ?></th>
      </tr>
      <tr>
        <td>Nr. zamówienia #<?php print $order->oid; ?></td>
        <td>Sposób dostawy: 
          <p><?php print $shipment_option; ?></p>
        </td>
        <?php if ($payment_method): ?>
          <td>
            Metoda płatności: 
            <p><?php print $payment_method; ?></p>
          </td>
        <?php endif; ?>
      </tr>
    </table>
  <?php endif; ?>
	
	<table class="products_tb">
		<tr>
			<th>L.p.</th>
			<th>Nazwa&nbsp;pozycji</th>
			<th class="c_1">Cena&nbsp;netto<br />[<?php print $currency; ?>]</th>
			<th class="c_1">Ilość</th>
			<th class="c_1">Wartość&nbsp;netto<br />[<?php print $currency; ?>]</th>
			<th class="c_1">Podatek&nbsp;VAT<br />[%]</th>
			<th class="c_1">Kwota&nbsp;VAT<br />[<?php print $currency; ?>]</th>
			<th class="c_1">Wartość&nbsp;brutto<br />[<?php print $currency; ?>]</th>
		</tr>
		
		<?php foreach ($items as $item) : ?>
			<tr>
				<td><?php print $item['item_no']; ?></td>
				<td><?php print $item['title']; ?></td>
				<td class="c_1"><?php print number_format($item['net_price'], 2, ',', ' '); ?></td>
				<td class="c_1"><?php print $item['quantity']; ?></td>
				<td class="c_1"><?php print number_format($item['net_value'], 2, ',', ' '); ?></td>
				<td class="c_1"><?php print number_format($item['vat'], 2, ',', ' '); ?>%</td>
				<td class="c_1"><?php print number_format($item['vat_value'], 2, ',', ' '); ?></td>
				<td class="c_1"><?php print number_format($item['gross_value'], 2, ',', ' '); ?></td>
			</tr>
		<?php endforeach; ?>
    
    <tr class="items-total">
      <td></td>
      <td>SUMA</td>
      <td class="c_1"></td>
      <td class="c_1"></td>
      <td class="c_1"><?php print number_format($totals['net'], 2, ',', ' '); ?></td>
      <td class="c_1"></td>
      <td class="c_1"><?php print number_format($totals['vat'], 2, ',', ' '); ?></td>
      <td class="c_1"><?php print number_format($totals['gross'], 2, ',', ' '); ?></td>
    </tr>
	</table>
	<table class="summary">
    <?php if (!empty($totals['amount_discount'])): ?>
      <tr>
        <td class="total_h">Voucher:</td> 
        <td class="total_d"><?php print number_format($totals['amount_discount'], 2, ',', ' '); ?> <?php print $currency; ?></td>
      </tr>
    <?php endif; ?>
		<tr>
			<td class="total_h"><b>Do zapłaty:</b></td>
			<td class="total_d"><b><?php print number_format($totals['to_pay'], 2, ',', ' '); ?> <?php print $currency; ?></b></td>
		</tr>
		<tr>
			<td class="total_h"><b>Słownie:</b></td>
			<td class="total_d"><b><?php print $totals['to_pay_words']; ?> <?php print $currency; ?></b></td>
		</tr>
	</table>
</div>

</body>
</html>
