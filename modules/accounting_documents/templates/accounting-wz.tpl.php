<!DOCTYPE  html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title><?php print $invoice_no; ?></title>
	<style type="text/css"> 
		body{color: black; font-family:Arial, sans-serif; font-size:12px;}
		.all_page{margin:0 auto; width:830px; padding:20px 40px;}
		.header {width: 100%; margin-bottom: 20px;}
		.header .title {text-align: center;}
		.header img {width: 250px;}
		h1{text-align:center;font-size:15px; margin:15px 0 0 0; padding:0;}
		h2{font-size:16px; font-weight:normal; margin:30px 0 5px 0; padding:0;}
		h3{font-size:15px; font-weight:normal; margin:0 0 5px 0; padding:0;}
		h4{text-align:center;font-size:15px; font-weight:normal; margin:20px 0; padding:0;}
		p{padding:0; margin:0 0 5px 0;}
		hr{clear:both;text-align:left;margin:20px 0;color:#000;border:0;background-color:#000;height:1px;}
		.footer hr {margin: 5px 0;}
		hr.hr_half{width:420px;}
		.data {text-align: left; width: 100%;}
		.data td{vertical-align:top;}
		.products_tb{margin-top:40px; border-collapse:collapse; width:100%; font-size:10px;}
		.products_tb td{border-bottom:1px solid #000; padding:3px 5px; vertical-align:top;}
		.products_tb th{border-top:1px solid #000;padding:3px 5px;text-align:left; background:#EFEFEF;}
		.products_tb td.c_1,.products_tb th.c_1{text-align:right;}
		.signatures {margin: 60px auto 0}
		.signatures td{text-align:center;}
	</style>
</head>
<body>
<div class="all_page">
	
	<table class="header"><tr>
	<td class="title"><h1>Wydanie na zewnątrz: <?php print $wz_no; ?></h1></td>
	</tr></table>
	
	<div class="date"><p>Z dnia: <?php print format_date(REQUEST_TIME, 'short'); ?></p></div>	
	<div class="invoice_no"><p>Do faktury nr: <?php print $invoice_no; ?></p></div>	
	
	<table class="data"><tr>
		<?php if (!empty($billing)) : ?>
			<td>
				<h3>Nabywca: </h3>
				<?php if ($billing['legal_entity'] == 'company'): ?>
					<p><?php print $billing['company']; ?></p>
					<p>NIP: <?php print $billing['tin']; ?></p>
				<?php elseif ($billing['legal_entity'] == 'person'): ?>
					<p><?php print $billing['name']; ?> <?php print $billing['family']; ?></p>
				<?php endif; ?>
				<p><?php print $billing['address']; ?></p>
				<p><?php print $billing['postal_code']; ?> <?php print $billing['city']; ?></p>
			</td>
		<?php endif; ?>
		
		<td>
			<h3>Adres wysyłki: </h3>
			<?php if (!empty($shipping['company'])): ?>
				<p>Firma: <?php print $shipping['company']; ?></p>
			<?php endif; ?>
      <?php if (!empty($shipping['name'])): ?>
        <p><?php print $shipping['name']; ?> <?php print $shipping['family']; ?></p>
      <?php endif; ?>
			<p><?php print $shipping['address']; ?></p>
			<p><?php print $shipping['postal_code']; ?> <?php print $shipping['city']; ?></p>
			<?php if (!empty($shipping['phone'])): ?>
				<p>tel.: <?php print $shipping['phone']; ?></p>
			<?php endif; ?>
		</td>
		
		<?php if (!empty($store_data)): ?>
		<td>
			<h2>Sprzedawca:</h2>
			<div class="b_s1">
				<p><?php print $store_data['company']; ?></p>
				<p><?php print $store_data['postal']; ?> <?php print $store_data['city']; ?></p>
				<p><?php print $store_data['street']; ?></p>
				<p>&nbsp;</p>
				<p>NIP: <?php print $store_data['nip']; ?></p>
				<p><?php print $store_data['bank']; ?></p>
			</div>
		</td>
		<?php endif; ?>
		
	</tr></table>
		
	<table class="products_tb">
		<tr>
			<th>L.p.</th>
			<th>Nazwa produktu</th>
			<th>Identyfikator</th>
			<th class="c_1">Ilość</th>
			<th class="c_1">j.m</th>
			<th class="c_1">Koszt zakupu [PLN]</th>
		</tr>
		
		<?php foreach ($items as $wid => $data): ?>
			<tr><td class="warehouse" colspan="6">Magazyn: <?php print $data['warehouse_name']; ?></td></tr>
			
			<?php foreach ($data['batches'] as $lp => $item): ?>
				<tr>
				<?php if (isset($item['product'])) : ?>
					<td><?php print $lp; ?></td>
					<td><?php print render($item['product']['name']); ?></td>
					<td><?php print $item['pid']; ?></td>
					<td class="c_1"><?php print number_format($item['product_quantity'], 2, ',', ' '); ?></td>
					<td class="c_1"><?php print render($item['product']['unit']); ?></td>
					<td class="c_1"><?php if (is_numeric($item['purchase_cost'])) print number_format($item['purchase_cost'], 2, ',', ' '); else print $item['purchase_cost']; ?></td>
				<?php else: ?>
					<td colspan="5">Brak danych o produkcie w magazynie.</td>
				<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</table>
	<table class="signatures">
		<tr>
			<td>................................................</td> 
			<td>................................................</td>
			<td>................................................</td>
		</tr>
		<tr>
			<td>podpis osoby wydającej</td> 
			<td>podpis osoby wystawiającej</td>
			<td>podpis osoby odbierającej</td>
		</tr>
	</table>
</div>

</body>
</html>