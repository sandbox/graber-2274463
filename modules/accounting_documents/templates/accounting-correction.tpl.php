<!DOCTYPE  html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title><?php print $invoice_no; ?></title>
	<style type="text/css"> 
		body{color: black; font-family:Arial, sans-serif; font-size:12px;}
		.all_page{margin:0 auto 25px; width:830px; padding:20px 40px;}
		.header {width: 100%;}
		.header .title h1 {text-align: center;}
		.header .logo, .header img {width: 250px;}
		h1{text-align:center;font-size:15px; margin:15px 0 0 0; padding:0;}
		h2{font-size:16px; font-weight:normal; margin:30px 0 5px 0; padding:0;}
		h3{font-size:15px; font-weight:normal; margin:0 0 5px 0; padding:0;}
		h4{text-align:center;font-size:15px; font-weight:normal; margin:20px 0; padding:0;}
		p{padding:0; margin:0 0 5px 0;}
		hr{clear:both;text-align:left;margin:20px 0;color:#000;border:0;background-color:#000;height:1px;}
		.footer hr {margin: 5px 0;}
		hr.hr_half{width:420px;}
		.data {text-align: left; width: 100%;}
		.invoice_tb{margin-top:40px;border-collapse:collapse;width:100%;}
		.invoice_tb td,.invoice_tb th{border:1px solid #000; padding:5px; vertical-align:top; width:33%;}
		.invoice_tb th{font-weight:normal; text-align:left; background:#EFEFEF;}
		.products_tb{margin-top:40px;margin-bottom: 15px;border-collapse:collapse;width:100%; font-size:10px;}
		.products_tb td{border-bottom:1px solid #000; padding:3px 5px; vertical-align:top;}
		.products_tb th{border-top:1px solid #000;padding:3px 5px;text-align:left; background:#EFEFEF;}
		.products_tb td.c_1,.products_tb th.c_1{text-align:right;}
    .products_tb .items-total td {font-weight: bold; border-bottom: 2px solid #000;}
		.total {margin:15px 5px 0 0;font-size:10px;}
		.total .total_h {text-align:right;font-weight:bold;}
		.total .total_d{text-align:left;}
		.summary {margin: 0 0 15px auto}
		.footer div{width:80%;margin:0 auto;text-align:center; font-size:10px;}
	</style>
</head>
<body>
<div class="all_page">
	
	<table class="header"><tr>
		<td class="logo" style="width: 250px; vertical-align: top;" ><img src="<?php print $logo_url; ?>" alt="." style="width: 150px;"/></td>
		<td class="title">
      <h1>
        Korekta nr <?php print $correction_no; ?> do dokumentu nr <?php print $invoice_no; ?>
      </h1>
			<div class="b_s2">
        <p>&nbsp;</p>
				<p>Data wystawienia: <?php print date('d.m.Y', $created); ?> r.</p>
				<?php if (isset($order->created)): ?>
          <p>Data zamówienia: <?php print date('d.m.Y', $order->created); ?> r.</p>
        <?php endif; ?>
				<p>Termin płatności: <?php print date('d.m.Y', $due_date); ?> r.</p>
        <?php if (!$order): ?>
          <p>Forma płatności: <?php print $payment_method; ?></p>
          <p>Nr konta: <?php print $store_data['bank']; ?></p>
          <p>Do zapłaty: <?php print number_format($difference['gross'], 2, ',', ' '); ?> PLN</p>
        <?php endif; ?>
			</div>
    
    </td>
	</tr></table>

	<hr />

	<table class="data"><tr>
    <td class="left" style="width: 250px;">
      <h2>Nabywca: </h2>
      <?php if ($billing['legal_entity'] == 'company'): ?>
        <p><?php print $billing['company']; ?></p>
        <p>NIP: <?php print $billing['tin']; ?></p>
      <?php elseif ($billing['legal_entity'] == 'person'): ?>
        <p><?php print $billing['name']; ?> <?php print $billing['family']; ?></p>
      <?php endif; ?>
      <p><?php print $billing['address']; ?></p>
      <p><?php print $billing['postal_code']; ?> <?php print $billing['city']; ?></p>
    </td>
		<td class="right">
			<h2>Sprzedawca:</h2>
			<div class="b_s1">
				<p><?php print $store_data['company']; ?></p>
				<p><?php print $store_data['postal']; ?> <?php print $store_data['city']; ?></p>
				<p><?php print $store_data['street']; ?></p>
				<p>NIP: <?php print $store_data['nip']; ?></p>
			</div>
		</td>
	</tr></table>
				
  <?php if ($order): ?>
    <table class="invoice_tb">
      <tr>
        <th colspan="<?php if (!empty($payment_method)) print '3'; else print '2'; ?>">
        <?php 
          if ($type == 'bill') print 'RACHUNEK';
          else print "FAKTURA VAT";
        ?> nr: <?php print $invoice_no; ?> z dnia <?php print date('d.m.Y', REQUEST_TIME); ?></th>
      </tr>
      <tr>
        <td>Nr. zamówienia #<?php print $order->oid; ?></td>
        <td>Sposób dostawy: 
          <p><?php print $shipment_option; ?></p>
        </td>
        <?php if ($payment_method): ?>
          <td>
            Metoda płatności: 
            <p><?php print $payment_method; ?></p>
          </td>
        <?php endif; ?>
      </tr>
    </table>
  <?php endif; ?>
	
	<table class="products_tb">
		<tr>
			<th>L.p.</th>
      <th>Opis</th>
			<th>Nazwa pozycji</th>
			<th class="c_1">Ilość</th>
			<th class="c_1">Rabat [%]</th>
			<th class="c_1">Cena netto [PLN]</th>
			<th class="c_1">Wartość netto [PLN]</th>
			<th class="c_1">Podatek VAT [%]</th>
			<th class="c_1">Kwota VAT [PLN]</th>
			<th class="c_1">Wartość brutto [PLN]</th>
		</tr>
		
		<?php foreach ($changes as $change) : ?>
			<tr>
        <td rowspan="2"><?php print $change['item_no']; ?></td>
        <td>PRZED</td>
        <?php if (!empty($change['before'])): ?>
          <td><?php print $change['before']['title']; ?></td>
          <td class="c_1"><?php print $change['before']['quantity']; ?></td>
          <td class="c_1"><?php print number_format(100*$change['before']['discount'], 2, ',', ' '); ?>%</td>
          <td class="c_1"><?php print number_format($change['before']['net_price'], 2, ',', ' '); ?></td>
          <td class="c_1"><?php print number_format($change['before']['net_value'], 2, ',', ' '); ?></td>
          <td class="c_1"><?php print number_format($change['before']['vat'], 2, ',', ' '); ?>%</td>
          <td class="c_1"><?php print number_format($change['before']['vat_value'], 2, ',', ' '); ?></td>
          <td class="c_1"><?php print number_format($change['before']['gross_value'], 2, ',', ' '); ?></td>
        <?php else: ?>
          <td colspan="9">Pozycja nie istniała</td>
        <?php endif; ?>
			</tr>
			<tr>
        <td>PO</td>
        <?php if (!empty($change['after'])): ?>
          <td><?php print $change['after']['title']; ?></td>
          <td class="c_1"><?php print $change['after']['quantity']; ?></td>
          <td class="c_1"><?php print number_format(100*$change['after']['discount'], 2, ',', ' '); ?>%</td>
          <td class="c_1"><?php print number_format($change['after']['net_price'], 2, ',', ' '); ?></td>
          <td class="c_1"><?php print number_format($change['after']['net_value'], 2, ',', ' '); ?></td>
          <td class="c_1"><?php print number_format($change['after']['vat'], 2, ',', ' '); ?>%</td>
          <td class="c_1"><?php print number_format($change['after']['vat_value'], 2, ',', ' '); ?></td>
          <td class="c_1"><?php print number_format($change['after']['gross_value'], 2, ',', ' '); ?></td>
        <?php else: ?>
          <td colspan="9">Pozycja została usunięta</td>
        <?php endif; ?>
			</tr>
      
		<?php endforeach; ?>
    <tr class="items-total">
      <td></td>
      <td colspan="2">SUMA RÓŻNIC</td>
      <td class="c_1"></td>
      <td class="c_1"></td>
      <td class="c_1"></td>
      <td class="c_1"><?php print number_format($difference['net'], 2, ',', ' '); ?></td>
      <td class="c_1"></td>
      <td class="c_1"><?php print number_format($difference['vat'], 2, ',', ' '); ?></td>
      <td class="c_1"><?php print number_format($difference['gross'], 2, ',', ' '); ?></td>
    </tr>
    
	</table>
	<table class="summary">
		<tr>
			<td class="total_h">Różnica w cenie słownie:</td>
			<td class="total_d"><?php print $difference['gross_words']; ?> PLN</td>
		</tr>
	</table>
  <p>Przyczyny zmian:</p>
  <ul>
  <?php foreach ($changelog as $change): ?>
    <?php if (!empty($change['reason'])): ?>
    <li><?php print $change['reason']; ?></li>
    <?php endif; ?>
  <?php endforeach; ?>
  </ul>
</div>

</body>
</html>
