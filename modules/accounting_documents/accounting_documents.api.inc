<?php

function accounting_documents_generate_pdf($html, $filename, $footer=NULL) {
  if (pdf_using_mpdf_library_exist() == TRUE) {
    $paper_size=array('w' => 210, 'h' => 297);
    $font_size = 8;
    $margin_top = variable_get('pdf_using_mpdf_margin_top', 16);;
    $margin_right = variable_get('pdf_using_mpdf_margin_right', 15);
    if ($footer) $margin_bottom = 30;
    else $margin_bottom = 16;
    $margin_left = variable_get('pdf_using_mpdf_margin_left', 15);
    $margin_header = variable_get('pdf_using_mpdf_margin_header', 9);
    $margin_footer = variable_get('pdf_using_mpdf_margin_footer', 20);

    $mpdf = new mPDF(
      '',
      array($paper_size['w'], $paper_size['h']),
      $font_size,
      '',
      $margin_left,
      $margin_right,
      $margin_top,
      $margin_bottom,
      $margin_header,
      $margin_footer
    );

    if (isset($footer) && $footer != NULL) {
      $mpdf->SetHTMLFooter($footer);
    }

    //$text=variable_get('site_name', '');
    if (!empty($text)) {
      $mpdf->SetWatermarkText($text, 0.2);
      $mpdf->showWatermarkText = TRUE;
    }

    $mpdf->WriteHTML($html);
    $mpdf->Output($filename, 'F');
  }
  else {
    drupal_set_message(t('No mPDF Library Found in "sites/all/libraries" or "!default_module_path". Please download PHP mPDF PDF generation tool from <a href="http://www.mpdf1.com/">mPDF1.com</a>', array(
      '!default_module_path' => drupal_get_path('module', 'pdf_using_mpdf'),
    )), 'warning');
  }
}

function _gad_get_language($currency) {
  var_monit($currency);
  if (isset($currency)) {
    switch ($currency) {
      case 'PLN':
      case 'pln':
        $output='pl';
        break;
      case 'EUR':
      case 'eur':
        $output='en';
        break;
      default:
        $output='pl';
    }
  }
  else $output='pl';
  return $output;
}

function gad_prepare_account_data($order, $cart) {
  if (!$cart->fully_loaded) $cart->load();
  
  $output['language']=_gad_get_language($order->currency);
    
  $output['items']=array();
  $output['totals']=array(
    'net' => 0,
    'vat' => 0,
    'gross' => 0,
    'discount' => 0
  );
  $order->order_data['data']['total_discount']=0;
  $item_no=0;
  $vat_cm=gp_get_settings('accounting_documents', 'vat_calculation_method');

  foreach ($cart->contents as $iid => $item) {
    if (isset($item['discount']) && $item['discount'] == 1) continue;
    $output['items'][$iid]['weight']=$item_no;
    $output['items'][$iid]['item_no']=++$item_no;
    if (isset($item['warehouse_pid'])) {
      $product=product_load($item['warehouse_pid']);
      if ($product) {
        $output['items'][$iid]['product_name']=$product->name;
        $output['items'][$iid]['vat']=$product->vat;
        $output['items'][$iid]['product']=product_view($product);
      }
      $output['items'][$iid]['product_quantity']=$item['quantity']=$item['warehouse_quantity']*$item['quantity'];
      $item['price']/=$item['warehouse_quantity'];
    }
    else {
      if (!isset($output['items'][$iid]['vat'])) $output['items'][$iid]['vat']=gp_get_settings('accounting_documents', 'default_vat');
    }
    
    //Calculate prices
    if ($vat_cm == 'g-n') {
      $output['items'][$iid]['gross_price']=$item['price'];
      $vat_single=round($item['price']*(1-1/(1+$output['items'][$iid]['vat']/100)), 2);
      $output['items'][$iid]['net_price']=$item['price']-$vat_single;
    }
    elseif ($vat_cm == 'n-g') {
      $output['items'][$iid]['net_price']=$item['price'];
      $vat_single=round($item['price']*($output['items'][$iid]['vat']/100), 2);
      $output['items'][$iid]['gross_price']=$item['price']+$vat_single;
    }
    $output['items'][$iid]['vat_value']=$vat_single*$item['quantity'];
    $output['items'][$iid]['net_value']=$item['quantity']*$output['items'][$iid]['net_price'];
    $output['items'][$iid]['gross_value']=$item['quantity']*$output['items'][$iid]['gross_price'];
    
    //Fill other required output fields
    $output['items'][$iid]['title']=$item['title'];
    $output['items'][$iid]['quantity']=(isset($output['items'][$iid]['product_quantity'])) ? $output['items'][$iid]['product_quantity'] : $item['quantity'];
    if (isset($item['sold_quantity'])) $output['items'][$iid]['quantity']*=$item['sold_quantity'];
    
    //Add to totals
    if (!isset($output['totals']['vat_different'][$output['items'][$iid]['vat']])) $output['totals']['vat_different'][$output['items'][$iid]['vat']]=$output['items'][$iid]['vat_value'];
    else $output['totals']['vat_different'][$output['items'][$iid]['vat']]+=$output['items'][$iid]['vat_value'];
    
    $output['totals']['vat']+=$output['items'][$iid]['vat_value'];
    $output['totals']['net']+=$output['items'][$iid]['net_value'];
    $output['totals']['gross']+=$output['items'][$iid]['gross_value'];
    
    if (isset($item['base_price']) && ($item['base_price'] != $item['price'])) {
      $output['items'][$iid]['discount']=$item['base_price']-$item['price'];
      $output['items'][$iid]['discount_p']=100*$output['items'][$iid]['discount']/$item['base_price'];
      $output['totals']['discount']+=$output['items'][$iid]['discount'];
    }
    else $output['items'][$iid]['discount']=$output['items'][$iid]['discount_p']=0;
  }
    
  //Shipping
  if (isset($order->order_data['data']['shipping_price']) && ($order->order_data['data']['shipping_price'] != 0)) {
    $shipping_price=$order->order_data['data']['shipping_price'];
    
    $output['items']['shipping']['weight']=$item_no;
    $output['items']['shipping']['item_no']=++$item_no;
    $output['items']['shipping']['title']=t('Shipping');
    $output['items']['shipping']['quantity']=1;
    $output['items']['shipping']['unit']=t('pieces');
    
    $output['items']['shipping']['vat']=23;
    
    if ($vat_cm == 'g-n') {
      $output['items']['shipping']['vat_value']=$vat_single=round($shipping_price*(1-1/(1+$output['items']['shipping']['vat']/100)), 2);
      $output['items']['shipping']['net_value']=$output['items']['shipping']['net_price']=$shipping_price-$vat_single;
      $output['items']['shipping']['gross_value']=$output['items']['shipping']['gross_price']=$shipping_price;
    }
    elseif ($vat_cm == 'n-g') {
      $output['items']['shipping']['vat_value']=$vat_single=round($shipping_price*($output['items']['shipping']['vat']/100), 2);
      $output['items']['shipping']['net_value']=$output['items']['shipping']['net_price']=$shipping_price;
      $output['items']['shipping']['gross_value']=$output['items']['shipping']['gross_price']=$shipping_price+$vat_single;
    }
    
    $output['totals']['net']+=$output['items']['shipping']['net_value'];
    $output['totals']['gross']+=$output['items']['shipping']['gross_value'];
    $output['totals']['vat']+=$output['items']['shipping']['vat_value'];
    
    //Discount for shipping will be set to 0 for now.
    $output['items']['shipping']['discount']=0;
    $output['items']['shipping']['discount_p']=0;
  }
    
  $output['totals']['to_pay']=$order->amount;
  if ($order->amount != $output['totals']['gross']) {
    $output['totals']['amount_discount']=$output['totals']['gross']-$order->amount;
  }
  $output['totals']['to_pay_words']=_gad_amount_in_words($output['totals']['to_pay'], 2, $output['language']);
  
  return $output;
}

function gad_get_document_serial($type, $my) {
  $serials=variable_get('accounting_documents_serials', array());
  if (isset($serials[$my][$type])) $serials[$my][$type]++;
  else $serials[$my][$type]=1;
  variable_set('accounting_documents_serials', $serials);
  return $serials[$my][$type];
}

function gad_prepare_batch_items($all_batches) {
  $items=array();
  $Lp=0;
  
  //Load products in one operation
  $pids=array();
  foreach ($all_batches as $wid => $data) {
    foreach ($data['batches'] as $batch) {
      if (!in_array($batch['pid'], $pids)) $pids[]=$batch['pid'];
    }
  }
  $products=product_load_multiple($pids);
  
  foreach ($all_batches as $wid => $data) {
    $items[$wid]=array(
      'warehouse_name' => $data['warehouse_name'],
      'wid' => $wid,
      'batches' => array()
    );
    foreach ($data['batches'] as $batch) {
      $Lp++;
      $items[$wid]['batches'][$Lp]=array(
        'pid' => $batch['pid'],
        'product' => product_view($products[$batch['pid']]),
        'product_quantity' => $batch['reduced'],
        'purchase_cost' => isset($batch['cost']) ? $batch['cost']*($batch['reduced']/$batch['init_quantity']) : t('N/A')
      );
      $items[$wid]['batches'][$Lp]+=$batch;
    }
    unset($data['batches']);
    $items[$wid]+=$data;
  }
  return $items;
}

function gad_get_logo_path($store_data=FALSE) {
  if (!$store_data) $store_data=variable_get('graber_accounting_invoice_data', array());
  if (isset($store_data['invoice_logo'])) {
    $file=file_load($store_data['invoice_logo']);
    if ($file) $logo_path=file_create_url($file->uri);
  }
  if (!isset($logo_path)) {
    if ($theme_name=variable_get('theme_default',FALSE)) $logo_path=$GLOBALS['base_url'].'/'.drupal_get_path('theme', $theme_name).'/logo.png';
  }
  if (!isset($logo_path)) $logo_path=FALSE;
  return $logo_path;
}

function gad_get_document_type($order) {
  if (isset($order->order_data['data']['invoice_type'])) $type=$order->order_data['data']['invoice_type'];
  else {
    if (isset($order->order_data['billing_addr']['invoice_selector']) && $order->order_data['billing_addr']['invoice_selector']) {
      $check=gp_get_settings('accounting_documents', 'generate_invoice');
      if (!$check) return 0;
      $type='invoice';
    }
    else {
      $check=gp_get_settings('accounting_documents', 'generate_bill');
      if (!$check) return 0;
      $type='bill';
    }
  }
  return $type;
}

/**
 * Returns amount in words.
 */
function _gad_amount_in_words($number, $decimals = FALSE, $lang = 'pl') {
  if ($decimals) {
    $number = round($number, $decimals);
  }
  $hyphen = '-';
  $separator = ' ';
  $dictionaries = array(
    'pl' => array(
      0 => 'zero',
      1 => 'jeden',
      2 => 'dwa',
      3 => 'trzy',
      4 => 'cztery',
      5 => 'pięć',
      6 => 'sześć',
      7 => 'siedem',
      8 => 'osiem',
      9 => 'dziewięć',
      10 => 'dziesięć',
      11 => 'jedenaście',
      12 => 'dwanaście',
      13 => 'trzynaście',
      14 => 'czternaście',
      15 => 'piętnaście',
      16 => 'szesnaście',
      17 => 'siedemnaście',
      18 => 'osiemnaście',
      19 => 'dziewiętnaście',
      20 => 'dwadzieścia',
      30 => 'trzydzieści',
      40 => 'czterdzieści',
      50 => 'pięćdziesiąt',
      60 => 'sześćdziesiąt',
      70 => 'siedemdziesiąt',
      80 => 'osiemdziesiąt',
      90 => 'dziewięćdziesiąt',
      100 => 'sto',
      200 => 'dwieście',
      300 => 'trzysta',
      400 => 'czterysta',
      500 => 'pięćset',
      600 => 'sześćset',
      700 => 'siedemset',
      800 => 'osiemset',
      900 => 'dziewięćset',
      1000 => array(
        'singular' => 'tysiąc',
        'transitional' => 'tysiące',
        'plural' => 'tysięcy',
      ),
      1000000 => array(
        'singular' => 'milion',
        'transitional' => 'miliony',
        'plural' => 'milionów',
      ),
      1000000000 => array(
        'singular' => 'miliard',
        'transitional' => 'miliardy',
        'plural' => 'miliardów',
      ),
      1000000000000 => array(
        'singular' => 'bilion',
        'transitional' => 'biliardy',
        'plural' => 'biliardów',
      ),
      1000000000000000 => array(
        'singular' => 'trylion',
        'transitional' => 'tryliony',
        'plural' => 'trylionów',
      ),
      1000000000000000000 => array(
        'singular' => 'tryliard',
        'transitional' => 'tryliardy',
        'plural' => 'tryliardów',
      ),
      'comma' => 'przecinek',
      'minus' => 'minus'
    ),
    'en' => array(
      0 => 'zero',
      1 => 'one',
      2 => 'two',
      3 => 'three',
      4 => 'four',
      5 => 'five',
      6 => 'six',
      7 => 'seven',
      8 => 'eight',
      9 => 'nine',
      10 => 'ten',
      11 => 'eleven',
      12 => 'twelve',
      13 => 'thirteen',
      14 => 'fourteen',
      15 => 'fifteen',
      16 => 'sixteen',
      17 => 'seventeen',
      18 => 'eighteen',
      19 => 'nineteen',
      20 => 'twenty',
      30 => 'thirty',
      40 => 'fourty',
      50 => 'fifty',
      60 => 'sixty',
      70 => 'seventy',
      80 => 'eighty',
      90 => 'ninety',
      100 => 'hundred',
      200 => 'two hundred',
      300 => 'three hundred',
      400 => 'four hundred',
      500 => 'five hundred',
      600 => 'six hundred',
      700 => 'seven hundred',
      800 => 'eight hundred',
      900 => 'nine hundred',
      1000 => array(
        'singular' => 'thousand',
        'transitional' => 'thousand',
        'plural' => 'thousand',
      ),
      1000000 => array(
        'singular' => 'million',
        'transitional' => 'million',
        'plural' => 'million',
      ),
      1000000000 => array(
        'singular' => 'billion',
        'transitional' => 'billion',
        'plural' => 'billion',
      ),
      1000000000000 => array(
        'singular' => 'trillion',
        'transitional' => 'trillion',
        'plural' => 'trillion',
      ),
      1000000000000000 => array(
        'singular' => 'quadrillion',
        'transitional' => 'quadrillion',
        'plural' => 'quadrillion',
      ),
      1000000000000000000 => array(
        'singular' => 'quintillion',
        'transitional' => 'quintillion',
        'plural' => 'quintillion',
      ),
      'comma' => 'comma',
      'minus' => 'minus'
    )
  );

  if (strlen($lang) > 2) {
    $lang = substr($lang, 0, 2);
  }
  if (isset($dictionaries[$lang])) {
    $dictionary = $dictionaries[$lang];
  }
  else {
    return '';
  }

  if (!is_numeric($number)) {
    return FALSE;
  }

  if ($number > 1000000000000000000 || $number < -1000000000000000000) {
    drupal_set_message(t('Are you kidding me?'), 'error');
    return FALSE;
  }

  if ($number < 0) {
    return $dictionary['minus'] . ' ' . _gad_amount_in_words(abs($number), FALSE, $lang);
  }

  $string = $fraction = NULL;

  if (strpos($number, '.') !== FALSE) {
    list($number, $fraction) = explode('.', $number);
  }

  switch (TRUE) {
    case $number < 21:
      $string = $dictionary[$number];
      break;

    case $number < 100:
      $tens = ((int) ($number / 10)) * 10;
      $units = $number % 10;
      $string = $dictionary[$tens];
      if ($units) {
        $string .= ' ' . $dictionary[$units];
      }
      break;

    case $number < 1000:
      $remainder = $number % 100;
      $hundreds = $number - $remainder;
      $string = $dictionary[$hundreds] . ' ';
      if ($remainder) {
        $string .= _gad_amount_in_words($remainder, FALSE, $lang);
      }
      break;

    default:
      $baseUnit = pow(1000, floor(log($number, 1000)));
      $numBaseUnits = (int) ($number / $baseUnit);
      $remainder = $number % $baseUnit;
      if ($numBaseUnits == 1) {
        $string = $dictionary[$baseUnit]['singular'];
      }
      elseif ($numBaseUnits > 1 && $numBaseUnits < 5) {
        $string = _gad_amount_in_words($numBaseUnits, FALSE, $lang) . ' ' . $dictionary[$baseUnit]['transitional'];
      }
      else {
        $string = _gad_amount_in_words($numBaseUnits, FALSE, $lang) . ' ' . $dictionary[$baseUnit]['plural'];
      }
      if ($remainder) {
        $string .= $separator;
        $string .= _gad_amount_in_words($remainder, FALSE, $lang);
      }
      break;
  }

  if (NULL !== $fraction && is_numeric($fraction)) {
    $string .= ' ' . $dictionary['comma'] . ' ';
    $words = array();
    foreach (str_split((string) $fraction) as $number) {
      $words[] = $dictionary[$number];
    }
    $string .= implode(' ', $words);
  }

  return $string;
}

//Prepare invoice/correction common variables
function _sd_prepare_common_vars($order) {
  if (isset($order->order_data['data']['shipping_method'])) $shipping_method=$order->order_data['data']['shipping_method'];
  else $shipping_method=FALSE;
  $payment_methods=module_invoke_all('graber_cart_payment_info', $shipping_method);
  if (empty($order->order_data['data']['payment_method'])) $order->order_data['data']['payment_method']='upon';
  if (!empty($payment_methods[$order->order_data['data']['payment_method']])) $vars['payment_method']=$payment_methods[$order->order_data['data']['payment_method']]['title'];
  else $vars['payment_method']=FALSE;

  if (module_exists('shipment_options')) $shipment_options=shipment_options_graber_cart_shipping_info();
  else $shipment_options=array();
  if (isset($shipment_options[$shipping_method])) $vars['shipment_option']=$shipment_options[$order->order_data['data']['shipping_method']]['title'];
  else $vars['shipment_option']=t('Not applicable');
  return $vars;
}

//Delete all documents
function _gad_recursive_delete($path, $remove_parent=FALSE) {
  if (is_dir($path)) {
    $dh=opendir($path);
    while ($sub=readdir($dh)) {
      if ($sub == '.' || $sub == '..') continue;
      $subpath=$path.'/'.$sub;
      _gad_recursive_delete($subpath, TRUE);
    }
    if ($remove_parent) rmdir($path);
  }
  elseif (file_exists($path)) unlink($path);
}
function _gad_purge_documents() {
  $base_dir='private://accounting';
  $dh=opendir($base_dir);
  while ($subdir=readdir($dh)) {
    if ($subdir == '.' || $subdir == '..') continue;
    $path=$base_dir.'/'.$subdir;
    _gad_recursive_delete($path);
  }
  db_delete('accounting_sd_data')->execute();
}

function _gad_get_sd_changes($old_data, $new_data) {
  $changes=array();
  $difference=array(
    'net' => 0,
    'vat' => 0,
    'gross' => 0
  );
  $Nchanges=0;
  //Added articles
  foreach ($new_data['items'] as $iid => $item) {
    if (!isset($old_data['items'][$iid])) {
      $changes[]=array(
        'item_no' => ++$Nchanges,
        'before' => FALSE,
        'after' => $item
      );
      $difference['net']+=$item['net_value'];
      $difference['vat']+=$item['vat_value'];
      $difference['gross']+=$item['gross_value'];
    }
  }
    
  foreach ($old_data['items'] as $iid => $item) {
    //Deleted articles
    if (!isset($new_data['items'][$iid])) {
      $changes[]=array(
        'item_no' => ++$Nchanges,
        'before' => $item,
        'after' => FALSE
      );
      $difference['net']-=$item['net_value'];
      $difference['vat']-=$item['vat_value'];
      $difference['gross']-=$item['gross_value'];
    }
    //Changed articles
    elseif (
      ($item['quantity'] != $new_data['items'][$iid]['quantity']) || 
      ($item['vat_value'] != $new_data['items'][$iid]['vat_value']) ||
      ($item['net_value'] != $new_data['items'][$iid]['net_value']) ||
      ($item['title'] != $new_data['items'][$iid]['title'])
    ) {
      $changes[]=array(
        'item_no' => ++$Nchanges,
        'before' => $item,
        'after' => $new_data['items'][$iid]
      );
      $difference['net']+=$new_data['items'][$iid]['net_value']-$item['net_value'];
      $difference['vat']+=$new_data['items'][$iid]['vat_value']-$item['vat_value'];
      $difference['gross']+=$new_data['items'][$iid]['gross_value']-$item['gross_value'];
    }
  }
  
  //Amount difference in words
  if (!empty($changes)) $difference['gross_words']=_gad_amount_in_words($difference['gross'], 2, $new_data['language']);


  return array(
    'changes' => $changes,
    'difference' => $difference
  );
}