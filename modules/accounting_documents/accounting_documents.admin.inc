<?php
function ad_settings_form($form, &$form_state) {
  $settings=gp_get_settings('accounting_documents');
  $form['settings']=array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Accounting documents settings')
  );
  $form['settings']['sd_pattern']=array(
    '#type' => 'textfield',
    '#title' => t('Sale document code pattern'),
    '#description' => t('<p>Available wildcards:</p><ul>
      <li>!prefix - document type prefix</li>
      <li>!order_id - order id</li>
      <li>!month - issue date month</li>
      <li>!year - issue date year</li>
      <li>!serial - serial number for current document type</li>
    </ul><p>Cannot contain slashes.</p>'),
    '#default_value' => $settings['sd_pattern']
  );
  $form['settings']['correction_pattern']=array(
    '#type' => 'textfield',
    '#title' => t('Sale document correction code pattern'),
    '#description' => t('<p>Available wildcards:</p><ul>
      <li>!original - original document number</li>
      <li>!month - issue date month</li>
      <li>!year - issue date year</li>
      <li>!correction_no - correction number for original document (1st correction, 2nd correction, ..)</li>
      <li>!serial - serial number for current correction type</li>
    </ul><p>Cannot contain slashes.</p>'),
    '#default_value' => $settings['correction_pattern']
  );
  $form['settings']['next_pz']=array(
    '#title' => t('Next PZ'),
    '#type' => 'textfield',
    '#description' => t('The date when next PZ set will be generated (at time 0:00:00) (set according to accountant\'s instructions)'),
    '#default_value' => $settings['next_pz']
  );
  if (module_exists('date_popup_gp')) gp_element_add_date_popup($form['settings']['next_pz']);
  $form['settings']['sd_due_period']=array(
    '#type' => 'textfield',
    '#title' => t('Sale document payment deadline'),
    '#description' => t('Insert value in days. Used only in custom documents. To change order payment period, go to global graber_cart settings.'),
    '#default_value' => $settings['sd_due_period']
  );
  
  //Superadmin settings
  if (user_access('graber cart superadmin')) {
    $form['settings']['warehouse_documents']=array(
      '#type' => 'checkbox',
      '#title' => t('Enable warehouse documents'),
      '#default_value' => $settings['warehouse_documents']
    );
    $form['settings']['sd_generation_event']=array(
      '#type' => 'select',
      '#title' => t('Sale document generation'),
      '#options' => array(
        'payment' => t('When payment is received or upon receiving payment method'),
        'pro' => t('Pro-forma invoice on order creation, normal when payment is received'),
        'order' => t('On order creation'),
        'never' => t('Do not generate sale documents (manual generation only)')
      ),
      '#description' => t('When do we generate a sale document?'),
      '#default_value' => $settings['sd_generation_event']
    );
    $form['settings']['vat_calculation_method']=array(
      '#type' => 'radios',
      '#title' => t('VAT calculation method'),
      '#options' => array(
        'g-n' => t('Gross to net'),
        'n-g' => t('Net to gross')
      ),
      '#default_value' => $settings['vat_calculation_method']
    );
    $form['settings']['vat_calculation_type']=array(
      '#type' => 'radios',
      '#title' => t('VAT calculation type'),
      '#options' => array(
        'unit' => t('Every unit'),
        'summary' => t('Sums')
      ),
      '#default_value' => $settings['vat_calculation_type']
    );
    $form['settings']['generate_bill']=array(
      '#type' => 'checkbox',
      '#title' => t('Bill generation'),
      '#default_value' => $settings['generate_bill']
    );
    $form['settings']['generate_invoice']=array(
      '#type' => 'checkbox',
      '#title' => t('Invoice generation'),
      '#default_value' => $settings['generate_invoice']
    );
    $form['settings']['vat_field']=array(
      '#type' => 'textfield',
      '#title' => t('VAT field'),
      '#description' => t('Used only if warehouse is not enabled. Provide machine name for decimal type field.'),
      '#default_value' => $settings['vat_field']
    );
    $form['settings']['default_vat']=array(
      '#type' => 'textfield',
      '#title' => t('default VAT'),
      '#description' => t('This VAT value will be used if no other data is provided (warehouse product or field).'),
      '#default_value' => $settings['default_vat']
    );
  }
  
  
  $form['invoice_data']=array(
    '#type' => 'fieldset',
    '#title' => t('Invoice data'),
    '#tree' => TRUE
  );
  $form['invoice_data']['company']=array(
    '#type' => 'textfield',
    '#title' => t('Company'),
  );
  $form['invoice_data']['country']=array(
    '#type' => 'textfield',
    '#title' => t('Country'),
  );
  $form['invoice_data']['postal']=array(
    '#type' => 'textfield',
    '#title' => t('Postal code'),
  );
  $form['invoice_data']['city']=array(
    '#type' => 'textfield',
    '#title' => t('City'),
  );
  $form['invoice_data']['street']=array(
    '#type' => 'textfield',
    '#title' => t('Street'),
  );
  $form['invoice_data']['nip']=array(
    '#type' => 'textfield',
    '#title' => 'NIP',
  );
  $form['invoice_data']['bank']=array(
    '#type' => 'textfield',
    '#title' => t('Bank and acc no'),
  );

  $form['invoice_data']['invoice_logo']=array(
    '#type' => 'file',
    '#title'  => t("Invoice logo"),
    '#theme' => 'image_upload_preview'
  );

  $invoice_data=variable_get('graber_accounting_invoice_data', array());
  foreach ($invoice_data as $field => $value) {
    if (isset($form['invoice_data'][$field])) $form['invoice_data'][$field]['#default_value']=$value;
  }
  
  $form['save_data']=array(
    '#type' => 'submit',
    '#value' => t('Save data')
  );
  
  return $form;
}

function ad_settings_form_validate($form, &$form_state) {
  if (!empty($_FILES['files']['name']['invoice_data'])) {
    $extensions=array('png', 'gif', 'jpg', 'jpeg');
    $file=file_save_upload('invoice_data', array('file_validate_extensions' => array(implode(' ', $extensions))), 'public://', FILE_EXISTS_RENAME);
    if (!$file) {
      form_set_error('invoice_data][invoice_logo', t('Send image in one of the following formats: !formats', array('!formats' => implode(', ', $extensions))));
      $form_state['values']['invoice_data']['invoice_logo']=FALSE;
    }
    else {
      $form_state['values']['invoice_data']['invoice_logo_obj']=$file;
    }
  }
  
  if (strpos($form_state['values']['settings']['sd_pattern'], '/') !== FALSE) form_set_error('settings][sd_pattern', t('Document pattern cannot contain slashes (/).'));
  else{
    if (strpos($form_state['values']['settings']['sd_pattern'], '!serial') === FALSE) form_set_error('settings][sd_pattern', t('Document pattern must contain document serial number.'));
    if (strpos($form_state['values']['settings']['sd_pattern'], '!prefix') === FALSE) form_set_error('settings][sd_pattern', t('Document pattern must contain document type prefix.'));
  }
  
  my_api_validate_integer($form['settings']['sd_due_period'], $form_state, 0);
  my_api_validate_number($form['settings']['default_vat'], $form_state, 0, 100);
}

function ad_settings_form_submit($form, &$form_state) {
  if (isset($form_state['values']['invoice_data']['invoice_logo_obj']) && is_object($form_state['values']['invoice_data']['invoice_logo_obj'])) {
    if (!empty($form_state['values']['invoice_data']['invoice_logo'])) {
      $old_file=file_load($form_state['values']['invoice_data']['invoice_logo']);
      file_delete($old_file);
    }
    $file=$form_state['values']['invoice_data']['invoice_logo_obj'];
    $extension=substr($file->filename, strrpos($file->filename, '.')+1);
    $file->filename='invoice_logo.'.$extension;
    if ($file->uri != 'public://'.$file->filename) {
      $file=file_move($file, 'public://'.$file->filename, FILE_EXISTS_REPLACE);
    }
    $file->status=FILE_STATUS_PERMANENT;
    file_save($file);
    $form_state['values']['invoice_data']['invoice_logo']=$file->fid;
  }
  
  variable_set('graber_accounting_invoice_data', $form_state['values']['invoice_data']);
  gp_save_settings('accounting_documents', $form_state['values']['settings']);
  drupal_set_message(t('Invoice settings saved.'));
}
