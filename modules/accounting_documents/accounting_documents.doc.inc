<?php

function _gad_generate_wz($order, $all_batches, $type="wz", $preview=FALSE) {
  $name_base='private://accounting/wz';
  if ($type == 'wz-pro') $name_prefix='WZ-PRO';
  else $name_prefix='WZ';
  $my=date('m-Y', REQUEST_TIME);
  $filename=$name_prefix.'-'.$order->oid.'-'.$my.'.pdf';
  $No=0;
  while (file_exists($name_base.'/'.$my.'/'.$filename)) {
    $No++;
    $filename=$name_prefix.'-'.$order->oid.'-'.$my.'('.$No.').pdf';
  }
  if ($No == 0) $wz_no=$name_prefix.'-'.$order->oid.'-'.$my.'-'.gad_get_document_serial($type, $my);
  else $wz_no=$name_prefix.'-K-'.$order->oid.'-'.$my.'-'.gad_get_document_serial($type.'k', $my);

  $store_data=variable_get('graber_accounting_invoice_data', array());

  $items=gad_prepare_batch_items($all_batches);
  
  if ($type == 'wz-pro') $theme='accounting_wz_pro';
  else $theme='accounting_wz';
  
  $invoice_no=db_select('accounting_sd_data', 'asd')->fields('asd', array('document_no'))->condition('oid', $order->oid)->orderBy('modified', 'DESC')->execute()->fetchColumn();
  
  $theme_vars=array(
    'logo_url' => gad_get_logo_path(),
    'wz_no' => $wz_no,
    'invoice_no' => $invoice_no ? $invoice_no : t('N/A'),
    'store_data' => $store_data,
    'billing' => isset($order->order_data['billing_addr']) ? $order->order_data['billing_addr'] : FALSE,
    'shipping' => isset($order->order_data['shipping_addr']) ? $order->order_data['shipping_addr'] : FALSE,
    'order' => $order,
    'items' => $items
  );
  
  $wz_html=theme($theme, $theme_vars);
  if ($wz_html) {
    if ($preview) return $wz_html;
    if ($type == 'wz-pro') $get_path='wz-pro';
    else $get_path='wz';
    $get_path.='/'.$my;
    $name_base='private://accounting/'.$get_path;

    if (!file_exists($name_base)) mkdir($name_base);
    $filepath=$name_base.'/'.$filename;
    
    $footer=theme('accounting_wz_footer', $theme_vars);

    accounting_documents_generate_pdf($wz_html, $filepath, $footer);
    $get_path.='/'.$filename;
    return $get_path;
  }
}

function _gad_generate_wz_pro($order, $preview=FALSE) {
  $cart=new graber_cart($order->order_data['cid']);
  $Lp=0;
  $all_batches=array();
  foreach ($cart->contents as $iid => $item) {
    $Lp++;
    if (isset($item['warehouse_pid'])) {
      if (!isset($all_batches[$item['warehouse_wid']])) {
        $warehouse_name=db_query("SELECT title FROM {warehouses} WHERE wid = :wid", array(':wid' => $item['warehouse_wid']))->fetchColumn();
        $all_batches[$item['warehouse_wid']]=array(
          'warehouse_name' => $warehouse_name,
          'wid' => $item['warehouse_wid'],
          'batches' => array()
        );
      }
      
      $product=product_load($item['warehouse_pid']);
      if ($product) {
        $all_batches[$item['warehouse_wid']]['batches'][$Lp]=array(
          'name' => $product->name,
          'pid' => $product->pid,
          'reduced' => $item['warehouse_quantity']*$item['quantity'],
          'unit' => $product->unit
        );
        $all_batches[$item['warehouse_wid']]['batches'][$Lp]['product']=product_view($product);
      }
      else $all_batches[$item['warehouse_wid']]['batches'][$Lp]=array(
        'name' => $item['title'],
        'pid' => t('N/A'),
        'reduced' => $item['warehouse_quantity']*$item['quantity'],
        'unit' => t('N/A'),
      );
    }
  }
  
  $output=_gad_generate_wz($order, $all_batches, 'wz-pro', $preview);
  if ($preview) return $output;
  elseif (!empty($output)) drupal_goto('admin/store/accounting/get-document', array('query' => array('path' => $output)));
  else drupal_set_message(t('Document not generated.'), 'error');
}

// Invoice/bill
function _gad_generate_sale_document(&$order, $cart, $preview=FALSE) {

  //Sale document type
  $type=gad_get_document_type($order);
  if (!$type) return 0;
  
  $store_data=variable_get('graber_accounting_invoice_data', array());

  $ex_data=db_select('accounting_sd_data', 'asd')->fields('asd', array('iid', 'document_no', 'path'))->condition('oid', $order->oid)->condition('document_type', $type)->execute()->fetchObject();
  
  if ($ex_data) $document_no=$ex_data->document_no;
  else {
    $my=$dir=date('m-Y', $order->created);
    switch ($type) {
      case 'bill':
        $number_prefix='R';
        $name_base='bills';
        break;
      case 'invoice':
        $number_prefix='FV';
        $name_base='invoices';
        break;
      case 'invoice_pro':
        $number_prefix='PRO';
        $name_base='invoices_pro';
        break;
      default:
        $name_base='invoices';
        if (isset($order->order_data['data']['invoice_no_prefix'])) $number_prefix=$order->order_data['data']['invoice_no_prefix'];
        else $number_prefix='FV';
    }
      
    $serial=gad_get_document_serial($type, $my);
    $date_parts=explode('-', $my);
    
    $pattern=gp_get_settings('accounting_documents', 'sd_pattern');
    
    $document_no=strtr($pattern, array(
      '!prefix' => $number_prefix,
      '!order_id' => $order->oid,
      '!month' => $date_parts[0],
      '!year' => $date_parts[1],
      '!serial' => $serial
    ));
  }
  
  $filename=$document_no.'.pdf';
    
  $invoice_params=array(
    'type' => $type,
    'created' => $order->created,
    'logo_url' => gad_get_logo_path($store_data),
    'invoice_no' => $document_no,
    'store_data' => $store_data,
    'due_date' => $order->deadline,
    'billing' => isset($order->order_data['billing_addr']) ? $order->order_data['billing_addr'] : FALSE,
    'shipping' => isset($order->order_data['shipping_addr']) ? $order->order_data['shipping_addr'] : FALSE,
    'order' => $order,
    'language' => isset($order->language) ? $order->language : 'pl'
  );
    
  //Items and totals
  $invoice_params+=gad_prepare_account_data($order, $cart);
  //Shipping and billing
  $invoice_params+=_sd_prepare_common_vars($order);
  $invoice_html=theme('accounting_invoice', $invoice_params);

  $filepath=FALSE;
  if ($invoice_html) {
    if ($preview) return $invoice_html;
    if ($ex_data) $get_path=$ex_data->path;
    else {
      if (!file_exists('private://accounting/'.$name_base.'/'.$dir)) mkdir('private://accounting/'.$name_base.'/'.$dir);
      $get_path=$name_base.'/'.$dir.'/'.$filename;
    }
    $filepath='private://accounting/'.$get_path;
    $footer=theme('accounting_invoice_footer', $invoice_params);
    accounting_documents_generate_pdf($invoice_html, $filepath, $footer);
    $invoice_params['path']=$get_path;
    
    $order->order_data['data']['invoice_no']=$document_no;
    
    $fields=array(
      'oid' => $order->oid,
      'uid' => $order->uid,
      'contractor' => $order->orderer,
      'amount' => $order->amount,
      'document_no' => $document_no,
      'created' => REQUEST_TIME,
      'modified' => REQUEST_TIME,
      'due_date' => $due_date,
      'document_type' => $type,
      'path' => $get_path,
      'data' => serialize($invoice_params)
    );
    if ($ex_data) db_update('accounting_sd_data')->fields($fields)->condition('iid', $ex_data->iid)->execute();
    else db_insert('accounting_sd_data')->fields($fields)->execute();
    
    module_invoke_all('sale_document_generate', $document_no, $invoice_params, $order, $cart);
  }
  return $invoice_params;
}

// Invoice/bill correction
function _gad_generate_correction(&$order, $cart, $type, $reason=FALSE, $output_to_browser=FALSE) {
  if (!$reason) $reason=t('Orders details have been changed.');
  $generated=FALSE;
  $invoice_data=db_select('accounting_sd_data', 'asd')->fields('asd')->condition('oid', $order->oid)->condition('document_type', array($type, $type.'_correction'), 'IN')->orderBy('created', 'DESC')->range(0, 1)->execute()->fetchAssoc();
  if ($invoice_data) {
    $invoice_params=unserialize($invoice_data['data']);
    
    //Load products data (after update or product added / changed)
    foreach ($invoice_params['items'] as $iid => $item) {
      if (isset($item['warehouse_pid'])) {
        $product=product_load($item['warehouse_pid']);
        $invoice_params['items'][$iid]['product']=product_view($product);
      }
    }
    
    $new_data=gad_prepare_account_data($order, $cart);
    $change_data=_gad_get_sd_changes($invoice_params, $new_data);
    if (!empty($change_data['changes'])) {
    
      //Document type
      if (!strpos($type, '_correction')) $type.='_correction';
      
      //Generate correction
      $store_data=variable_get('graber_accounting_invoice_data', array());
      
      //Correction No
      if (!empty($invoice_data['changelog'])) {
        $changelog=unserialize($invoice_data['changelog']);
        $original_data=db_select('accounting_sd_data', 'asd')->fields('asd')->condition('iid', $invoice_params['original_id'])->execute()->fetchAssoc();
      }
      else {
        $changelog=array();
        $invoice_params['original_id']=$invoice_data['iid'];
        $original_data=$invoice_data;
      }
      
      $serial_no=sizeof($changelog)+1;

      $pattern=gp_get_settings('accounting_documents', 'correction_pattern');
      
      $my=date('m-Y', REQUEST_TIME);
      $date_parts=explode('-', $my);
      $correction_serial=gad_get_document_serial($type, date('m-Y', REQUEST_TIME));
      
      $correction_no=strtr($pattern, array(
        '!original' => $original_data['document_no'],
        '!month' => $date_parts[0],
        '!year' => $date_parts[1],
        '!correction_no' => $serial_no,
        '!serial' => $correction_serial
      ));
                  
      //Document file
      $filename=$correction_no.'.pdf';
      
      if ($type == 'bill' || $type == 'bill_correction') $get_path='bills/'.date('m-Y', $order->created).'/'.$filename;
      else $get_path='invoices/'.date('m-Y', $order->created).'/'.$filename;

      $changelog[]=array(
        'reason' => $reason,
        'get_path' => $get_path,
        'filename' => $filename,
        'correction_no' => $correction_no
      );
      
      //Update invoice params
      $invoice_params['created']=REQUEST_TIME;
      $invoice_params['logo_url']=gad_get_logo_path($store_data);
      $invoice_params['items']=$new_data['items'];
      $invoice_params['totals']=$new_data['totals'];
      $invoice_params['billing']=isset($order->order_data['billing_addr']) ? $order->order_data['billing_addr'] : FALSE;
      $invoice_params['shipping']=isset($order->order_data['shipping_addr']) ? $order->order_data['shipping_addr'] : FALSE;
      $invoice_params['order']=$order;
      
      $invoice_bs=_sd_prepare_common_vars($order);
      $invoice_params['payment_method']=$invoice_bs['payment_method'];
      $invoice_params['shipment_option']=$invoice_bs['shipment_option'];
      
      
      $theme_vars=array(
        'changes' => $change_data['changes'],
        'difference' => $change_data['difference'],
        'changelog' => $changelog,
        'correction_no' => $correction_no,
        'reason' => $reason,
        'language' => isset($order->language) ? $order->language : 'pl'
      );
      $theme_vars+=$invoice_params;
      
      $correction_html=theme('accounting_correction', $theme_vars);

      if ($correction_html) {
        $filepath='private://accounting/'.$get_path;
        
        $footer=theme('accounting_invoice_footer', $invoice_params);
        accounting_documents_generate_pdf($correction_html, $filepath, $footer);
        $invoice_params['path']=$get_path;
        $invoice_params['type']=$type;

        $fields=array(
          'oid' => $order->oid,
          'document_no' => $correction_no,
          'created' => REQUEST_TIME,
          'modified' => REQUEST_TIME,
          'document_type' => $type,
          'path' => $get_path,
          'data' => serialize($invoice_params),
          'changelog' => serialize($changelog)
        );
        unset($invoice_data['iid']);
        $fields+=$invoice_data;
        
        db_insert('accounting_sd_data')->fields($fields)->execute();
      }
      
      $invoice_params['changes']=$change_data['changes'];
      module_invoke_all('sale_document_generate', $correction_no, $invoice_params, $order, $cart);
      
      return $invoice_params;
    }
  }
  return FALSE;
}

// PZ
function _gad_generate_pz($code, $op) {
  $query=db_select('warehouse_batches', 'b');
  $query->condition('b.code', $code);
  $query->condition('b.transfer', 0);
    
  $query->fields('b', array('pid', 'quantity', 'cost', 'currency', 'received', 'gross_weight', 'remarks'));
  $query->orderBy('b.received', 'ASC');
  $results=$query->execute()->fetchAll();

  if (!empty($results)) {
    $items=array();
    $totals=array('weight' => 0, 'cost' => 0);
    $Lp=0;
    foreach ($results as $result) {
      $product=product_load($result->pid);
      if (!isset($pz_timestamp)) $pz_timestamp=$result->received;
      $Lp++;
      $items[$Lp]=array(
        'Lp' => $Lp,
        'code' => $code,
        'cost' => $result->cost.' '.$result->currency,
        'product' => product_view($product)
      );
      $items[$Lp]+=(array) $result;
      $totals['weight']+=$result->gross_weight;
      $totals['cost']+=$result->cost;
    }

    //Generate pz
    $name_base='private://accounting/pz';
    $dir=date('m-Y', $pz_timestamp);
    $store_data=variable_get('graber_accounting_invoice_data', array());

    if (!file_exists($name_base.'/'.$dir)) mkdir($name_base.'/'.$dir);

    $filename='PZ-'.$dir.'-'.strtr($code, array('/' => '-')).'.pdf';
    $No=0;
    while (file_exists($name_base.'/'.$dir.'/'.$filename)) {
      $No++;
      $filename='PZ-'.$dir.'-'.strtr($code, array('/' => '-')).'('.$No.')'.'.pdf';
    }
    if ($No > 0) $document_no='PZ-K-'.$dir.'-'.$code.'-'.gad_get_document_serial('pzk', $dir);
    else $document_no='PZ-'.$dir.'-'.$code.'-'.gad_get_document_serial('pz', $dir);
    $single_html=theme('accounting_pz', array(
      'logo_url' => gad_get_logo_path($store_data),
      'document_ts' => $pz_timestamp,
      'invoice_no' => $document_no,
      'store_data' => $store_data,
      'items' => $items,
      'totals' => $totals
    ));
    if ($single_html) {
      $filepath=$name_base.'/'.$dir.'/'.$filename;
      accounting_documents_generate_pdf($single_html, $filepath);
    }
  }
}

// Stock transfer
function _gad_generate_transfer(&$dst_batch, $params) {
  $src_items=gad_prepare_batch_items($params['src_batches']);
  $temp=array_values($src_items);
  $src_warehouse=$temp[0]['warehouse_name'];
  $src_wid=$temp[0]['wid'];

  $settings=warehouse_get_settings();
  
  $product=product_load($dst_batch['pid']);

  $dst_warehouse=db_select('warehouses', 'w')->fields('w', array('title'))->condition('wid', $dst_batch['wid'])->execute()->fetchColumn();
  $dst_items[$dst_batch['wid']]=array(
    'warehouse_name' => $dst_warehouse,
    'wid' => $dst_batch['wid'],
    'batches' => array(
      1 => array(
        'product' => product_view($product),
        'product_quantity' => $dst_batch['quantity'],
        'purchase_cost' => $dst_batch['cost']
      )
    )
  );
  $dst_items[$dst_batch['wid']]['batches'][1]+=$dst_batch;
  $dst_items[$dst_batch['wid']]['batches'][1]['product']=product_view($product);

  $my=$dir=date('m-Y', REQUEST_TIME);
  $name_base='private://accounting/transfers';
  if (!file_exists($name_base.'/'.$dir)) mkdir($name_base.'/'.$dir);

  $serial=gad_get_document_serial('transfer', $my);
  $filename='T-'.$my.'-'.$serial.'.pdf';
  $document_no='T-'.$my.'-'.$dst_batch['pid'].'-'.$src_wid.'-'.$dst_batch['wid'].'-'.$serial;
  $store_data=variable_get('graber_accounting_invoice_data', array());

  $document_html=theme('accounting_transfer', array(
    'logo_url' => gad_get_logo_path($store_data),
    'document_no' => $document_no,
    'store_data' => $store_data,
    'src_warehouse' => $src_warehouse,
    'dst_warehouse' => $dst_warehouse,
    'src_items' => $src_items,
    'dst_items' => $dst_items
  ));

  $filepath=FALSE;
  if ($document_html) {
    $filepath=$name_base.'/'.$dir.'/'.$filename;
    accounting_documents_generate_pdf($document_html, $filepath);
  }

  $dst_batch['code']=$document_no;

  return $document_no;
}
