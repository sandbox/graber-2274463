<?php
/**
 * Implements hook_entity_info().
 */
function warehouse_entity_info() {
  return array(
    'product' => array(
      'label' => t('Warehouse product'),
      'base table' => 'warehouse_products',
      'static cache' => TRUE,
      'field cache' => TRUE,
      'controller class' => 'ProductController',
      'uri callback' => 'product_entity_info_uri',
      'label callback' => 'product_entity_info_label',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'pid',
        'label' => 'name',
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Full content'),
          'custom settings' => FALSE,
        ),
        'teaser' => array(
          'label' => t('Teaser'),
          'custom settings' => TRUE,
        ),
        'rss' => array(
          'label' => t('RSS'),
          'custom settings' => FALSE,
        ),
      ),
      'bundles' => array(
        'product' => array(
          'label' => 'Warehouse product',
          'admin' => array(
            'path' => 'admin/structure/products',
            'access arguments' => array('graber cart superadmin'),
          ),
        ),
      ),
      
    ),
  );
}

function warehouse_build_modes($obj_type) {
  $modes = array();
  if ($obj_type == 'product') {
    $modes = array(
      'full' => t('Full node'),
      'teaser' => t('Teaser'),
      'rss' => t('RSS')
    );
  }
  return $modes;
}

function product_load_multiple($pids=array(), $conditions=array(), $reset=FALSE) {
  return entity_load('product', $pids, $conditions, $reset);
}

function product_load($pid=NULL, $reset=FALSE) {
  $pids=(isset($pid) ? array($pid) : array());
  $product=product_load_multiple($pids, array(), $reset);
  return $product ? reset($product) : FALSE;
}

function product_save(&$entity) {
  return entity_get_controller('product')->save($entity);
}

function product_update_quantities(&$entity) {
  return entity_get_controller('product')->update_quantities($entity);
}

function product_create() {
  return entity_get_controller('product')->create();
}

function product_delete($entity) {
  return entity_get_controller('product')->delete($entity);
}

function product_view($entity, $view_mode='default', $langcode=NULL) {
  return entity_get_controller('product')->view($entity, $view_mode, $langcode);
}


function product_entity_info_uri($product) {
  return array(
    'path' => 'product/' . $product->pid,
  );
}

function product_entity_info_label($product, $product_type) {
  return empty($product->name) ? 'Untitled product' : $product->name;
}


interface ProductControllerInterface
  extends DrupalEntityControllerInterface {
  public function create();
  public function save(&$entity);
  public function update_quantities(&$entity);
  public function load($pids=array(), $conditions=array());
  public function view($entity, $view_mode='default', $langcode=NULL);
  public function delete($entity);
}


class ProductController 
  extends DrupalDefaultEntityController
  implements ProductControllerInterface  {
  
  public function create() {
    $product=new stdClass();
    $product->pid=0;
    return $product;
  }

  public function save(&$product) {
    if (empty($product->pid)) {
      $product->created=REQUEST_TIME;
      $product->uid=$GLOBALS['user']->uid;
    }
    
    $warehouses=_warehouse_get_warehouses(TRUE);
    if (!isset($product->stock_data)) $product->stock_data=array();
    foreach ($warehouses as $wid => $w_name) {
      if (!isset($product->stock_data[$wid])) {
        $product->stock_data[$wid]=array(
          'wid' => $wid,
          'stock' => 0,
          'available' => 0
        );
      }
    }
    
    $product->modified=REQUEST_TIME;
    $primary_keys = $product->pid ? 'pid' : array();
    drupal_write_record('warehouse_products', $product, $primary_keys);
    
    $this->update_quantities($product);
    
    if (empty($primary_keys)) {
      field_attach_insert('product', $product);
    }
    else {
      field_attach_update('product', $product);
    }
    return $product;
  }
  
  public function update_quantities(&$product) {
    foreach ($product->stock_data as $wid => $data) {
      db_merge('warehouse_quantities')->key(array('pid' => $product->pid, 'wid' => $wid))->fields(array(
        'pid' => $product->pid,
        'wid' => $wid,
        'stock' => $data['stock'],
        'available' => $data['available']
      ))->execute();
    }
  }
  
  public function load($pids=array(), $conditions=array()) {
    $entities=array();
    $revision_id=FALSE;

    $passed_ids= !empty($pids) ? array_flip($pids) : FALSE;
    if ($this->cache) {
      $entities+=$this->cacheGet($pids, $conditions);
      if ($passed_ids) {
        $pids=array_keys(array_diff_key($passed_ids, $entities));
      }
    }

    if ($pids === FALSE || $pids || $revision_id || ($conditions && !$passed_ids)) {
      // Build the query.
      $query=$this->buildQuery($pids, $conditions, $revision_id);
      $queried_entities=$query->execute()->fetchAllAssoc($this->idKey);
    }

   if (!empty($queried_entities)) {
      $this->attachLoad($queried_entities, $revision_id);
      
      $identifier_field=warehouse_get_settings('product_identifier');
      if (!empty($identifier_field) && $identifier_field !== 'pid') {
        $field_info_instance=field_info_instance('product', $identifier_field, 'product');
      }
      
      //Load warehouse quantity data
      $quantity_db_data=db_select('warehouse_quantities', 'q')->fields('q')->condition('pid', $pids)->execute()->fetchAll();
      $quantity_data=array();
      foreach ($quantity_db_data as $item) {
        $quantity_data[$item->pid][$item->wid]=array(
          'wid' => $item->wid,
          'stock' => $item->stock,
          'available' => $item->available
        );
      }
      
      foreach ($queried_entities as $product) {
        //Process warehouse_quantities field
        if (isset($quantity_data[$product->pid])) $product->stock_data=$quantity_data[$product->pid];
        else $product->stock_data=array();
                
        //Add identifier field data
        if (empty($identifier_field) || $identifier_field == 'pid' || !isset($product->{$identifier_field}[LANGUAGE_NONE])) {
          $product->identifier_data['field']='pid';
          $product->identifier_data['value']=$product->pid;
          $product->identifier_data['string']=$product->name.' [pid='.$product->pid.']';
        }
        else {
          $field_data=$product->{$identifier_field}[LANGUAGE_NONE][0];
          $keys=array_keys($field_data);
          if (isset($keys['value'])) $column='value';
          else $column=reset($keys);
          $product->identifier_data['field']=$identifier_field;
          $product->identifier_data['value']=$field_data[$column];
          $product->identifier_data['string']=$product->name.' ['.$field_info_instance['label'].'='.$field_data[$column].']';
        }
      }
      
      
      
      $entities+=$queried_entities;
    }

    if ($this->cache) {
      // Add entities to the cache if we are not loading a revision.
      if (!empty($queried_entities) && !$revision_id) {
        $this->cacheSet($queried_entities);
      }
    }

    // Ensure that the returned array is ordered the same as the original
    // $ids array if this was passed in and remove any invalid ids.
    if ($passed_ids) {
      // Remove any invalid ids from the array.
      $passed_ids = array_intersect_key($passed_ids, $entities);
      foreach ($entities as $entity) {
        $passed_ids[$entity->{$this->idKey}]=$entity;
      }
      $entities=$passed_ids;
    }

    return $entities;
  }
  
  public function view($product, $view_mode='default', $langcode=NULL) {
    $renderable=array();
    static $instances;
    if (!isset($instances)) {
      $instances=array_keys(field_info_instances('product', 'product'));
    }
    foreach ($product as $property => $data) {
      if (in_array($property, $instances)) {
        $renderable[$property]=field_view_field('product', $product, $property, $view_mode, $langcode);
      }
    }
    $renderable+=array(
      'name' => array('#markup' => $product->name, '#raw' => $product->name),
      'created' => array('#markup' => format_date($product->created, 'short'), '#raw' => $product->created),
      'modified' => array('#markup' => format_date($product->modified, 'short'), '#raw' => $product->modified),
      'unit' => array('#markup' => $product->unit, '#raw' => $product->unit),
      'vat' => array('#markup' => $product->vat, '#raw' => $product->vat),
    );
    return $renderable;
  }

  public function delete($product) {
    $this->delete_multiple(array($product));
  }

  public function delete_multiple($products) {
    $pids=array();
    if (!empty($products)) {
      $transaction=db_transaction();
      try {
        foreach ($products as $product) {
          if (is_int($product)) $product=product_load($product);
          field_attach_delete('product', $product);
          $pids[]=$product->pid;
        }
        db_delete('warehouse_products')
          ->condition('pid', $pids, 'IN')
          ->execute();
        db_delete('warehouse_batches')
          ->condition('pid', $pids, 'IN')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('warehouse', $e);
        throw $e;
      }
    }
  }

  protected function buildQuery($ids, $conditions=array(), $revision_id=FALSE) {
    $query=parent::buildQuery($ids, $conditions, $revision_id);
    return $query;
  }
}