<?php
function warehouse_views_data() {
  // Products table.
  $data['warehouse_products']['table']['group'] = t('Warehouse product');
  $data['warehouse_products']['table']['base'] = array(
    'field' => 'pid',
    'title' => t('Products'),
    'help' => t('Products from your store warehouse.'),
  );
  $data['warehouse_products']['table']['entity type'] = 'product';

  $data['warehouse_products']['table']['join'] = array(
    'warehouse_quantities' => array(
      'handler' => 'views_join', // this is actually optional
      'left_table' => 'warehouse_quantities', // Because this is a direct link it could be left out.
      'left_field' => 'pid',
      'field' => 'pid',
      // also supported:
      // 'type' => 'INNER',
      // 'extra' => array(array('field' => 'fieldname', 'value' => 'value', 'operator' => '='))
      //   Unfortunately, you can't specify other tables here, but you can construct
      //   alternative joins in the handlers that can do that.
      // 'table' => 'the actual name of this table in the database',
     ),
  );

  $data['warehouse_products']['pid'] = array(
    'title' => t('Product ID'),
    'help' => t('The product ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'name',
      'numeric' => TRUE,
      'validate type' => 'pid',
    ),
  );

  $data['warehouse_products']['name'] = array(
    'title' => t('Product name'),
    'help' => t('The product name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    )
  );

  $data['warehouse_products']['unit'] = array(
    'title' => t('Product unit'),
    'help' => t('The product unit.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    )
  );
  
  $data['warehouse_products']['vat'] = array(
    'title' => t('Product VAT'),
    'help' => t('The product VAT.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    )
  );

  // uid field
  $data['warehouse_products']['uid'] = array(
    'title' => t('Author uid'),
    'help' => t('The user authoring the product. If you need more fields than the uid add the content: author relationship'),
    'relationship' => array(
      'title' => t('Author'),
      'help' => t('Relate content to the user who created it.'),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('author'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'field' => array(
      'handler' => 'views_handler_field_user',
    ),
  );
  
  // created field
  $data['warehouse_products']['created'] = array(
    'title' => t('Creation date'), // The item it appears as on the UI,
    'help' => t('The date the product was created.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // changed field
  $data['warehouse_products']['modified'] = array(
    'title' => t('Last modification date'), // The item it appears as on the UI,
    'help' => t('The date the product was last updated.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  //View, edit and delete links for products
  $data['warehouse_products']['view_product'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a simple link to the product.'),
      'handler' => 'views_handler_field_product_link',
    ),
  );

  $data['warehouse_products']['edit_product'] = array(
    'field' => array(
      'title' => t('Edit link'),
      'help' => t('Provide a simple link to edit the product.'),
      'handler' => 'views_handler_field_product_link_edit',
    ),
  );
  $data['warehouse_products']['delete_product'] = array(
    'field' => array(
      'title' => t('Delete link'),
      'help' => t('Provide a simple link to delete the product.'),
      'handler' => 'views_handler_field_product_link_delete',
    ),
  );
  $data['warehouse_products']['transfer_product'] = array(
    'field' => array(
      'title' => t('Transfer link'),
      'help' => t('Provide a simple link to transfer the product.'),
      'handler' => 'views_handler_field_product_link_transfer',
    ),
  );
  

  //Warehouse quantities table
  $data['warehouse_quantities']['table']['group']  = t('Warehouse stock');

  $data['warehouse_quantities']['table']['base'] = array(
    'field' => 'pid',
    'title' => t('Warehouse quantities'),
    'help' => t('Stores warehouse quantities data.'),
  );

  $data['warehouse_quantities']['table']['join'] = array(
    'warehouse_products' => array(
      'left_field' => 'pid',
      'field' => 'pid',
    ),
    'warehouses' => array(
      'left_field' => 'wid',
      'field' => 'wid',
    )
  );
  
  //warehouse id
  $data['warehouse_quantities']['wid'] = array(
    'title' => t('Warehouse ID'),
    'help' => t('ID of the warehouse.'),

    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
    
  // available
  $data['warehouse_quantities']['available'] = array(
    'title' => t('Available stock'),
    'help' => t('The available stock of the product in a warehouse (to buy).'),

    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // stock
  $data['warehouse_quantities']['stock'] = array(
    'title' => t('Actual stock'),
    'help' => t('The total stock of the product in a warehouse.'),

    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  
  // Warehouses table
  $data['warehouses']['table']['group']  = t('Warehouse');

  // Advertise this table as a possible base table
  $data['warehouses']['table']['base'] = array(
    'field' => 'wid',
    'title' => t('Warehouses'),
    'help' => t('Stores store warehouses.'),
  );

  $data['warehouses']['table']['join'] = array(
    'warehouse_quantities' => array(
      'left_field' => 'wid',
      'field' => 'wid',
    )
  );
  
  $data['warehouses']['wid'] = array(
    'title' => t('Warehouse ID'),
    'help' => t('The warehouse ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    )
  );

  // title
  $data['warehouses']['title'] = array(
    'title' => t('Warehouse name'),
    'help' => t('Name of the warehouse.'),

    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
     'argument' => array(
      'handler' => 'views_handler_argument_string',
     ),
  );
  
  //Warehouse batches table
  $data['warehouse_batches']['table']['group']  = t('Warehouse batches');

  // Advertise this table as a possible base table
  $data['warehouse_batches']['table']['base'] = array(
    'field' => 'bid',
    'title' => t('Warehouse batches'),
    'help' => t('Stores store warehouse batches information.'),
  );

  $data['warehouse_batches']['table']['join'] = array(
    'warehouse_products' => array(
      'left_field' => 'pid',
      'field' => 'pid',
    ),
    'warehouses' => array(
      'left_field' => 'wid',
      'field' => 'wid',
    )
  );
  
  $data['warehouse_batches']['bid'] = array(
    'title' => t('Batch ID'),
    'help' => t('The product batch ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
      'validate type' => 'bid',
    ),
  );
  
  //Warehouse id
  $data['warehouse_batches']['wid'] = array(
    'title' => t('Warehouse ID'),
    'help' => t('The batch warehouse ID.'),
    'field' => array(
      'handler' => 'warehouse_handler_field_warehouse_id',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
  );

  // uid field
  $data['warehouse_batches']['uid'] = array(
    'title' => t('Author uid'),
    'help' => t('The user authoring the batch. If you need more fields than the uid add the content: author relationship'),
    'relationship' => array(
      'title' => t('Author'),
      'help' => t('Relate content to the user who created it.'),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('author'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'field' => array(
      'handler' => 'views_handler_field_user',
    ),
  );
  
  //Batch code
  $data['warehouse_batches']['code'] = array(
    'title' => t('Batch code'),
    'help' => t('Inner code of the batch.'),

    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
     'argument' => array(
      'handler' => 'views_handler_argument_string',
     ),
  );

  //Invoice number
  $data['warehouse_batches']['invoice_no'] = array(
    'title' => t('Invoice number'),
    'help' => t('The number of the invoice the batch belonbs to.'),

    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
     'argument' => array(
      'handler' => 'views_handler_argument_string',
     ),
  );
  
  // quantity
  $data['warehouse_batches']['quantity'] = array(
    'title' => t('Batch quantity'),
    'help' => t('Product quantity in this batch.'),

    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // gross weight
  $data['warehouse_batches']['gross_weight'] = array(
    'title' => t('Batch weight'),
    'help' => t('Gross weight of this batch.'),

    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // remaining
  $data['warehouse_batches']['remaining'] = array(
    'title' => t('Remaining quantity'),
    'help' => t('How much of this batch is still on the warehouse.'),

    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // cost
  $data['warehouse_batches']['cost'] = array(
    'title' => t('Batch cost'),
    'help' => t('How much did this entire batch cost.'),

    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  //Currency
  $data['warehouse_batches']['currency'] = array(
    'title' => t('Batch currency'),
    'help' => t('The invoice for this batch was paid in this currency.'),

    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
     'argument' => array(
      'handler' => 'views_handler_argument_string',
     ),
  );

  // received field
  $data['warehouse_batches']['received'] = array(
    'title' => t('Batch recieving date'),
    'help' => t('The date the batch was received.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  // received field
  $data['warehouse_batches']['expiry'] = array(
    'title' => t('Batch expiry date'),
    'help' => t('The date the batch expires.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  //remarks
  $data['warehouse_batches']['remarks'] = array(
    'title' => t('Remarks'),
    'help' => t('Additional info for this batch.'),

    'field' => array(
      'handler' => 'views_handler_field',
     )
  );

  return $data;
}

function warehouse_views_query_substitutions($view) {
  return array('***CURRENT_WAREHOUSE***' => warehouse_get_current_wid());
}

function warehouse_views_plugins() {
  return array(
    'module' => 'warehouse', // This just tells our themes are elsewhere.
    'argument default' => array(
      'current_warehouse' => array(
        'title' => t('Warehouse ID of the current user selected warehouse or default warehouse'),
        'handler' => 'views_plugin_argument_default_current_warehouse',
        'path' => drupal_get_path('module', 'warehouse') . '/views', // not necessary for most modules
      ),
    )
  );
}


/**
 * Implements hook_views_data_alter().
 */
function warehouse_views_data_alter(&$data) {
  $data['node']['product_reference'] = array(
    'title' => t('Warehouse products assigned to node'),
    'help' => t('Relate nodes (offers) to products they represent. This relationship will cause duplicated records if there are multiple products.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship_product_reference',
      'label' => t('product'),
      'base' => 'warehouse_products',
    ),
    'field' => array(
      'title' => t('All products'),
      'help' => t('Display all products associated with a node.'),
      'handler' => 'views_handler_field_product_reference',
      'no group by' => TRUE,
    ),
  );
}

function warehouse_field_views_data($field) {
  $data = field_views_field_default_views_data($field);
  foreach ($data as $table_name => $table_data) {

    // Add the relationship only on the pid field.
    $data[$table_name][$field['field_name'] . '_pid']['relationship'] = array(
      'handler' => 'views_handler_relationship',
      'base' => 'warehouse_products',
      'base field' => 'pid',
      'label' => t('product from !field_name', array('!field_name' => $field['field_name'])),
    );

  }

  return $data;
}

/**
 * Implements hook_field_views_data_views_data_alter().
 *
 * Views integration to provide reverse relationships on product references.
 */
function warehouse_field_views_data_views_data_alter(&$data, $field) {
  foreach ($field['bundles'] as $entity_type => $bundles) {
    $entity_info = entity_get_info($entity_type);
    $pseudo_field_name = 'reverse_' . $field['field_name'] . '_' . $entity_type;

    list($label, $all_labels) = field_views_field_label($field['field_name']);
    $entity = $entity_info['label'];

    $data['warehouse_products'][$pseudo_field_name]['relationship'] = array(
      'title' => t('@entity using @field', array('@entity' => $entity, '@field' => $label)),
      'help' => t('Relate each @entity with a @field set to the product.', array('@entity' => $entity, '@field' => $label)),
      'handler' => 'views_handler_relationship_entity_reverse',
      'field_name' => $field['field_name'],
      'field table' => _field_sql_storage_tablename($field),
      'field field' => $field['field_name'] . '_pid',
      'base' => $entity_info['base table'],
      'base field' => $entity_info['entity keys']['id'],
      'label' => t('!field_name', array('!field_name' => $field['field_name'])),
      'join_extra' => array(
        0 => array(
          'field' => 'entity_type',
          'value' => $entity_type,
        ),
        1 => array(
          'field' => 'deleted',
          'value' => 0,
          'numeric' => TRUE,
        ),
      ),
    );
  }
}