<?php
class warehouse_handler_field_warehouse_id extends views_handler_field {
  /**
   * Override init function to provide generic option to link to warehouse.
   */
  function init(&$view, &$data) {
    parent::init($view, $data);
    if (!empty($this->options['display_as_name'])) {
      $this->additional_fields['wid'] = array('table' => 'warehouse_batches', 'field' => 'wid');
    }
  }

  /**
   * Overrides views_handler::option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['display_as_name'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Overrides views_handler::options_form().
   *
   * Provides link to product administration page.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['display_as_name'] = array(
      '#title' => t('Display as a warehouse name'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['display_as_name']),
    );
  }

  /**
   * Overrides views_handler_field::render().
   */
  function render($values) {
    if (!empty($this->options['display_as_name'])) {
      $wid=$this->get_value($values, 'wid');
      if ($wid) {
        $wid=(int) $wid;
        $value=db_query("SELECT title FROM {warehouses} WHERE wid = :wid", array(':wid' => $wid))->fetchColumn();
        if (empty($value)) $value=t('default');
        return $this->sanitize_value($value);
      }
    }
    $value = $this->get_value($values);
    return $this->sanitize_value($value);
  }

}