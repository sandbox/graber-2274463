<?php

/**
 * @file
 * Definition of views_handler_field_node_link_delete.
 */

/**
 * Field handler to present a link to delete a node.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_product_link_delete extends views_handler_field_product_link {

  /**
   * Renders the link.
   */
  function render_link($product, $values) {
    // Ensure user has access to delete this node.
    if (!user_access('administer graber cart')) {
      return;
    }

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = "admin/store/warehouse/delete-product/$product->pid";
    $this->options['alter']['query'] = drupal_get_destination();

    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
    return $text;
  }
}
