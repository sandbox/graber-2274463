<?php

/**
 * @file
 * Definition of views_handler_relationship_product_reference.
 */

/**
 * Relationship handler to return products of nodes.
 *
 * @ingroup views_relationship_handlers
 */
class views_handler_relationship_product_reference extends views_handler_relationship  {

  /**
   * Called to implement a relationship in a query.
   */
  function query() {
    $this->ensure_my_table();

    $def = $this->definition;
    $def['table'] = 'warehouse_products';

    $warehouse_products = $this->query->add_table('warehouse_products', $this->relationship);
    $def['left_table'] = $warehouse_products;
    $def['left_field'] = 'pid';
    $def['field'] = 'pid';
    $def['type'] = empty($this->options['required']) ? 'LEFT' : 'INNER';

    $join = new views_join();

    $join->definition = $def;
    $join->construct();
    $join->adjusted = TRUE;

    // use a short alias for this:
    $alias = $def['table'] . '_' . $this->table;

    $this->alias = $this->query->add_relationship($alias, $join, 'warehouse_products', $this->relationship);
  }
}
