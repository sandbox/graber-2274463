<?php

/**
 * @file
 * Definition of views_handler_field_term_node_tid.
 */

/**
 * Field handler to display all taxonomy terms of a node.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_product_reference extends views_handler_field_prerender_list {
  function init(&$view, &$options) {
    parent::init($view, $options);
    // @todo: Wouldn't it be possible to use $this->base_table and no if here?
    if ($view->base_table == 'node_revision') {
      $this->additional_fields['nid'] = array('table' => 'node_revision', 'field' => 'nid');
    }
    else {
      $this->additional_fields['nid'] = array('table' => 'node', 'field' => 'nid');
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['link_to_product'] = array('default' => TRUE, 'bool' => TRUE);
    $options['limit'] = array('default' => FALSE, 'bool' => TRUE);

    return $options;
  }

  /**
   * Provide "link to term" option.
   */
  function options_form(&$form, &$form_state) {
    $form['link_to_product'] = array(
      '#title' => t('Link this field to its product page'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_product']),
    );

    $form['limit'] = array(
      '#type' => 'checkbox',
      '#title' => t('Limit products'),
      '#default_value'=> $this->options['limit'],
    );
    parent::options_form($form, $form_state);
  }

  /**
   * Add this term to the query
   */
  function query() {
    $this->add_additional_fields();
  }

  function pre_render(&$values) {
    $this->field_alias = $this->aliases['nid'];
    $nids = array();
    foreach ($values as $result) {
      if (!empty($result->{$this->aliases['nid']})) {
        $nids[] = $result->{$this->aliases['nid']};
      }
    }

    if ($nids) {
      $query = db_select('warehouse_products', 'p');
      $query->fields('p');
      $query->addTag('product_access');
      $result = $query->execute();

      foreach ($result as $product) {
        $this->items[$product->pid]['name'] = check_plain($product->name);
        $this->items[$product->pid]['pid'] = $product->pid;

        if (!empty($this->options['link_to_taxonomy'])) {
          $this->items[$product->pid]['make_link'] = TRUE;
          $this->items[$product->pid]['path'] = 'product/' . $product->pid;
        }
      }
    }
  }

  function render_item($count, $item) {
    return $item['name'];
  }

  function document_self_tokens(&$tokens) {
    $tokens['[' . $this->options['id'] . '-pid' . ']'] = t('The product ID.');
    $tokens['[' . $this->options['id'] . '-name' . ']'] = t('The product name.');
  }

  function add_self_tokens(&$tokens, $item) {
    foreach(array('tid', 'name') as $token) {
      // Replace _ with - for the vocabulary machine name.
      $tokens['[' . $this->options['id'] . '-' . str_replace('_', '-', $token). ']'] = isset($item[$token]) ? $item[$token] : '';
    }
  }
}
