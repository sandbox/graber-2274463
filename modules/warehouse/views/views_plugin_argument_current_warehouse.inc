<?php

class views_plugin_argument_default_current_warehouse extends views_plugin_argument_default {
  function get_argument() {
    return warehouse_get_current_wid();
  }
}
