<?php
function warehouse_field_info() {
	return array(
		'product_reference' => array(
			'label' => t('Warehouse product reference'),
			'description' => '',
			'default_widget' => 'warehouse_product_autocomplete',
			'default_formatter' => 'warehouse_related_view',
		),
    'warehouse_quantities' => array(
			'label' => t('Warehouse quantities'),
			'description' => '',
			'default_widget' => 'warehouse_quantities_widget',
			'default_formatter' => 'warehouse_quantities_view',
    ),
	);
}

function warehouse_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
	foreach ($items as $delta => $item) {
		if (!empty($item['pid'])) {
			if (!is_numeric($item['pid'])) {
				$errors[$field['field_name']][$langcode][$delta][] = array(
					'error' => 'field_product_reference_invalid',
					'message' => t('Error in product reference: product does not exist.'),
				);
			}
			if (!empty($item['quantity']) && !is_numeric($item['quantity'])) {
				$errors[$field['field_name']][$langcode][$delta][] = array(
					'error' => 'field_product_reference_invalid',
					'message' => t('Error in product reference: quantity must be numeric.'),
				);
			}
		}
	}
}

function warehouse_field_is_empty($item, $field) {
  if ($field['type'] == 'product_reference') {
    if (empty($item['pid']) || empty($item['wid'])) return true;
    else return false;
  }
  elseif ($field['type'] == 'warehouse_quantities') {
    if (empty($item['wid'])) return true;
    else return false;
  }
}

function warehouse_field_formatter_info() {
	return array(
		'warehouse_related_view' => array(
			'label' => t('Default'),
			'field types' => array('product_reference'),
		),
    'warehouse_quantities_view' => array(
			'label' => t('Default'),
			'field types' => array('warehouse_quantities'),
    )
	);
}

function warehouse_field_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items) {
  if ($field['type'] == 'warehouse_quantities') {
    foreach ($entities as $pid => $product) {
      $items[$pid]=array();
      if (!empty($product->stock_data)) {
        foreach ($product->stock_data as $stock_data) $items[$pid][]=$stock_data;
      }
    }
  }
}

function warehouse_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
	$element = array();
	switch ($display['type']) {
		case 'warehouse_related_view':
			foreach ($items as $delta => $item) {
        $value=_warehouse_get_identifier_string($item['pid']);
        if (!$value) $value=('No identifier specified for this offer\'s base product.');
				$element[$delta]['#markup']=$value;
			}
			break;
    case 'warehouse_quantities_view':
      $warehouses=_warehouse_get_warehouses(TRUE);
			foreach ($items as $delta => $item) {
        $element[$delta]['#markup']=$warehouses[$item['wid']].': stock '.$item['stock'].', available '.$item['available'];
      }
      break;
	}
	return $element;
}

function warehouse_field_widget_info() {
	return array(
		'warehouse_product_autocomplete' => array(
			'label' => t('Default'),
			'field types' => array('product_reference'),
		),
    'warehouse_quantities_widget' => array(
			'label' => t('Default'),
			'field types' => array('warehouse_quantities'),
    )
	);
}

function warehouse_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

	if ($instance['widget']['type'] == 'warehouse_product_autocomplete') {
    $form_state['warehouse_field']['#field_name']=$field['field_name'];

		if (!empty($form_state['values'][$field['field_name']][$langcode][$delta])) {
			$values=$form_state['values'][$field['field_name']][$langcode][$delta];
		}
		elseif (!empty($items[$delta])) $values=$items[$delta];

		if (!empty($values['pid'])) {
      $form_state['warehouse_product']=$product=product_load($values['pid']);
		}
		$element+=array(
			'#type' => 'fieldset',
			'#title' => t('Warehouse product reference')
		);
		
		$Nwarehouses=db_query("SELECT COUNT(wid) FROM {warehouses}")->fetchColumn();
		if ($Nwarehouses > 1) {
			$options=db_select('warehouses', 'w')->fields('w', array('wid', 'title'))->execute()->fetchAllKeyed(0,1);
			$fields['wid']=array(
				'#type' => 'select',
				'#title' => t('Warehouse'),
				'#options' => $options,
				'#default_value' => isset($values['wid']) ? $values['wid'] : 0
			);
		}
		else {
			$wid=db_query("SELECT wid FROM {warehouses} LIMIT 1")->fetchColumn();
			$fields['wid']=array('#type' => 'value', '#value' => $wid);
		}
		$fields['pid']=array(
			'#type' => 'textfield',
			'#autocomplete_path' => 'admin/store/warehouse/product-autocomplete/name',
			'#title' => t('Product'),
			'#ajax' => array(
				'wrapper' => 'field-package-quantity-wrapper',
				'event' => 'blur',
				'callback' => 'warehouse_widget_form_ajax',
				'method' => 'replace',
				'effect' => 'fade',
			),
			'#element_validate' => array('warehouse_validate_product_reference'),
			'#default_value' => empty($product) ? '' : $product->identifier_data['string']
		);
		if (!empty($product)) {
			$fields['quantity']=array(
				'#type' => 'textfield',
				'#title' => t('Offer quantity'),
				'#size' => 15,
				'#field_suffix' => ' '.$product->unit,
				'#default_value' => empty($values['quantity']) ? '' : $values['quantity'],
        '#element_validate' => array('my_api_validate_element_number'),
			);
		}
		$fields['quantity']['#suffix']='</div>';
		$fields['quantity']['#prefix']='<div id="field-package-quantity-wrapper">';
		
		$element+=$fields;
	}
  elseif ($instance['widget']['type'] == 'warehouse_quantities_widget') {
    //There is no widget form :)
  }
  
	return $element;
}

function warehouse_validate_product_reference($element, &$form_state, $form) {
  if (!empty($element['#value'])) {
    if ($pid=warehouse_get_autocomplete_pid($element['#value'])) {
      form_set_value($element, $pid, $form_state);
    }
    else {
      form_error($element, t('Product does not exist. Please use autocomplete function.'));
    }
  }
}

function warehouse_widget_form_ajax($form, $form_state) {
	$lang=$form[$form_state['warehouse_field']['#field_name']]['#language'];
	$field=$form[$form_state['warehouse_field']['#field_name']][$lang][0]['quantity'];
	return $field;
}


//Views integration
function _warehouse_potential_references($field, $options=array()) {
  // Fill in default options.
  $options += array(
    'string' => '',
    'match' => 'contains',
    'ids' => array(),
    'limit' => 0,
  );

  $results = &drupal_static(__FUNCTION__, array());

  // Create unique id for static cache.
  $cid=$field['field_name'].':'.$options['match'].':'.($options['string'] !== '' ? $options['string'] : implode('-', $options['ids'])).':'.$options['limit'];
    
  if (!isset($results[$cid])) {
  
    $references=array();
    // Avoid useless work
    if (!count($field['settings']['referenceable_types'])) {
      $results[$cid]=array();
      return $results[$cid];
    }

    $query=db_select('warehouse_products', 'p');
    $product_pid_alias=$query->addField('p', 'pid');
    $product_name_alias=$query->addField('p', 'name', 'product_name');
    $query->addTag('warehouse_access')
      ->addMetaData('id', ' _warehouse_potential_references')
      ->addMetaData('field', $field)
      ->addMetaData('options', $options);

    if ($options['string'] !== '') {
      switch ($options['match']) {
        case 'contains':
          $query->condition('p.name', '%' . $options['string'] . '%', 'LIKE');
          break;

        case 'starts_with':
          $query->condition('p.name', $options['string'] . '%', 'LIKE');
          break;

        case 'equals':
        default: // no match type or incorrect match type: use "="
          $query->condition('p.name', $options['string']);
          break;
      }
    }

    if ($options['ids']) {
      $query->condition('p.pid', $options['ids'], 'IN');
    }

    if ($options['limit']) {
      $query->range(0, $options['limit']);
    }

    $query->orderBy($product_name_alias);

    $result=$query->execute()->fetchAll();
    foreach ($result as $product) {
      $references[$product->pid] = array(
        'title'    => $product->product_name,
        'rendered' => check_plain($product->product_name),
      );
    }
  
    $results[$cid] = !empty($references) ? $references : array();
  }

  return $results[$cid];
}

function _warehouse_reference_options($field, $flat = TRUE) {
  $references=_warehouse_potential_references($field);

  $options = array();
  foreach ($references as $key => $value) {
    // The label, displayed in selects and checkboxes/radios, should have HTML
    // entities unencoded. The widgets (core's options.module) take care of
    // applying the relevant filters (strip_tags() or filter_xss()).
    $label = html_entity_decode($value['rendered'], ENT_QUOTES);
    if (empty($value['group']) || $flat) {
      $options[$key] = $label;
    }
    else {
      // The group name, displayed in selects, cannot contain tags, and should
      // have HTML entities unencoded.
      $group = html_entity_decode(strip_tags($value['group']), ENT_QUOTES);
      $options[$group][$key] = $label;
    }
  }

  return $options;
}

function warehouse_views_filter_options($field_name) {
  $options = array();

  if ($field = field_info_field($field_name)) {
    $options = _warehouse_reference_options($field, TRUE);

    foreach ($options as $key => $value) {
      $options[$key] = strip_tags($value);
    }
  }

  return $options;
}
