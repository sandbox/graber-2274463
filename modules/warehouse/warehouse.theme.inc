<?php
function theme_warehouse_add_batch_table($vars) {
	$batches=$vars['fieldset'];
	$output='';
	
	$header=array();
	foreach ($batches as $index => $batch) {
		if (substr($index, 0, 1) !== '#') {
			foreach ($batch as $name => $cell) {
				if (substr($name, 0, 1) !== '#') {
					if ($cell['#type'] != 'value') {
						$label=isset($cell['#title']) ? $cell['#title'] : $cell['#value'];
						$header[$name]=array('data' => $label, 'class' => array($name));
					}
				}
			}
			break;
		}
	}
	
	$rows=array();
	$Nrows=0;
	foreach ($batches as $index => $batch) {
		if (substr($index, 0, 1) != '#') {
			foreach ($header as $name => $obsolete) {
				unset($batches[$index][$name]['#title']);
				unset($batches[$index][$name]['#description']);
				if ($batches[$index][$name]['#type'] == 'date_popup') {
					unset($batches[$index][$name]['date']['#title']);
					unset($batches[$index][$name]['date']['#description']);
				}
				$rows[$Nrows][$name]=array('data' => drupal_render($batches[$index][$name]), 'class' => array($name));
			}
			unset($batches[$index]);
			$Nrows++;
		}
	}
	
	$table=theme('table', array('header' => $header, 'rows' => $rows, 'sticky' => FALSE));
	$output.=$batches['#prefix'].$table.$batches['#suffix'];
	
	return $output;
}