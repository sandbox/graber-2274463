<?php
function warehouse_settings_form($form, &$form_state) {
  $settings=warehouse_get_settings();
  $form['settings']=array(
    '#tree' => TRUE
  );
  $form['settings']['units']=array(
    '#type' => 'textarea',
    '#title' => t('Units'),
    '#rows' => 3,
    '#default_value' => implode(', ', $settings['units'])
  );
  
  $form['settings']['show_quantities']=array(
    '#type' => 'checkbox',
    '#title' => t('Allow users to see exact stock values when unable to add to cart'),
    '#default_value' => isset($settings['show_quantities']) ? $settings['show_quantities'] : 1
  );

  $product_identifiers=array(
    'pid' => t('Product ID')
  );
  $product_fields=array();
  $field_info=field_info_instances('product');
  if (!empty($field_info['product'])) {
    foreach ($field_info['product'] as $m_name => $data) {
      $detail_info=field_info_field($m_name);
      if ($detail_info['cardinality'] == 1) {
        $product_identifiers[$m_name]=$data['label'];
      }
      $product_fields[$m_name]=$data['label'];
    }
  }
    
      
  $form['settings']['product_identifier']=array(
    '#type' => 'radios',
    '#title' => t('Product identifier field'),
    '#description' => t('Displayed on cart and order administration pages. Note: Only single-value fields can be set as product identifier.'),
    '#options' => $product_identifiers,
    '#default_value' => isset($settings['product_identifier']) ? $settings['product_identifier'] : 'pid'
  );
  
  $form['settings']['product_fields']=array(
    '#type' => 'fieldset',
    '#title' => t('Product fields settings'),
    '#theme' => 'graber_form_table',
    '#tree' => TRUE,
    '#settings' => array('row_numbers' => FALSE)
  );
  
  foreach (array('display' => t('Display'), 'filter' => t('Filter')) as $key => $label) {
    $form['settings']['product_fields'][$key]=array();
    
    $form['settings']['product_fields'][$key]['label']=array(
      '#markup' => $label,
      '#title' => t('Property')
    );
    foreach (array(
      'products_table' => t('Product list'),
      'summary_table' => t('Summary table')
    ) as $place => $place_label) {
    
      $form['settings']['product_fields'][$key][$place]=array(
        '#type' => 'checkboxes',
        '#title' => $place_label,
        '#options' => $product_fields,
        '#default_value' => isset($settings['product_fields'][$key][$place]) ? $settings['product_fields'][$key][$place] : array()
      );
    }
  }
  
  $form['save']=array(
    '#type' => 'submit',
    '#value' => t('Save data')
  );
  return $form;
}

function warehouse_settings_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['settings']['units'])) {
    $units=explode(',', $form_state['values']['settings']['units']);
    $units_arr=array();
    foreach ($units as $index => $value) {
      $unit=trim($value);
      $units_arr[$unit]=trim($unit);
    }
    $form_state['values']['settings']['units']=$units_arr;
  }
}

function warehouse_settings_form_submit($form, &$form_state) {
  $settings=$form_state['values']['settings'];
  variable_set('warehouse_settings', $settings);
  drupal_set_message(t('Data saved.'));
}