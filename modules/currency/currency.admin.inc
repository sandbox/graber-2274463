<?php
function currency_settings_form($form, &$form_state) {
  $form['settings']=array('#tree' => TRUE);
  $settings=gp_get_settings('currency');
  $currencies_str=implode(', ', $settings['currencies']);
  
  $form['settings']['currencies']=array(
    '#type' => 'textarea',
    '#title' => t('Currencies'),
    '#default_value' => $currencies_str,
    '#required' => TRUE
  );
  $form['settings']['base']=array(
    '#type' => 'radios',
    '#title' => t('Base currency'),
    '#options' => $settings['currencies'],
    '#default_value' => $settings['base']
  );
  
  $form['actions']=array('#type' => 'actions');
  $form['actions']['save']=array(
    '#type' => 'submit',
    '#value' => t('Save settings')
  );
  
  return $form;
}

function currency_settings_form_validate($form, &$form_state) {
  $currencies=explode(',', $form_state['values']['settings']['currencies']);
  $form_state['values']['settings']['currencies']=array();
  foreach ($currencies as $index => $currency) {
    $currency=trim($currency);
    $form_state['values']['settings']['currencies'][$currency]=$currency;
  }
}

function currency_settings_form_submit($form, &$form_state) {
  gp_save_settings('currency', $form_state['values']['settings'], FALSE, TRUE);
  drupal_set_message(t('Settings saved.'));
}

function currency_rates_form($form, &$form_state) {
  $form['rates']=array(
    '#tree' => TRUE,
    '#theme' => 'graber_form_table'
  );
  $settings=gp_get_settings('currency');
  $rates=variable_get('currency_exchange_rates', array());
  
  foreach ($settings['currencies'] as $currency) {
    if ($currency === $settings['base']) continue;
    $form['rates'][$currency]['label']=array(
      '#title' => t('Currency'),
      '#markup' => $currency.'/'.$settings['base']
    );
    $form['rates'][$currency]['modified']=array(
      '#title' => t('Date'),
      '#markup' => isset($rates[$currency]) ? format_date($rates[$currency]['modified'], 'short') : ''
    );
    $form['rates'][$currency]['rate']=array(
      '#title' => t('Exchange rate'),
      '#type' => 'textfield',
      '#default_value' => isset($rates[$currency]) ? $rates[$currency]['rate'] : ''
    );
  }
  
  $form['actions']=array('#type' => 'actions');
  $form['actions']['save']=array(
    '#type' => 'submit',
    '#value' => t('Save settings')
  );
  
  return $form;
}

function currency_rates_form_validate($form, &$form_state) {
  foreach ($form['rates'] as $currency => $fields) {
    if (substr($currency, 0, 1) === '#') continue;
    my_api_validate_number($fields['rate'], $form_state, 0);
  }
}

function currency_rates_form_submit($form, &$form_state) {
  $rates=variable_get('currency_exchange_rates', array());
  foreach ($form_state['values']['rates'] as $currency => $data) {
    if (isset($rates[$currency])) {
      if ($rates[$currency]['rate'] != $data['rate']) {
        $rates[$currency]=array(
          'rate' => $data['rate'],
          'modified' => REQUEST_TIME
        );
      }
    }
    else $rates[$currency]=array(
      'rate' => $data['rate'],
      'modified' => REQUEST_TIME
    );
  }
  variable_set('currency_exchange_rates', $rates);
  drupal_set_message(t('Settings saved.'));
}
