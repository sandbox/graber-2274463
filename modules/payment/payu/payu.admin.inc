<?php

function payu_view_settings(){
	$out = drupal_get_form('payu_settings_form');
	return $out;
}
function payu_settings_form($form_state) {
	$payu_values=variable_get('payu_settings', array());
	
	if (!empty($payu_values)) {
		global $base_url;
		$addresses=array(
			'Poprawna autoryzacja: '.url('payu/success', array('absolute' => TRUE)),
			'Błędna autoryzacja: '.url('payu/error', array('absolute' => TRUE)),
			'Adres raportów: '.url('payu/webapi', array('absolute' => TRUE)),
		);
		$form['payu_addresses']=array(
			'#prefix' => '<h3>Adresy do wstawienia w konfiguracji punktu płatności:</h3>',
			'#markup' => theme('item_list', array('items' => $addresses))
		);
	}
	
	$form['payu_pos_id']=array(
		'#type' => 'textfield',
		'#title' => 'ID punktu płatności',
		'#default_value' => (isset($payu_values['pos_id'])) ? $payu_values['pos_id'] : ''
	);
	$form['payu_pos_auth_key']=array(
		'#type' => 'textfield',
		'#title' => 'Klucz autoryzacyjny punktu płatności',
		'#default_value' => (isset($payu_values['pos_auth_key'])) ? $payu_values['pos_auth_key'] : ''
	);
	$form['payu_key1']=array(
		'#type' => 'textfield',
		'#title' => 'Klucz 1',
		'#default_value' => (isset($payu_values['key1'])) ? $payu_values['key1'] : ''
	);
	$form['payu_key2']=array(
		'#type' => 'textfield',
		'#title' => 'Klucz 2',
		'#default_value' => (isset($payu_values['key2'])) ? $payu_values['key2'] : ''
	);
	$form['payu_submit']=array(
		'#type' => 'submit',
		'#name' => 'payu',
		'#value' => 'Zapisz ustawienia'
	);
	
	$form['#submit']=array('payu_save_settings');
	
	return $form;
}

function payu_save_settings($form, &$form_state) {
	if ($form_state['clicked_button']['#name']=='payu') {
		$payu_settings=array(
			'pos_id' => $form_state['values']['payu_pos_id'],
			'pos_auth_key' => $form_state['values']['payu_pos_auth_key'],
			'key1' => $form_state['values']['payu_key1'],
			'key2' => $form_state['values']['payu_key2']
		);
		variable_set('payu_settings', $payu_settings);
		drupal_set_message('Ustwaienia PayU zostały zapisane.');
	}
}