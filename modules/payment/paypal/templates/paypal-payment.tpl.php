<div class="info">
  <p>Za <span id="timer"><?php print $counter_time; ?></span> sekund zostaniesz przekierowany do serwisu paypal.com, celem opłacenia zamówienia. Jeżeli to nie nastąpi, prosimy o wciśnięcie poniższego przycisku.</p>
  <p>Kwota do opłacenia: <?php print number_format($amount, 2, ',', ' ');?> <?php print $currency;?></p>
</div>
<a href="<?php print $redirect; ?>">Zapłać</a>
<script language="JavaScript" type="text/javascript"> 
	function counter(count){
		document.getElementById('timer').innerHTML=count;
		if (count > 0) setTimeout(function(){counter(count-1);}, 1000);
		else window.location = '<?php print $redirect; ?>';
	}
	counter(<?php print $counter_time; ?>);
</script>
