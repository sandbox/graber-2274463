<?php
function paypal_settings_form($form, &$form_state) {
	$settings=variable_get('paypal_settings', array());
	$form['e_mail']=array(
		'#type' => 'textfield',
		'#title' => t('Paypal e-mail'),
		'#default_value' => isset($settings['e_mail']) ? $settings['e_mail'] : ''
	);
	$form['api']=array(
		'#type' => 'fieldset',
		'#title' => t('API credentials'),
		'#description' => t('To get information about getting your paypal API credentials, please visit <a href="https://developer.paypal.com/webapps/developer/docs/classic/api/apiCredentials/" target="_blank">This site</a>')
	);
	$form['api']['api_username']=array(
		'#type' => 'textfield',
		'#title' => 'API username',
		'#default_value' => isset($settings['api_username']) ? $settings['api_username'] : ''
	);
	$form['api']['api_pass']=array(
		'#type' => 'textfield',
		'#title' => 'API password',
		'#default_value' => isset($settings['api_pass']) ? $settings['api_pass'] : ''
	);
	$form['api']['api_sig']=array(
		'#type' => 'textfield',
		'#title' => 'API signature',
		'#default_value' => isset($settings['api_sig']) ? $settings['api_sig'] : ''
	);
	
	$form['type']=array(
		'#type' => 'radios',
		'#options' => array(
			'sandbox' => t('Sandbox'),
			'real' => t('Real thing')
		),
		'#title' => t('Service type'),
		'#default_value' => isset($settings['type']) ? $settings['type'] : 'sandbox'
	);
	$form['save']=array(
		'#type' => 'submit',
		'#value' => t('Save settings')
	);
	return $form;
}

function paypal_settings_form_submit($form, &$form_state) {
	$variable=array();
	if (isset($form_state['values']['e_mail'])) $variable['e_mail']=$form_state['values']['e_mail'];
	if (isset($form_state['values']['api_username'])) $variable['api_username']=$form_state['values']['api_username'];
	if (isset($form_state['values']['api_pass'])) $variable['api_pass']=$form_state['values']['api_pass'];
	if (isset($form_state['values']['api_sig'])) $variable['api_sig']=$form_state['values']['api_sig'];
	if (isset($form_state['values']['type'])) $variable['type']=$form_state['values']['type'];
	variable_set('paypal_settings', $variable);
	drupal_set_message(t('Settings saved.'));
}