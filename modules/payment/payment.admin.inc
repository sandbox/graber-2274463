<?php
function payment_management_form($form, &$form_state) {
	$form['actions']=array(
		'#type' => 'fieldset',
		'#title' => t('Actions'),
		'#description' => t('Select which action to perform on all selected payments')
	);
	$form['actions']['action']=array(
		'#type' => 'select',
		'#options' => array(
			0 => t('Select action'),
			1 => t('Set as received'),
			2 => t('Delete')
		)
	);
	$form['actions']['submit']=array(
		'#type' => 'submit',
		'#value' => t('Execute')
	);
	
	//Tableselect construction
	$header=array(t('Created'), t('Order ID'), t('Amount'), t('Payer e-mail'), t('Order description'), t('Status'));
	
	$query=db_select('payment_data', 'p');
	$query->join('graber_orders', 'o', 'p.oid = o.oid');
	$query->fields('p', array('pid', 'oid', 'mail', 'created', 'amount', 'currency', 'status'));
	$query->fields('o', array('description'));
	$query->condition('p.status', 'Completed', '<>');
	$payments=$query->execute()->fetchAllAssoc('pid', PDO::FETCH_ASSOC);

	$rows=array();
	foreach ($payments as $pid => $payment) {
		$rows[$pid]=array(
			empty($payment['created']) ? '' : format_date($payment['created'], 'medium'),
			l($payment['oid'], 'order/'.$payment['oid'].'/edit'),
			$payment['amount'].' '.$payment['currency'],
			$payment['mail'],
			$payment['description'],
			$payment['status'],
		);		
	}
	$form['payments']=array(
		'#type' => 'tableselect',
		'#header' => $header,
		'#options' => $rows,
		'#empty' => t('No payments.'),
	);
	return $form;
}

function payment_management_form_validate($form, &$form_state) {
	$form_state['values']['pids']=array();
	foreach ($form_state['values']['payments'] as $pid => $value) {
		if ($value) $form_state['values']['pids'][]=$value;
	}
	if (empty($form_state['values']['pids'])) form_set_error('payments', t('Please select at least one payment to perform an action on.'));
	if ($form_state['values']['action'] == 0) form_set_error('action', t('Please select an action to perform on selected payments.'));
}

function payment_management_form_submit($form, &$form_state) {
	if ($form_state['values']['action'] == 1) {
		foreach ($form_state['values']['pids'] as $pid) payment_received($pid);
		drupal_set_message(t('Payments received.'));
	}
	elseif ($form_state['values']['action'] == 2) {
		foreach ($form_state['values']['pids'] as $pid) remove_payment($pid);
		drupal_set_message(t('Payments deleted.'));
	}
}

function payment_upon_settings_form($form, &$form_state) {
	$form['payment_upon_receiving_landing']=array(
		'#type' => 'textfield',
		'#title' => t('Upon receiving landing page'),
		'#description' => t('Input valid page address on the site. If left empty, user will be redirected to front page after order submission.'),
		'#default_value' => variable_get('payment_upon_receiving_landing', '')
	);
	return system_settings_form($form);
}

function payment_upon_settings_form_validate($form, &$form_state) {
	if ($path=drupal_lookup_path('source', $form_state['values']['payment_upon_receiving_landing'])) {
		$form_state['values']['payment_upon_receiving_landing']=$path;
		drupal_set_message(t('The path you provided is an alias, value saved as !path.', array('!path' => $path)));
	}
	elseif (!menu_get_item($form_state['values']['payment_upon_receiving_landing'])) {
		form_set_error('payment_upon_receiving_landing', t('Provided path does not exist on the site.'));
	}
}