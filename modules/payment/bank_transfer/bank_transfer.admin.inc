<?php
function bank_transfer_settings_form($form, &$form_state) {
	$default=variable_get('bank_transfer_details', array());
	$form['account']=array(
		'#type' => 'textfield',
		'#title' => t('Account number'),
		'#required' => TRUE,
		'#default_value' => isset($default['account']) ? $default['account'] : ''
	);
	$form['receiver']=array(
		'#type' => 'textfield',
		'#title' => t('Receiver name'),
		'#required' => TRUE,
		'#default_value' => isset($default['receiver']) ? $default['receiver'] : ''
	);
	$form['address']=array(
		'#type' => 'textfield',
		'#title' => t('Receiver address'),
		'#required' => TRUE,
		'#default_value' => isset($default['address']) ? $default['address'] : ''
	);
	$form['city']=array(
		'#type' => 'textfield',
		'#title' => t('City, postal code'),
		'#required' => TRUE,
		'#default_value' => isset($default['city']) ? $default['city'] : ''
	);
	$form['save']=array(
		'#type' => 'submit',
		'#value' => t('Save data')
	);
	return $form;
}

function bank_transfer_settings_form_submit($form, &$form_state) {
	variable_set('bank_transfer_details', array(
		'account' => $form_state['values']['account'],
		'receiver' => $form_state['values']['receiver'],
		'address' => $form_state['values']['address'],
		'city' => $form_state['values']['city']
	));
	drupal_set_message(t('Payment gateway data saved.'));
}