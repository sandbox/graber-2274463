<?php
/*
Variables:
  $links - array of links to: frontpage ['site'], logo ['logo']
  $site_name - site name
  $slogan - site slogan
  $mail_menu - rendered gn_mail_menu (if exists)
  $content - content of the mail
*/
?>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style>
	.total {
		font-weight: bold;
	}
	.last-row {
		border-bottom: 2px solid #000000;
	}
</style>
</head>
<body>
	<div style="width: 80%; margin: auto; border: 2px ridge silver;">
		<div style="background: #cccccc; height: 155px; overflow: hidden;">
			<a href="<?php print $links['site']; ?>" target="_blank" title="<?php print $site_name; ?>">
				<img src="<?php print $links['logo']; ?>" style="float: left; width: 219px; padding: 5px;" alt="<?php print $site_name; ?>">
			</a>
			<?php if (!empty($slogan)): ?>
			<div style="margin-left: 219px; height: 100%">
				<h3 style="font-size: 35px; margin: 45px auto; text-align: center;"><?php print $slogan; ?></h3>
			</div>
			<?php endif; ?>
		</div>
		<div style="padding: 10px">
			<!-- Content start -->
			<?php print $content; ?>
			<!-- Content end -->
		</div>
	</div>
</body>
</html>			