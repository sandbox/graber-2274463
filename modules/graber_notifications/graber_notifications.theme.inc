<?php
function theme_gn_order_components($vars) {
	$order=$vars['order'];
	$fields=empty($vars['fields']) ? (array) $order : $vars['fields'];
	$cart=new graber_cart($order->order_data['cid']);
  $params=array('return_array' => TRUE, 'absolute' => TRUE);
  if (isset($vars['admin'])) $params['admin']=$vars['admin'];
	$cart_elements=$cart->render('summary', $params);
	$output=array(
    'order_id' => $order->oid,
		'order_code' => $order->code,
		'cart' => empty($cart_elements['cart']) ? '' : $cart_elements['cart'],
		'options' => empty($cart_elements['options']) ? '' : $cart_elements['options'],
    'user_name' => $fields['orderer'],
		'price' => empty($cart_elements['prices']) ? '' : $cart_elements['prices'],
	);
  
  //Payment link
  $output['payment_link']='';
  if (isset($fields['order_data']['data']['payment_method'])) {
    $payment_links=get_payment_links($order->oid, $fields['order_data']['data']['payment_method'], TRUE);
    if (!empty($payment_links)) {
      $output['payment_link']=render_payment_links($payment_links);
    }
  }
  
  //Package info
  $output['package_info']=isset($fields['order_data']['data']['package_info']) ? $fields['order_data']['data']['package_info'] : '';
  
	if (!empty($fields['order_data']['billing_addr'])) $output['orderer']=theme('graber_address_data', array('data' => $fields['order_data']['billing_addr'], 'type' => 'billing'));
	if (!empty($fields['order_data']['shipping_addr'])) $output['shipping']=theme('graber_address_data', array('data' => $fields['order_data']['shipping_addr'], 'type' => 'shipping'));
	return $output;
}
