<?php

function graber_notifications_settings_form($form, &$form_state) {
  $deadline_settings=variable_get('graber_notifications_settings', array());
  $administrators=variable_get('graber_notifications_admin_mails', array());
  $form['administrators']=array(
    '#type' => 'textarea',
    '#title' => t('Administrators'),
    '#default_value' => empty($administrators) ? '' : implode('; ', $administrators),
    '#description' => t('Input e-mail or e-mails of administrators/people that will receive new order notifications, separated with ";"'),
    '#required' => TRUE
  );
  $form['first_reminder']=array(
    '#type' => 'checkbox',
    '#size' => 3,
    '#title' => t('First deadline reminder'),
    '#default_value' => isset($deadline_settings['first_reminder']) ? $deadline_settings['first_reminder'] : '',
  );
  $form['second_reminder']=array(
    '#type' => 'textfield',
    '#size' => 3,
    '#title' => t('Second deadline reminder'),
    '#description' => t('How many hours from the first reminder a second reminder is sent if the user has still not paid. Leave blank for no reminder.'),
    '#default_value' => isset($deadline_settings['second_reminder']) ? $deadline_settings['second_reminder'] : '',
  );
  
  $form['messages']=array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Edit notification messages'),
  );
  
  $messages=graber_notifications_get_msg_types();
  
  $language=$GLOBALS['language']->language;
  
  $format=filter_format_load('full_html') ? 'full_html' : 'plain_text';

  foreach ($messages as $type => $data) {
    $message_txt=db_query("SELECT message FROM {graber_notifications_messages} WHERE (type = :type AND language = :language)", array(':type' => $type, ':language' => $language))->fetchColumn();
    
    $description='';
    if (!empty($data['placeholders'])) {
      $description.=t('Placeholders: ');
      foreach ($data['placeholders'] as $placeholder => $desc) {
        $description.='<br />'.$placeholder.' - '.$desc;
      }
    }
    $form['messages'][$type]=array(
      '#title' => $data['localized_title'],
      '#type' => 'text_format',
      '#format' => $format,
      '#rows' => 10,
      '#description' => $description,
      '#default_value' => empty($message_txt) ? '' : $message_txt
    );
  }
  $form['save_data']=array(
    '#type' => 'submit',
    '#value' => t('Save settings')
  );
  $form['#after_build'][]='graber_notifications_clear_text_format_selection';
  return $form;
}

function graber_notifications_clear_text_format_selection(&$form) {
  $messages=graber_notifications_get_msg_types();
  
  if (filter_format_load('full_html')) {
    foreach ($messages as $type => $data) {
      if (isset($form['messages'][$type]['format'])) {
        $form['messages'][$type]['format']['#attributes']=array('style' => 'display:none;');
        $form['messages'][$type]['#required']=FALSE;
        $form['messages'][$type]['format']['format']['#access'] = FALSE;
        unset($form['messages'][$type]['format']['guidelines']);
        unset($form['messages'][$type]['format']['help']);
        unset($form['messages'][$type]['format']['format']['#options']['plain_text']);
      }
    }
  }
  else {
    foreach ($messages as $type => $data) {
      if (isset($form['messages'][$type]['format'])) {
        $form['messages'][$type]['format']['#attributes']=array('style' => 'display:none;');
      }
    }
  }
  return $form;
}

function graber_notifications_settings_form_validate($form, &$form_state) {
  if ((!empty($form_state['values']['second_reminder'])) && ($form_state['values']['second_reminder'] < 1 || $form_state['values']['second_reminder'] > 720)) form_set_error('second_reminder', t('Second reminder value must range from 1 to 720 hours.'));
  $administrators=explode(';', $form_state['values']['administrators']);
  foreach ($administrators as $index => $mail) {
    $mail=strtolower(trim($mail));
    if (!valid_email_address($mail)) {
      form_set_error('administrators', t('You typed in an invalid e-mail address: !mail.', array('!mail' => $mail)));
      break;
    }
    $administrators[$index]=$mail;
  }
  $form_state['values']['administrators']=$administrators;
}

function graber_notifications_settings_form_submit($form, &$form_state) {
  variable_set('graber_notifications_admin_mails', $form_state['values']['administrators']);
  $settings=array(
    'first_reminder' =>  $form_state['values']['first_reminder'],
    'second_reminder' => $form_state['values']['second_reminder']
  );
  variable_set('graber_notifications_settings', $settings);
  
  $messages=graber_notifications_get_msg_types();
  global $user;
  if (empty($user->language)) $user->language='en';
  foreach ($messages as $type => $data) {
    if (is_array($form_state['values'][$type])) $msg=$form_state['values'][$type]['value'];
    else $msg=$form_state['values'][$type];
    if (!empty($msg)) {
      db_merge('graber_notifications_messages')->key(array('type' => $type, 'language' => $user->language))->fields(array(
        'type' => $type,
        'language' => $user->language,
        'message' => $form_state['values'][$type]['value']
      ))->execute();
    }
  }
    
  drupal_set_message(t('Configuration saved.'));
}
