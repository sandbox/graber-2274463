<?php
function cart_param_field_settings_form($form, &$form_state) {
  $fields=implode(PHP_EOL, variable_get('cart_param_field_fields', array()));
  $form['param_fields']=array(
    '#type' => 'textarea',
    '#title' => t('Fields'),
    '#default_value' => $fields
  );
  $form['save_data']=array(
    '#type' => 'submit',
    '#value' => t('Save data')
  );
  
  return $form;
}

function cart_param_field_settings_form_validate($form, &$form_state) {
  $fields=$form_state['values']['param_fields'];
  $fields=explode(PHP_EOL, $fields);
  foreach ($fields as $index => $value) $fields[$index]=trim($value);
  $form_state['values']['param_fields']=$fields;
}

function cart_param_field_settings_form_submit($form, &$form_state) {
  if (empty($form_state['values']['param_fields'])) variable_del('cart_param_field_fields');
  else variable_set('cart_param_field_fields', $form_state['values']['param_fields']);
  drupal_set_message(t('Settings saved.'));
}