<?php

/**
 * @file
 * Install hooks for the Allegro module.
 */

/**
 * Implements hook_requirements().
 */
function allegro_requirements($phase) {
  $t = get_t();

  $requirements = array();
  if (!class_exists('SoapClient')) {
    $requirements['allegro_soap'] += array(
      'title' => $t('PHP SoapClient class'),
      'severity' => REQUIREMENT_ERROR,
      'description' => $t(
        'Allegro requires the PHP <a href="@soap-url">SOAP extension</a>.',
        array('@soap-url' => 'http://php.net/manual/en/book.soap.php')),
    );
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function allegro_schema() {
  $schema['allegro_offers']=array(
    'fields' => array(
      'iid' => array(
        'type' => 'int',
        'size' => 'big',
        'unsigned' => TRUE
      ),
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE
      ),
      'username' => array(
        'type' => 'varchar',
        'length' => 128,
        'default' => '0'
      ),
      'exported' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'tiny',
        'default' => 0
      )
    ),
    'primary key' => array('iid'),
    'indexes' => array('nid' => array('nid'))
  );
  
  $schema['allegro_taxonomy']=array(
		'fields' => array(
			'iid' => array(
				'type' => 'varchar',
				'length' => 36
			),
			'tid' => array(
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE
			)
    ),
		'primary key' => array('iid'),
		'indexes' => array('tid' => array('tid')),
  );
  
  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function allegro_uninstall() {
  gp_del_settings('allegro');
}

function allegro_install() {
  mkdir('private://allegro_templates');
}

/**
 * Create allegro_taxonomy table.
 */
function allegro_update_7110() {
  $table=drupal_get_schema('allegro_taxonomy');
  db_create_table('allegro_taxonomy', $table);
}

/**
 * Create username field in allegro_offers table.
 */
function allegro_update_7120() {
  $table='allegro_offers';
  $field='username';
  $schema=drupal_get_schema($table);
  db_add_field($table, $field, $schema['fields'][$field]);
  db_update($table)->fields(array($field => _allegro_get_settings('login')))->execute();
}