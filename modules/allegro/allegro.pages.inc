<?php
/**
 * Main integration form.
 */
function allegro_main_page_form($form, &$form_state) {
  if (isset($form_state['confirm'])) {
    if ($form_state['operation'] == 'import') {
      $data=array(
        'message' => t('Are you sure you wish to import all offers from your Allegro account?'),
        'op' => 'import',
        'op_label' => t('Import')
      );
    }
    elseif ($form_state['operation'] == 'export') {
      $data=array(
        'message' => t('Are you sure you wish to export all offers to your Allegro account?'),
        'op' => 'export',
        'op_label' => t('Export')
      );
    }
    elseif ($form_state['operation'] == 'delete') {
      $data=array(
        'message' => t('Are you sure you wish to delete all offers imported from your Allegro account?'),
        'op' => 'delete',
        'op_label' => t('Delete')
      );
    }
    return gp_confirm_form_elements($data, $form);
  }
  
  $form['offer_id']=array(
    '#type' => 'textfield',
    '#title' => t('Offer ID in Allegro'),
    '#description' => t('Leave empty to perform a bulk operation on all synchronized offers.')
  );
  $form['actions']=array('#type' => 'actions');
  $form['actions']['import']=array(
    '#type' => 'submit',
    '#op' => 'import',
    '#value' => t('Import')
  );
  $form['actions']['export']=array(
    '#type' => 'submit',
    '#op' => 'export',
    '#value' => t('Export')
  );
  $form['actions']['find']=array(
    '#type' => 'submit',
    '#op' => 'find',
    '#value' => t('Find offer')
  );
  $form['actions']['delete']=array(
    '#type' => 'submit',
    '#op' => 'delete',
    '#value' => t('Delete all imported offers')
  );
  
  return $form;
}

function allegro_main_page_form_validate($form, &$form_state) {
  $op=$form_state['triggering_element']['#op'];
  if (!empty($form_state['values']['offer_id'])) {
    $form_state['values']['offer_id']=trim($form_state['values']['offer_id']);
    my_api_validate_integer($form['offer_id'], $form_state, 0);
  }
  if ($op == 'find') {
    if (empty($form_state['values']['offer_id'])) form_set_error('offer_id', t('Offer ID is required to find an offer.'));
  }
}

function allegro_main_page_form_submit($form, &$form_state) {
  $op=$form_state['triggering_element']['#op'];
  $values=$form_state['values'];
  if ($op == 'import') {
    if (!empty($values['offer_id'])) {
      module_load_include('inc', 'allegro', 'allegro.batch');
      $result=_allegro_import_get_offer($values['offer_id'], FALSE);
      if ($result['op'] == 'create') drupal_set_message(t('New offer created: !link.', array('!link' => l($result['node']->title, 'node/'.$result['node']->nid))));
      elseif ($result['op'] == 'update') drupal_set_message(t('Offer updated: !link.', array('!link' => l($result['node']->title, 'node/'.$result['node']->nid))));
    }
    elseif (!isset($values['confirmed'])) {
      $form_state['confirm']=TRUE;
      $form_state['operation']='import';
      $form_state['rebuild']=TRUE;
    }
    else {
      $batch=array(
        'title' => 'Importing offers',
        'operations' => array(
          array('allegro_import_operation', array(1))
        ),
        'finished' => 'allegro_batch_finished',
        'init_message' => t('Initializing offer import.'),
        'progress_message' => 'Estimated time left: @estimate, elapsed: @elapsed.',
        'error_message' => t('Something unexpected happened.'),
        'file' => drupal_get_path('module', 'allegro').'/allegro.batch.inc'
      );
      batch_set($batch);
      batch_process('admin/store/allegro');
    }
  }
  
  elseif ($op == 'export') {
    if (!empty($values['offer_id'])) {
      module_load_include('inc', 'allegro', 'allegro.batch');
      $result=_allegro_export_push_offer($values['offer_id']);
      if ($result == 'modify') drupal_set_message(t('Offer updated.'));
      elseif ($result == 'skip') drupal_set_message(t('Offer skipped.'));
      elseif ($result == 'error') drupal_set_message(t('Offer could not be exported. Check if offer ID is valid.'));
    }
    elseif (!isset($values['confirmed'])) {
      $form_state['confirm']=TRUE;
      $form_state['operation']='export';
      $form_state['rebuild']=TRUE;
    }
    else {
      $batch=array(
        'title' => 'Exporting offers',
        'operations' => array(
          array('allegro_export_operation', array())
        ),
        'finished' => 'allegro_batch_finished',
        'init_message' => t('Initializing offer export.'),
        'progress_message' => 'Estimated time left: @estimate, elapsed: @elapsed.',
        'error_message' => t('Something unexpected happened.'),
        'file' => drupal_get_path('module', 'allegro').'/allegro.batch.inc'
      );
      batch_set($batch);
      batch_process('admin/store/allegro');
    }
  }
  
  elseif ($op == 'delete') {
    if (!empty($values['offer_id'])) {
      $nid=db_query("SELECT nid FROM {allegro_offers} WHERE iid = :iid", array(':iid' => $values['offer_id']))->fetchField();
      if ($nid) {
        node_delete($nid);
        drupal_set_message(t('Offer deleted.'));
      }
      else drupal_set_message(t('Offer not found.'), 'error');
    }
    elseif (!isset($values['confirmed'])) {
      $form_state['confirm']=TRUE;
      $form_state['operation']='delete';
      $form_state['rebuild']=TRUE;
    }
    else {
      $batch=array(
        'title' => 'Deleting offers',
        'operations' => array(
          array('allegro_delete_operation', array())
        ),
        'finished' => 'allegro_batch_finished',
        'init_message' => t('Initializing offer deleting.'),
        'progress_message' => 'Estimated time left: @estimate, elapsed: @elapsed.',
        'error_message' => t('Something unexpected happened.'),
        'file' => drupal_get_path('module', 'allegro').'/allegro.batch.inc'
      );
      batch_set($batch);
      batch_process('admin/store/allegro');
    }
  }
  
  elseif ($op == 'find') {
    $nid=db_query("SELECT nid FROM {allegro_offers} WHERE iid = :iid", array(':iid' => $values['offer_id']))->fetchField();
    if ($nid && $node=node_load($nid)) {
      drupal_set_message(
        t(
          'Allegro offer !iid corresponds to store offer !offer.',
          array(
            '!iid' => $values['offer_id'],
            '!offer' => l($node->title, 'node/'.$nid, array('attributes' => array('target' => '_blank')))
          )
        )
      );
    }
    else drupal_set_message(t('Offer not found.'), 'error');
  }
}

//Preview page
function allegro_auction_preview_page($nid) {
  $node=node_load($nid);
  if (!$node) return drupal_not_found();
  $fields=_allegro_prepare_fields_from_node($node);
  drupal_alter('allegro_export', $fields);
  return theme('allegro_offer_template', $fields);
}
