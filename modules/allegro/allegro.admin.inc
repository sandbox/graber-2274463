<?php

/**
 * @file
 * Allegro administration pages.
 */

/**
 * Helper function to test allegro connection.
 */
function allegro_test_connection($login = NULL, $password = NULL, $webapikey = NULL, $countryCode = NULL, $testMode = FALSE) {
  if (!$login || !$password || !$webapikey || !$countryCode) {
    return FALSE;
  }

  try {
    AllegroAPI::testConnection($login, $password, $webapikey, $countryCode, $testMode);
    return TRUE;
  }
  catch (SoapFault $e) {
    $placeholders = array(
      '@error' => $e->getMessage(),
    );
    drupal_set_message(t('An error occurred while connecting to WebAPI service: @error', $placeholders), 'error');
    return FALSE;
  }
}

/**
 * Settings page.
 */
function allegro_settings_form($form, &$form_state) {
  $settings = $form_state['#allegro_settings'] = _allegro_get_settings();

  $form['settings'] = array('#tree' => TRUE);
  $form['settings']['login'] = array(
    '#type' => 'textfield',
    '#title' => t('Login', array(), array('context' => 'username')),
    '#description' => t('Your Allegro account login.'),
    '#required' => TRUE,
  );

  $form['settings']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('Your Allegro account password.'),
  );
  if (!empty($settings['password'])) {
    $form['settings']['password']['#description'] = t("Your Allegro account password. Leave blank if you don't wish to change it.");
  }

  $form['settings']['webapikey'] = array(
    '#type' => 'textfield',
    '#title' => t('WebAPI Key'),
    '#description' => t('Allegro WebAPI service key. !url.', array('!url' => l(t('Get API key'), 'http://allegro.pl/myaccount/webapi.php'))),
    '#required' => TRUE,
  );

  // App settings.
  $app_creation_url = 'https://apps.developer.allegro.pl/new';
  $form['settings']['app'] = array(
    '#type' => 'fieldset',
    '#title' => t('Allegro app settings'),
    '#description' => t('Required because of new after-sale fields. Please create an app on !link and pass the required details below.', array(
      '!link' => l($app_creation_url, $app_creation_url, array(
        'external' => TRUE,
        'attributes' => array(
          'target' => '_blank',
        ),
      )),
    )),
    '#tree' => TRUE,
  );
  $form['settings']['app']['client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
  );
  $form['settings']['app']['client_secret'] = array(
    '#type' => 'textarea',
    '#rows' => 2,
    '#title' => t('Client secret'),
  );
  $form['settings']['app']['api_key'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#title' => t('API key'),
  );
  $form['settings']['app']['redirect_uri'] = array(
    '#title' => t('Redirect URI'),
    '#type' => 'textfield',
    '#value' => url('allegro/rest-api', array('absolute' => TRUE)),
    '#description' => t('Please use this value when creating an app.'),
    '#attributes' => array('readonly' => 'readonly'),
  );
  $form['settings']['app']['force_renew'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force access token renew'),
  );

  if (isset($settings['access_token']['refresh_token'])) {
    $allegro = allegro_client();
    $after_sale_options = $allegro->getAfterSalesServiceConditions();
    if (!empty($after_sale_options)) {
      $form['settings']['after_sale'] = array(
        '#type' => 'fieldset',
        '#title' => t('Allegro after sale options'),
        '#tree' => TRUE,
      );
      foreach ($after_sale_options as $id => $data) {
        $form['settings']['after_sale'][$id] = array(
          '#type' => 'select',
          '#title' => $data['title'],
          '#options' => $data['options'],
        );
      }
    }
  }

  $form['settings']['use_template'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use auction template'),
    '#description' => t('Usage of auction templates will be disabled on 01.06.2018. Discouraged.'),
  );

  if (user_access('graber cart superadmin')) {
    $form['settings']['body_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Offer template body ID (deprecated)'),
    );

    $form['settings']['country_code'] = array(
      '#type' => 'select',
      '#title' => t('Auction site'),
      '#description' => t('Auction site to connect with.'),
      '#options' => AllegroAPI::getSupportedWebsites(),
      '#required' => TRUE,
    );

    $form['settings']['test_mode'] = array(
      '#type' => 'checkbox',
      '#title' => t('Turn on test mode'),
      '#description' => t('When test mode is active all operations are executed at <a href="@sandbox-url">WebAPI Sandbox</a> instead of production website.', array('@sandbox-url' => 'http://webapisandbox.pl/')),
    );
  }

  foreach ($settings as $setting => $value) {
    if (isset($form['settings'][$setting])) {
      if (!empty(element_children($form['settings'][$setting])) && is_array($value)) {
        foreach ($value as $subkey => $subvalue) {
          if (isset($form['settings'][$setting][$subkey])) {
            $form['settings'][$setting][$subkey]['#default_value'] = $subvalue;
          }
        }
      }
      else {
        $form['settings'][$setting]['#default_value'] = $value;
      }
    }
  }

  $form['user_specific'] = array(
    '#type' => 'checkbox',
    '#title' => t('User-specific settings'),
    '#default_value' => empty($settings['user_specific']) ? 0 : 1,
    '#description' => t('Check this box to make tose settings user-specific. Uncheck to revert to defaults.'),
  );

  $form['save_data'] = array(
    '#type' => 'submit',
    '#value' => t('Save data'),
  );

  return $form;
}

/**
 * Settings form submit handler.
 */
function allegro_settings_form_submit($form, &$form_state) {
  $settings = &$form_state['values']['settings'];
  $force_token_renew = $settings['app']['force_renew'];
  unset($settings['app']['force_renew']);

  if (isset($settings['templates'])) {
    gp_process_form_kv_pairs($settings['templates']);
  }

  if (empty($settings['password'])) {
    $settings['password'] = gp_get_settings('allegro', 'password');
  }
  elseif (AllegroAPI::isPasswordEncrypted()) {
    // Encrypt password before saving.
    $settings['password'] = AllegroAPI::hashPassword($settings['password']);
  }

  if ($form_state['values']['user_specific']) {
    $settings['user_specific'] = TRUE;
  }
  elseif (!empty($form_state['#allegro_settings']['user_specific'])) {
    $edit['data'] = $GLOBALS['user']->data;
    $edit['data']['allegro_settings'] = NULL;
    user_save($GLOBALS['user'], $edit);
  }
  _allegro_save_settings($settings);
  drupal_set_message(t('Setings saved.'));

  // Test SOAP connection.
  if (!isset($settings['country_code'])) {
    $settings['country_code'] = _allegro_get_settings('country_code');
  }
  if (!isset($settings['test_mode'])) {
    $settings['test_mode'] = _allegro_get_settings('test_mode');
  }
  $test_passed = allegro_test_connection(
    $settings['login'], $settings['password'],
    $settings['webapikey'], $settings['country_code'],
    $settings['test_mode']
  );

  if ($test_passed) {
    // Access token stuff.
    $access_token = _allegro_get_settings('access_token');
    if (!$access_token || empty($access_token['refresh_token']) || $force_token_renew) {
      $form_state['redirect'] = url(
        'https://allegro.pl/auth/oauth/authorize',
        array(
          'query' => array(
            'response_type' => 'code',
            'client_id' => $settings['app']['client_id'],
            'api-key' => $settings['app']['api_key'],
            'redirect_uri' => $settings['app']['redirect_uri'],
          ),
        )
      );
    }
    else {
      drupal_set_message(t('Connected successfully to WebAPI service.'));
    }
  }

}

/**
 * Rest API callback function.
 */
function allegro_rest_api_callback() {
  if (isset($_GET['code'])) {
    $allegro = allegro_client();
    $settings = _allegro_get_settings();
    $result = $allegro->getAccessToken($_GET['code']);
    if (isset($result['error'])) {
      drupal_set_message($result['error_description'] . ' (' . $result['error'] . ')', 'error');
    }
    else {
      $settings['access_token'] = $result;
      _allegro_save_settings($settings);
    }
  }
  else {
    drupal_set_message(t('Sorry, something went wrong. Please try again later. If the problem persists, please contact the site administrator.'));
  }

  // Take the user back to the settings page.
  drupal_goto('admin/store/settings/allegro');
}

/**
 * Import settings form builder.
 */
function allegro_import_settings_form($form, &$form_state) {
  if (!module_exists('taxonomy')) {
    drupal_set_message(t('No options here without taxonomy module.'));
    return array();
  }

  $category_vid = gp_get_settings('allegro', 'category_vid');
  $vocabularies = taxonomy_get_vocabularies();
  if (!empty($vocabularies)) {
    $voc_options = array(0 => t('* no category synchronization *'));
    foreach ($vocabularies as $vid => $vocabulary) {
      $voc_options[$vid] = $vocabulary->name;
    }
    $form['category_vid'] = array(
      '#type' => 'select',
      '#title' => t('Vocabulary for new terms'),
      '#description' => t('New terms created during offer import will be inserted to this vocabulary.'),
      '#options' => $voc_options,
      '#default_value' => $category_vid,
    );
  }

  if ($category_vid) {
    if (!isset($form_state['values']['category_map'])) {
      $form_state['values']['category_map'] = array();
      $catmap = gp_get_settings('allegro', 'category_map');
      foreach ($catmap as $iid => $tid) {
        $form_state['values']['category_map'][] = array(
          'iid' => $iid,
          'tid' => $tid,
        );
      }
    }

    $form['category_map'] = array(
      '#title' => t('Category map'),
      '#prefix' => '<div id="category-map-wrapper">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    );

    if (!empty($form_state['values']['category_map'])) {
      $form['category_map']['#theme'] = 'graber_form_table';
      foreach ($form_state['values']['category_map'] as $index => $data) {
        $form['category_map'][$index]['iid'] = array(
          '#title' => t('Allegro category ID'),
          '#type' => 'textfield',
          '#default_value' => $data['iid'],
        );
        $form['category_map'][$index]['tid'] = array(
          '#title' => t('Taxonomy term ID'),
          '#type' => 'textfield',
          '#default_value' => $data['tid'],
        );

        $form['category_map'][$index]['delete'] = array(
          '#type' => 'submit',
          '#value' => t('remove'),
          '#op' => 'delete_item',
          '#name' => 'delete_' . $index,
          '#ajax' => array(
            'callback' => 'allegro_import_settings_form_ajax',
            'wrapper' => 'category-map-wrapper',
          ),
          '#title-display' => 'invisible',
        );
      }
    }
    else {
      $form['category_map']['#type'] = 'fieldset';
      $form['category_map']['info'] = array('#markup' => t('Category map empty.'));
    }

    $form['add_item'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
      '#op' => 'add_new',
      '#ajax' => array(
        'callback' => 'allegro_import_settings_form_ajax',
        'wrapper' => 'category-map-wrapper',
      ),
    );
  }

  $form['save_data'] = array(
    '#type' => 'submit',
    '#value' => t('Save data'),
    '#op' => 'save_data',
  );
  return $form;
}

/**
 * AJAX callback.
 */
function allegro_import_settings_form_ajax($form, $form_state) {
  return $form['category_map'];
}

/**
 * Settings form validate handler.
 */
function allegro_import_settings_form_validate($form, &$form_state) {
  $op = $form_state['triggering_element']['#op'];
  if ($op == 'save_data') {
    $vid = gp_get_settings('allegro', 'category_vid');
    if (!empty($form['category_map'])) {
      foreach ($form['category_map'] as $index => $fields) {
        if ($index[0] === '#' || !isset($fields['iid'])) {
          continue;
        }
        my_api_validate_integer($fields['iid'], $form_state, 0);
        if (my_api_validate_integer($fields['tid'], $form_state, 0)) {
          if (!db_query("SELECT COUNT(tid) FROM {taxonomy_term_data} WHERE (tid = :tid AND vid = :vid)", array(':tid' => $form_state['values']['category_map'][$index]['tid'], ':vid' => $vid))->fetchField()) {
            form_set_error('category_map][' . $index . '][tid', t('Taxonomy term does not exist.'));
          }
        }
      }
    }
  }
}

/**
 * Settings form submit handler.
 */
function allegro_import_settings_form_submit($form, &$form_state) {
  $op = $form_state['triggering_element']['#op'];
  if ($op == 'save_data') {
    $catmap = array();
    if (!empty($form_state['values']['category_map'])) {
      foreach ($form_state['values']['category_map'] as $item) {
        $catmap[$item['iid']] = $item['tid'];
      }
    }

    $settings = gp_get_settings('allegro');
    $settings['category_map'] = $catmap;
    $settings['category_vid'] = $form_state['values']['category_vid'];
    gp_save_settings('allegro', $settings, FALSE, TRUE);
    drupal_set_message(t('Data saved.'));
  }
  elseif ($op == 'add_new') {
    $form_state['rebuild'] = TRUE;
    $form_state['values']['category_map'][] = array('iid' => '', 'tid' => '');
  }
  elseif ($op == 'delete_item') {
    $form_state['rebuild'] = TRUE;
    $index = $form_state['triggering_element']['#parents'][1];
    unset($form_state['values']['category_map'][$index]);
  }
}

/**
 * Export settings form builder.
 */
function allegro_export_settings_form($form, $form_state) {
  if (!isset($form_state['settings'])) {
    $form_state['settings'] = _allegro_get_settings();
  }

  if (user_access('graber cart superadmin')) {
    $form['templates'] = array(
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => t('Existing templates'),
      '#theme' => 'graber_form_table',
    );
    $index = 0;
    foreach ($form_state['settings']['templates'] as $filename => $title) {
      $index++;
      $form['templates'][$filename]['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#default_value' => $title,
      );
      $form['templates'][$filename]['filename'] = array(
        '#markup' => $filename,
        '#title' => t('Filename'),
      );
      $form['templates'][$filename]['remove'] = array(
        '#type' => 'submit',
        '#title' => t('Remove'),
        '#name' => 'remove_' . $index,
        '#op' => 'remove',
        '#value' => t('Remove'),
      );
    }
    $form['add_template'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add template'),
      '#tree' => TRUE,
    );
    $form['add_template']['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Template name'),
    );
    $form['add_template']['file'] = array(
      '#type' => 'file',
      '#title' => t('Template file'),
    );
    $form['add_template']['upload'] = array(
      '#type' => 'submit',
      '#op' => 'upload',
      '#value' => t('Upload'),
    );
  }

  if (!empty($form_state['settings']['templates'])) {
    if (isset($form_state['values']['templates']['default_template'])) {
      $default_template = $form_state['values']['templates']['default_template'];
    }
    else {
      if (isset($form_state['settings']['default_template'])) {
        $default_template = $form_state['settings']['default_template'];
      }
      else {
        $default_template = FALSE;
      }
    }
    $form['user_data']['default_template'] = array(
      '#type' => 'select',
      '#title' => t('Default template'),
      '#op' => 'load_template',
      '#options' => $form_state['settings']['templates'],
      '#ajax' => array(
        'callback' => '_allegro_export_load_template',
      ),
      '#default_value' => $default_template,
    );

    if ($default_template && file_exists('private://allegro_templates/' . $default_template)) {
      $form['user_data']['edited'] = array(
        '#type' => 'textarea',
        '#title' => t('Template code'),
        '#rows' => 15,
        '#description' => t("If you don't have basic HTML knowledge, don't edit any HTML tags, only text."),
        '#default_value' => file_get_contents('private://allegro_templates/' . $default_template),
      );
    }

  }

  $form['save_data'] = array(
    '#type' => 'submit',
    '#op' => 'save',
    '#value' => t('Save data'),
  );

  return $form;
}

/**
 * Export settings form validate handler.
 */
function allegro_export_settings_form_validate($form, &$form_state) {
  $op = $form_state['triggering_element']['#op'];

  if ($op == 'upload') {
    $uploaded = FALSE;
    if (!empty($_FILES['files']['name']['add_template'])) {
      $destination = 'private://allegro_templates/' . $_FILES['files']['name']['add_template'];
      if (file_exists($destination)) {
        form_set_error('add_template][file', t('File already exists. For safety reasons, delete it first.'));
      }
      else {
        if (drupal_move_uploaded_file($_FILES['files']['tmp_name']['add_template'], $destination)) {
          $uploaded = TRUE;
          $form_state['values']['add_template']['filename'] = $_FILES['files']['name']['add_template'];
        }
      }
    }
    if (!$uploaded) {
      form_set_error('add_template][file', t('Template file not sent.'));
    }
  }
}

/**
 * Export settings form submit handler.
 */
function allegro_export_settings_form_submit($form, &$form_state) {
  $op = $form_state['triggering_element']['#op'];

  if ($op == 'upload') {
    $form_state['rebuild'] = TRUE;
    $values = $form_state['values']['add_template'];
    $form_state['settings']['templates'] = gp_get_settings('allegro', 'templates');
    $form_state['settings']['templates'][$values['filename']] = $values['title'];
    gp_save_settings('allegro', $form_state['settings']['templates'], 'templates');
  }

  elseif ($op == 'remove') {
    $form_state['rebuild'] = TRUE;
    $filename = $form_state['triggering_element']['#parents'][1];
    $form_state['settings']['templates'] = gp_get_settings('allegro', 'templates');
    unset($form_state['settings']['templates'][$filename]);
    unlink('private://allegro_templates/' . $filename);
    gp_save_settings('allegro', $form_state['settings']['templates'], 'templates');
  }

  elseif ($op == 'save') {
    $settings = array();
    foreach ($form_state['values']['templates'] as $filename => $data) {
      $settings['templates'][$filename] = $data['title'];
    }
    $settings['default_template'] = $form_state['values']['default_template'];
    _allegro_save_settings($settings);
    if (!empty($form_state['values']['edited'])) {
      file_put_contents('private://allegro_templates/' . $settings['default_template'], $form_state['values']['edited']);
    }
    drupal_set_message(t('Settings saved.'));
  }
}

/**
 * Template loader helper function.
 */
function _allegro_export_load_template($form, $form_state) {
  $filename = $form_state['triggering_element']['#value'];
  $uri = 'private://allegro_templates/' . $filename;
  if (file_exists($uri)) {
    $commands = array();
    $selector = '#edit-edited';
    $commands[] = ajax_command_invoke($selector, 'val', array(file_get_contents($uri)));
    return array('#type' => 'ajax', '#commands' => $commands);
  }
}
