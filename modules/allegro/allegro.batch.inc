<?php

/**
 * @file
 * Contains allegro Batch API functions.
 */

/**
 * Helper function to get a single offer.
 */
function _allegro_import_get_offer($iid, $batch=TRUE) {
  $allegro=allegro_client();
  if (!$allegro) {
    return;
  }

  $status_data = db_select('allegro_offers', 'o')->fields('o', array('nid', 'exported'))->condition('iid', $iid)->execute()->fetchAssoc();

  try {
    $offer_data = $allegro->GetItemFields($iid);
  }
  catch (Exception $e) {
    drupal_set_message(t('Error in offer !index.<br>', array('!index' => $iid)) . $e->getMessage(), 'error');
    $op = 'error';
  }

  if (!empty($offer_data)) {
    if ($status_data) {
      $op = 'update';
    }
    else {
      $status_data=array(
        'nid' => FALSE,
        'exported' => 0
      );
      $op='create';
    }
            
    if ($op !== 'skip') {
      if (!function_exists('allegro_import_fields_map')) {
        module_load_include('inc', 'allegro', 'allegro_import_map');
      }
      $data=allegro_import_fields_map($status_data, $iid, $offer_data, $allegro);
      if (empty($data)) $op='skip';
      else {
        $my_node=new my_node();
        $node=$my_node->prepare($data['fields'], $data['title'], $data['type'], $status_data['nid']);
        $node->uid=$GLOBALS['user']->uid;
        try {
          node_save($node);
          db_merge('allegro_offers')->key(array('iid' => $iid))->fields(array(
            'iid' => $iid,
            'nid' => $node->nid,
            'username' => _allegro_get_settings('login')
          ))->execute();
        }
        catch (Exception $e) {
          drupal_set_message(t('Error in offer !index.<br>', array('!index' => $iid)).$e->getMessage(), 'error');
          $op='error';
        }
      }
    }
  }
  else {
    $op='skip';
    drupal_set_message(t('Allegro offer not found.'), 'error');
  }
  
  if ($batch) return $op;
  else return array(
    'op' => $op,
    'node' => isset($node) ? $node : FALSE
  );
}

function allegro_import_operation($Nbatch, &$context) {
  $allegro=allegro_client();
  if (!$allegro) {
    return;
  }

  if (!isset($context['sandbox']['current'])) {
    $page=0;
    $result=$allegro->GetMySellItems(500, $page);
    if (isset($result->sellItemsList->item)) $item_list=$result->sellItemsList->item;
    $context['sandbox']['total']=$result->sellItemsCounter;
    $context['sandbox']['current']=0;
    
    //Create a list of auction IDs
    $context['sandbox']['ids']=array();
    while (!empty($item_list)) {
      foreach ($item_list as $item) {
        $context['sandbox']['ids'][]=$item->itemId;
      }
      $page++;
      $result=$allegro->GetMySellItems(500, $page);
      if (isset($result->sellItemsList->item)) $item_list=$result->sellItemsList->item;
      else $item_list=FALSE;
    }
  }
  for ($i=0; $i<$Nbatch; $i++) {
    if ($iid=$context['sandbox']['ids'][$context['sandbox']['current']]) {
      $op=_allegro_import_get_offer($iid);
      if ($op == 'error') return;
      $context['results'][]=$op;
      $context['sandbox']['current']++;
      $context['finished']=$context['sandbox']['current']/$context['sandbox']['total'];
      $context['message']=t('Now processing offer !current of !total', array('!current' => $context['sandbox']['current'], '!total' => $context['sandbox']['total']));
    }
  }
}

function _allegro_export_push_offer($iid, $client=FALSE) {
  if ($nid=db_select('allegro_offers', 'a')->fields('a', array('nid'))->condition('iid', $iid)->execute()->fetchField()) {

    $node=node_load($nid);
    $fields=_allegro_prepare_fields_from_node($node);
    $modified=_allegro_push_offer($iid, $nid, $fields);
    if ($modified) $op='modify';
    else $op='skip';
  }
  else $op='error';
  return $op;
}

function allegro_export_operation(&$context) {
  if (!isset($context['sandbox']['current'])) {
    $context['sandbox']['current']=0;
    $context['sandbox']['login']=_allegro_get_settings('login');
    $context['sandbox']['total']=db_query("SELECT COUNT(iid) FROM {allegro_offers} WHERE username = :username", array(':username' => $context['sandbox']['login']))->fetchField();
    $context['sandbox']['iid']=0;
  }
  
  if ($iid=db_query("SELECT iid FROM {allegro_offers} WHERE (iid > :current AND username = :username) ORDER BY iid ASC LIMIT 1", array(':current' => $context['sandbox']['iid'], ':username' => $context['sandbox']['login']))->fetchField()) {
    $op=_allegro_export_push_offer($iid);
    if ($op == 'error') return;
    $context['results'][]=$op;
    $context['sandbox']['iid']=$iid;
    $context['sandbox']['current']++;
    $context['finished']=$context['sandbox']['current']/$context['sandbox']['total'];
    $context['message']=t('Exporting offer !current of !total.', array('!current' => $context['sandbox']['current'], '!total' => $context['sandbox']['total']));
  }
}


function allegro_delete_operation(&$context) {
  if (!isset($context['sandbox']['last'])) {
    $context['sandbox']['last']=0;
    $context['sandbox']['progress']=0;
    $context['sandbox']['login']=_allegro_get_settings('login');
    $context['sandbox']['total']=db_query("SELECT COUNT(nid) FROM {allegro_offers} WHERE username = :username", array(':username' => $context['sandbox']['login']))->fetchColumn();
  }
  $nid=db_query("SELECT nid FROM {allegro_offers} WHERE (nid > :last AND username = :username) ORDER BY nid ASC LIMIT 1", array(':last' => $context['sandbox']['last'], ':username' => $context['sandbox']['login']))->fetchColumn();
  if ($nid) {
    node_delete($nid);
    $context['sandbox']['last']=$nid;
    $context['results'][]='delete';
    $context['sandbox']['progress']++;
    $context['finished']=$context['sandbox']['progress']/$context['sandbox']['total'];
    $context['message']=t('Deleting offer !current of !total', array('!current' => $context['sandbox']['progress'], '!total' => $context['sandbox']['total']));
  }
}

function allegro_batch_finished($success, $results, $operations) {
  if ($success) {
    $Nres=count($results);
    $operations=array_count_values($results);
    $details=array();
    foreach ($operations as $op => $count) $details[]=t($op).': '.$count;
    $message = t('!nres operations performed: !operations.', array('!nres' => $Nres, '!operations' => implode(', ', $details)));
    drupal_set_message($message);
  }
  else {
    $error_operation=reset($operations);
    $message=t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}
