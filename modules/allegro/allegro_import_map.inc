<?php
function allegro_import_fields_map($status_data, $iid, $offer_data, $allegro) {
  $data['type']='offer';
  $data['title']=$offer_data['title'];
  
  //Preprocess body if required
  $body_id=gp_get_settings('allegro', 'body_id');
  if ($status_data['exported'] || strpos($offer_data['body'], 'id="'.$body_id.'"')) {
    try {
      $doc=new DOMDocument();
      $doc->validateOnParse=TRUE;
      $doc->loadHTML('<?xml encoding="utf-8" ?>'.$offer_data['body']);
      if ($body=$doc->getElementById($body_id)) {
        $output='';
        foreach ($body->childNodes as $childNode) {
          $output.=$doc->saveHTML($childNode);
        }
      }
      if (!empty($output)) $offer_data['body']=$output;
    }
    catch (Exception $e) {
    }
  }
  else {
    $begin_str='TUTAJ ZACZYNA SIE OPIS PRZEDMIOTU !!!!!!!!!!!!!! -->';
    $end_str='Kliknij aby zobaczyć nasze pozostałe aukcje!';
    if ($begin_pos=strpos($offer_data['body'], $begin_str)) {
      $begin_pos+=strlen($begin_str);
      if ($end_pos=strpos($offer_data['body'], $end_str, $begin_pos)) {
        $end_pos+=strlen($end_str);
        $offer_data['body']=substr($offer_data['body'], $begin_pos, ($end_pos-$begin_pos)).'</span></a></strong>';
        $offer_data['body']=preg_replace('/(<img[^>]+>)/i', '', $offer_data['body']);
      }
    }
  }
  $data['fields']=array(
    'field_product_price' => $offer_data['price'],
    'body' => $offer_data['body']
  );
  
  //Taxonomy
  if ($vid=gp_get_settings('allegro', 'category_vid')) {
    
    $catmap=gp_get_settings('allegro', 'category_map');
    if (isset($catmap[$offer_data['category']])) $tid=$catmap[$offer_data['category']];
    else $tid=db_query("SELECT tid FROM {allegro_taxonomy} WHERE iid = :iid", array(':iid' => $offer_data['category']))->fetchField();
    
    if (!$tid) {
      $category_path=$allegro->categoryPath($offer_data['category']);
      $category=$category_path[$offer_data['category']];
      $term=new stdClass();
      $term->vid=$vid;
      $term->name=$category['title'];
      $term->parent=0;
      taxonomy_term_save($term);
      db_insert('allegro_taxonomy')->fields(array(
        'iid' => $offer_data['category'],
        'tid' => $term->tid,
      ))->execute();
      $tid=$term->tid;
    }
    $data['fields']['field_category'][]=$tid;
  }
  
  //Images
  if (!$status_data['exported']) {
    for ($i=1; $i<9; $i++) {
      if (!empty($offer_data['image'.$i])) {
        $data['fields']['field_product_image'][]=array(
          'filename' => gp_machine_name($data['title']).'_'.$i.'.jpg',
          'content' => base64_decode($offer_data['image'.$i])
        );
      }
    }
  }
  
  return $data;
}
