<?php
module_load_include('inc', 'graber_cart', 'graber_cart.checkout');

function ajax_cart_module_implements_alter(&$implementations, $hook) {
  //Move ajax_cart implementation to the bottom.
  $hooks=array('form_graber_cart_add_form_alter');
  if (in_array($hook, $hooks)) {
    $group=$implementations['ajax_cart'];
    unset($implementations['ajax_cart']);
    $implementations['ajax_cart']=FALSE;
  }
}

function ajax_cart_preprocess_node(&$vars) {
  $vars['classes_array'][]='ac-node-'.$vars['node']->nid;
}

function ajax_cart_menu() {
  return array(
    'admin/store/settings/ajax-cart' => array(
      'type' => MENU_LOCAL_TASK,
      'access arguments' => array('graber cart superadmin'),
      'title' => 'Ajax cart settings',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('ajax_cart_settings_form'),
      'file' => 'ajax_cart.admin.inc'
    )
  );
}

function ajax_cart_theme() {
  return array(
    'ajax_cart_checkout' => array(
      'variables' => array('form' => NULL),
      'template' => 'ajax-cart-checkout'
    ),
    'ac_modal_add_info' => array(
      'variables' => array('messages' => array()),
      'file' => 'ajax_cart.theme.inc'
    )
  );
}

function ajax_cart_node_view($node, $view_mode, $langcode) {
  $settings=gp_get_settings('graber_cart');
  if (user_access('make shopping')) {
    if (in_array($node->type, $settings['buyable_types'], TRUE)) {
      if (in_array($view_mode, $settings['view_modes'], TRUE)) {
        $action_type=variable_get('ajax_cart_add_action', 'animate');
        if ($action_type == 'animate' || $action_type == 'simple_msg') {
          $node->content['ajax_msg_wrapper']=array('#markup' => '<div class="nid-'.$node->nid.'-ajax-msg-wrapper"></div>', '#weight' => 11);
        }
      }
    }
  }
}

//Add to cart form
function ajax_cart_form_graber_cart_add_form_alter(&$form, &$form_state) {

  $action_type=variable_get('ajax_cart_add_action', 'animate');
  
  if ($action_type == 'animate' || $action_type == 'simple_msg') {
    $form['#attached']['js'][]=drupal_get_path('module', 'ajax_cart').'/ajax_cart.js';
  }
  else {
    _graber_cart_modal_initialize(TRUE);
  }
  
  $add_params=array(
    '#ajax' => array(
      'callback' => 'ajax_cart_add_callback'
    ),
    '#attributes' => array(
      'class' => array('ctools-modal-gc-add-modal')
    )
  );
  
  foreach ($add_params as $name => $param) $form['actions']['add'][$name]=$param;
  unset($form['#destination']);
}

function ajax_cart_add_callback($form, &$form_state) {
  $action_type=variable_get('ajax_cart_add_action', 'animate');
  $commands=array();
  if (isset($form_state['ajax_commands'])) {
    foreach ($form_state['ajax_commands'] as $command) $commands[]=$command;
  }
  
  if ($action_type == 'animate' || $action_type == 'simple_msg') {
    if (isset($form_state['#add_success']) && $form_state['#add_success']) {
      $title='';
      $block=graber_cart_block_view('graber_cart_block');
      $block_html=$block['content'];
      $commands[]=ajax_command_replace('#graber-cart-block-content', $block_html);
      if ($action_type == 'animate' && $form_state['view_mode'] !== 'full') {
        $commands[]=ajax_command_replace('.nid-'.$form_state['nid'].'-ajax-msg-wrapper', '<div class="ajax-msg-wrapper"></div>');
        $commands[]=ajax_command_invoke('.ac-node-'.$form_state['nid'].' .field-type-image img', 'ac_animate_image', array());
      }
      else {
        drupal_set_message(t('Article has been added to your cart. !goto.', array('!goto' => l(t('Go to cart'), 'cart'))));
        $output='<div class="nid-'.$form_state['nid'].'-ajax-msg-wrapper">'.theme('status_messages').'</div>';
        $commands[]=ajax_command_replace('.nid-'.$form_state['nid'].'-ajax-msg-wrapper', $output);
      }
    }
    else {
      $output='<div class="nid-'.$form_state['nid'].'-ajax-msg-wrapper">'.theme('status_messages').'</div>';
      $commands[]=ajax_command_replace('.nid-'.$form_state['nid'].'-ajax-msg-wrapper', $output);
    }
  
  }
  else {
    ctools_include('ajax');
    ctools_include('modal');
    $messages=array();
    if (isset($form_state['#add_success']) && $form_state['#add_success']) {
      $node=node_load($form_state['nid']);
      $title=t('Success');
      $messages[]=array('data' => $node->title, 'class' => array('title'));
      $messages[]=t('Article has been added to cart.');
      $block=graber_cart_block_view('graber_cart_block');
      $block_html=$block['content'];
      $commands[]=ajax_command_replace('#graber-cart-block-content', $block_html);
    }
    else {
      $title=t('Failure');
      $status_messages=drupal_get_messages();
      if (!empty($status_messages)) {
        foreach ($status_messages as $type => $type_messages) {
          foreach ($type_messages as $message) {
            $messages[]=array(
              'data' => $message,
              'class' => array($type)
            );
          }
        }
      }
      else $messages[]=array('data' => t('Something went wrong. Try again.'), 'class' => array('error'));
      $output=theme('status_messages');
    }
    
    $output=theme('ac_modal_add_info', array('messages' => $messages));
    $commands[]=ctools_modal_command_display($title, $output);
  }
  
  return array('#type' => 'ajax', '#commands' => $commands);  
}

//Cart form
function ajax_cart_form_graber_cart_form_alter(&$form, &$form_state) {
  if (isset($form['submit-wrapper']['checkout'])) {
    $form['submit-wrapper']['checkout']=array(
      '#type' => 'submit',
      '#name' => 'checkout',
      '#value' => t('Go to checkout'),
      '#weight' => 51,
      '#ajax' => array(
        'callback' => 'ajax_cart_load_checkout',
        'wrapper' => 'checkout-form-wrapper',
      )
    );
  }
  foreach ($form_state['#cart']->contents as  $iid => $item) {
    if (isset($form['#cart_params']['admin']) && $form['#cart_params']['admin']) {
      $form['cart_table'][$iid]['remove_item']=array('#markup' => '');
    }
    else {
      $form['cart_table'][$iid]['remove_item']['#ajax']=array(
        'callback' => 'ajax_cart_update_cart_data',
        'wrapper' => 'graber-cart-form',
      );
      $form['cart_table'][$iid]['quantity']['#ajax']=array(
        'callback' => 'ajax_cart_update_cart_data',
        'wrapper' => 'graber-cart-form',
      );
    }
  }
  
  if (!isset($form['#cart_params']['admin']) || !$form['#cart_params']['admin'])  unset($form['submit-wrapper']['update']);
  $form['#prefix']='<div id="graber-cart-form-wrapper">';
  $form['#suffix']='</div>';
  $form['#submit'][]='ajax_cart_load_checkout_submit';
  $form['#redirect_to_checkout']=FALSE;
}

function ajax_cart_load_checkout_submit($form, &$form_state) {
  $form_state['#checkout_loaded']=TRUE;
}

function ajax_cart_update_cart_data($form, $form_state) {
  $commands=array();
  if (empty($form_state['clicked_button'])) {
    graber_cart_update_cart($form, $form_state);
    $form=drupal_rebuild_form('graber_cart_form', $form_state, $form);
    $commands[]=ajax_command_replace('#graber-cart-form-wrapper', drupal_render($form));
  }
  elseif ($form_state['clicked_button']['#parents'][2] == 'remove_item') {
    if (empty($form_state['#cart']->contents)) {
      $commands[]=ajax_command_remove('#checkout-form-wrapper');
    }
    $commands[]=ajax_command_replace('#graber-cart-form-wrapper', drupal_render($form));

  }
  
  if (!isset($form['#cart_params']['admin']) || !$form['#cart_params']['admin']) {
    $block=graber_cart_block_view('graber_cart_block');
    $block_html=$block['content'];
    $commands[]=ajax_command_replace('#graber-cart-block-content', $block_html);
  }

  $messages=theme('status_messages');
  $commands[]=ajax_command_prepend('#graber-cart-form-wrapper', $messages);
  return array('#type' => 'ajax', '#commands' => $commands);
}

function ajax_cart_load_checkout($form, $form_state) {
  $commands=array();
  $errors=form_get_errors();
  if (empty($errors)) {
    $checkout_form=drupal_get_form('graber_cart_checkout_form');
    $commands[]=ajax_command_invoke('#graber-cart-form-wrapper #cart-submit-buttons', 'hide', array('fast'));
    $checkout_html='<h2 class="checkout-title">'.t('Checkout').'</h2>'.theme('ajax_cart_checkout', array('form' => $checkout_form));
    $commands[]=ajax_command_invoke('#checkout-form-wrapper', 'hide');
    $commands[]=ajax_command_html('#checkout-form-wrapper', $checkout_html);
    $commands[]=ajax_command_invoke('#checkout-form-wrapper', 'slideDown', array(1000));
  }
  $messages=theme('status_messages');
  $commands[]=ajax_command_replace('#ajax-message', '<div id="ajax-message">'.$messages.'</div>');
  print ajax_render($commands);
  exit;
}

function ajax_cart_form_graber_cart_checkout_form_alter(&$form, &$form_state) {
  if ($form_state['step'] == 1) {
    if (isset($form['submit-wrapper']['continue'])) $form['submit-wrapper']['continue']['#ajax']=array(
      'callback' => 'ajax_cart_checkout_ajax_submit',
      'wrapper' => 'graber-cart-checkout-form'
    );
  }
  if ($form_state['step'] == 2) {
    $form['submit-wrapper']['back']['#ajax']=array(
      'callback' => 'ajax_cart_checkout_ajax_submit',
      'wrapper' => 'graber-cart-checkout-form'
    );
  }
}

function ajax_cart_checkout_ajax_submit($form, $form_state) {
  $commands=array();
  if ($form_state['clicked_button']['#parents'][0] == 'continue') {
    $errors=form_get_errors();
    if (empty($errors)) {
      $commands[]=ajax_command_html('#graber-cart-form-wrapper', '');
      $commands[]=ajax_command_html('h1', t('Checkout - please check your order'));
      $commands[]=ajax_command_invoke('h2.checkout-title', 'hide');
    }
  }
  elseif ($form_state['clicked_button']['#parents'][0] == 'back') {
    global $cart;
    $commands[]=ajax_command_html('h1', t('Shopping cart'));
    $commands[]=ajax_command_invoke('h2.checkout-title', 'show');
    $commands[]=ajax_command_replace('#graber-cart-form-wrapper', $cart->render());
    $commands[]=ajax_command_remove('#edit-checkout');
  }
  $commands[]=ajax_command_replace('.graber-checkout-form', drupal_render($form));
  $commands[]=ajax_command_invoke('.graber-checkout-form', 'ac_scrollTo');
  $messages=theme('status_messages');
  $commands[]=ajax_command_prepend('.graber-checkout-form', $messages);
  return array('#type' => 'ajax', '#commands' => $commands);
}

function ajax_cart_menu_alter(&$items) {
  $items['cart']['page callback']='ajax_cart_cart_page';
  $items['cart']['file']='graber_cart.checkout.inc';
}

function ajax_cart_cart_page() {
  global $cart;
  $checkout_form='';//drupal_render(drupal_get_form('graber_cart_checkout_form'));
  $wrapper = array();
  $wrapper['#attached']['js'][]=drupal_get_path('module', 'ajax_cart').'/ajax_cart.js';
  $wrapper['cart']['#markup'] = $cart->render();
  $wrapper['ajax_message']['#markup']='<div id="ajax-message"></div>';
  $wrapper['checkout']['#markup'] = '<div id="checkout-form-wrapper">'.$checkout_form.'</div>';
  
  return $wrapper;  
}