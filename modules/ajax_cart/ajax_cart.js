(function ($) {
  $.fn.ac_scrollTo=function(time, verticalOffset) {
    time = typeof(time) != 'undefined' ? time : 1000;
    verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : -100;
    offset = this.offset();
    if (!offset) return false;
    offsetTop = offset.top + verticalOffset;
    $('html, body').animate({
      scrollTop: offsetTop
    }, time);
  };

  $.fn.ac_animate_image=function() {
    var node_img=this;
    if (node_img.length) {
      var positions=node_img.offset();
      var cb_positions=$('#graber-cart-block-content').offset();
      var Newimage=node_img.clone();
      Newimage.css('position', 'absolute').css('z-index', 1001).css('top', positions.top).css('left', positions.left);
      jQuery('body').append(Newimage);
                    
      Newimage.animate({
        opacity: 0.4,
        left: cb_positions.left,
        top: cb_positions.top,
        height: 50,
        width: 50
      }, 1000, function() {
        $(this).remove();
      });
    }
  };

})(jQuery);
