<?php
function ajax_cart_settings_form($form, &$form_state) {
	$form['ajax_cart_add_action']=array(
		'#type' => 'radios',
		'#options' => array(
			'animate' => t('Animate image (flies to cart block)'),
			'simple_msg' => t('Simple message (best with better_messages)'),
			'popup' => t('Display popup message (stylable Ctools modal)')
		),
		'#title' => t('Add to cart action'),
		'#default_value' => variable_get('ajax_cart_add_action', 'animate')
	);
	
	$form['save']=array(
		'#type' => 'submit',
		'#value' => t('Save settings')
	);
	
	return $form;
}

function ajax_cart_settings_form_submit($form, &$form_state) {
	variable_set('ajax_cart_add_action', $form_state['values']['ajax_cart_add_action']);
	drupal_set_message(t('Settings saved.'));
}