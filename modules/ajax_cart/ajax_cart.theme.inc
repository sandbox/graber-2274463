<?php
function theme_ac_modal_add_info($vars) {
	$messages=$vars['messages'];
	foreach ($messages as $index => $message) {
		if (!is_array($message)) $message=array('data' => $message);
		if (isset($message['class'])) $class_html=' '.implode(' ', $message['class']);
		else $class_html='';
		$messages[$index]='<p class="message'.$class_html.'">'.$message['data'].'</p>';
	}
	$output='<div class="modal-messages">'.implode(PHP_EOL, $messages).'</div>';
	$output.='<div class="ac-modal-actoins-wrapper"><div class="close-wrapper"><a class="ctools-close-modal" href="">'.t('Continue shopping').'</a></div><div class="goto-cart-wrapper"><a class="goto-cart" href="/cart">'.t('Go to cart').'</a></div></div>';
	return $output;
}