<?php
//User representatives tab
function representative_panel_user_form($form, &$form_state, $uid) {
  $form_state['#uid']=(int) $uid;
  $query=db_select('representatives_permissions', 'rp');
  $query->join('users', 'u', 'u.uid = rp.ruid');
  $query->fields('rp', array('ruid'))->fields('u', array('mail'));
  $results=$query->condition('rp.uid', $form_state['#uid'])->execute()->fetchAll();
  $form['rep_table']=array(
    '#tree' => TRUE,
    '#prefix' => '<div id="representative-user-table">',
    '#suffix' => '</div>'
  );
  
  if (!empty($results)) {
    $Nres=0;
    $form['rep_table']['#theme']='graber_form_table';
    foreach ($results as $result) {
      $form['rep_table'][$result->ruid]['mail']=array(
        '#title' => t('E-mail'),
        '#markup' => $result->mail
      );
      $form['rep_table'][$result->ruid]['remove']=array(
        '#title' => t('Remove'),
        '#type' => 'submit',
        '#name' => 'remove_'.$result->ruid,
        '#value' => t('remove'),
        '#ajax' => array(
          'callback' => 'representative_panel_user_form_ajax',
          'wrapper' => 'representative-user-table'
        ),
        '#limit_validation_errors' => array()
      );
    }
  }
  
  $form['add']=array(
    '#type' => 'fieldset',
    '#title' => t('Add a representative'),
    '#tree' => TRUE
  );
  $form['add']['mail']=array(
    '#type' => 'textfield',
    '#title' => t('Representative e-mail'),
  );
  $form['add']['add']=array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#ajax' => array(
      'callback' => 'representative_panel_user_form_ajax',
      'wrapper' => 'representative-user-table'
    )
  );
  
  return $form;
}

function representative_panel_user_form_validate($form, &$form_state) {
  if ($form_state['triggering_element']['#parents'][0] == 'add') {
    $ruid=db_select('users', 'u')->fields('u', array('uid'))->condition('mail', $form_state['values']['add']['mail'])->execute()->fetchColumn();
    if ($ruid) {
      $query=db_select('representatives_permissions', 'rp');
      $query->join('users', 'u', 'u.uid = rp.ruid');
      $query->condition('rp.ruid', $ruid)->condition('rp.uid', $uid);
      $query->fields('rp', array('ruid'))->fields('u', array('mail'));
      $result=$query->execute()->fetchColumn();
      if ($result) form_set_error('add][mail', t('This user is already your representative.'));
      else $form_state['values']['add']['ruid']=$ruid;
    }
    else form_set_error('add][mail', t('No user is registered with this e-mail.'));
  }
}

function representative_panel_user_form_submit($form, &$form_state) {
  if ($form_state['triggering_element']['#parents'][0] == 'add') {
    $form_state['rebuild']=TRUE;
    db_insert('representatives_permissions')->fields(array(
      'ruid' => $form_state['values']['add']['ruid'],
      'uid' => $form_state['#uid']
    ))->execute();
  }
  elseif ($form_state['triggering_element']['#parents'][2] == 'remove') {
    $form_state['rebuild']=TRUE;
    $ruid=$form_state['triggering_element']['#parents'][1];
    db_delete('representatives_permissions')->condition('ruid', $ruid)->condition('uid', $form_state['#uid'])->execute();
  }
}

function representative_panel_user_form_ajax($form, $form_state) {
  return $form['rep_table'];
}


//Representative panel
function representative_panel_page() {
  return drupal_get_form('representative_panel_main_form');
}

function representative_panel_main_form($form, &$form_state) {
  $form['user']=array(
    '#tree' => TRUE
  );
  $query=db_select('representatives_associations', 'ra');
  $query->join('users', 'u', 'u.uid = ra.uid');
  $query->fields('ra', array('uid'))->fields('u', array('name', 'mail'))->condition('ra.ruid', $GLOBALS['user']->uid);
  $user=$query->execute()->fetchObject();
  $form['user']['uid']=array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#autocomplete_path' => 'admin/store/representative-panel/get-users/current-user',
    '#default_value' => ($user) ? ($user->name.' ('.$user->mail.') [uid='.$user->uid.']') : ''
  );
  if (user_access('global representative')) $form['user']['uid']['#autocomplete_path']='admin/store/representative-panel/get-users/all';
  
  $form['user']['load']=array(
    '#type' => 'submit',
    '#value' => t('Select user')
  );
  
  if ($user) {
    $form['user']['cancel']=array(
      '#type' => 'submit',
      '#value' => t('Cancel selection')
    );
  }
  
  //We'll get to that later. Hopefully.
  /*
  if ($user) {
    $form['panel']=array(
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => t('Panel')
    );
  }
  */
  
  return $form;
}

function representative_panel_main_form_validate($form, &$form_state) {
  if ($form_state['clicked_button']['#parents'][1] == 'load') {
    if (my_api_validate_autocomplete_id($form['user']['uid'], $form_state, 'uid', 'users')) {
      if (!user_access('global representative')) {
        if (!db_query("SELECT ruid FROM {representatives_permissions} WHERE (ruid = :ruid AND uid = :uid)", array(':ruid' => $GLOBALS['user']->uid, ':uid' => $form_state['values']['user']['uid']))->fetchColumn()) form_set_error('user][uid', t('You have no permission to act on behalf of this user.'));
      }
    }
  }
  elseif ($form_state['clicked_button']['#parents'][1] == 'cancel') {
    
  }
}

function representative_panel_main_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#parents'][1] == 'load') {
    db_merge('representatives_associations')->key(array('ruid' => $GLOBALS['user']->uid))->fields(array(
      'ruid' => $GLOBALS['user']->uid,
      'uid' => $form_state['values']['user']['uid']
    ))->execute();
    drupal_set_message('User to represent selected.');
  }
  elseif ($form_state['clicked_button']['#parents'][1] == 'cancel') {
    db_delete('representatives_associations')->condition('ruid', $GLOBALS['user']->uid)->execute();
  }
}