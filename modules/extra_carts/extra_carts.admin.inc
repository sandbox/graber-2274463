<?php

function extra_carts_settings_form($form, &$form_state) {
	$settings=variable_get('extra_carts_settings', array());
	
	$entity_info=entity_get_info('node');
	$vm_options=array();
	foreach($entity_info['view modes'] as $mode => $data) $vm_options[$mode]=$data['label'];
	$form['view_modes']=array(
		'#type' => 'checkboxes',
		'#title' => t('View modes with add to package option'),
		'#options' => $vm_options,
		'#default_value' => isset($settings['view_modes']) ? $settings['view_modes'] : array('teaser', 'full')	
	);
  $form['allow_public']=array(
    '#type' => 'checkbox',
    '#title' => t('Allow public packages'),
		'#default_value' => isset($settings['allow_public']) ? $settings['allow_public'] : 1	
  );

	$form['save']=array(
		'#type' => 'submit',
		'#value' => t('Save settings'),
		'#weight' => 100
	);
	return $form;
}

function extra_carts_settings_form_validate($form, &$form_state) {
}

function extra_carts_settings_form_submit($form, &$form_state) {
	$settings=array(
		'view_modes' => $form_state['values']['view_modes'],
    'allow_public' => $form_state['values']['allow_public'],
	);
	variable_set('extra_carts_settings', $settings);
}
