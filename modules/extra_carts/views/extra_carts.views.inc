<?php
function extra_carts_views_data () {
  $data['node']['add_to_package']['moved to'] = array('views_entity_node', 'add_to_package');
  $data['views_entity_node']['add_to_package'] = array(
    'field' => array(
      'title' => t('Add to package form'),
      'help' => t('Add to package option.'),
      'handler' => 'views_handler_field_package_form',
    ),
  );
  return $data;
}
