<?php

/**
 * @file
 * Definition of views_handler_field_cart_form.
 */

/**
 * Field handler to present aa add to cart form.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_package_form extends views_handler_field_entity {

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // The path is set by render_link function so don't allow to set it.
    $form['alter']['path'] = array('#access' => FALSE);
    $form['alter']['external'] = array('#access' => FALSE);
  }

  function render($values) {
    if ($entity = $this->get_value($values)) {
      return $this->render_form($entity, $values);
    }
  }

  function render_form($node, $values) {
    if (user_access('make shopping')) {
      $settings=gp_get_settings('graber_cart');
      if (in_array($node->type, $settings['buyable_types'], TRUE)) {
        _graber_cart_modal_initialize(TRUE);
        $package_link=array(
          '#prefix' => '<div class="xc-add-link-wrapper">',
          '#markup' => l(
            t('Add to package'),
            'package/add/nojs/'.$node->nid,
            array('attributes' => array(
              'class' => array(
                'ctools-use-modal',
                'ctools-modal-gc-add-modal',
                'package-add-'.$node->nid.'-btn'
              )
            ))
          ),
          '#suffix' => '</div>'
        );
        return render($package_link);
      }
    }
  }
}
