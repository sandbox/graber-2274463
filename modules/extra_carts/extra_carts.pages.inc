<?php
/*
	Packages list page
*/
function extra_carts_user_page($cid=FALSE, $action='view', $uid=FALSE, $limit=0) {
	global $user;
	if (!$uid) $uid=$user->uid;
	elseif ($uid != $user->uid) $restrict=TRUE;
	
	if (user_access('use packages')) {
		if (!$cid) {
			$header=array(
				array('data' => t('Name'), 'field' => 'c.name'),
				array('data' => t('Last modified'), 'field' => 'c.modified'),
				array('data' => t('Products')),
				array('data' => t('Privacy'), 'field' => 'c.status'),
				array('data' => t('Actions'))
			);
			
			$query=db_select('graber_carts', 'c')
				->extend('TableSort');
			$query->fields('c', array('cid', 'name', 'modified', 'status'));
			$query->condition('uid', $uid);
			if (isset($restrict) && $restrict) $query->condition('status', 4);
			else $query->condition('status', array(3, 4));
			if ($limit) $query->range(0, $limit);
			$result = $query
				->orderByHeader($header)
				->execute();
				
			$rows = array();
			foreach ($result as $row) {
				$count=db_query("SELECT COUNT(iid) FROM {graber_cartdata} WHERE cid = :cid", array(':cid' => $row->cid))->fetchColumn();

				$actions=array();
				$actions[]=l(t('View'), 'user/'.$uid.'/packages/'.$row->cid);
				if (!isset($restrict) || !$restrict) {
					$actions[]=l(t('Edit'), 'user/'.$uid.'/packages/'.$row->cid.'/edit');
					$actions[]=l(t('Delete'), 'user/'.$uid.'/packages/'.$row->cid.'/delete');
				}
				$rows[]=array(
					'data' => array(
						l($row->name, 'user/'.$uid.'/packages/'.$row->cid),
						format_date($row->modified),
						$count,
						($row->status == 3) ? t('private') : t('public'),
						implode(' ', $actions)
					)
				);
			}
			
			$build['tablesort_table'] = array(
				'#theme' => 'table',
				'#header' => $header,
				'#rows' => $rows,
			);

			return $build;
		}
		
		else return extra_carts_package_page($cid, $action);
	}
	else return drupal_access_denied();
}

/*
	Package pages
*/
function extra_carts_package_page($cid, $action='view') {
	if (!is_numeric($cid)) return MENU_ACCESS_DENIED;
	global $user;
	$own_access=FALSE;
	if (db_query("SELECT cid, name FROM {graber_carts} WHERE (uid = :uid AND cid = :cid)", array(':uid' => $user->uid, ':cid' => $cid))->fetchColumn()) $own_access=TRUE;
	
	$package=new graber_cart($cid);
	
	if ($action=='delete') {
		if ($own_access) {
			drupal_set_title(t('Delete package "!package_name"', array('!package_name' => $package->name)));
			return drupal_get_form('extra_carts_delete_confirm', $package);
		}
		else return MENU_ACCESS_DENIED;
	}
	
	if ($action=='edit') {
		if ($own_access) {
			drupal_set_title(t('Edit package "!package_name"', array('!package_name' => $package->name)));
			return $package->render('form', array('checkout' => FALSE));
		}
		else return MENU_ACCESS_DENIED;
	}
	
	else {
		if (($package->status == 3 && $own_access) || $package->status == 4) {
			drupal_set_title(t('Package "!package_name"', array('!package_name' => $package->name)));
			return extra_carts_package_view($package);
		}
		else return MENU_ACCESS_DENIED;
	}
}

function extra_carts_delete_confirm($form, &$form_state, $package) {
	$form['info']=array(
		'#markup' => '<div class="confirm">'.t('Are you sure you want to delete the package?').'</div>'
	);
	$form['confirm']=array(
		'#type' => 'submit',
		'#value' => t('delete')
	);
	$form['#package']=$package;
	
	return $form;
}

function extra_carts_delete_confirm_submit($form, &$form_state) {
	drupal_set_message(t('Package "!package" has been deleted.', array('!package' => $form['#package']->name)));
	$form['#package']->delete();
	global $user;
	$form_state['redirect']=url('user/'.$user->uid.'/packages');;
}

function extra_carts_package_view($package) {
  $back_link=l(t('Back'), 'packages');
	$summary=$package->render('summary');
	$form=drupal_get_form('graber_package_view_widgets', $package);
	return '<div class="back">'.$back_link.'</div><div class="summary">'.$summary.'</div><div class="widgets">'.render($form).'</div>';
}

function graber_package_view_widgets($form, &$form_state, $package) {
	
	global $user;
	$form['#package']=$package;

	//Recommend widget
  if ($package->status == 4) {
    if (isset($form_state['values']['recommend'])) {
      $form['recommend']=array(
        '#type' => 'fieldset',
        '#title' => t('Recommend'),
      );
      $form['recommend']['mail']=array(
        '#type' => 'textfield',
        '#title' => t('E-mail')
      );
      $form['recommend']['message']=array(
        '#type' => 'textarea',
        '#title' => t('Message text')
      );
      $form['recommend']['recommend_submit']=array(
        '#type' => 'submit',
        '#name' => 'recommend',
        '#value' => t('Send'),
        '#submit' => array('graber_package_view_widgets_submit')
      );
    }
    
    else {
      $form['recommend']=array(
        '#type' => 'button',
        '#value' => t('Recommend'),
        '#ajax' => array(
          'callback' => 'graber_package_view_widgets_ajax',
          'wrapper' => 'graber-packages-recommend-widget',
          'method' => 'replace',
          'effect' => 'fade',
          'progress' => array('type' => 'none')
        )
      );
    }

    $form['recommend']+=array(
      '#prefix' => '<div id="graber-packages-recommend-widget">',
      '#suffix' => '</div>'
    );
    
    if (module_exists('fblikebutton')) {
      $conf = fblikebutton_conf();
      $conf['action']='recommend';
      $form['like']=array(
        '#markup' => _fblikebutton_field(url('user/'.$user->uid.'/packages/'.$package->cid, array('absolute' => TRUE)), $conf)
      );
    }
    
  }
	
	if ($user->uid == $package->uid) {
		$form['edit']=array(
			'#markup' => l(t('Edit package'), 'user/'.$user->uid.'/packages/'.$package->cid.'/edit')
		);
	}
	
	//Insert to cart widget
	$form['cart_insert']=array(
		'#type' => 'submit',
		'#name' => 'cart_insert',
		'#value' => t('Insert to cart')
	);
	
	//Add to my packages
	if ($user->uid != $package->uid) {
		$form['add_to_packages']=array(
			'#type' => 'submit',
			'#name' => 'add_to_packages',
			'#value' => t('Add to my packages')
		);
	}
	
	return $form;
}


function graber_package_view_widgets_ajax($form, $form_state) {
	return $form['recommend'];
}

function graber_package_view_widgets_validate($form, &$form_state) {
	if ($form_state['clicked_button']['#name']=='recommend') {
		if (empty($form_state['values']['mail']) || !valid_email_address($form_state['values']['mail'])) form_set_error('mail', t('Please provide a valid e-mail address.'));
	}
	if ($form_state['clicked_button']['#name']=='add_to_packages') {
		if (!user_access('use packages')) return drupal_access_denied();
	}
}

function graber_package_view_widgets_submit($form, &$form_state) {
	global $language;
	global $user;
	global $base_url;
	$package=$form['#package'];

	if ($form_state['clicked_button']['#name']=='recommend') {
		if ($package->status == 3) {
			$package->status=4;
			db_update('graber_carts')->fields(array('status' => $package->status))->condition('cid', $package->cid)->execute();
			drupal_set_message(t('Your package has been set to public.'));
		}

		$package_link=$base_url.'/package/'.$package->cid;
		$result=drupal_mail(
			'graber_notifications',
			'package_recommendation',
			$form_state['values']['mail'],
			$user->language,
			array (
				'package' => $package->render('min_summary'),
				'sender_username' => $user->name,
				'sender_mail' => empty($user->mail) ? '' : $user->mail,
				'package_link' => l($package->name, $package_link),
				'message' => $form_state['values']['message']
			),
			empty($user->mail) ? '' : $user->mail
		);
		if ($result) drupal_set_message(t('Your package has been recommended.'));
		else drupal_set_message(t('Error.'));
	}
	
	elseif ($form_state['clicked_button']['#name']=='cart_insert') {
		global $cart;
		foreach ($cart->contents as $iid => $data) {
			$cart->remove($iid);
		}
		foreach ($package->contents as $iid => $data) {
			$node=node_load($data['unique']['nid']);
			if (isset($data['node'])) unset($data['node']);
			if (isset($data['price'])) unset($data['price']);
			if (isset($data['base_price'])) unset($data['base_price']);
			$cart->add($node, $data);
		}
		drupal_set_message(t('Package contents have been added to your cart.'));
		drupal_goto('cart');
	}
	
	elseif ($form_state['clicked_button']['#name']=='add_to_packages') {
		$index=1;
		$new_name=$package->name;
		while ($name=db_query("SELECT name FROM {graber_carts} WHERE (uid = :uid AND name = :name)", array(':uid' => $user->uid, ':name' => $new_name))->fetchColumn()) {
			$index++;
			$new_name=$package->name.' ('.$index.')';
		}
		$new_package=new graber_cart(0);
		$new_package->uid=$user->uid;
		$new_package->sid='0';
		$new_package->status=4;
		$new_package->name=$new_name;
		
		foreach($package->contents as $data) {
			$node=node_load($data['unique']['nid']);
			if (isset($data['node'])) unset($data['node']);
			if (isset($data['price'])) unset($data['price']);
			$new_package->add($node, $data);
		}
		
		drupal_set_message(t('Package "!package_name" has been added to your packages.', array('!package_name' => $new_name)));
	}
}