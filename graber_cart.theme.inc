<?php
function _prepare_graber_cart_fields($cart, $params, $form_element) {
  $u_props=array();
  $invoke_output=module_invoke_all('graber_cart_display_properties', $cart->contents);
  if (!empty($invoke_output)) {
    foreach ($invoke_output as $prop => $data) {
      $u_props[$prop]=$data;
    }
  }
    
  //Load images

  if (isset($params['type']) && $params['type'] == 'block') $image_style='cart_block_thumb';
  else $image_style='thumbnail';
  $cart_images=array();
  foreach ($cart->contents as $iid => $item) {
    if (!isset($cart_images[$item['unique']['nid']]) && is_object($item['node'])) {
      $image_field=field_get_items('node', $item['node'], 'field_product_image');
      $cart_images[$item['unique']['nid']]=array(
        'style_name' => $image_style,
        'path' => $image_field[0]['uri']
      );
    }
  }
  
  $item_no=0;
  $item_fields=array();
  foreach ($cart->contents as $iid => $item) {
    $item_no++;
    $weight=0;
    //Base params
    $item_fields[$iid]['uri']='node/'.$item['unique']['nid'];
        
    //Product image
    if (isset($cart_images[$item['unique']['nid']])) $item_fields[$iid]['image']=$cart_images[$item['unique']['nid']];
        
    //Item fields
            
    //Title
    $item_fields[$iid]['title']=array(
      'class' => array('title'),
      'label' => t('Article'),
      'raw' => $item['title']
    );
    if (!isset($params['type']) || ($params['type'] == 'normal' || $params['type'] == 'block')) {
      $item_fields[$iid]['title']['value']=l($item['title'], 'node/'.$item['unique']['nid'], array('absolute' => $params['absolute']));
    }
    else $item_fields[$iid]['title']['value']=$item['title'];
          
    //Unique properties defined by other modules
    foreach ($u_props as $prop_key => $data) {
      if (!empty($data['label']) && !empty($data['values'][$iid])) {
        $item_fields[$iid]['fields'][$prop_key]=array(
          'label' => $data['label'],
          'weight' => isset($data['weight']) ? $data['weight'] : $weight++,
          'class' => array(drupal_html_class($prop_key)),
          'value' => $data['values'][$iid]
        );
      }
    }
    
    //Unit price
    $item_fields[$iid]['fields']['unit_price']=array(
      'weight' => $weight++,
      'label' => t('Unit price'),
      'raw' => $item['price'],
      'value' => number_format($item['price'], 2, ',', ' ').' '.$cart->currency,
      'class' => array('unit-price')
    );
    
    //Quantity
    if (isset($params['type']) && ($params['type'] == 'normal' || $params['type'] == 'block')) {
      $item_fields[$iid]['fields']['quantity']=array(
        'weight' => $weight++,
        'label' => t('Quantity'),
        'raw' => $item['quantity'],
        'value' => number_format($item['quantity'], 0, ',', ' '),
        'class' => array('quantity')
      );
    } else $item_fields[$iid]['fields']['quantity']=array('raw' => $item['quantity']);

    //Total price
    $item_total=$item['quantity']*$item['price'];
    if ($item['quantity'] > 1) {
      $item_fields[$iid]['fields']['total']=array(
        'weight' => $weight++,
        'label' => t('Total'),
        'raw' => $item_total,
        'value' => number_format($item_total, 2, ',', ' ').' '.$cart->currency,
        'class' => array('total-price')
      );
    }
    else $item_fields[$iid]['fields']['total']['raw']=$item_total;
    
    //Actions
    if (isset($form_element[$iid])) {
      //Quantity
      $form_element[$iid]['quantity']['#title_display']='before';
      $item_fields[$iid]['actions']['quantity']=$form_element[$iid]['quantity'];

      //Remove
      $item_fields[$iid]['actions']['remove']=$form_element[$iid]['remove_item'];
    }
  }
  
  //Allow other modules to alter item fields in any possible way
  $invoke_data=array('cart' => $cart, 'params' => $params);
  drupal_alter('graber_cart_fields', $item_fields, $invoke_data);

  return $item_fields;
}

function theme_graber_cart($vars) {
  if (!empty($vars['cart_table'])) $form_elements=$vars['cart_table'];
  else $form_elements=FALSE;
  if (!isset($vars['cart'])) $cart=$form_elements['#cart'];
  else $cart=$vars['cart'];
  if (isset($vars['params'])) $params=$vars['params'];
  else $params=array('absolute' => FALSE);
  
  if (empty($vars['cart_table']['#cart']->contents)) return '<div class="cart-empty">'.t('Your cart is empty.').'</div>';

  $item_fields=_prepare_graber_cart_fields($cart, $params, $form_elements);
  if (isset($vars['params'])) $params=$vars['params'];
  else $params=array(
    'absolute' => FALSE
  );
  
  //Header
  if (isset($params['type']) && ($params['type'] == 'min')) $colspan=2;
  else $colspan=3;
  $header=array(
    'article' => array('data' => t('Article'), 'colspan' => $colspan, 'class' => array('title'))
  );
  foreach ($item_fields as $row) {
    uasort($row['fields'], 'drupal_sort_weight');
    foreach ($row['fields'] as $cell => $data) {
      if (!isset($data['value']) || $cell === 'title') continue;
      if (!isset($header[$cell])) $header[$cell]=array(
        'data' => isset($data['label']) ? $data['label'] : $cell,
        'class' => is_array($data['class']) ? $data['class'] : NULL
      );
    }
  }
  if ($form_elements) {
    $header['actions']=array(
      'data' => '',
      'class' => array('actions')
    );
  }
    
  //Rows
  $rows=array();
  $row_no=0;
  foreach ($item_fields as $iid => $row) {    
    $rows[$iid]=array('data' => array());
    
    //Row number
    $rows[$iid]['data']['number']=array(
      'data' => ++$row_no,
      'class' => array('row-no')
    );
    
    //Image
    if (!isset($params['type']) || ($params['type'] == 'normal' || $params['type'] == 'block')) {
      if (isset($row['image'])) $rows[$iid]['data']['image']=array(
        'data' => l(theme('image_style', $row['image']), $row['uri'], array('html' => TRUE, 'absolute' => $params['absolute'])),
        'class' => array('cart-image')
      );
      else $rows[$iid]['data']['image']=array(
        'data' => '',
        'class' => array('cart-image', 'missing')
      );
    }
    
    //Title
    $rows[$iid]['data']['title']=array(
      'data' => $row['title']['value'],
      'class' => $row['title']['class']
    );
    
    //Other fields
    foreach ($header as $cell => $header_data) {
      if (in_array($cell, array('article', 'actions', 'unit_price', 'total'))) continue;
      if (isset($row['fields'][$cell])) {
        if (empty($row['fields'][$cell]['value'])) continue;
        $rows[$iid]['data'][$cell]=array(
          'data' => $row['fields'][$cell]['value'],
          'class' => $row['fields'][$cell]['class']
        );
      }
      else $rows[$iid]['data'][$cell]=array(
        'data' => '',
        'class' => $row['fields'][$cell]['class']+array('missing')
      );
    }
    
    //Prices & actions
    $rows[$iid]['data']['unit_price']=array(
      'data' => $row['fields']['unit_price']['value'],
      'class' => $row['fields']['unit_price']['class']
    );
    if ($form_elements) {
      $row['actions']['quantity']['#title_display']='invisible';
      $rows[$iid]['data']['quantity']=array(
        'data' => drupal_render($row['actions']['quantity']),
        'class' => array('quantity')
      );
    }
    if (!empty($row['fields']['total'])) $rows[$iid]['data']['total']=array(
      'data' => $row['fields']['total']['value'],
      'class' => $row['fields']['total']['class']
    );
    if ($form_elements) $rows[$iid]['data']['remove']=array(
      'data' => drupal_render($row['actions']['remove']),
      'class' => array('remove')
    );
  }
  $output=theme('table', array('header' => $header, 'rows' => $rows, 'sticky' => FALSE));
  return $output;
}

function theme_graber_cart_responsive($vars) {
  if (!empty($vars['cart_table'])) $form_elements=$vars['cart_table'];
  else $form_elements=FALSE;
  if (!isset($vars['cart'])) $cart=$form_elements['#cart'];
  else $cart=$vars['cart'];
  if (isset($vars['params'])) $params=$vars['params'];
  else $params=array('absolute' => FALSE);
  
  if (empty($cart->contents)) return '<div class="cart-empty">'.t('Your cart is empty.').'</div>';
  
  $item_fields=_prepare_graber_cart_fields($cart, $params, $form_elements);
  if (isset($vars['params'])) $params=$vars['params'];
  else $params=array(
    'absolute' => FALSE
  );

  $output='<div class="responsive">';
  
  foreach ($item_fields as $iid => $data) {
    $output.='<div class="cart-item">';
    
    //Image
    if (!isset($params['type']) || ($params['type'] == 'normal' || $params['type'] == 'block')) {
      if (isset($data['image'])) $output.='<div class="cart-image">'.l(theme('image_style', $data['image']), $data['uri'], array('html' => TRUE, 'absolute' => $params['absolute'])).'</div>';
      else $output.='<div class="cart-image missing"></div>';
    }
    
    //Title
    if (!empty($data['title']['class'])) $class=' class="'.implode(' ', $data['title']['class']).'"';
    else $class='';
    $output.='<li'.$class.'>'.(empty($data['title']['label']) ? '' : '<span class="label">'.$data['title']['label'].':</span> ').'<span class="value">'.$data['title']['value'].'</span></li>';
    
    //Other fields
    uasort($data['fields'], 'drupal_sort_weight');
    $output.='<ul class="params">';
    foreach ($data['fields'] as $field) {
      if (empty($field['value'])) continue;
      if (!empty($field['class'])) $class=' class="'.implode(' ', $field['class']).'"';
      else $class='';
      $output.='<li'.$class.'>'.(empty($field['label']) ? '' : '<span class="label">'.$field['label'].':</span> ').'<span class="value">'.$field['value'].'</span></li>';
    }
    $output.='</ul>';
    
    //Form elements
    if (!empty($data['actions'])) {
      $output.='<div class="actions">';
      foreach ($data['actions'] as $action) $output.=drupal_render($action);
      $output.='</div>';
    }
    
    $output.='</div>';    
  }
  
  $output.='</div>';
  
  return $output;
}

function _checkout_summary_prepare_row($pane_key, $field_key, $field, $values) {
  if (empty($field['#title'])) return FALSE;
  $value=FALSE;
  if ($pane_key == $field_key) {
    if (isset($values[$pane_key]) && !is_array($values[$pane_key])) $value=$values[$pane_key];
  }
  elseif (isset($values[$pane_key][$field_key]) || isset($values[$field_key])) {
    if ($field_key == 'street') {
      $value=$values[$pane_key][$field_key]['street'].' '.$values[$pane_key][$field_key]['building_no'];
      if (!empty($values[$pane_key][$field_key]['flat_no'])) $value.='/'.$values[$pane_key][$field_key]['flat_no'];
    }
    elseif (isset($values[$pane_key][$field_key])) {
      if (is_array($values[$pane_key][$field_key])) $value=implode(', ', $values[$pane_key][$field_key]);
      else $value=$values[$pane_key][$field_key];
    }
    else $value=$values[$field_key];
  }
  
  if ($value === FALSE) return FALSE;
    
  $classes=array();
  if ($pane_key == $field_key) $classes[]='pane';
  else $classes[]='pane-'.$pane_key;
  $classes[]=$pane_key.'-'.$field_key;
  
  if ($value === '') {
    $value=t('not provided');
    $classes[]='not-provided';
  }
  elseif (empty($value)) $value=t('no');
  if ($value == 1) $value=t('yes');
  
  
  $row=array(
    'data' => array(
      array('data' => $field['#title'], 'class' => array('title')),
      array('data' => $value, 'class' => array('value')),
    ),
    'class' => $classes
  );
  return $row;
}

function theme_graber_checkout_summary($vars) {
  $form=$vars['form'];
  $values=$vars['values'];
  foreach (array('billing', 'shipping', 'invoice') as $pane_key) {
    if (isset($values[$pane_key]['legal_entity'])) {
      $values[$pane_key]['legal_entity']=$form[$pane_key]['legal_entity']['#options'][ $values[$pane_key]['legal_entity']];
    }
  }
  $header=array(array('data' => t('Check your data'), 'colspan' => 2, 'class' => array('data-review')));
  $rows=array();
  $Nrows=0;
  foreach ($form as $pane_key => $pane) {
    if (isset($pane['#no_summary']) && $field['#no_summary']) continue;
    if (substr($pane_key, 0, 1) == '#') continue;
    if ($row=_checkout_summary_prepare_row($pane_key, $pane_key, $pane, $values)) {
      $Nrows++;
      $rows[$Nrows]=$row;
    }
    else {
      if (isset($pane['#title'])) {
        $Nrows++;
        $rows[$Nrows]=array(
          'data' => array(
            array('data' => $pane['#title'], 'colspan' => 2)
          ),
          'class' => array('pane-title')
        );
        if (isset($pane['#attributes']['class'])) {
          foreach ($pane['#attributes']['class'] as $class) $rows[$Nrows]['class'][]=$class;
        }
      }
      foreach ($pane as $field_key => $field) {
        if (isset($field['#no_summary']) && $field['#no_summary']) continue;
        if (substr($field_key, 0, 1) == '#') continue;
        if ($row=_checkout_summary_prepare_row($pane_key, $field_key, $field, $values)) {
          $Nrows++;
          $rows[$Nrows]=$row;
        }
      }
    }
  }
  $output=theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('checkout-input-summary')), 'sticky' => FALSE));
  if (isset($form['html_container']['#short_summary'])) $output.=$form['html_container']['#short_summary'];
  return $output;
}

function theme_cart_summary($vars) {
  $cart=$vars['cart'];
  $type=$vars['type'];
  $params=$vars['params'];
  $params['type']=$type;
  
  if (empty($cart->contents)) return '';
  
  $elements=array();
  
  if (!isset($params['absolute'])) $params['absolute']=FALSE;
  $unique_props=array();
  $invoke_output=module_invoke_all('graber_cart_properties');
  if (is_array($invoke_output)) $unique_props+=$invoke_output;
  
  $variables=array(
    'cart' => $cart,
    'params' => $params
  );
  $cart_version=gp_get_settings('graber_cart', 'cart_version');
  if ($cart_version == 'responsive') $elements['cart']=theme('graber_cart_responsive', $variables);
  else $elements['cart']=theme('graber_cart', $variables);

  //Selected billing and shipping methods and price summary tables
  if ($type=='normal') {
    
    //Selected options
    $option_data=array();
    
    $shipping_methods=module_invoke_all('graber_cart_shipping_info', $cart);
    $option_data['shipping_method']['title']=t('Shipping method');
    foreach ($shipping_methods as $method => $data) $option_data['shipping_method']['data'][$method]=$data['title'];
    
    if (isset($cart->data['shipping_method'])) $shipping_method=$cart->data['shipping_method'];
    else $shipping_method=FALSE;
    $payment_methods=module_invoke_all('graber_cart_payment_info', $shipping_method);
    $option_data['payment_method']['title']=t('Payment method');
    foreach ($payment_methods as $method => $data) {
      if (!is_array($data)) $option_data['payment_method']['data'][$method]=$data;
      else $option_data['payment_method']['data'][$method]=$data['title'];
    }
    
    $additional_options=module_invoke_all('graber_cart_summary_options', $cart);
    if (!empty($additional_options) && is_array($additional_options)) $option_data+=$additional_options;
    
    $rows=array();
    foreach ($option_data as $option => $data) {
      if (isset($cart->data[$option]) && isset($data['data'][$cart->data[$option]])) {
        $rows[]=array(
          'data' => array(
            array('data' => $data['title'], 'class' => array('title')),
            array('data' => $data['data'][$cart->data[$option]], 'class' => array('value'))
          ),
          'class' => array(drupal_html_class($option))
        );
      }
    }
    $elements['options']=theme('table', array('rows' => $rows, 'sticky' => FALSE, 'attributes' => array('class' => array('options', drupal_html_class($type)))));
    
    //Price summary
    $rows=array();
    if (!isset($cart->data['articles_price'])) $cart->calculate();
    
    $theme_data=array();
    $fields=array('articles_price' => t('Articles price'), 'shipping_price' => t('Shipping price'), 'total_price' => t('Total price'));
    $weight=0;
    foreach ($fields as $field => $label) {
      if (isset($cart->data[$field])) {
        $theme_data[$field]=array(
          'weight' => $weight++,
          'label' => $label,
          'raw' => $cart->data[$field],
          'value' => number_format($cart->data[$field], 2, ',', ' ').' '.$cart->currency,
          'class' => array(drupal_html_class($field))
        );
      }
    }
    
    drupal_alter('gc_price_summary', $theme_data, $cart);
    
    uasort($theme_data, 'drupal_sort_weight');
    
    foreach ($theme_data as $item) { 
      $rows[]=array(
        'data' => array(
          array('data' => $item['label'], 'class' => array('title')),
          array('data' => $item['value'], 'class' => array('value'))
        ),
        'class' => isset($item['class']) ? $item['class'] : NULL
      );
    }
    
    $elements['prices']=theme('table', array('rows' => $rows, 'sticky' => FALSE, 'attributes' => array('class' => array('price', drupal_html_class($type)))));
  }
  
  if (!isset($params['return_array']) || !$params['return_array']) {
    $output='<div class="cart-summary">';
    $output.=$elements['cart'];
    if ($type=='normal') $output.='<div class="info-wrapper">'.$elements['options'].$elements['prices'].'</div>';
    $output.='</div>';
    return $output;
  }
  else return $elements;
}

function theme_price_summary($vars) {
  $summary_data=$vars['summary_data'];
  $title='<legend class="title">'.t('Price summary').'</legend>';

  foreach ($summary_data as $container => $c_data) {
    if (!is_array($c_data)) $c_data=array('data' => $c_data);
    foreach ($c_data as $key => $data) {
      if (substr($key, 0, 1) === '#') continue;
      if (!is_array($data)) $data=array('data' => $data);
      if (!isset($data['#weight'])) $data['#weight']=0;
      $c_data[$key]=$data;
    }
    uasort($c_data, 'element_sort');
    if (!isset($c_data['#weight'])) $c_data['#weight']=0;
    $summary_data[$container]=$c_data;
  }  
  uasort($summary_data, 'element_sort');
  
  $info_html='';
  foreach ($summary_data as $container => $c_data) {
    $info_html.='<div class="fieldset-wrapper '.drupal_html_class($container).'">';
    foreach ($c_data as $key => $data) {
      if (substr($key, 0, 1) == '#') continue;
      $info_html.='<div class="'.strtr($key, '_', '-').'">'.$data['data'].'</div>';
    }
    $info_html.='</div>';
  }
  return '<fieldset class="order-summary">'.$title.'<div class="fieldset-wrapper">'.$info_html.'</div></fieldset>';
}

function theme_graber_address_data($vars) {
  $data=$vars['data'];
  $rows=array();
  if (!isset($data['legal_entity'])) {
    if (isset($data['name'])) $rows[]=array(
      'data' => array(t('Name', array(), array('context' => 'person')), $data['name']),
      'class' => array('name')
    );
    if (isset($data['family'])) $rows[]=array(
      'data' => array(t('Family'), $data['family']),
      'class' => array('family')
    );
    if (isset($data['company'])) $rows[]=array(
      'data' => array(t('Company'), $data['company']),
      'class' => array('company')
    );
    if (isset($data['tin'])) $rows[]=array(
      'data' => array(t('TIN'), $data['tin']),
      'class' => array('tin')
    );

  }
  elseif ($data['legal_entity'] == 'person') {
    $rows[]=array(
      'data' => array(t('Name', array(), array('context' => 'person')), isset($data['name']) ? $data['name'] : t('N/A')),
      'class' => array('name')
    );
    $rows[]=array(
      'data' => array(t('Family'), isset($data['family']) ? $data['family'] : t('N/A')),
      'class' => array('family')
    );
  }
  else {
    $rows[]=array(
      'data' => array(t('Company'), isset($data['company']) ? $data['company'] : t('N/A')),
      'class' => array('company')
    );
    $rows[]=array(
      'data' => array(t('TIN'), isset($data['tin']) ? $data['tin'] : t('N/A')),
      'class' => array('tin')
    );
  }
  $rows[]=array(
    'data' => array(t('Address'), $data['address']),
    'class' => array('address')
  );
  $rows[]=array(
    'data' => array(t('City'), $data['city']),
    'class' => array('city')
  );
  $rows[]=array(
    'data' => array(t('Postal code'), $data['postal_code']),
    'class' => array('postal_code')
  );
  
  if (!empty($data['phone'])) {
    $rows[]=array(
      'data' => array(t('Phone'), $data['phone']),
      'class' => array('phone')
    );
  }
  
  foreach ($rows as $index => $row) {
    if (is_array($row['data']) && sizeof($row['data']) > 1) {
      $rows[$index]['data'][0]=array('data' => $row['data'][0], 'class' => array('label'));
      $rows[$index]['data'][1]=array('data' => $row['data'][1], 'class' => array('value'));
    }
  }


  $template_vars=array('rows' => $rows, 'sticky' => FALSE, 'attributes' => array('class' => array($vars['type'])));
  
  if (isset($vars['header'])) $template_vars['header']=array(
    array(
      'colspan' => 2,
      'data' => $vars['header']
    )
  );
    
  return theme('table', $template_vars);
}

function theme_graber_order_status($vars) {
  $output='';
  $vars['items']=array();
  foreach ($vars['statuses'] as $key => $status) {
    if ($key == $vars['status']) $vars['items'][]=array('data' => $status, 'class' => array('current'));
    else $vars['items'][]=array('data' => $status);
  }
  $output.='<div class="order-status"><label>'.t('Order status').'</label>'.theme('item_list', $vars).'</div>';
  return $output;
}
